import React, { useState } from "react";
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Row,
  Col,
} from "reactstrap";
import classnames from "classnames";
// import Fade from 'react-reveal/Fade';

import PressReleases from "./PressReleases/PressReleases";
import News from "./News/News";
import PressKit from "./PressKit/PressKit";

const NewsRoomTab = (props) => {
  const [activeTab, setActiveTab] = useState({
    actTB: props.newsrtab[1].id,
    tabOptions: props.newsrtab,
  });
  console.log(props.newsrtab);
  const toggle = (tab) => {
    if (activeTab.actTB !== tab) setActiveTab(tab);
  };

  return (
    <div className="news-tab">
      <Nav tabs>
        {activeTab.tabOptions.map((tabOptionsObj) => {
          <NavItem id={tabOptionsObj.id}>
            <NavLink
              className={classnames({
                active: activeTab.actTB === tabOptionsObj.id,
              })}
              onClick={() => {
                toggle(tabOptionsObj.id);
              }}
            >
              {tabOptionsObj.newsroomtabname}
            </NavLink>
          </NavItem>;
        })}
      </Nav>
      <TabContent activeTab={activeTab.actTB}>
        <TabPane tabId="1">
          <PressReleases />
        </TabPane>
        <TabPane tabId="2">
          <News />
        </TabPane>
        <TabPane tabId="3">
          <PressKit />
        </TabPane>
      </TabContent>
    </div>
  );
};

export default NewsRoomTab;
