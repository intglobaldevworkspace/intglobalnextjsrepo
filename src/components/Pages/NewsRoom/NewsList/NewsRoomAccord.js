import React, { Component } from "react";
import Slider from "react-slick";

import PressReleases from "./PressReleases/PressReleases";
import News from "./News/News";
import PressKit from "./PressKit/PressKit";

export default class NewsRoomAccord extends Component {
  constructor(props) {
    super(props);
    console.log(props);
    this.state = {
      accordSettings: this.props.newsroomaccord.settings,
      accordTitles: this.props.newsroomaccord.tabtitles,
    };
  }

  render() {
    const { accordSettings, accordTitles } = this.state;
    // const settings = {
    //   dots: false,
    //   arrow: true,
    //   infinte: false,
    //   speed: 500,
    //   slidesToShow: 1,
    //   slidesToScroll: 1,
    //   autoplay: true,
    //   autoplaySpeed: 20000,
    //   classNames: "slides",
    // };

    return (
      <div className="news-carousl">
        <Slider {...accordSettings}>
          {accordTitles.map((accprdObj, index) => {
            switch (index) {
              case 0:
                return (
                  <div className="news-item" key={accprdObj.id}>
                    <h3>{accprdObj.newsroomtabname}</h3>
                    <PressReleases />
                  </div>
                );
              case 1:
                return (
                  <div className="news-item" key={accprdObj.id}>
                    <h3>{accprdObj.newsroomtabname}</h3>
                    <News />
                  </div>
                );
              case 2:
                return (
                  <div className="news-item" key={accprdObj.id}>
                    <h3>{accprdObj.newsroomtabname}</h3>
                    <PressKit />
                  </div>
                );
            }
          })}
          {/* <div className="news-item">
            <h3>Press Releases</h3>
            <PressReleases />
          </div>
          <div className="news-item">
            <h3>INT. In The News</h3>
            <News />
          </div>
          <div className="news-item">
            <h3>Press Kit</h3>
            <PressKit />
          </div> */}
        </Slider>
      </div>
    );
  }
}
