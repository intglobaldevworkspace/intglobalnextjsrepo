import React from 'react';
import { 
  Row,
  Col
} from 'reactstrap';
import Link from "next/link";

export default class PressKit extends React.Component {
  render() {
    return (
      <div className="presskit-releases">
        <Row>
          <Col sm="12">
            <div className="presskit-wrapper">
              <div className="presskit-container">
                <Col md="4" className="presskit-col fade-in">
                  <div className="presskit-col_box">
                    <Col md="6">
                      <Row>
                        <figure>
                          <img src="/static/images/news-room/press-kit/technology-profile.png" className="img-fluid" alt="" />
                        </figure>
                      </Row>
                    </Col>
                    <Col md="6">
                      <h4>INT. Profile</h4>
                      <Link href="/static/media/Docs/PressKit/Pdf/Corporate-Profile.pdf">
                        <a className="dwld-prof" target="_blank" title="Detailed Profile">Detailed Profile</a>
                      </Link>
                    </Col>
                  </div>
                </Col>
                <Col md="4" className="presskit-col fade-in">
                  <div className="presskit-col_box">
                    <Col md="6">
                      <Row>
                        <figure>
                          <img src="/static/images/news-room/press-kit/ar-profile-pic.png" className="img-fluid" alt="" />
                        </figure>
                      </Row>
                    </Col>
                    <Col md="6">
                      <h4>Mr. Abhishek Rungta <span>Founder & CEO</span></h4>
                      <Link href="/static/media/Docs/PressKit/Photos/Photos-AbhishekRungta.zip">
                        <a className="dwld-prof" target="_blank" title="Photos" download>Photos</a>
                      </Link>
                      <Link href="/static/media/Docs/PressKit/Pdf/AbhishekRungtaResume.pdf">
                        <a href="#" className="dwld-prof" target="_blank" title="Resume">Resume</a>
                      </Link>
                    </Col>
                  </div>
                </Col>
                <Col md="4" className="presskit-col fade-in">
                  <div className="presskit-col_box">
                    <Col md="6">
                      <Row>
                        <figure>
                          <img src="/static/images/news-room/press-kit/technology-profile-02.png" className="img-fluid" alt="" />
                        </figure>
                      </Row>
                    </Col>
                    <Col md="6">
                      <h4>Logos and Image Gallery</h4>
                      <p>Download hi-resolution and low-resolution logos and images for use in your publication</p>
                      <Link href="/static/media/Docs/PressKit/Emblem/Logo/Logos.zip">
                        <a className="dwld-prof" target="_blank" title="Dowload Now" download>Dowload Now</a>
                      </Link>
                    </Col>
                  </div>
                </Col>
              </div>
              <div className="dwld-resources">
                <span>Download All Resources</span>
                <Link href="/static/media/Docs/PressKit/Resources/intPress-kit.zip">
                  <a className="dwld-all" target="_blank" title="Download Zip (File size 13.2 MB)" download>Download Zip (File size 13.2 MB)</a>
                </Link>
              </div>
            </div>
          </Col>
        </Row>  
      </div>
    )
  }
}