import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

import pressData from "./pressData";

export default class PressReleases extends Component {
  constructor(props) {
    super(props);

    this.state = {
      items: pressData,
      visible: 9,
      error: false,
    };

    this.loadMore = this.loadMore.bind(this);
  }

  loadMore() {
    this.setState((prev) => {
      return { visible: prev.visible + 3 };
    });
  }

  render() {
    const { items, visible, length } = this.state;
    return (
      <div className="press-releases">
        <Row>
          <Col md="12">
            <div className="press-wrapper" aria-live="polite">
              {items.slice(0, visible).map((item) => {
                return (
                  <Col md="4" className="press-col fade-in" key={item.id}>
                    <Fade top>
                      <div className="press-col_box">
                        <h4>{item.presstitle}</h4>
                        <div className="press-action-row">
                          <a
                            href={item.readmorelink}
                            className="readmore-link"
                            target="_blank"
                          >
                            {item.readmore}
                          </a>
                          <span className="publish-date">{item.pressdate}</span>
                        </div>
                      </div>
                    </Fade>
                  </Col>
                );
              })}
              <Fade top>
                {visible < length && (
                  <button
                    onClick={this.loadMore}
                    type="button"
                    className="load-more"
                  >
                    Load more
                  </button>
                )}
              </Fade>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}
