/* eslint max-len: 0 */

export const presslist = [
  {
    id: '1',
    presstitle: 'Abhishek Rungta, Founder & CEO of INT. (Indus Net Technologies), accepted into Forbes Business Council.',
    pressdate: 'October 06, 2020',
    readmore: 'Read More »',
    readmorelink: '/static/media/Docs/PressReleases/Pdf/Abhishek-Rungta-Founder-&-CEO-of-INT.-Indus Net Technologies-accepted-into-Forbes-Business-Council.pdf'
  },
  {
    id: '2',
    presstitle: 'INT awarded project of designing & developing a user friendly, smart, and highly secured portal for Student Police Cadet Programme.',
    pressdate: 'August 25, 2020',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/INT-awarded-project-of-designing-&-developing-a-user-friendly-smart-and-highly-secured-portal-for-Student-Police-Cadet-Programme.pdf'
  },
  {
    id: '3',
    presstitle: 'Indus Net Technologies Aims To Grow 3X In Post COVID World. Launches a new brand identity.',
    pressdate: 'August 22, 2020',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Indus-Net-Technologies-Aims-To-Grow-3X-In-Post-COVID-World-Launches-a-new-brand-identity.pdf'
  },
  {
    id: '4',
    presstitle: 'Bailout, Credit Risks and Technology - How will the Banking Industry Survive Covid-19?',
    pressdate: 'April 04, 2020',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Bailout-Credit-Risks-and-Technology-How-will-the-Banking-Industry-Survive-Covid-19.pdf'
  },
  {
    id: '5',
    presstitle: 'Indus Net Technologies announced the second edition of ‘Digital Success Summit 2019’',
    pressdate: 'June 21, 2019',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Indus-Net-Technologies-announced-second-edition-of-DSS-V2.0-2019.pdf'
  },
  {
    id: '6',
    presstitle: 'Indus Net Technologies Joins International Association of Outsourcing Professional’s (IAOP).',
    pressdate: 'October 29, 2018',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Indus-Net-Technologies-Joins-International-Association-of-Outsourcing-Professional%E2%80%99s-(IAOP)-29-October-2018.pdf'
  },
  {
    id: '7',
    presstitle: 'Indus Net Technologies Hosts Kolkata’s First Ever Digital Success Summit on 10th August, 2018',
    pressdate: 'August 10, 2018',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Indus-Net-Technologies-Hosts-Kolkata%E2%80%99s-First-Ever-Digital-Success-Summit-10-August-2018.pdf'
  },
  {
    id: '8',
    presstitle: 'Indus Net Technologies to Participate in InsurTech Rising London.',
    pressdate: 'October 16, 2017',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Indus%20Net%20Technologies%20participates%20in%20InsurTech%20Rising,%20London%20(16%20October%202017).pdf'
  },
  {
    id: '9',
    presstitle: 'EnergyTech Ventures To Host First Ever Energy Data Analytics Summit in India.',
    pressdate: 'August 03, 2017',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/EnergyTech%20Ventures%20To%20Host%20First%20Ever%20Energy%20Data%20Analytics%20Summit%20in%20India%20(3%20August,%202017).pdf'
  },
  {
    id: '10',
    presstitle: 'Indus Net Technologies is set to participate in Asia Pacific’s largest platform for ICT Industry - CommunicAsia2017 in Singapore.',
    pressdate: 'May 05, 2017',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Indus%20Net%20Technologies%20is%20set%20to%20participate%20in%20Asia%20Pacifics%20largest%20platform%20for%20ICT%20Industry%20-%20CommunicAsia%202017%20in%20Singapore(4th%20May,2017).pdf'
  },
  {
    id: '11',
    presstitle: 'Indus Net Technologies is set to showcase its excellence in Innovative Enterprise Mobility Solutions at MWC 2017',
    pressdate: 'February 20, 2017',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/IndusNetTechnologiesissettoshowcaseitsexcellenceinInnovativeEnterpriseMobilitySolutionsatMWC2017.pdf'
  },
  {
    id: '12',
    presstitle: 'Indus Net Technologies appraised at CMMI (Capability Maturity Model Integration) level 3',
    pressdate: 'February 14, 2017',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/IndusNetTechnologiesappraisedatCMMI(Capability%20Maturity%20Model%20Integration)level%203.pdf'
  },
  {
    id: '13',
    presstitle: 'Indus Net Technologies gets recognised for Mobile Application Development at the coveted CIO Choice 2017',
    pressdate: 'January 09, 2017',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/IndusNetTechnologiesgetsrecognisedforMobileApplicationDevelopmentatthecovetedCIOChoice%202017.pdf'
  },
  {
    id: '14',
    presstitle: 'Indus Net Technologies and E-Cube Energy announce EnergyTech Ventures.',
    pressdate: 'January 18, 2017',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/IndusNetTechnologiesandE-CubeEnergyannounceEnergyTechVentures.pdf'
  },
  {
    id: '15',
    presstitle: 'Google Awards Premier Partner Status to Indus net Techshu.',
    pressdate: 'November 14, 2016',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/GOOGLEAWARDSPREMIERPARTNERSTATUSTOINDUSNETTECHSHU.pdf'
  },
  {
    id: '16',
    presstitle: 'Indus Net Technologies launches ‘Indus Net Labs’ - the first Digital Acceleration Lab in Kolkata.',
    pressdate: 'September 13, 2016',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/IndusNetTechnologieslaunches%E2%80%98Indus%20Net%20Labs%E2%80%99-thefirstDigital.pdf'
  },
  {
    id: '17',
    presstitle: 'Indus Net Technologies in partnership with E-Cube Energy launches ‘Motor Tool’.',
    pressdate: 'August 2, 2016',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/IndusNetTechnologiesinpartnershipwithE-CubeEnergylaunches%20%E2%80%98Motor%20Tool%E2%80%99.pdf'
  },
  {
    id: '18',
    presstitle: 'Indus Net Technologies To Participate In CommunicAsia 2016',
    pressdate: 'May 25, 2016',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/IndusNetTechnologiesToParticipateInCommunicAsia2016PressRelease.pdf'
  },
  {
    id: '19',
    presstitle: 'Indus Net Technologies Acquires Digital Marketing Giant, Techsu.',
    pressdate: 'April 18, 2016',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/IndusNetTechnologiesAcquiresDigitalMarketingGant,Techsu.pdf'
  },
  {
    id: '20',
    presstitle: 'Indus Net Technologies Acquires InFluxERP.',
    pressdate: 'February 15, 2016',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Indus%20Net%20Technologies%20Acquires%20InFluxERP.pdf'
  },
  {
    id: '21',
    presstitle: 'Indus Net Technologies help people thank their Doctors on ‘Doctor’s Day’16’.',
    pressdate: 'July 01, 2016',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Indus%20Net%20Technologies%20help%20people%20thank%20their%20Doctors%20on%20%E2%80%98Doctor%E2%80%99s%20Day%E2%80%9916%E2%80%99.pdf'
  },
  {
    id: '22',
    presstitle: 'Indus Net Technologies Hosted Hackathon To Promote Young Talent And Technology.',
    pressdate: 'May 02, 2016',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Indus%20Net%20Technologies%20Hosted%20Hackathon%20To%20Promote.pdf'
  },
  {
    id: '23',
    presstitle: 'Talenjaro – India’s First App–Based Career Counseling Service By Zapasya.',
    pressdate: 'January 28, 2016',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Talenjaro%20%E2%80%93%20India%E2%80%99s%20First%20App%E2%80%93Based%20Career%20Counseling%20Service%20By%20Zapasya.pdf'
  },
  {
    id: '24',
    presstitle: 'Indus Net Technologies Develops PickJi, India’s First App-Based Logistic Service For Auspice Transitos.',
    pressdate: 'November 06, 2015',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Indus%20Net%20Technologies%20PickJi%20Announcement%20Press%20Release.pdf'
  },
  {
    id: '25',
    presstitle: 'Abhishek Rungta Appointed TIE Vice President.',
    pressdate: 'October 06, 2015',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Abhishek%20Rungta%20Appointed%20TIE%20Vice%20President%20Press%20Release.pdf'
  },
  {
    id: '26',
    presstitle: 'Makeyourtax.com, a cloud based tax filing application launched by Indus Net Technologies and Taxmantra.',
    pressdate: 'September 25, 2015',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Makeyourtax.com,%20a%20cloud%20based%20tax%20filing%20application%20launched%20by%20Indus%20Net%20Technologies%20and%20Taxmantra.pdf'
  },
  {
    id: '27',
    presstitle: 'ICC and Indus Net Technologies Jointly Refurbish the ICC Website.',
    pressdate: 'July 21, 2015',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/ICC%20and%20Indus%20Net%20Technologies%20Jointly%20Refurbish%20the%20ICC%20Website.pdf'
  },
  {
    id: '28',
    presstitle: 'Prime Minister Narendra Modi Launches Mobile App for MyGov.in.',
    pressdate: 'July 10, 2015',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Prime%20Minister%20Narendra%20Modi%20Launches%20Mobile%20App%20for%20MyGov.in.pdf'
  },
  {
    id: '29',
    presstitle: 'Indus Net Technologies’ Success Story Gets Covered by Swiss Radio and Television.',
    pressdate: 'April 22, 2015',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Indus%20Net%20Technologies%E2%80%99%20Success%20Story%20Gets%20Covered%20by%20Swiss%20Radio%20and%20Television.pdf'
  },
  {
    id: '30',
    presstitle: 'Indus Net Technologies Demystified Digital Marketing Once Again.',
    pressdate: 'April 16, 2015',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Indus%20Net%20Technologies%20Demystified%20Digital%20Marketing%20Once%20Again.pdf'
  },
  {
    id: '31',
    presstitle: 'Indus Net Technologies Accompanies Siddha Group in Their Real Estate E-commerce Journey.',
    pressdate: 'February 10, 2015',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Indus%20Net%20Technologies%20Accompanies%20Siddha%20Group%20in%20Their%20Real%20Estate%20E-commerce%20Journey.pdf'
  },
  {
    id: '32',
    presstitle: 'Indus Net Technologies is All Set to Showcase Its Excellence in Innovative Enterprise Mobility Solutions at MWC 2015',
    pressdate: 'February 02, 2015',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Indus%20Net%20Technologies%20is%20All%20Set%20to%20Showcase%20Its%20Excellence%20in%20Innovative%20Enterprise%20Mobility%20Solutions%20at%20MWC%202015.pdf'
  },
  {
    id: '33',
    presstitle: 'Indus Net Technologies Architects Poompuhar’s E-commerce Website.',
    pressdate: 'December 23, 2014',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Indus%20Net%20Technologies%20Architects%20Poompuhar%E2%80%99s%20E-commerce%20Website.pdf'
  },
  {
    id: '34',
    presstitle: 'Indus Net Technologies Helped Re-igniting Cricket Fantasy with LeagueGuru.com.',
    pressdate: 'November 27, 2014',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Indus%20Net%20Technologies%20Helped%20Re-igniting%20Cricket%20Fantasy%20with%20LeagueGuru.com.pdf'
  },
  {
    id: '35',
    presstitle: 'Indus Net Technologies Successfully Revamps the MHRD Website.',
    pressdate: 'November 19, 2014',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Indus%20Net%20Technologies%20Successfully%20Revamps%20the%20MHRD%20Website.pdf'
  },
  {
    id: '36',
    presstitle: 'Indus Net Technologies Celebrated Its Grand Success of 17 Years.',
    pressdate: 'September 20, 2014',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Indus%20Net%20Technologies%20Celebrated%20Its%20Grand%20Success%20of%2017%20Years.pdf'
  },
  {
    id: '37',
    presstitle: 'Hon’ble Prime Minister Narendra Modi Launches NCERT Website and Kala Utsav Festival Today.',
    pressdate: 'September 04, 2014',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Hon%E2%80%99ble%20Prime%20Minister%20Narendra%20Modi%20Launches%20NCERT%20Website%20and%20Kala%20Utsav%20Festival%20Today.pdf'
  },
  {
    id: '38',
    presstitle: 'Prestashop Certified Indus Net Technologies as Its Partner Agency.',
    pressdate: 'January 12, 2014',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Prestashop%20Certified%20Indus%20Net%20Technologies%20as%20Its%20Partner%20Agency.pdf'
  },
  {
    id: '39',
    presstitle: 'Indus Net Technologies Achieves the Status of “Google Partner”.',
    pressdate: 'April 19, 2014',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Indus%20Net%20Technologies%20Achieves%20the%20Status%20of%20%E2%80%9CGoogle%20Partner%E2%80%9D.pdf'
  },
  {
    id: '40',
    presstitle: 'Indus Net Technologies Patents the “I Love Technology” Logo.',
    pressdate: 'January 29, 2014',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Indus%20Net%20Technologies%20Patents%20the%20%E2%80%9CI%20Love%20Technology%E2%80%9D%20Logo.pdf'
  },
  {
    id: '41',
    presstitle: 'Indus Net Technologies Is All Set To Gift a Joyous Christmas to Underprivileged Children.',
    pressdate: 'December 20, 2013',
    readmore: 'Read More »',
    readmorelink: 'https://www.indusnet.co.in/docs/pressReleases/Indus%20Net%20Technologies%20Is%20All%20Set%20To%20Gift%20a%20Joyous%20Christmas%20to%20Underprivileged%20Children.pdf'
  },
];

export default presslist;