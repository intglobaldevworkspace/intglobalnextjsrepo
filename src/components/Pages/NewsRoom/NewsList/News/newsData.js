/* eslint max-len: 0 */

export const newslist = [
  {
    id: '1',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/forbes.jpg',
    newstitle: '15 Strategic Approaches To Accurately Forecast Sales',
    newsdate: 'October 06, 2020',
    newspublisher: 'Forbes Business',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'https://www.forbes.com/sites/forbesbusinesscouncil/2020/10/06/15-strategic-approaches-to-accurately-forecast-sales/#75365dd229d1'
  },
  {
    id: '2',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/businessindia.jpg',
    newstitle: 'INT launches latest offering BreezeERP',
    newsdate: 'October 05, 2020',
    newspublisher: 'Business India',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'https://businessindia.co/emagazine/cyrus-mistry-the-20-billion-question'
  },
  {
    id: '3',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/indiablooms.jpg',
    newstitle: 'INT launches latest offering BreezeERP',
    newsdate: 'September 24, 2020',
    newspublisher: 'India Blooms',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'https://www.indiablooms.com/finance-details/12627/int-launches-latest-offering-breezeerp.html'
  },
  {
    id: '4',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/issuewire.jpg',
    newstitle: 'INT. (Indus Net Technologies) launches its latest offering BreezeERP',
    newsdate: 'September 15, 2020',
    newspublisher: 'Issue Wire',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'https://www.issuewire.com/int-indus-net-technologies-launches-its-latest-offering-breezeerp-1677860063158629'
  },
  {
    id: '5',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/economics.jpg',
    newstitle: 'Road to Digital Success',
    newsdate: 'August 31, 2020',
    newspublisher: 'The Economic Times',
    newspublishersrc: 'Kolkata',
    readmore: 'Read More »',
    readmorelink: '/static/media/Docs/News/Pdf/EconomicTimes_NewAgePharma.pdf'
  },
  {
    id: '6',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/prabhatkhabar.jpg',
    newstitle: 'इंडस नेट पर अहम पोर्टल बनाने का जिम्मा',
    newsdate: 'August 27, 2020',
    newspublisher: 'प्रभात खबर',
    newspublishersrc: 'कोलकाता',
    readmore: 'Read More »',
    readmorelink: 'https://epaper.prabhatkhabar.com/c/54501403'
  },
  {
    id: '7',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/morning-india.jpg',
    newstitle: 'INT. awarded project of designing and developing a user friendly, smart and highly secured portal for Student Police Cadet Programme',
    newsdate: 'August 27, 2020',
    newspublisher: 'Morning India',
    newspublishersrc: 'Kolkata',
    readmore: 'Read More »',
    readmorelink: 'http://epaper.sanmarglive.com/m5/2799560/KOLKATA/27aug#page/3/1'
  },
  {
    id: '8',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/arthik-lipi.jpg',
    newstitle: 'INT. develops a smart portal for police cadet programme',
    newsdate: 'August 27, 2020',
    newspublisher: 'আর্থিক লিপি',
    newspublishersrc: 'কলকাতা',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Arthik_Lipi _August_27_2020.jpg'
  },
  {
    id: '9',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/newsmania.jpg',
    newstitle: 'INT. awarded project of designing and developing a user friendly, smart, and hightly secured portal for Student Police Cadet Programme',
    newsdate: 'August 27, 2020',
    newspublisher: 'News Mania',
    newspublishersrc: 'Kolkata',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/News_Mania_August_27_2020.jpg'
  },
  {
    id: '10',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/indiaeducationdiary.jpg',
    newstitle: 'Indus Net Technologies aims To Grow 3X In Post COVID World. Launches a new brand identity',
    newsdate: 'August 20, 2020',
    newspublisher: 'India Education Diary',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'https://indiaeducationdiary.in/indus-net-technologies-aims-to-grow-3x-in-post-covid-world-launches-a-new-brand-identity/'
  },
  {
    id: '11',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/asomiyapratidin.jpg',
    newstitle: 'পাঁচ বছরে তিনগুণ বিকাশের লক্ষ্য ইন্ডাস নেট টেকনোলজির',
    newsdate: 'July 31, 2020',
    newspublisher: 'অসমীয়া প্রতিদিন',
    newspublishersrc: 'গুয়াহাটি, যোরহাট, লখিমপুৰ',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Asomiya_Pratidin_July_31_2020.jpg'
  },
  {
    id: '12',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/cision-pr-newswire.jpg',
    newstitle: 'GoodFirms Announces Top Mobile App Development Companies Help Businesses Fight COVID-19 Pandemic',
    newsdate: 'July 28, 2020',
    newspublisher: 'Cision PR Newswire',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'https://www.prnewswire.com/news-releases/goodfirms-announces-top-mobile-app-development-companies-help-businesses-fight-covid-19-pandemic-301101257.html'
  },
  {
    id: '13',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/newsmania.jpg',
    newstitle: 'Indus Net Technologies aims to grow 3x in post covid world, Launches a new brand identity',
    newsdate: 'July 27, 2020',
    newspublisher: 'News Mania',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: '/static/media/Docs/News/Pdf/Indus-Net-Technologies-aims-to-grow-3x-in-post-covid-world-and-launches-a-new-brand-identity.pdf'
  },
  {
    id: '14',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/asiannewsservice.jpg',
    newstitle: 'INT aims growth in Post-Covid World',
    newsdate: 'July 27, 2020',
    newspublisher: 'Asian News Service',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'http://asiannewsservice.in/en/business/int-aims-growth-post-covid-world/'
  },
  {
    id: '15',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/hellomumbainews.jpg',
    newstitle: 'Indus Net Technologies aims To Grow 3X In Post COVID World. Launches a new brand identity',
    newsdate: 'July 27, 2020',
    newspublisher: 'Hello Mumbai News',
    newspublishersrc: 'Mumbai',
    readmore: 'Read More »',
    readmorelink: 'https://www.hellomumbainews.com/business_news/mumbai-indus-net-technologies-aims-to-grow-3x-in-post-covid-world-launches-a-new-brand-identity-read-detailed-story-here/'
  },
  {
    id: '16',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/dainikjagran.jpg',
    newstitle: 'इंडस नेट टेक्नोलॉजीज की नई ब्रांड आइडेंटिटी लॉन्च',
    newsdate: 'July 27, 2020',
    newspublisher: 'दैनिक जागरण',
    newspublishersrc: 'नई दिल्ली',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Dainik_Jagran_July_27_2020.jpg'
  },
  {
    id: '17',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/haribhoomi.jpg',
    newstitle: 'इंडस नेट टेक्नो का तीन गुना बृद्धि का लक्ष्य',
    newsdate: 'July 27, 2020',
    newspublisher: 'हरिभूमि',
    newspublishersrc: 'हरियाणा',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Haribhoomi_July_27_2020.jpg'
  },
  {
    id: '18',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/biznextindia.jpg',
    newstitle: 'Indus Net Technologies Aims To Grow 3X In Post COVID World, Launches A New Brand Identity',
    newsdate: 'July 25, 2020',
    newspublisher: 'BizNext India',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'https://www.biznextindia.com/companies/indus-net-technologies-aims-to-grow-3x-in-post-covid-world-launches-a-new-brand-identity/'
  },
  {
    id: '19',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/smestreet.jpg',
    newstitle: 'Indus Net Technologies Aims To Grow 3X In Post COVID',
    newsdate: 'July 25, 2020',
    newspublisher: 'SME Street',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'https://smestreet.in/technology/cloud/indus-net-technologies-aims-to-grow-3x-in-post-covid/'
  },
  {
    id: '20',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/rajexpress.jpg',
    newstitle: 'इंडस नेट ने नई ब्रांड आइडेंटिटी लॉन्च की',
    newsdate: 'July 25, 2020',
    newspublisher: 'राज एक्सप्रेस',
    newspublishersrc: 'नई दिल्ली',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Raj_Express_July_25_2020.jpg'
  },
  {
    id: '21',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/dainik-purvoday.jpg',
    newstitle: 'इंडस नेट टेक्नोलॉजीज ने लांच की एक नई ब्रांड आइडेंटिटी',
    newsdate: 'July 24, 2020',
    newspublisher: 'दैनिक पुरबोदोय',
    newspublishersrc: 'गुआहाटी',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Dainik_Purbodoy_July_25_2020.jpg'
  },
  {
    id: '22',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/infeed.jpg',
    newstitle: 'Indus Net Technologies Aims To Grow 3X In Post COVID World',
    newsdate: 'July 24, 2020',
    newspublisher: 'Infeed',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'https://infeed.in/indus-net-technologies-aims-to-grow-3x-in-post-covid-world/'
  },
  {
    id: '23',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/odisha-ray.jpg',
    newstitle: 'Indus Net Technologies Aims To Grow 3X In Post COVID World, Launches a new brand identity',
    newsdate: 'July 24, 2020',
    newspublisher: 'OdishaRay',
    newspublishersrc: 'Kolkata',
    readmore: 'Read More »',
    readmorelink: 'https://www.odisharay.com/pages/single_page.php?id=19090'
  },
  {
    id: '24',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/indianexpressbusiness.jpg',
    newstitle: 'Indus Net picks up stake in the European start-up Rezzonation',
    newsdate: 'June 23, 2020',
    newspublisher: 'The New Indian Express',
    newspublishersrc: 'Chennai',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Indian_Express_Business_June_23_2020.jpg'
  },
  {
    id: '25',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/statesman.jpg',
    newstitle: 'INT Partnership with Rezzonation to build the world’s most interactive entertainment platform “One App”',
    newsdate: 'June 23, 2020',
    newspublisher: 'The Statesman',
    newspublishersrc: 'Kolkata',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/The_States_Man_June_23_2020.jpg'
  },
  {
    id: '26',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/aajkaal.jpg',
    newstitle: 'বিনোদন উদ্যোগ',
    newsdate: 'June 22, 2020',
    newspublisher: 'আজকাল',
    newspublishersrc: 'কলকাতা',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Aaj_Kaal_June_21_2020.jpg'
  },
  {
    id: '27',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/salamduniya.jpg',
    newstitle: 'आईएनटी ने रजोनेशन के साथ किया करार',
    newsdate: 'June 22, 2020',
    newspublisher: 'सलाम दुनिया',
    newspublishersrc: 'कोलकाता',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Salam_Duniya_June_22_2020.jpg'
  },
  {
    id: '28',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/dainik-statesman.jpg',
    newstitle: 'ইউরোপীয় রেজোনেশনের অংশীদারিত্ব গ্রহণ করল ইন্দাস নেট টেকনোলজিস',
    newsdate: 'June 21, 2020',
    newspublisher: 'দৈনিক স্টেটসম্যান',
    newspublishersrc: 'কলকাতা',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/The_States_Man_June_21_2020.jpg'
  },
  {
    id: '29',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/prabhatkhabar.jpg',
    newstitle: 'आइएनटी ने यूरोपीय स्टार्टअप रजोनेशन के साथ की साझेदारी',
    newsdate: 'June 21, 2020',
    newspublisher: 'प्रभात खबर',
    newspublishersrc: 'कोलकाता',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Prabhat_Khabar_June_21_2020.jpg'
  },
  {
    id: '30',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/indiablooms.jpg',
    newstitle: 'INT picks a stake in the European startup Rezzonation',
    newsdate: 'June 21, 2020',
    newspublisher: 'India Blooms',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'https://www.indiablooms.com/finance-details/12069/int-picks-a-stake-in-the-european-startup-rezzonation.html'
  },
  {
    id: '31',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/dailyhunt.jpg',
    newstitle: 'INT picks a stake in the European startup Rezzonation',
    newsdate: 'June 20, 2020',
    newspublisher: 'Daily Hunt',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'https://m.dailyhunt.in/news/india/english/business+fortnight-epaper-busifor/int+picks+a+stake+in+the+european+startup+rezzonation-newsid-n192674072'
  },
  {
    id: '32',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/dainik-statesman.jpg',
    newstitle: 'ইন্ডাস নেট টেকনোলজিস কর্মীদের সুবিধা দিচ্ছে',
    newsdate: 'May 03, 2020',
    newspublisher: 'দৈনিক স্টেটসম্যান',
    newspublishersrc: 'ইপেপার - কলকাতা',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Dainik_Statesman_May_03_2020.jpg'
  },
  {
    id: '33',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/dainik-jugasankha.jpg',
    newstitle: 'করোনা মহামারীর মধ্যেই নয়া নির্দেশিকাকে স্বাগত জানাল কলকাতার আইএনটি',
    newsdate: 'April 20, 2020',
    newspublisher: 'দৈনিক যুগশঙ্খ',
    newspublishersrc: 'কলকাতা',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Dainik_Jugashanka_April_20_2020.jpg'
  },
  {
    id: '34',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/dainondin_barta.jpg',
    newstitle: 'কেন্দ্রীয় সরকারের লোকডাউন রেহাইয়ে কার্যত উদ্যোগীক ক্ষেত্রে সন্তোষ প্রকাশ',
    newsdate: 'April 18, 2020',
    newspublisher: 'দৈনন্দিন বার্তা',
    newspublishersrc: 'অসম',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Dainondin_Barta_April_18_2020.jpg'
  },
  {
    id: '35',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/statesman.jpg',
    newstitle: 'Indus Net Technologies continues to increase its working strength and looks to full fill all the job commitments',
    newsdate: 'April 16, 2020',
    newspublisher: 'The Statesman',
    newspublishersrc: 'Kolkata',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/The_States_Man_April_16_2020.jpg'
  },
  {
    id: '36',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/rajasthanpatrika.jpg',
    newstitle: 'इंडस नेट की नई नियुक्तियों की घोषणा',
    newsdate: 'September 19, 2019',
    newspublisher: 'राजस्थान पत्रिका',
    newspublishersrc: 'कोलकाता',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Rajasthan_Patrika_September_19_2019.jpg'
  },
  {
    id: '37',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/sanmarg.jpg',
    newstitle: 'कोलकाता मैं इंडस नेट करेगी 125 भर्ती',
    newsdate: 'September 19, 2019',
    newspublisher: 'सन्मार्ग',
    newspublishersrc: 'कोलकाता',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Sanmarg_September_19_2019.jpg'
  },
  {
    id: '38',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/indiablooms.jpg',
    newstitle: 'Abhishek Rungta wins “CEO with HR orientation” at the 9th Genius Excellence Awards 2019',
    newsdate: 'September 17, 2019',
    newspublisher: 'India Blooms',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'https://www.indiablooms.com/finance-details/10604/stay-happy-and-not-under-pressure-at-work-says-leading-banker-chandra-sekhar-ghosh-at-hr-award-ceremony.html'
  },
  {
    id: '39',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/aajkaal.jpg',
    newstitle: 'নিজের পছন্দ মতোই ব্যবহার ইন্টারনেটের',
    newsdate: 'June 24, 2019',
    newspublisher: 'আজকাল',
    newspublishersrc: 'কলকাতা',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Aajkaal_June_24_2019.jpg'
  },
  {
    id: '40',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/sambadprabaha.jpg',
    newstitle: 'ইন্ডাস-এর ডিজিটাল সাকসেস সামিট ২০১৯',
    newsdate: 'June 23, 2019',
    newspublisher: 'সংবাদ প্রবাহ',
    newspublishersrc: 'কলকাতা',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Sambad_Prabaha_June_23_2019.jpg'
  },
  {
    id: '41',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/theasianage.jpg',
    newstitle: 'Fake News Negative Force in Media: Expert',
    newsdate: 'June 22, 2019',
    newspublisher: 'The Asian Age',
    newspublishersrc: 'Kolkata',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/The_Asian_Age_June_22_2019.jpg'
  },
  {
    id: '42',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/jansatta.jpg',
    newstitle: 'डिजिटल समिट 8 अगस्त से',
    newsdate: 'June 22, 2019',
    newspublisher: 'जनसत्ता',
    newspublishersrc: 'कोलकाता',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Jansatta_June_22_2019.jpg'
  },
  {
    id: '43',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/salamduniya.jpg',
    newstitle: '8 अगस्त को होगा ‘डिजिटल सक्सेस समिट’',
    newsdate: 'June 22, 2019',
    newspublisher: 'सलाम दुनिया',
    newspublishersrc: 'कोलकाता',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Salam_Duniya_June_22_2019.jpg'
  },
  {
    id: '44',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/sanmarg.jpg',
    newstitle: 'इंडस नेट टेक्नोलॉजी करेगी डिजिटल समिट',
    newsdate: 'June 22, 2019',
    newspublisher: 'सन्मार्ग',
    newspublishersrc: 'कोलकाता',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Sanmarg_June_22_2019.jpg'
  },
  {
    id: '45',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/businessstandard.jpg',
    newstitle: '‘Digital Success Summit’ to be held in city in Aug',
    newsdate: 'June 21, 2019',
    newspublisher: 'Business Standard',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'https://www.business-standard.com/article/pti-stories/digital-success-summit-to-be-held-in-city-in-aug-119062101243_1.html'
  },
  {
    id: '46',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/theweek.jpg',
    newstitle: 'BIZ-SUMMIT',
    newsdate: 'June 21, 2019',
    newspublisher: 'The Week',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'https://www.theweek.in/wire-updates/national/2019/06/21/erg8-biz-summit.html'
  },
  {
    id: '47',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/devdiscourse.jpg',
    newstitle: '‘Digital Success Summit’ to be held in city in Aug',
    newsdate: 'June 21, 2019',
    newspublisher: 'Devdiscourse',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'https://www.devdiscourse.com/article/national/569619-digital-success-summit-to-be-held-in-city-in-aug'
  },
  {
    id: '48',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/uniindia.jpg',
    newstitle: 'Indus Net Technologies is set to host second edition of Digital Success Summit 2019',
    newsdate: 'June 21, 2019',
    newspublisher: 'Uni India',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'http://www.uniindia.com/indus-net-technologies-is-set-to-host-second-edition-of-digital-success-summit-2019/east/news/1639914.html'
  },
  {
    id: '49',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/dailyhunt.jpg',
    newstitle: 'কলকাতায় হতে চলেছে ডিজিটাল সাকসেস সামিট',
    newsdate: 'June 21, 2019',
    newspublisher: 'ডেইলি হান্ট',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'https://m.dailyhunt.in/news/india/bangla/mongalkote+com-epaper-maglkot/kalakatay+hate+chaleche+dijital+sakases+samit-newsid-121192240'
  },
  {
    id: '50',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/economics.jpg',
    newstitle: '‘Upskill or Perish’ is Now the Norm for Software Engineers',
    newsdate: 'May 18, 2019',
    newspublisher: 'The Economic Times',
    newspublishersrc: 'Delhi',
    readmore: 'Read More »',
    readmorelink: '/static/media/Docs/News/Pdf/Upskill-or-Perish-The-Economic-Times-Delhi_18-05-19.pdf'
  },
  {
    id: '51',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/uniindia.jpg',
    newstitle: 'Indus Net Technologies organises conference on ‘Digital Success for Real Estate’',
    newsdate: 'May 13, 2019',
    newspublisher: 'Uni India',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'http://www.uniindia.com/indus-net-technologies-organises-conference-on-digital-success-for-real-estate/east/news/1595629.html'
  },
  {
    id: '52',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/eisamay.jpg',
    newstitle: 'আবাসন বিক্রিতে ব্যবহার বাড়ছে ডিজিটাল প্রযুক্তির',
    newsdate: 'May 13, 2019',
    newspublisher: 'এই সময়',
    newspublishersrc: 'কলকাতা',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Ei_Samay_May_13_2019.jpg'
  },
  {
    id: '53',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/humancapital.jpg',
    newstitle: 'De Novo Recruitement',
    newsdate: 'May 12, 2019',
    newspublisher: 'Human Capital',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: '/static/media/Docs/News/Pdf/De-Novo-Recruitement-Human-Capital-May-2019.pdf'
  },
  {
    id: '54',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/sanmarg.jpg',
    newstitle: 'रियल एस्टेट में डिजिटल की हैं महत्तपूर्ण भूमिका',
    newsdate: 'May 11, 2019',
    newspublisher: 'सन्मार्ग',
    newspublishersrc: 'कोलकाता',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Sanmarg_11_May_2019.jpg'
  },
  {
    id: '55',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/samagya.jpg',
    newstitle: 'इंडस नेट टेक्नोलॉजीज और क्रेडाई बंगाल की ओर से ‘रियल एस्टेट के लिए डिजिटल सफलता’ आयोजित',
    newsdate: 'May 11, 2019',
    newspublisher: 'समाज्ञा',
    newspublishersrc: 'कोलकाता',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Samagya_11_May_2019.jpg'
  },
  {
    id: '56',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/aajkaal.jpg',
    newstitle: 'আবাসনে প্রযুক্তি: আলোচনা',
    newsdate: 'May 11, 2019',
    newspublisher: 'আজকাল',
    newspublishersrc: 'কলকাতা',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Aajkaal_11_May_2019.jpg'
  },
  {
    id: '57',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/salamduniya.jpg',
    newstitle: 'मेट्रो इवेंट',
    newsdate: 'May 10, 2019',
    newspublisher: 'सलाम दुनिया',
    newspublishersrc: 'कोलकाता',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Salam_Duniya_10_May_2019.jpg'
  },
  {
    id: '58',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/dailyhunt.jpg',
    newstitle: 'Indus Net Technologies to present ‘Digital Success for Real Estate’ with CREDAI Bengal',
    newsdate: 'May 10, 2019',
    newspublisher: 'Daily Hunt',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'https://m.dailyhunt.in/news/india/english/the+times+of+bengal+english-epaper-timbanen/indus+net+technologies+to+present+digital+success+for+real+estate+with+credai+bengal-newsid-115803179'
  },
  {
    id: '59',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/specialbulletin.jpg',
    newstitle: 'Indus Net Technologies to present ‘Digital Success for Real Estate’ with CREDAI Bengal',
    newsdate: 'May 10, 2019',
    newspublisher: 'Special Bulletin',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'https://www.facebook.com/2304279523229297/posts/2326586744331908/'
  },
  {
    id: '60',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/songoti.jpg',
    newstitle: 'Indus Net Technologies to present ‘Digital Success for Real Estate’ with CREDAI Bengal',
    newsdate: 'May 09, 2019',
    newspublisher: 'Songoti',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Songoti_9_May_2019.jpg'
  },
  {
    id: '61',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/dainikjagran.jpg',
    newstitle: '10 को महानगर में एक दिवसीय उद्दोग सम्मेलन',
    newsdate: 'May 08, 2019',
    newspublisher: 'दैनिक जागरण',
    newspublishersrc: 'कोलकाता',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Dainik_Jagran_8_May_2019.jpg'
  },
  {
    id: '62',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/techgig.jpg',
    newstitle: 'Discussing technology trends with Indus Net Technologies CEO',
    newsdate: 'May 07, 2019',
    newspublisher: 'TechGig',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'https://content.techgig.com/discussing-technology-trends-with-indus-net-technologies-ceo/articleshow/69216944.cms'
  },
  {
    id: '63',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/24x7taazasamachar.jpg',
    newstitle: 'Indus Net Technologies to present ‘Digital Success for Real Estate’ with CREDAI Bengal',
    newsdate: 'May 07, 2019',
    newspublisher: '24x7 Taaza Samachar',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'http://www.24x7taazasamachar.com/Business/details/431/Indus-Net-Technologies-to-present-Digital-Success-for-Real-Estate-with-CREDAI-Bengal'
  },
  {
    id: '64',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/aajkaal.jpg',
    newstitle: 'স্বাস্থ সেবায় মান বাড়াতে উদ্যোগ',
    newsdate: 'March 07, 2019',
    newspublisher: 'আজকাল',
    newspublishersrc: 'আইসিসি',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Aajkaal_March_27_ICC.jpg'
  },
  {
    id: '65',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/bartaman.jpg',
    newstitle: 'সুলভে স্বাস্থ পরিষেবাকে সমাজের সর্বস্তরে পৌছাতে সরকারি ও বেসরকারি উদ্যোগ',
    newsdate: 'March 28, 2019',
    newspublisher: 'বর্তমান',
    newspublishersrc: 'আইসিসি',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Bartaman_March_28_ICC.jpg'
  },
  {
    id: '66',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/ekdin.jpg',
    newstitle: 'চিকিৎসার জন্য প্রয়োজন উন্নত তথ্যপ্রযুক্তি, দাবি চিকিৎসকদের',
    newsdate: 'March 28, 2019',
    newspublisher: 'একদিন',
    newspublishersrc: 'আইসিসি',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Ekdin_March_28_ICC.jpg'
  },
  {
    id: '67',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/morningindia.jpg',
    newstitle: 'Bengal Advances Much in Healthcare By Making It Affordable & Accessible',
    newsdate: 'March 27, 2019',
    newspublisher: 'Morning India',
    newspublishersrc: 'ICC',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Morning_India_March_27_ICC.jpg'
  },
  {
    id: '68',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/eoi.jpg',
    newstitle: 'As Govt. Body We Welcome New Ideas: State H&WF Dept',
    newsdate: 'March 27, 2019',
    newspublisher: 'The Echo of India',
    newspublishersrc: 'ICC',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/The_Echo_of_India_March_27_ICC.jpg'
  },
  {
    id: '69',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/salamduniya.jpg',
    newstitle: 'स्वास्थ क्षेत्रों को और बेहतर बनाने की कोशिश जारी: बिनोद कुमार',
    newsdate: 'March 27, 2019',
    newspublisher: 'सलाम दुनिया',
    newspublishersrc: 'आईसीसी',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Salam_Duniya_March_27_ICC.jpg'
  },
  {
    id: '70',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/statesman.jpg',
    newstitle: 'We Organised East India’s Largest Digital Success Summit Conference at Hyatt Regency',
    newsdate: 'August 08, 2018',
    newspublisher: 'The Statesman',
    newspublishersrc: 'Kolkata Plus',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Digital_Success_Summit_Conference_August_18.jpg'
  },
  {
    id: '71',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/times.jpg',
    newstitle: 'AI-Based Voice Tools For Unorganised Sector Soon: Govt',
    newsdate: 'August 18, 2018',
    newspublisher: 'The Times of India',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Govt_AI_Based_Voice_Tools_For_Unorganised_Sector_August_18.jpg'
  },
  {
    id: '72',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/morningindia.jpg',
    newstitle: 'City To Host First Digital Summit',
    newsdate: 'July 23, 2018',
    newspublisher: 'Morning India',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/City_To_Host_First_Digital_Summit_July_2018.jpg'
  },
  {
    id: '73',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/easternchronicle.jpg',
    newstitle: 'Digital Summit To Target Tech-Lovers',
    newsdate: 'July 22, 2018',
    newspublisher: 'Eastern Chronicle',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Digital_Summit_To_Target_Tech_Lovers_July_2018.jpg'
  },
  {
    id: '74',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/sanmarg.jpg',
    newstitle: 'डिजिटल सक्सेस सम्मलेन १० अगस्त को',
    newsdate: 'July 21, 2018',
    newspublisher: 'सन्मार्ग',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Digital_Success_Sanmelan_10_August_Ko_July_18.jpg'
  },
  {
    id: '75',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/times.jpg',
    newstitle: 'Indus Net Tech’s Digital Success Meet',
    newsdate: 'July 21, 2018',
    newspublisher: 'The Times of India',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Indus_Net_Techs_Digital_Success_Meet_July_18.jpg'
  },
  {
    id: '76',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/songbad.jpg',
    newstitle: 'বাংলাদেশের সঙ্গে যৌথ বিনিয়োগে আগ্রহী ভারতীয় উদ্যোক্তারা',
    newsdate: 'April 26, 2018',
    newspublisher: 'দি সংবাদ',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'http://print.thesangbad.net/news/business/%E0%A6%AC%E0%A6%BE%E0%A6%82%E0%A6%B2%E0%A6%BE%E0%A6%A6%E0%A7%87%E0%A6%B6%E0%A7%87%E0%A6%B0%20%E0%A6%B8%E0%A6%99%E0%A7%8D%E0%A6%97%E0%A7%87%20%E0%A6%AF%E0%A7%8C%E0%A6%A5%20%E0%A6%AC%E0%A6%BF%E0%A6%A8%E0%A6%BF%E0%A6%AF%E0%A6%BC%E0%A7%8B%E0%A6%97%E0%A7%87%20%E0%A6%86%E0%A6%97%E0%A7%8D%E0%A6%B0%E0%A6%B9%E0%A7%80%20%E0%A6%AD%E0%A6%BE%E0%A6%B0%E0%A6%A4%E0%A7%80%E0%A6%AF%E0%A6%BC%20%E0%A6%89%E0%A6%A6%E0%A7%8D%E0%A6%AF%E0%A7%8B%E0%A6%95%E0%A7%8D%E0%A6%A4%E0%A6%BE%E0%A6%B0%E0%A6%BE-24575/'
  },
  {
    id: '77',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/kalerkontho.jpg',
    newstitle: 'পাটপণ্যে ভারতের শুল্ক প্রত্যাহারের আহ্বান',
    newsdate: 'April 26, 2018',
    newspublisher: 'কালের কণ্ঠ',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'https://www.kalerkantho.com/print-edition/industry-business/2018/04/26/629301'
  },
  {
    id: '78',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/prothomaloo.jpg',
    newstitle: 'দেশের চারখাতে ব্যাবসায়ে আগ্রহ ভারতীয়দের',
    newsdate: 'April 26, 2018',
    newspublisher: 'প্রথম আলো',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/img-business-delegation-bangladesh-2_April_18.jpg'
  },
  {
    id: '79',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/financialexpress.jpg',
    newstitle: 'Indian entrepreneurs looking to work under joint collaboration',
    newsdate: 'April 26, 2018',
    newspublisher: 'The Financial Express',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'https://today.thefinancialexpress.com.bd/print/indian-entrepreneurs-looking-to-work-under-joint-collaboration-1524682259'
  },
  {
    id: '80',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/dhakatribune.jpg',
    newstitle: 'West Bengal investors keen on health, education and IT',
    newsdate: 'April 25, 2018',
    newspublisher: 'Dhaka Tribune',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'https://www.dhakatribune.com/business/2018/04/25/west-bengal-investors-keen-health-education/'
  },
  {
    id: '81',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/economics.jpg',
    newstitle: 'We won the ET Bengal Corporate Award for Best Financial Performance',
    newsdate: 'March 23, 2018',
    newspublisher: 'The Economic Times',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: '/static/media/Docs/News/Pdf/The-Economic-Times-Kolkata-20180323.pdf'
  },
  {
    id: '82',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/femina.jpg',
    newstitle: 'Abhishek Rungta talks about the importance of Influencer Marketing by Femina',
    newsdate: 'March, 2018',
    newspublisher: 'Femina Magazine',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: '/static/images/news-room/news/publisher/articles/Big_Story_March_18.jpg'
  },
  {
    id: '83',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/economics.jpg',
    newstitle: 'For SMEs, Meaningful Relationships Are More Important Than Marketing',
    newsdate: 'May 28, 2018',
    newspublisher: 'The Economic Times',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'https://economictimes.indiatimes.com/small-biz/marketing-branding/marketing/for-smes-meaningful-relationships-are-more-important-than-marketing/articleshow/64350775.cms'
  },
  {
    id: '84',
    thumbUrl: '/static/images/news-room/news/publisher/thumb/business-standard.jpg',
    newstitle: 'Back Indian textile industry to double productivity by 2030',
    newsdate: 'January 27, 2017',
    newspublisher: 'Business Standard',
    newspublishersrc: 'Online',
    readmore: 'Read More »',
    readmorelink: 'https://www.business-standard.com/article/b2b-connect/back-indian-textile-industry-to-double-productivity-by-2030-117012700707_1.html'
  },
];

export default newslist;