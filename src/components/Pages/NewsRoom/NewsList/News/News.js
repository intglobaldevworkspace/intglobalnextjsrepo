import React from 'react';
import { 
  Row,
  Col
} from 'reactstrap';
import Fade from 'react-reveal/Fade';

import newsData from './newsData';

export default class News extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      items: newsData,
      visible: 12,
      error: false
    };

    this.loadMore = this.loadMore.bind(this);
  }

  loadMore() {
    this.setState((prev) => {
      return {visible: prev.visible + 3};
    });
  }

  render() {
    return (
      <div className="news-releases">
        <Row>
          <Col md="12">
            <div className="news-container" aria-live="polite">
              {this.state.items.slice(0, this.state.visible).map((item, index) => {
                return (
                  <Col md="4" className="news-col fade-in" key={item.id}>
                    <Fade top>
                      <div className="news-col_box">
                        <figure>
                          <img src={item.thumbUrl} className="img-fluid" alt={item.newstitle} />
                        </figure>
                        <h4>{item.newstitle}</h4>
                        <span className="news-publisher">{item.newspublisher} | <small>{item.newspublishersrc}</small></span>
                        <div className="news-action-row">
                          <a href={item.readmorelink} className="readmore-link" target="_blank">{item.readmore}</a>
                          <span className="news-date">
                            {item.newsdate}
                          </span>
                        </div>
                      </div>
                    </Fade>
                  </Col>
                );
              })}
            <Fade top>
              {this.state.visible < this.state.items.length &&
                <button onClick={this.loadMore} type="button" className="load-more">Load more</button>
              }
            </Fade>
          </div>
          </Col>
        </Row>  
      </div>
    )
  }
}