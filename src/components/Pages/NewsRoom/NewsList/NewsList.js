import React from "react";
import { Row, Col } from "reactstrap";
import { useMediaQuery } from "react-responsive";
import Fade from "react-reveal/Fade";

import NewsRoomTab from "./NewsRoomTab";
import NewsRoomAccord from "./NewsRoomAccord";

const NewsList = (props) => {
  const isDesktopOrLaptop = useMediaQuery({ minDeviceWidth: 1024 });
  const isTabletOrMobile = useMediaQuery({ maxDeviceWidth: 1023 });
  // console.log(props);
  const accordDetails = {
    settings: props.newsl.newsroomaccordsettings,
    tabtitles: props.newsl.newsroomtabtitle,
  };
  return (
    <section className="news-wrapper">
      <Row>
        <Col md="10">
          <Fade bottom>
            <h2>{props.newsl.newslistheader}</h2>
          </Fade>
        </Col>
      </Row>
      {isDesktopOrLaptop && (
        <NewsRoomTab newsrtab={props.newsl.newsroomtabtitle} />
      )}
      {isTabletOrMobile && (
        <NewsRoomAccord newsroomaccord={{ ...accordDetails }} />
      )}
    </section>
  );
};

export default NewsList;
