import React from "react";
import { TabContent, TabPane, Nav, NavItem, NavLink, Col } from "reactstrap";
import classnames from "classnames";

export default class ExecuteVerticalTabs extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: this.props.effexecution[0].id,
      verticalSliderArray: this.props.effexecution,
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  render() {
    // console.log(this.state);
    const urlprefix = process.env.servUploadImg;
    return (
      <div className="execution-tab_wrapper">
        <Col md="3">
          <Nav tabs vertical pills>
            {this.state.verticalSliderArray.map((vtabObj) => {
              //vtabObj.rtverticaltabpane[0]
              return (
                <NavItem key={vtabObj.id}>
                  <NavLink
                    className={classnames({
                      active: this.state.activeTab === vtabObj.id,
                    })}
                    onClick={() => {
                      this.toggle(vtabObj.id);
                    }}
                  >
                    {vtabObj.rtverticaltabpane[0].rtverticalslidemaintab}
                  </NavLink>
                </NavItem>
              );
            })}
          </Nav>
        </Col>

        <Col md="9">
          <TabContent activeTab={this.state.activeTab}>
            {this.state.verticalSliderArray.map((vtabObj) => {
              return (
                <TabPane tabId={vtabObj.id} key={vtabObj.id}>
                  <ul className="execution-list">
                    {vtabObj.rtverticaltabpane[0].rttabpanecontent.map(
                      (innertabbObj) => {
                        return (
                          <li key={innertabbObj.id}>
                            <figure>
                              <img
                                src={urlprefix + innertabbObj.rttabpaneimg.url}
                                className="img-fluid"
                              />
                            </figure>
                            <h4
                              dangerouslySetInnerHTML={{
                                __html: innertabbObj.rttabpaneheading,
                              }}
                            ></h4>
                            <p
                              dangerouslySetInnerHTML={{
                                __html: innertabbObj.rttabpanecontent,
                              }}
                            ></p>
                          </li>
                        );
                      }
                    )}
                  </ul>
                </TabPane>
              );
            })}
          </TabContent>
        </Col>
      </div>
    );
  }
}
