import React from "react";
import { Row } from "reactstrap";
import Fade from "react-reveal/Fade";
import { useMediaQuery } from "react-responsive";

import ExecuteVerticalTabs from "./ExecuteVerticalTabs";
import ExecuteAccord from "./ExecuteAccord";

const Execution = (props) => {
  const isDesktopOrLaptop = useMediaQuery({ minDeviceWidth: 1024 });
  const isTabletOrMobile = useMediaQuery({ maxDeviceWidth: 1023 });

  return (
    <section className="execution">
      <Fade bottom>
        <h2
          dangerouslySetInnerHTML={{
            __html: props.effexecution.verticalSliderheader,
          }}
        ></h2>
        <Row>
          <div className="execution-tab">
            {isDesktopOrLaptop && (
              <ExecuteVerticalTabs
                effexecution={props.effexecution.rteamverticalslidercolls}
              />
            )}
            {isTabletOrMobile && (
              <ExecuteAccord
                effexecution={props.effexecution.rteamverticalslidercolls}
              />
            )}
          </div>
        </Row>
      </Fade>
    </section>
  );
};

export default Execution;
