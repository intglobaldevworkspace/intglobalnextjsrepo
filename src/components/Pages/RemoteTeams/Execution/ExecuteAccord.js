import React, { Component } from "react";
import { Accordion, Card } from "react-bootstrap";
import { Col } from "reactstrap";

export default class ExecuteAccord extends Component {
  constructor(props) {
    super(props);

    this.state = {
      verticalSliderArray: this.props.effexecution,
      activetab: this.props.effexecution[0].id,
    };
  }
  render() {
    const urlprefix = process.env.servUploadImg;
    // console.log(this.state);
    return (
      <Col md="12">
        <Accordion defaultActiveKey="0">
          {this.state.verticalSliderArray.map((vsliderObj) => {
            return (
              <Card key={vsliderObj.id}>
                <Accordion.Toggle as={Card.Header} eventKey={vsliderObj.id}>
                  {vsliderObj.rtverticaltabpane[0].rtverticalslidemaintab}
                  <i className="creat"></i>
                </Accordion.Toggle>
                <Accordion.Collapse eventKey={vsliderObj.id}>
                  <Card.Body>
                    <i className="creat"></i>
                    <ul className="execution-list">
                      {vsliderObj.rtverticaltabpane[0].rttabpanecontent.map(
                        (innertabContent) => {
                          return (
                            <li key={innertabContent.id}>
                              <figure>
                                <img
                                  src={
                                    urlprefix + innertabContent.rttabpaneimg.url
                                  }
                                  className="img-fluid"
                                />
                              </figure>
                              <h4
                                dangerouslySetInnerHTML={{
                                  __html: innertabContent.rttabpaneheading,
                                }}
                              ></h4>
                              <p
                                dangerouslySetInnerHTML={{
                                  __html: innertabContent.rttabpanecontent,
                                }}
                              ></p>
                            </li>
                          );
                        }
                      )}
                    </ul>
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
            );
          })}
        </Accordion>
      </Col>
    );
  }
}
