import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

// import featuresData from './featuresData';

const ExtendedTeam = (props) => {
  // console.log(props.extendedteam);
  const urlprefix = process.env.servUploadImg;
  const featuresData = props.extendedteam.rtextendedteams;
  return (
    <section className="extend-team">
      <Fade bottom>
        <div
          dangerouslySetInnerHTML={{
            __html: props.extendedteam.reextendedteamheader,
          }}
        ></div>
        <Row>
          <Col md="12">
            <ul className="features-list">
              {featuresData.map((featureDetail) => {
                return (
                  <li id={featureDetail.id} key={featureDetail.id}>
                    <figure>
                      <img
                        src={urlprefix + featureDetail.rtextendedteamimg.url}
                        className="img-fluid"
                        alt={featureDetail.extendedteamtitle}
                      />
                    </figure>
                    <h4>{featureDetail.extendedteamtitle}</h4>
                  </li>
                );
              })}
            </ul>
          </Col>
        </Row>
      </Fade>
    </section>
  );
};

export default ExtendedTeam;
