/* eslint max-len: 0 */

export const features = [
  {
    id: '1',
    iconUrl: '/static/images/icons/remote-team/features/quick-start.svg',
    title: 'Quick Start'
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/remote-team/features/zero-setup-fee.svg',
    title: 'Zero Set-up Fees'
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/remote-team/features/no-long-term-contracts.svg',
    title: 'No Long Term Contracts'
  },
  {
    id: '4',
    iconUrl: '/static/images/icons/remote-team/features/3x-productive.svg',
    title: '3x More Productive'
  },
  {
    id: '5',
    iconUrl: '/static/images/icons/remote-team/features/lowest-tco.svg',
    title: 'Lowest TCO'
  },
  {
    id: '6',
    iconUrl: '/static/images/icons/remote-team/features/on-time-delivery.svg',
    title: 'On-Time Delivery'
  },
  {
    id: '7',
    iconUrl: '/static/images/icons/remote-team/features/on-demand-support.svg',
    title: 'On-Demand Support'
  },
  {
    id: '8',
    iconUrl: '/static/images/icons/remote-team/features/quick-scaling.svg',
    title: 'Quick Scaling'
  },
];

export default features;
