import React from "react";
import { Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const Banner = (props) => {
  // console.log(props.headerban);
  return (
    <section className="hdBan_inner">
      <div className="hdBan_inner__teaser">
        <Col md="6">
          <Fade left>
            <h1
              dangerouslySetInnerHTML={{
                __html: props.headerban.reheaderbannertxt,
              }}
            ></h1>
          </Fade>
        </Col>
      </div>
    </section>
  );
};

export default Banner;
