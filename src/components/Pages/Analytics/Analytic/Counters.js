import React from "react";
import CountUp from "react-countup";
import VisibilitySensor from "react-visibility-sensor";
import { Row, Col } from "reactstrap";

export default function Counter(props) {
  // console.log(props.analyticsctr);
  const [focus, setFocus] = React.useState(false);
  // setFocus({
  //   ...focus,
  //   counterfocus: true,
  // });
  return (
    <div className="analytic-counter">
      <Row>
        {props.analyticsctr.map((analystObj) => {
          return (
            <Col md="3" key={analystObj.id}>
              <div className="c-block">
                <CountUp
                  start={focus ? 0 : null}
                  end={analystObj.analyticscounterconfig.end}
                  delay={analystObj.analyticscounterconfig.delay}
                  duration={analystObj.analyticscounterconfig.duration}
                  redraw={true}
                  useEasing={true}
                  useGrouping={true}
                >
                  {({ countUpRef }) => (
                    <h4>
                      <span ref={countUpRef} />
                      <VisibilitySensor
                        onChange={(isVisible) => {
                          if (isVisible) {
                            setFocus(true);
                          }
                        }}
                      >
                        <a>+</a>
                      </VisibilitySensor>
                    </h4>
                  )}
                </CountUp>
                <span className="det">{analystObj.counterheader}</span>
              </div>
            </Col>
          );
        })}
      </Row>
    </div>
  );
}
