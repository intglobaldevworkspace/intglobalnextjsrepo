import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

import Counters from "./Counters";

const Analytic = (props) => {
  // console.log(props.anly);
  const {
    headertextone,
    headertexttwo,
    scheduleacallbutton,
    analyticscounter,
  } = props.anly;
  return (
    <section className="analytic">
      <Row>
        <Col md="11">
          <Fade bottom>
            <h2>{headertextone}</h2>
          </Fade>
        </Col>
        <Col md="12">
          <Fade bottom>
            <Counters analyticsctr={analyticscounter} />
          </Fade>
        </Col>
        <Col md="10">
          <Fade bottom>
            <p>{headertexttwo}</p>
          </Fade>
        </Col>
        <Col md="10">
          <Fade bottom>
            {scheduleacallbutton.scheduleacallbuttonstatus ? (
              <a
                href={scheduleacallbutton.scheduleacallurl}
                className="btn link"
              >
                {scheduleacallbutton.scheduleacalltext}
              </a>
            ) : null}
          </Fade>
        </Col>
      </Row>
    </section>
  );
};

export default Analytic;
