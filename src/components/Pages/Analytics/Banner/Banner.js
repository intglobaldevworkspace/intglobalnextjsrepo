import React from "react";
import { Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const Banner = (props) => {
  // console.log(props.bdata);
  const {
    bannertextone,
    bannertexttwo,
    bannertextthree,
    bannertextfour,
  } = props.bdata;
  return (
    <section className="hdBan_inner">
      <div className="hdBan_inner__teaser">
        <Col md="7">
          <Fade left>
            <h1>
              <b>
                {bannertextone} <u>{bannertexttwo}</u>
              </b>
              {bannertextthree}
              <strong>{bannertextfour}</strong>
            </h1>
          </Fade>
        </Col>
      </div>
    </section>
  );
};

export default Banner;
