import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";
import { useMediaQuery } from "react-responsive";

import ServicesVerticalTabs from "./ServicesVerticalTabs";
import ServicesAccord from "./ServicesAccord";

const Services = (props) => {
  // console.log(props.servicesdata);
  const {
    servicesmainheader,
    servicesmainsubheader,
    servicesdata,
  } = props.servicesdata;
  const isDesktopOrLaptop = useMediaQuery({ minDeviceWidth: 1024 });
  const isTabletOrMobile = useMediaQuery({ maxDeviceWidth: 1023 });

  return (
    <section className="our-services">
      <Fade bottom>
        <h2>{servicesmainheader}</h2>
      </Fade>
      <Col md="10">
        <Fade bottom>
          <p>{servicesmainsubheader}</p>
        </Fade>
      </Col>
      <Fade bottom>
        <Row>
          <div className="services-tab">
            {isDesktopOrLaptop && (
              <ServicesVerticalTabs servdata={servicesdata} />
            )}
            {isTabletOrMobile && <ServicesAccord servdata={servicesdata} />}
          </div>
        </Row>
      </Fade>
    </section>
  );
};

export default Services;
