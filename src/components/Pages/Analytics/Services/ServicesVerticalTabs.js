import React, { Component } from "react";
import { TabContent, TabPane, Nav, NavItem, NavLink, Col } from "reactstrap";
import classnames from "classnames";
import Fade from "react-reveal/Fade";

export default class ServicesVerticalTabs extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: this.props.servdata[0].id,
      tabcontent: this.props.servdata,
    };
    // console.log(this.props);
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  render() {
    const { activeTab, tabcontent } = this.state;
    const urlprefix = process.env.servUploadImg;
    return (
      <div>
        <Col md="3">
          <Nav tabs vertical pills>
            {tabcontent.map((tabcontentdata) => {
              return (
                <NavItem key={tabcontentdata.id}>
                  <NavLink
                    className={classnames({
                      active: activeTab === tabcontentdata.id,
                    })}
                    onClick={() => {
                      this.toggle(tabcontentdata.id);
                    }}
                  >
                    {tabcontentdata.servanalyticsheader}
                  </NavLink>
                </NavItem>
              );
            })}
          </Nav>
        </Col>
        <Col md="9">
          <TabContent activeTab={activeTab}>
            {tabcontent.map((tabcontentdata) => {
              return (
                <TabPane tabId={tabcontentdata.id} key={tabcontentdata.id}>
                  <ul className="services-list">
                    {tabcontentdata.servanalyticscontdetails.map(
                      (analyticsinnerObj) => {
                        return (
                          <li key={analyticsinnerObj.id}>
                            <Fade bottom>
                              <div className="hvr-blocks-out">
                                <figure>
                                  <img
                                    src={
                                      urlprefix + analyticsinnerObj.contimg.url
                                    }
                                    className="img-fluid"
                                  />
                                </figure>
                                <h4>{analyticsinnerObj.contdetailheader}</h4>
                                <div>
                                  <p>{analyticsinnerObj.contdetailsubheader}</p>
                                </div>
                                <a></a>
                              </div>
                            </Fade>
                          </li>
                        );
                      }
                    )}
                  </ul>
                </TabPane>
              );
            })}
          </TabContent>
        </Col>
      </div>
    );
  }
}
