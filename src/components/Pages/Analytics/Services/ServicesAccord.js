import React, { Component } from "react";
import { Accordion, Card } from "react-bootstrap";
import { Col } from "reactstrap";

export default class ServicesAccord extends Component {
  constructor(props) {
    super(props);

    // this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: this.props.servdata[0].id,
      tabcontent: this.props.servdata,
    };
    // console.log(this.props);
  }
  // toggle(tab) {
  //   if (this.state.activeTab !== tab) {
  //     this.setState({
  //       activeTab: tab,
  //     });
  //   }
  // }

  // onClick={() => {
  //                   this.toggle(tabcontentdata.id);
  //                 }}

  render() {
    const { activeTab, tabcontent } = this.state;
    const urlprefix = process.env.servUploadImg;
    return (
      <Col md="12">
        <Accordion defaultActiveKey={activeTab}>
          {tabcontent.map((tabcontentdata) => {
            return (
              <Card key={tabcontentdata.id}>
                <Accordion.Toggle as={Card.Header} eventKey={tabcontentdata.id}>
                  {tabcontentdata.servanalyticsheader}
                  <i className="creat"></i>
                </Accordion.Toggle>
                <Accordion.Collapse eventKey={tabcontentdata.id}>
                  <Card.Body>
                    <i className="creat"></i>
                    <ul className="services-list">
                      {tabcontentdata.servanalyticscontdetails.map(
                        (analyticsinnerObj) => {
                          return (
                            <li key={analyticsinnerObj.id}>
                              <figure>
                                <img
                                  src={
                                    urlprefix + analyticsinnerObj.contimg.url
                                  }
                                  className="img-fluid"
                                />
                              </figure>
                              <h4>{analyticsinnerObj.contdetailheader}</h4>
                              <p>{analyticsinnerObj.contdetailsubheader}</p>
                            </li>
                          );
                        }
                      )}
                    </ul>
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
            );
          })}
        </Accordion>
      </Col>
    );
  }
}
