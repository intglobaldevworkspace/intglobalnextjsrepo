/* eslint max-len: 0 */

export const whyus = [
  {
    id: '1',
    iconUrl: '/static/images/icons/analytics/why-us/data-science.svg',
    tooltipID: 'Toolti01',
    tooltipText: 'Our data scientists enable the system to learn, become smarter and predict outcomes to bring in to delight customers, optimize systems and pivot new business models.',
    title: 'Data Science'
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/analytics/why-us/big-data-engineering-warehousing.svg',
    tooltipID: 'Toolti02',
    tooltipText: "We help businesses with real-time data ingestion, ETL & batch processing and storage from different & complex data sources leveraging our deep expertise across big data technologies.",
    title: 'Big Data Engineering & Warehousing'
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/analytics/why-us/data-compliance-security.svg',
    tooltipID: 'Toolti03',
    tooltipText: 'We help businesses identify potential threats through, data governance and access & identity management to help ensure data security.',
    title: 'Data Compliance & Security'
  },
  {
    id: '4',
    iconUrl: '/static/images/icons/analytics/why-us/machine-learning-deep-learning.svg',
    tooltipID: 'Toolti04',
    tooltipText: 'Our team of machine learning experts leverage the technology through strategic experiments, connecting historical data to predictive analytics engines.',
    title: 'Machine Learning & Deep Learning'
  },
  {
    id: '5',
    iconUrl: '/static/images/icons/analytics/why-us/data-visualization-design.svg',
    tooltipID: 'Toolti05',
    tooltipText: 'We use various tools such as Tableau, Chart.js, Dygraphs, D3.js and HighCharts to produce visuals and stories that generate high business impact.',
    title: 'Data Visualization Design'
  },
  {
    id: '6',
    iconUrl: '/static/images/icons/analytics/why-us/intelligent-products-and-systems.svg',
    tooltipID: 'Toolti06',
    tooltipText: 'Our analytics prowess helps us build personalized experiences, enhance or create new products or services, conceptualize and design better processes and overall provide the best customer services.',
    title: 'Intelligent Products and Systems'
  },
];

export default whyus;
