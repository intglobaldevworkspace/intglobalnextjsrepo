import React, { useState } from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";
import Contactus from "../../../shared/Contactus";
// import whyusData from "./whyusData";

const WhyUS = (props) => {
  // console.log(props.whyuscont);
  const [whyState, setwhyState] = useState({
    isOpen: false,
    showContactusForm: true,
    showContactusSubmitted: false,
    ContactusSubmitfailed: false,
    showSubmitLoader: false,
    //----
    // tooltipOpen: false,
    //--------
    // productSolutionitems: props.sols.productSolutionitems,
    // productSolutionheaderOne: props.sols.productSolutionheaderOne,
    // productSolutionheaderTwo: props.sols.productSolutionheaderTwo,
    // letustalkbuttonendtoendsol: props.sols.letustalkbuttonendtoendsol,
    contactuspopup: props.whyuscont.contactuspopup,
  });
  const toggleModal = () => {
    setwhyState((prevState) => ({
      ...prevState,
      isOpen: !prevState.isOpen,
      showContactusForm: true,
      showContactusSubmitted: false,
      ContactusSubmitfailed: false,
      showSubmitLoader: false,
    }));
  };

  const urlprefix = process.env.servUploadImg;
  const cntpopup = {
    isOpen: whyState.isOpen,
    showContactusForm: whyState.showContactusForm,
    showContactusSubmitted: whyState.showContactusSubmitted,
    ContactusSubmitfailed: whyState.ContactusSubmitfailed,
    showSubmitLoader: whyState.showSubmitLoader,
    contactusPopup: whyState.contactuspopup,
  };

  const {
    whyusdatacont,
    whyusheader,
    whyussubheader,
    whyusletustalk,
  } = props.whyuscont;
  return (
    <section className="whyus">
      <Row>
        <Fade bottom>
          <h2>{whyusheader}</h2>
        </Fade>
        <Fade bottom>
          <Row>
            <Col md="8">
              <p>{whyussubheader}</p>
            </Col>
          </Row>
        </Fade>
        <Col md="12">
          <ul className="whyus-item">
            {whyusdatacont.map((whyusDetail) => {
              return (
                <li id={whyusDetail.id} md="3" key={whyusDetail.id}>
                  <Fade bottom>
                    <div className="hvr-blocks-out">
                      <figure>
                        <img
                          src={urlprefix + whyusDetail.whyusimage.url}
                          className="img-fluid"
                          alt={whyusDetail.whyustextone}
                        />
                      </figure>
                      <h6>{whyusDetail.whyustextone}</h6>
                      <div>
                        <p>{whyusDetail.whyuscontent}</p>
                      </div>
                      <a></a>
                    </div>
                  </Fade>
                </li>
              );
            })}
          </ul>
          <Fade bottom>
            {whyusletustalk.letustalkstatus ? (
              <a
                href={whyusletustalk.letustalkurl}
                className="btn link"
                onClick={(e) => {
                  e.preventDefault();
                  // toggle();
                  toggleModal();
                }}
              >
                {whyusletustalk.letustalktext}
              </a>
            ) : null}
          </Fade>
        </Col>
      </Row>
      {whyState.isOpen ? (
        <Contactus
          cntpopup={{
            ...cntpopup,
            contactText: whyusletustalk.letustalktext,
          }}
          handler={toggleModal}
        />
      ) : null}
    </section>
  );
};

export default WhyUS;
