import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const TechnologyPartner = (props) => {
  return (
    <section className="tech_partnr">
      <Fade bottom>
        <div
          dangerouslySetInnerHTML={{ __html: props.techpart.petechheaderone }}
        ></div>
        <Row>
          <Col md="6">
            <div
              dangerouslySetInnerHTML={{
                __html: props.techpart.petechheadertwo,
              }}
            ></div>
          </Col>
          <Col md="6">
            <div
              dangerouslySetInnerHTML={{
                __html: props.techpart.petechheaderthree,
              }}
            ></div>
          </Col>
        </Row>
      </Fade>
    </section>
  );
};

export default TechnologyPartner;
