import React, { useState } from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";
import Contactus from "../../../shared/Contactus";
// import productsData from "./productsData";

const Solutions = (props) => {
  const [solutionState, setsolutionState] = useState({
    isOpen: false,
    showContactusForm: true,
    showContactusSubmitted: false,
    ContactusSubmitfailed: false,
    showSubmitLoader: false,
    //----
    // tooltipOpen: false,
    //--------
    productSolutionitems: props.sols.productSolutionitems,
    productSolutionheaderOne: props.sols.productSolutionheaderOne,
    productSolutionheaderTwo: props.sols.productSolutionheaderTwo,
    letustalkbuttonendtoendsol: props.sols.letustalkbuttonendtoendsol,
    contactuspopup: props.sols.contactuspopup,
  });

  const toggleModal = () => {
    setsolutionState((prevState) => ({
      ...prevState,
      isOpen: !prevState.isOpen,
      showContactusForm: true,
      showContactusSubmitted: false,
      ContactusSubmitfailed: false,
      showSubmitLoader: false,
    }));
  };
  const urlprefix = process.env.servUploadImg;

  const cntpopup = {
    isOpen: solutionState.isOpen,
    showContactusForm: solutionState.showContactusForm,
    showContactusSubmitted: solutionState.showContactusSubmitted,
    ContactusSubmitfailed: solutionState.ContactusSubmitfailed,
    showSubmitLoader: solutionState.showSubmitLoader,
    contactusPopup: solutionState.contactuspopup,
  };
  return (
    <section className="solution">
      <Fade bottom>
        <div
          dangerouslySetInnerHTML={{
            __html: solutionState.productSolutionheaderOne,
          }}
        ></div>
      </Fade>
      <Row>
        <Col md="4">
          <Fade left>
            <div
              dangerouslySetInnerHTML={{
                __html: solutionState.productSolutionheaderTwo,
              }}
            ></div>
          </Fade>
        </Col>
        <Col md="8">
          <Row>
            {solutionState.productSolutionitems.map((productDetail, index) => {
              return (
                <Col
                  id={productDetail.id}
                  md="3"
                  className="prdt-item"
                  key={productDetail.id}
                >
                  <Fade bottom>
                    <div className="hvr-blocks-out">
                      <figure>
                        <img
                          src={
                            urlprefix +
                            productDetail.productsoldetails.productimage.url
                          }
                          className="img-fluid"
                          alt={productDetail.productsoldetails.producttitle}
                        />
                      </figure>
                      <h6>{productDetail.productsoldetails.producttitle}</h6>
                      <div>
                        <p>{productDetail.productsoldetails.toolTipText}</p>
                      </div>
                      <a href="#"></a>
                    </div>
                  </Fade>
                </Col>
              );
            })}
          </Row>
        </Col>
      </Row>
      <Fade bottom>
        <a
          href={solutionState.letustalkbuttonendtoendsol.letustalkurl}
          className="btn link"
          onClick={(e) => {
            e.preventDefault();
            // toggle();
            toggleModal();
          }}
        >
          {solutionState.letustalkbuttonendtoendsol.letustalktext}
        </a>
      </Fade>
      {solutionState.isOpen ? (
        <Contactus
          cntpopup={{
            ...cntpopup,
            contactText: solutionState.letustalkbuttonendtoendsol.letustalktext,
          }}
          handler={toggleModal}
        />
      ) : null}
    </section>
  );
};

export default Solutions;
