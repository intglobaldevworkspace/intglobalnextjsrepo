/* eslint max-len: 0 */

export const products = [
  {
    id: '1',
    iconUrl: '/static/images/icons/prdt-eng/solutions/product-consulting.svg',
    title: 'Product Consulting',
    tooltipID: 'Toolti01',
    tooltipText: 'We ideate and identify possibilities, and help you choose the ones that make the maximum impact.'
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/prdt-eng/solutions/product-architecture.svg',
    title: 'Product Architecture',
    tooltipID: 'Toolti02',
    tooltipText: 'We map user journey, define the functional flow, create a wireframe and design the user experience that engages and delights end-users.'
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/prdt-eng/solutions/product-development.svg',
    title: 'Product Development',
    tooltipID: 'Toolti03',
    tooltipText: 'We build the technology architecture of the application in a way that it incorporates evolving business needs.'
  },
  {
    id: '4',
    iconUrl: '/static/images/icons/prdt-eng/solutions/product-design.svg',
    title: 'Product Design',
    tooltipID: 'Toolti04',
    tooltipText: 'We do iterative software development using cutting-edge technologies for robustness, security, and scale.'
  },
  {
    id: '5',
    iconUrl: '/static/images/icons/prdt-eng/solutions/product-testing.svg',
    title: 'Product Testing',
    tooltipID: 'Toolti05',
    tooltipText: 'We follow best-in-class quality assurance processes to ensure superior product quality.'
  },
  {
    id: '6',
    iconUrl: '/static/images/icons/prdt-eng/solutions/product-launch.svg',
    title: 'Product Launch',
    tooltipID: 'Toolti06',
    tooltipText: 'We help you continually evolve the product based on iterative feedback cycle from users, manage the cloud and DevOps infra to help you scaleWe.'
  },
  {
    id: '7',
    iconUrl: '/static/images/icons/prdt-eng/solutions/product-scaling.svg',
    title: 'Product Scaling',
    tooltipID: 'Toolti07',
    tooltipText: 'Our customer-centric approach helps us serve you the best.'
  },
  {
    id: '8',
    iconUrl: '/static/images/icons/prdt-eng/solutions/devops.svg',
    title: 'DevOps',
    tooltipID: 'Toolti08',
    tooltipText: 'Our customer-centric approach helps us serve you the best.'
  },
];

export default products;
