import React, { useState } from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";
import Contactus from "../../../shared/Contactus";
// import systemsData from './systemsData';

const SoftwareSolutions = (props) => {
  const [softSolState, setSoftSolState] = useState({
    isOpen: false,
    showContactusForm: true,
    showContactusSubmitted: false,
    ContactusSubmitfailed: false,
    showSubmitLoader: false,
    //----
    softSolHeader: props.softsol.softSolHeader,
    softSolItems: props.softsol.softSolItems,
    letustalkbuttonSoftwarereEngineering:
      props.softsol.letustalkbuttonSoftwarereEngineering,
    contactuspopup: props.softsol.contactuspopup,
  });
  const urlprefix = process.env.servUploadImg;

  const toggleModal = () => {
    setSoftSolState((prevState) => ({
      ...prevState,
      isOpen: !prevState.isOpen,
      showContactusForm: true,
      showContactusSubmitted: false,
      ContactusSubmitfailed: false,
      showSubmitLoader: false,
    }));
  };

  const cntpopup = {
    isOpen: softSolState.isOpen,
    showContactusForm: softSolState.showContactusForm,
    showContactusSubmitted: softSolState.showContactusSubmitted,
    ContactusSubmitfailed: softSolState.ContactusSubmitfailed,
    showSubmitLoader: softSolState.showSubmitLoader,
    contactusPopup: softSolState.contactuspopup,
  };
  // console.log(softSolState.softSolItems);

  return (
    <section className="soft-solutions">
      <Col md="10">
        <Row>
          <Fade bottom>
            <div
              dangerouslySetInnerHTML={{ __html: softSolState.softSolHeader }}
            ></div>
          </Fade>
          <Col md="12">
            <ul className="soft-item">
              {softSolState.softSolItems.map((systemDetail) => {
                return (
                  <li id={systemDetail.id} md="3" key={systemDetail.id}>
                    <Fade bottom>
                      <div className="hvr-blocks-out">
                        <figure>
                          <img
                            src={urlprefix + systemDetail.softsolimg.url}
                            className="img-fluid"
                            alt={systemDetail.softsoltitle}
                          />
                        </figure>
                        <h6>{systemDetail.softsoltitle}</h6>
                        <div>
                          <p>{systemDetail.softsoltooltiptext}</p>
                        </div>
                        <a href="#"></a>
                      </div>
                    </Fade>
                  </li>
                );
              })}
            </ul>
          </Col>
          <Fade bottom>
            <a
              href={
                softSolState.letustalkbuttonSoftwarereEngineering
                  .reengineerletustalkurl
              }
              className="btn link"
              onClick={(e) => {
                e.preventDefault();
                // toggle();
                toggleModal();
              }}
            >
              {
                softSolState.letustalkbuttonSoftwarereEngineering
                  .reengineerletustalktext
              }
            </a>
          </Fade>
        </Row>
      </Col>
      {softSolState.isOpen ? (
        <Contactus
          cntpopup={{
            ...cntpopup,
            contactText:
              softSolState.letustalkbuttonSoftwarereEngineering
                .reengineerletustalktext,
          }}
          handler={toggleModal}
        />
      ) : null}
    </section>
  );
};

export default SoftwareSolutions;
