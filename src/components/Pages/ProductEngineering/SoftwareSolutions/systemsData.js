/* eslint max-len: 0 */

export const systems = [
  {
    id: '1',
    iconUrl: '/static/images/icons/prdt-eng/solutions/system-migration.svg',
    title: 'Legacy System Migration',
    tooltipText: 'Identify the gaps that exist between your current software and the user needs, and help you create a roadmap to navigate the change.'
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/prdt-eng/solutions/system-audit.svg',
    title: 'Legacy System Audit & Gap Analysis',
    tooltipText: 'Technology changes and brings new possibilities. We help you identify those possibilities and modernize your software to support these changes to meet evolving user needs and capitalize on them.'
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/prdt-eng/solutions/system-modernization.svg',
    title: 'Legacy System Modernization',
    tooltipText: 'We help you migrate your data, processes, and users from your old system to new through risk management, best practices, and systems training.'
  },
];

export default systems;
