import React from "react";
import { Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const Banner = (props) => {
  return (
    <section className="hdBan_inner">
      <div className="hdBan_inner__teaser">
        <Col md="6">
          <Fade left>
            <h1
              dangerouslySetInnerHTML={{
                __html: props.hbcontents.bannerheader,
              }}
            ></h1>
          </Fade>
        </Col>
      </div>
    </section>
  );
};

export default Banner;
