/* eslint max-len: 0 */

export const awardlist = [
  {
    id: '1',
    thumbUrl: '/static/images/awards/thumb/deloitte-technology-fast-500-asia-pacific-in-2010-2011-2017.jpg',
    awardsname: 'Deloitte Technology Fast 500 Asia Pacific in 2010, 2011, 2017',
    tooltipText: 'Indus Net Technologies was acknowledged at Deloitte Technology Fast 500 Asia Pacific in 2010, 2011, 2017. The awardees are selected for the Technology Fast 500 ranking based on percentage fiscal year revenue growth over three years.',
    readmore: 'Read More »'
  },
  {
    id: '2',
    thumbUrl: '/static/images/awards/thumb/deloitte-technology-fast-50-india-in-2011-2017.jpg',
    awardsname: 'Deloitte Technology Fast 50 India in 2011, 2017',
    tooltipText: 'Indus Net Technologies was recognized at the “Deloitte Technology Fast 50 India” in 2011, 2017. This coveted program recognizes the 50 fastest-growing technology companies in India, based on percentage revenue growth over the last three financial years.',
    readmore: 'Read More »'
  },
  {
    id: '3',
    thumbUrl: '/static/images/awards/thumb/dun-and-bradstreet-2008.jpg',
    awardsname: 'Dun & Bradstreet - SME Business Excellence Awards 2008',
    tooltipText: 'Indus Net Technologies was acknowledged at the Dun & Bradstreet “SME Business Excellence Awards” in 2008, a platform that identifies and felicitates SMEs that have demonstrated exemplary performance in their respective fields.',
    readmore: 'Read More »'
  },
  {
    id: '4',
    thumbUrl: '/static/images/awards/thumb/cio-choice-mobile-application-development-under-enterprise-mobility-2017.jpg',
    awardsname: 'CIO Choice - Mobile Application Development under Enterprise Mobility, 2017',
    tooltipText: 'Indus Net Technologies bagged the “CIO Choice” award in 2017 in Mumbai. The process for the “CIO Choice” recognition is conducted via an independent advisory panel of imminent CIOs and an independent survey from across the country with CIOs and ICT decision makers.',
    readmore: 'Read More »'
  },
  {
    id: '5',
    thumbUrl: '/static/images/awards/thumb/top-100-smes-of-india-by-dun-and-bradstreet-in-2015.jpg',
    awardsname: 'Dun & Bradstreet - Top Leading 100 SMEs of India Awards 2015',
    tooltipText: 'Indus Net Technologies is amongst Top 100 SMEs of India in the publication “Leading SMEs of India 2015” by Dun & Bradstreet.',
    readmore: 'Read More »'
  },
  {
    id: '6',
    thumbUrl: '/static/images/awards/thumb/et-bengal-corporate-awards-nomination-for-best-financial-performance-2017.jpg',
    awardsname: 'ET Bengal Corporate Awards - Nomination for Best Financial Performance 2017',
    tooltipText: 'Indus Net Technologies was nominated for “Best Financial Performance” in the ET Bengal Corporate Awards, 2017. This corporate award in West Bengal recognizes the efforts of top heads of companies having their head offices/ corporate offices or registered offices in West Bengal.',
    readmore: 'Read More »'
  },
  {
    id: '7',
    thumbUrl: '/static/images/awards/thumb/et-bengal-corporate-Award-2013.jpg',
    awardsname: 'ET Bengal Corporate Award, 2013',
    tooltipText: 'Indus Net Technologies won the award for “Best Existing Large Corporate” with turnover less than Rs 300 Crore at the ET Bengal Corporate Awards in 2014.',
    readmore: 'Read More »'
  },
  {
    id: '8',
    thumbUrl: '/static/images/awards/thumb/nasscom-emerge-50-in-2010.jpg',
    awardsname: 'NASSCOM EMERGE 50 in 2010',
    tooltipText: 'Indus Net Technologies was recognized at the NASSCOM Emerge 50 Awards in 2010. This initiative identifies India’s Most Innovative Top 50 Emerging IT Product Companies.',
    readmore: 'Read More »'
  },
  {
    id: '9',
    thumbUrl: '/static/images/awards/thumb/india-sme-100-awards-2014.jpg',
    awardsname: 'India SME 100 Awards, 2014',
    tooltipText: 'Indus Net Technologies was chosen among the “TOP 100 SMEs” by the Jury of India SME Forum. This award recognises the companies across categories of growth, financial strength, innovation, international outlook, corporate governance and people capital.',
    readmore: 'Read More »'
  },
  {
    id: '10',
    thumbUrl: '/static/images/awards/thumb/prsi-engage-2016-award-for-best-mobile-application-development-and-use-for-swas.jpg',
    awardsname: 'PRSI Engage 2016 Award for Best Mobile Application Development & Use for SWAS',
    tooltipText: 'Indus Net Technologies won the award for best Engage Mobile Application Development & Use for SWAS. The award was given by Shri Bratya Basu, Hon’ble Minister of Information Technology & Electronics, West Bengal.',
    readmore: 'Read More »'
  },
  {
    id: '11',
    thumbUrl: '/static/images/awards/thumb/prsi-engage-2016-award-for-best-digital-agency-of-the-year.jpg',
    awardsname: 'PRSI Engage 2016 Award for Best Digital Agency of the Year',
    tooltipText: 'Indus Net Technologies received the PRSI Engage 2016 Award for Best Digital Agency of The Year from Shri Bratya Basu, Hon’ble Minister of Information Technology & Electronics, West Bengal. Engage is Eastern India’s largest Digital Media Conference.',
    readmore: 'Read More »'
  },
  {
    id: '12',
    thumbUrl: '/static/images/awards/thumb/award-and-certificate-in-the-medium-scale-category-for-outstanding-performance-in-it-and-ites-2016-2017.jpg',
    awardsname: 'Outstanding Performance in Medium Scale Category in IT & ITeS, 2016, 2017',
    tooltipText: 'Indus Net Technologies has won this award both in 2016 and 2017. In 2016, INT received this award from Shri Kalraj Mishra, Hon’ble Union Minister for Micro, Small and Medium Enterprises, India at the Federation of Industry Trade & Services (FITS) National Conference. In 2017, the award was given by Shri Haribhai Parthibhai Chaudhary, Hon’ble Minister of State for MSME.',
    readmore: 'Read More »'
  },
  {
    id: '13',
    thumbUrl: '/static/images/awards/thumb/national-prsi-award-for-best-online-marketing-2015.jpg',
    awardsname: 'National PRSI Award for “Best Online Marketing”, 2015',
    tooltipText: 'Indus Net Technologies bagged the first prize in the category of “Best Online Campaign” for our innovative campaign for Siddha Online Flat Booking App Promotion.',
    readmore: 'Read More »'
  },
  {
    id: '14',
    thumbUrl: '/static/images/awards/thumb/national-prsi-award-for-social-media-for-pr-and-branding-2015.jpg',
    awardsname: 'National PRSI Award for “Social Media for PR & Branding”, 2015',
    tooltipText: 'Indus Net Technologies received the National PSRI Award in the “Social Media – PR & Branding” category, 2015 for our work for renowned music director Shri Debojyoti Mishra.',
    readmore: 'Read More »'
  },
  {
    id: '15',
    thumbUrl: '/static/images/awards/thumb/bcc-and-i-msme-excellence-award-in-the-service-category-2016.jpg',
    awardsname: 'BCC&I MSME Excellence Award in the Service Category, 2016',
    tooltipText: 'Indus Net Technologies bagged the ‘BCC&I MSME Excellence Award’ in the service category in 2016. An annual programme of BCC&I, this event is organized to reward the MSMEs for their “Excellence in Best Practices” despite infrastructural and logistical constraints.',
    readmore: 'Read More »'
  },
  {
    id: '16',
    thumbUrl: '/static/images/awards/thumb/itpv-partner-leadership-award-2016-for-best-enterprise-mobility-solutions.jpg',
    awardsname: 'ITPV Partner Leadership Award 2016 for Best Enterprise Mobility Solutions',
    tooltipText: 'Indus Net Technologies was conferred with the ITPV Partnership Leadership Award 2016 for Best Enterprise Mobility Solutions in the category ‘Best Enterprise Mobility Partner’. The award acknowledges the best practices in enterprise solutions by Systems Integrators, Solution VARs, MSPs and ISVs across INDIA.',
    readmore: 'Read More »'
  },
  {
    id: '17',
    thumbUrl: '/static/images/awards/thumb/franchise-india-small-business-of-the-year-award-2014.jpg',
    awardsname: 'Franchise India “Small Business of the Year” Award, 2014',
    tooltipText: 'Small Business Award is a glorious opportunity for a business to get countrywide acclaim and recognition. Considered as the most coveted awards show complementing the leagues best across SMEs, SMB and new and emerging small-scale ventures, Small Business Awards is bound to leap ahead of the achievers across sectors and sub-sectors.',
    readmore: 'Read More »'
  },
  {
    id: '18',
    thumbUrl: '/static/images/awards/thumb/my-fm-and-stars-of-the-industry-group-national-it-excellence-award-2016.jpg',
    awardsname: '94.3 My FM & Stars of The Industry Group - National IT Excellence Award 2016',
    tooltipText: 'The National Awards in IT Excellence is a benchmark to recognize excellence throughout the IT industry. The Awards focus on the contribution of Individuals, projects, Organizations, and Technologies that excels in the use, development and deployment of IT and will be presented to companies that have demonstrated leadership in using IT for Business Transformation, optimizing costs, developing synergies across businesses and enhancing customer satisfaction - Indus Net Technologies won this award for Best eGovernance Implementation for our MyGov mobile app.',
    readmore: 'Read More »'
  },
  {
    id: '19',
    thumbUrl: '/static/images/awards/thumb/prsi-national-pr-day-excellence-in-csr-event-communication.jpg',
    awardsname: 'PRSI National PR Day — Excellence in CSR Event Communication',
    tooltipText: 'Indus Net Technologies was conferred the “Excellence in CSR Event Communication” award by Public Relations Society of India (PRSI), Kolkata Chapter in 2015. This is for the first time PRSI-Kolkata launched the National PR Day Award for excellence in various fields across industries.',
    readmore: 'Read More »'
  },
  {
    id: '20',
    thumbUrl: '/static/images/awards/thumb/silicon-valley-business-application-award-for-nasscom-conclave-mobile-app-2015.jpg',
    awardsname: 'Silicon Valley Business Application Award for Nasscom conclave mobile app 2015',
    tooltipText: 'Indus Net Technologies bagged the SVBA (Silicon Valley Business App) Awards in 2015. The event recognizes emerging strategic trends and best practices in apps and devices design, development, advertising, marketing, content distribution and commerce.',
    readmore: 'Read More »'
  },
  {
    id: '21',
    thumbUrl: '/static/images/awards/thumb/bcc-and-i-the-bengal-entrepreneurship-recognition-2015.jpg',
    awardsname: 'BCC&I the Bengal Entrepreneurship Recognition 2015',
    tooltipText: 'Indus Net Technologies was recognized at The Bengal Entrepreneurship Recognition, 2016. This initiative was undertaken by BCC&I identify and rewards entrepreneurs of West Bengal across all industry verticals.',
    readmore: 'Read More »'
  },
];

export default awardlist;