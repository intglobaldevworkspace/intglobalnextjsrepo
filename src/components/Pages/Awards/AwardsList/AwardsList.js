import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

// import awardsData from "./awardsData";

export default class AwardsList extends Component {
  constructor(props) {
    super(props);
    console.log(props);
    this.state = {
      items: this.props.awards.awardsList,
      visible: 6,
      error: false,
      header: this.props.awards.awardHeader,
      readm: this.props.awards.readmorelink,
    };

    this.loadMore = this.loadMore.bind(this);
  }

  loadMore() {
    this.setState((prev) => {
      return { visible: prev.visible + 3 };
    });
  }

  // componentDidMount() {
  //   fetch("https://jsonplaceholder.typicode.com/posts").then(
  //     res => res.json()
  //   ).then(res => {
  //     this.setState({
  //       items: res
  //     });
  //   }).catch(error => {
  //     console.error(error);
  //     this.setState({
  //       error: true
  //     });
  //   });
  // }

  render() {
    const urlprefix = process.env.servUploadImg;
    const { items, visible, readm } = this.state;
    // const readmore = "Read More »";
    return (
      <section className="awards-list">
        <Fade top>
          <h2>Our all Awards Showcase</h2>
        </Fade>
        <Row>
          <div className="awards-wrapper" aria-live="polite">
            {items.slice(0, visible).map((item, index) => {
              return (
                <Col md="4" key={item.id}>
                  <div className="awards-block fade-in">
                    <Fade top>
                      <div className="hvr-blocks-out">
                        <div className="awards-block_box">
                          <figure>
                            <img
                              src={urlprefix + item.awardImage.url}
                              className="img-fluid"
                              title={item.awardsname}
                              alt={item.awardsname}
                            />
                          </figure>
                        </div>
                        <div className="awards-title">
                          <h4>{item.awardsdescrip}</h4>
                        </div>
                        <div className="awards-action">
                          <a title={readm}>{readm}</a>
                        </div>
                        <div>
                          <p>{item.awardBriefDescription}</p>
                        </div>
                        <a></a>
                      </div>
                    </Fade>
                  </div>
                </Col>
              );
            })}
            <Fade top>
              {visible < items.length && (
                <button
                  onClick={this.loadMore}
                  type="button"
                  className="load-more"
                >
                  Load more
                </button>
              )}
            </Fade>
          </div>
        </Row>
      </section>
    );
  }
}
