import React from "react";
import { Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const Banner = (props) => {
  // console.log(props.ban);
  return (
    <section className="hdBan_inner">
      <div className="hdBan_inner__teaser">
        <Col md="6">
          <Fade left>
            <h1>
              <b>
                {props.ban.bText1} <u>{props.ban.bText2}</u> <br />
                {props.ban.bText3} <u>{props.ban.bText4}</u>
              </b>
              {props.ban.bText5} <br />
              <strong>{props.ban.bText6}</strong>
            </h1>
          </Fade>
        </Col>
      </div>
    </section>
  );
};

export default Banner;
