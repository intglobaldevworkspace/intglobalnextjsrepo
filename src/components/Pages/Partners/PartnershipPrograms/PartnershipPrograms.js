import React, { useState } from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

// import programsData from "./programsData";

const PartnershipPrograms = (props) => {
  console.log(props);
  const urlprefix = process.env.servUploadImg;
  return (
    <section className="eng-models">
      <Fade bottom>
        <h2>{props.partnershipprog.pheader}</h2>
      </Fade>
      <Row>
        <Col md="12">
          {props.partnershipprog.partnerprogram.map((programDetail) => {
            return (
              <Col
                id={programDetail.id}
                md="4"
                className="model-item"
                key={programDetail.id}
              >
                <Fade bottom>
                  <div className="hvr-blocks-out">
                    <figure>
                      <img
                        src={
                          urlprefix + programDetail.partnershipprogramimg.url
                        }
                        className="img-fluid"
                        alt={programDetail.partnershipprogramtitle}
                      />
                    </figure>
                    <h6>{programDetail.partnershipprogramtitle}</h6>
                    <div>
                      <p>{programDetail.partnershipprogramtooltip}</p>
                    </div>
                    <a></a>
                  </div>
                </Fade>
              </Col>
            );
          })}
        </Col>
      </Row>
    </section>
  );
};

export default PartnershipPrograms;
