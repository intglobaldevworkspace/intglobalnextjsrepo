/* eslint max-len: 0 */

export const programs = [
  {
    id: '1',
    iconUrl: '/static/images/icons/partner/partner-program/web-mobile-app-development.svg',
    title: 'Reliable & Robust',
    tooltipID: 'Toolti01',
    tooltipText: 'INT. designs fast, flexible and robust digital solutions to scale up your business process.'
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/partner/partner-program/ecommerce-app-development.svg',
    title: 'Consultative Approach',
    tooltipID: 'Toolti02',
    tooltipText: 'Our consultative approach helps you choose the apt partnership program based on requirements.'
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/partner/partner-program/cross-platform-app-services.svg',
    title: 'Technical Expertise',
    tooltipID: 'Toolti03',
    tooltipText: 'We bring top-notch technical expertise to deliver top-of-the-line products and solutions.'
  },
  {
    id: '4',
    iconUrl: '/static/images/icons/partner/partner-program/ecommerce-app-development.svg',
    title: 'Competitive Pricing',
    tooltipID: 'Toolti04',
    tooltipText: 'Hyper-competitive pricing models to help organizations realize their digital objectives.'
  },
];

export default programs;
