import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const PartnershipsContracts = (props) => {
  return (
    <section className="partnerships">
      <Fade bottom>
        <Row>
          <Col md="11">
            <p>
              <span>{props.pcontract.ptxtone}</span> {props.pcontract.ptxttwo}{" "}
              <span>{props.pcontract.ptxtthree}</span>
            </p>
          </Col>
        </Row>
      </Fade>
    </section>
  );
};

export default PartnershipsContracts;
