/* eslint max-len: 0 */

export const values = [
  {
    id: '1',
    iconUrl: '/static/images/icons/partner/partner-values/web-mobile-app-development.svg',
    title: 'Associate Partner',
    tooltipID: 'Toolti01',
    tooltipText: 'Designed for the IT Consultants, Entrepreneurs, and small and midsize system integrators.'
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/partner/partner-values/ecommerce-app-development.svg',
    title: 'Reseller',
    tooltipID: 'Toolti02',
    tooltipText: 'Basically a white label service that is intended for next-gen performance agencies marketing agencies, and software development firms.'
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/partner/partner-values/cross-platform-app-services.svg',
    title: 'Referral Partner',
    tooltipID: 'Toolti03',
    tooltipText: 'Ideal for partners looking for time-bound  engagement. We offer dedicated and flexi hiring models for successful partnerships.'
  },
  {
    id: '4',
    iconUrl: '/static/images/icons/partner/partner-values/web-mobile-app-development.svg',
    title: 'Country Partner',
    tooltipID: 'Toolti04',
    tooltipText: 'Associate Partners qualify as country partners based on the level of commitment, volume, and relationship.'
  },
];

export default values;
