import React, { useState } from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

// import valuesData from "./valuesData";

const PartnershipValues = (props) => {
  // console.log(props);
  const urlprefix = process.env.servUploadImg;
  return (
    <section className="eng-models">
      <Fade bottom>
        <h2>{props.pvals.partnershipheadertxtone}</h2>
        <p>{props.pvals.partnershipheadertxttwo}</p>
      </Fade>
      <Row>
        <Col md="12">
          {props.pvals.partval.map((valueDetail) => {
            return (
              <Col
                id={valueDetail.id}
                md="4"
                className="model-item"
                key={valueDetail.id}
              >
                <Fade bottom>
                  <div className="hvr-blocks-out">
                    <figure>
                      <img
                        src={
                          urlprefix +
                          valueDetail.aboutuspartnershipitems
                            .partnershipvalueimg.url
                        }
                        className="img-fluid"
                        alt={
                          valueDetail.aboutuspartnershipitems
                            .partnershipvaltitle
                        }
                      />
                    </figure>
                    <h6>
                      {valueDetail.aboutuspartnershipitems.partnershipvaltitle}
                    </h6>
                    <div>
                      <p>
                        {
                          valueDetail.aboutuspartnershipitems
                            .partnershipvaltooltiptxt
                        }
                      </p>
                    </div>
                    <a></a>
                  </div>
                </Fade>
              </Col>
            );
          })}
        </Col>
      </Row>
    </section>
  );
};

export default PartnershipValues;
