/* eslint max-len: 0 */

export const models = [
  {
    id: '1',
    iconUrl: '/static/images/icons/partner/engagement-models/web-mobile-app-development.svg',
    title: 'Equity Sharing',
    tooltipID: 'Toolti01',
    tooltipText: 'Our interactive Web Application Development integrates your business to enhance the user experience (UX) that is engaging, efficient and elegant.'
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/partner/engagement-models/ecommerce-app-development.svg',
    title: 'Revenue Sharing',
    tooltipID: 'Toolti02',
    tooltipText: 'Revenue sharing is designed for a business who has not yet reached at the height of sharing equity. Based on the current growth statistics of your business, our Investment Committee decides the feasibility of the model.'
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/partner/engagement-models/cross-platform-app-services.svg',
    title: 'Success Fee',
    tooltipID: 'Toolti04',
    tooltipText: 'We believe in shared success that sustains. Our “Success Fee” model is primarily realized as an upfront discount (only for enterprises, Annual Revenue above 100 crores~10 million) on the service charge of INT.'
  },
];

export default models;
