import React, { useState } from 'react';
import {
  Row,
  Col
} from 'reactstrap';
import Fade from 'react-reveal/Fade';

import modelsData from './modelsData';

const EngagementModels = (props) => {

  return (
    <section className="eng-models">
      <Fade bottom>
        <h2>Our Engagement Models</h2>
      </Fade>
      <Row>
        <Col md="12">
          {modelsData.map((modelDetail, index) => {
            return (
              <Col id={modelDetail.id} md="4" className="model-item" key={modelDetail.id}>
                <Fade bottom>
                  <div className="hvr-blocks-out">
                    <figure>
                      <img src={modelDetail.iconUrl} className="img-fluid" alt={modelDetail.title} />
                    </figure>
                    <h6>{modelDetail.title}</h6>
                    <div>
                      <p>{modelDetail.tooltipText}</p>
                    </div><a></a>
                  </div>
                </Fade>
              </Col>
            )
          })}
        </Col>
      </Row>
    </section>
  );
}

export default EngagementModels;