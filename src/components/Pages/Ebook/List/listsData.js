/* eslint max-len: 0 */

export const ebooklist = [
  {
    id: '1',
    thumbUrl: '/static/images/ebook/thumb/insurance-digital-claim-journey-analytics-overlay_ebook.jpg',
    sstorytitle: 'Insurance Digital Claim Journey – Analytics Overlay'
  },
  {
    id: '2',
    thumbUrl: '/static/images/ebook/thumb/twelve-pointer-action-plan-for-during-and-post-covid-19_ebook.jpg',
    sstorytitle: '12 Pointer Action Plan Before, During And After Covid-19'
  },
  {
    id: '3',
    thumbUrl: '/static/images/ebook/thumb/leaving-the-office-behind_the-new-normal-ebook.jpg',
    sstorytitle: 'Leaving The Office Behind : The New Normal'
  },
  {
    id: '4',
    thumbUrl: '/static/images/ebook/thumb/comparison-of-mobile-applications-of-top-indian-banks-ebook.jpg',
    sstorytitle: 'Comparison of Mobile Applications of Top Indian Banks'
  },
  {
    id: '5',
    thumbUrl: '/static/images/ebook/thumb/outsourcing-101-ebook.jpg',
    sstorytitle: 'Outsourcing 101'
  },
  {
    id: '6',
    thumbUrl: '/static/images/ebook/thumb/deep-learning-101-ebook.jpg',
    sstorytitle: 'Deep Learning 101'
  },
  {
    id: '7',
    thumbUrl: '/static/images/ebook/thumb/current-trends-in-CMS-platforms-2019-ebook.jpg',
    sstorytitle: 'Current Trends in CMS Platforms 2019'
  },
  {
    id: '8',
    thumbUrl: '/static/images/ebook/thumb/ten-mobile-app-development-technologies-to-watch-out-in-2019-ebook.jpg',
    sstorytitle: '10 Mobile App Development Technologies to Watch Out in 2019'
  },
  {
    id: '9',
    thumbUrl: '/static/images/ebook/thumb/outsourcing-trends-in-insurance-industry-ebook.jpg',
    sstorytitle: 'Outsourcing Trends in Insurance Industry'
  },
  {
    id: '10',
    thumbUrl: '/static/images/ebook/thumb/migrating-application-from-zend-2.X-to-3.0-ebook.jpg',
    sstorytitle: 'Migrating an Application from Zend 2.X to 3.0'
  },
  {
    id: '11',
    thumbUrl: '/static/images/ebook/thumb/top-ten-digital-trends-in-banking-ebook.jpg',
    sstorytitle: 'Top Ten Digital Trends in Banking – 2018 And Beyond'
  },
  {
    id: '12',
    thumbUrl: '/static/images/ebook/thumb/ecm-ebook.jpg',
    sstorytitle: 'A Detailed Study on Enterprise Content Management (ECM)'
  },
  {
    id: '13',
    thumbUrl: '/static/images/ebook/thumb/a-quick-guide-to-android-instant-apps-ebook.jpg',
    sstorytitle: 'A Quick Guide to Android Instant Apps'
  },
  {
    id: '14',
    thumbUrl: '/static/images/ebook/thumb/socia-media-not-just-a-digital-marketing-channel-ebook.jpg',
    sstorytitle: 'Social media – Not just a digital marketing channel'
  },
  {
    id: '15',
    thumbUrl: '/static/images/ebook/thumb/mobile-app-development-trends-in-2018-ebook.jpg',
    sstorytitle: 'Mobile App Development Trends in 2018'
  },
  {
    id: '16',
    thumbUrl: '/static/images/ebook/thumb/insurance-digital-claim-journey-analytics-overlay_ebook.jpg',
    sstorytitle: 'The Big CMS Comparison Report'
  },
  {
    id: '17',
    thumbUrl: '/static/images/ebook/thumb/twelve-pointer-action-plan-for-during-and-post-covid-19_ebook.jpg',
    sstorytitle: 'State of Digital: 2018'
  },
  {
    id: '18',
    thumbUrl: '/static/images/ebook/thumb/leaving-the-office-behind_the-new-normal-ebook.jpg',
    sstorytitle: 'Social Media Tools'
  },
  {
    id: '19',
    thumbUrl: '/static/images/ebook/thumb/comparison-of-mobile-applications-of-top-indian-banks-ebook.jpg',
    sstorytitle: 'Register As A Seller in Top 50 Marketplaces of The World'
  },
  {
    id: '20',
    thumbUrl: '/static/images/ebook/thumb/outsourcing-101-ebook.jpg',
    sstorytitle: 'Top 100 InsurTech Influencers'
  },
  {
    id: '21',
    thumbUrl: '/static/images/ebook/thumb/deep-learning-101-ebook.jpg',
    sstorytitle: 'Digital Marketing Audit'
  },
  {
    id: '22',
    thumbUrl: '/static/images/ebook/thumb/current-trends-in-CMS-platforms-2019-ebook.jpg',
    sstorytitle: 'Top 30 Influencers in Insurtech'
  },
  {
    id: '23',
    thumbUrl: '/static/images/ebook/thumb/ten-mobile-app-development-technologies-to-watch-out-in-2019-ebook.jpg',
    sstorytitle: 'Fintech and Insurtech Innovations'
  },
  {
    id: '24',
    thumbUrl: '/static/images/ebook/thumb/outsourcing-trends-in-insurance-industry-ebook.jpg',
    sstorytitle: 'Top 35 Digital Innovations in FMCG Industry'
  },
  {
    id: '25',
    thumbUrl: '/static/images/ebook/thumb/migrating-application-from-zend-2.X-to-3.0-ebook.jpg',
    sstorytitle: 'Checklist For Improving Your Website Speed and Performance'
  },
  {
    id: '26',
    thumbUrl: '/static/images/ebook/thumb/top-ten-digital-trends-in-banking-ebook.jpg',
    sstorytitle: 'How to Improve Your Website Security Checklist'
  },
  {
    id: '27',
    thumbUrl: '/static/images/ebook/thumb/ecm-ebook.jpg',
    sstorytitle: 'How Can Predictive Analysis Help You Grow Your Business?'
  },
  {
    id: '28',
    thumbUrl: '/static/images/ebook/thumb/a-quick-guide-to-android-instant-apps-ebook.jpg',
    sstorytitle: 'Agile Methodology'
  },
  {
    id: '29',
    thumbUrl: '/static/images/ebook/thumb/socia-media-not-just-a-digital-marketing-channel-ebook.jpg',
    sstorytitle: 'Magento 2.0 Vs Magento 1.9'
  },
  {
    id: '30',
    thumbUrl: '/static/images/ebook/thumb/mobile-app-development-trends-in-2018-ebook.jpg',
    sstorytitle: 'A Quick Guide To Website Maintenance'
  },
];

export default ebooklist;