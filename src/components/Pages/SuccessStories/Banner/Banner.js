import React, { Component } from "react";
import { Col, Row } from "reactstrap";
import Slider from "react-slick";
import Fade from "react-reveal/Fade";

// import bannerData from "./bannerData";

export default class Banner extends Component {
  constructor(props) {
    super(props);

    this.state = {
      slider: this.props.banslider,
      companydetails: this.props.compdetails,
    };
  }

  render() {
    // console.log(this.state);
    const urlprefix = process.env.servUploadImg;
    const { slider, companydetails } = this.state;
    const { infinite, fade, autoplay, autoplaySpeed, speed } = slider;
    const settings = {
      dots: false,
      arrows: true,
      infinite: infinite,
      fade: fade,
      speed: speed,
      autoplay: autoplay,
      autoplaySpeed: autoplaySpeed,
      draggable: true,
      cssEase: "ease-in-out",
      slidesToShow: 1,
      slidesToScroll: 1,
    };

    return (
      <section className="hdBan_inner">
        <div className="hdBan_inner__teaser">
          <div className="successstory-ban">
            <Slider {...settings}>
              {companydetails.map((bannerDetail) => {
                {
                  return bannerDetail.showinsuccessstoryslider ? (
                    <div
                      id={bannerDetail.id}
                      className="successstory-items"
                      key={bannerDetail.id}
                    >
                      <Row>
                        <Col md="6">
                          <div className="story-logo">
                            <Fade left>
                              <figure>
                                <img
                                  src={urlprefix + bannerDetail.logobody.url}
                                  className="img-fluid"
                                  alt={bannerDetail.cname}
                                />
                              </figure>
                            </Fade>
                          </div>
                          <div className="story-content">
                            <Fade left>
                              <p>
                                {bannerDetail.successstorycompanydescription}
                              </p>
                            </Fade>
                          </div>
                          {bannerDetail.successstoryreadmorecomp
                            .readmorestatus ? (
                            <div className="story-action">
                              <Fade left>
                                <a
                                  href={
                                    bannerDetail.successstoryreadmorecomp
                                      .readmoreurl
                                  }
                                  className="read-more"
                                >
                                  {
                                    bannerDetail.successstoryreadmorecomp
                                      .readmoretext
                                  }
                                </a>
                              </Fade>
                            </div>
                          ) : null}
                        </Col>
                        <Col md="6">
                          <Fade right>
                            <figure className="story-banr">
                              <span>
                                <img
                                  src={urlprefix + bannerDetail.bannerimg.url}
                                  className="img-fluid"
                                  alt={bannerDetail.cname}
                                />
                              </span>
                            </figure>
                          </Fade>
                        </Col>
                      </Row>
                    </div>
                  ) : null;
                }
              })}
            </Slider>
          </div>
        </div>
      </section>
    );
  }
}
