/* eslint max-len: 0 */

export const successstorylist = [
  {
    id: '1',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-bridgestone.png',
    sstorytitle: 'Bridgestone'
  },
  {
    id: '2',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-fox-sports.png',
    sstorytitle: 'Fox Sports'
  },
  {
    id: '3',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-ciplamed.png',
    sstorytitle: 'CiplaMed'
  },
  {
    id: '4',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-tesco-bank.png',
    sstorytitle: 'Tesco Bank'
  },
  {
    id: '5',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-idbi-federal.png',
    sstorytitle: 'IDBI Federal'
  },
  {
    id: '6',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-sbi-general.png',
    sstorytitle: 'SBI General'
  },
  {
    id: '7',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-bridgestone.png',
    sstorytitle: 'Bridgestone'
  },
  {
    id: '8',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-fox-sports.png',
    sstorytitle: 'Fox Sports'
  },
  {
    id: '9',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-ciplamed.png',
    sstorytitle: 'CiplaMed'
  },
  {
    id: '10',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-tesco-bank.png',
    sstorytitle: 'Tesco Bank'
  },
  {
    id: '11',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-idbi-federal.png',
    sstorytitle: 'IDBI Federal'
  },
  {
    id: '12',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-sbi-general.png',
    sstorytitle: 'SBI General'
  },
  {
    id: '13',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-bridgestone.png',
    sstorytitle: 'Bridgestone'
  },
  {
    id: '14',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-fox-sports.png',
    sstorytitle: 'Fox Sports'
  },
  {
    id: '15',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-ciplamed.png',
    sstorytitle: 'CiplaMed'
  },
  {
    id: '16',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-tesco-bank.png',
    sstorytitle: 'Tesco Bank'
  },
  {
    id: '17',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-idbi-federal.png',
    sstorytitle: 'IDBI Federal'
  },
  {
    id: '18',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-sbi-general.png',
    sstorytitle: 'SBI General'
  },
  {
    id: '19',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-bridgestone.png',
    sstorytitle: 'Bridgestone'
  },
  {
    id: '20',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-fox-sports.png',
    sstorytitle: 'Fox Sports'
  },
  {
    id: '21',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-ciplamed.png',
    sstorytitle: 'CiplaMed'
  },
  {
    id: '22',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-tesco-bank.png',
    sstorytitle: 'Tesco Bank'
  },
  {
    id: '23',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-idbi-federal.png',
    sstorytitle: 'IDBI Federal'
  },
  {
    id: '24',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-sbi-general.png',
    sstorytitle: 'SBI General'
  },
  {
    id: '25',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-bridgestone.png',
    sstorytitle: 'Bridgestone'
  },
  {
    id: '26',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-fox-sports.png',
    sstorytitle: 'Fox Sports'
  },
  {
    id: '27',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-ciplamed.png',
    sstorytitle: 'CiplaMed'
  },
  {
    id: '28',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-tesco-bank.png',
    sstorytitle: 'Tesco Bank'
  },
  {
    id: '29',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-idbi-federal.png',
    sstorytitle: 'IDBI Federal'
  },
  {
    id: '30',
    thumbUrl: '/static/images/success-stories/case-study/logo/thumb/logo-sbi-general.png',
    sstorytitle: 'SBI General'
  },
];

export default successstorylist;