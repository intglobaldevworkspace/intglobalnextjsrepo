import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import Link from "next/link";
import Fade from "react-reveal/Fade";
import axios from "axios";

import CategoryList from "../CategoryList/CategoryList";
// import listsData from "./listsData";

export default class List extends Component {
  constructor(props) {
    super(props);
    // console.log(this.props);
    let thumbnaildet = this.props.clientfull.map((clientfullObj) => {
      return {
        customercolldet: {
          id: clientfullObj.customercolldet.id,
          customertitle: clientfullObj.customercolldet.customertitle,
          customerlogo: {
            url: clientfullObj.customercolldet.customerlogo.url,
          },
          successstorysingleurl: clientfullObj.successstorydetailpageurl,
        },
      };
    });
    // console.log(thumbnaildet);
    this.state = {
      items: thumbnaildet,
      visible: 9,
      error: false,
      selval: "NA",
      catlist: this.props.customercat,
      headertxt: this.props.ssheader,
    };

    this.loadMore = this.loadMore.bind(this);
  }

  loadMore() {
    this.setState((prev) => {
      return { visible: prev.visible + 3 };
    });
  }

  // componentDidMount() {

  // }

  fetchSpecificCompanies = (e) => {
    const urlprefix = process.env.servUploadImg;
    // console.log("target value -", e.target.value);
    if (e.target.value !== 0 || e.target.value !== "NA") {
      const headers = {
        "Content-Type": "application/json",
      };
      axios
        .get(urlprefix + "/successstorycats?id=" + e.target.value)
        .then((response) => {
          // console.log("response -", response.data);
          // console.log(response.data[0].customercolls);
          let filteredApiresp = response.data[0].customercolls.map(
            (clientfullObj) => {
              return {
                customercolldet: {
                  id: clientfullObj.customercolldet.id,
                  customertitle: clientfullObj.customercolldet.customertitle,
                  customerlogo: {
                    url: clientfullObj.customercolldet.customerlogo.url,
                  },
                  successstorysingleurl:
                    clientfullObj.successstorydetailpageurl,
                },
              };
            }
          );

          this.setState({
            items: filteredApiresp,
            selval: response.data[0].id,
          });
        })
        .catch((error) => {
          console.log(error);
        });

      //  .post(
      //           urlprefix + "/successstorycats/companiesbydropdown",
      //           { cid: e.target.value },
      //           headers
      //         )
    }
  };

  fetchallrecords = (e) => {
    const urlprefix = process.env.servUploadImg;
    // console.log(e.target.value);
    e.preventDefault();
    axios
      .get(urlprefix + "/customercolls")
      .then((response) => {
        // console.log("RESP-", response.data);

        let fullsuccessstory = response.data
          .filter((respObj) => respObj.customercolldet.customerstatus)
          .map((clientfullObj) => {
            return {
              customercolldet: {
                id: clientfullObj.customercolldet.id,
                customertitle: clientfullObj.customercolldet.customertitle,
                customerlogo: {
                  url: clientfullObj.customercolldet.customerlogo.url,
                },
                successstorysingleurl: clientfullObj.successstorydetailpageurl,
              },
            };
          });

        // console.log("FILTER-", fullsuccessstory);
        this.setState({ items: fullsuccessstory, selval: "NA" });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  render() {
    // console.log(this.state);
    const urlprefix = process.env.servUploadImg;
    const { catlist, headertxt, selval } = this.state;
    return (
      <section className="successstory-list">
        <Fade top>
          <h2>{headertxt}</h2>
        </Fade>
        <Row>
          <Col md="12">
            <CategoryList
              categorydropdown={catlist}
              catSpecificValues={this.fetchSpecificCompanies}
              selval={selval}
              getallrecordsforcat={this.fetchallrecords}
            />
          </Col>
          <div className="successstory-wrapper" aria-live="polite">
            {this.state.items && this.state.items.length > 0 ? (
              this.state.items.slice(0, this.state.visible).map((item) => {
                return (
                  <Col
                    md="4"
                    className="successstory fade-in"
                    key={item.customercolldet.id}
                  >
                    <Fade top>
                      <div className="successstory_box">
                        <figure>
                          <Link
                            href="/success-stories/[successdetail]"
                            as={`/success-stories/${item.customercolldet.successstorysingleurl}`}
                          >
                            <a title={item.customercolldet.customertitle}>
                              <img
                                src={
                                  urlprefix +
                                  item.customercolldet.customerlogo.url
                                }
                                className="img-fluid"
                                alt={item.customercolldet.customertitle}
                              />
                            </a>
                          </Link>
                        </figure>
                      </div>
                    </Fade>
                  </Col>
                );
              })
            ) : (
<<<<<<< HEAD
              <Col md="4" className="successstory fade-in">
                <Fade top>
                  <div className="text-center">No Records Found</div>
                </Fade>
=======
              <Col md="12" className="successstory fade-in">
                <div className="norecords-found">No Records Found</div>
>>>>>>> c4b0ebe5ca441408c29521b507bb9b68f684b54c
              </Col>
            )}
            <Fade top>
              {this.state.visible < this.state.items.length && (
                <button
                  onClick={this.loadMore}
                  type="button"
                  className="load-more"
                >
                  Load more
                </button>
              )}
            </Fade>
          </div>
        </Row>
      </section>
    );
  }
}
