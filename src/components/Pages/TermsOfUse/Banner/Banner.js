import React from "react";
import { Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const Banner = (props) => {
  // console.log(props);
  return (
    <section className="hdBan_inner">
      <div className="hdBan_inner__teaser">
        <Col md="7">
          <h1>
            <Fade left>
              <b>
                {props.bdata.btextone} <u>{props.bdata.btexttwo}</u>
              </b>
            </Fade>
            <Fade left>{props.bdata.btextthree}</Fade>
            <Fade left>
              <strong>{props.bdata.btextfour}</strong>
            </Fade>
          </h1>
        </Col>
      </div>
    </section>
  );
};

export default Banner;
