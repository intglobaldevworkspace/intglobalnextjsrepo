import React from "react";
import { Col, Row } from "reactstrap";

const TermsContent = (props) => {
  // console.log(props);
  return (
    <section className="term-content">
      <Col md="12">
        <Row>
          <h2>{props.tdata.termsheaderone}</h2>
          <div className="terms-list">
            <ul className="mrgntplt">
              {props.tdata.termsArrayService.map((servObj) => {
                return (
                  <li key={servObj.id}>
                    <p>{servObj.termsofuseservicetxt}</p>
                  </li>
                );
              })}
            </ul>
          </div>
          <h2>{props.tdata.termsheadertwo}</h2>
          <div className="terms-list">
            <ul className="mrgntplt">
              {props.tdata.termsArrayFeepayment.map((feepayObj) => {
                return (
                  <li key={feepayObj.id}>
                    <p>{feepayObj.feepaytext}</p>
                  </li>
                );
              })}
            </ul>
          </div>
          <h2>{props.tdata.termsheaderthree}</h2>
          <div className="terms-list">
            <ul className="mrgntplt">
              {props.tdata.termsArrayTermination.map((terminationObj) => {
                return (
                  <li key={terminationObj.id}>
                    <p>{terminationObj.termsterminationtext}</p>
                  </li>
                );
              })}
            </ul>
          </div>
        </Row>
      </Col>
    </section>
  );
};

export default TermsContent;
