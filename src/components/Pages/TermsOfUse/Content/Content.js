import React from "react";
import { Col, Row } from "reactstrap";
import Fade from "react-reveal/Fade";

const Content = (props) => {
  // console.log(props);
  return (
    <section className="terms-content">
      <Col md="12">
        <Row>
          <Fade bottom>
            {props.contentdata.map((contentObj) => {
              return <p key={contentObj.id}>{contentObj.contentdatatext}</p>;
            })}
          </Fade>
        </Row>
      </Col>
    </section>
  );
};

export default Content;
