import React from 'react';
import {
  Col,
  Row
} from 'reactstrap';

const PolicyContent = (props) => {
  return (
    <section className="policy-content">
      <Col md="12">
        <Row>
          <h2>Definitions</h2>
          <div className="policy-list">
            <ul className="mrgntplt">
              <li>
                <p>GDPR Key Definitions:</p>
                <ul>
                  <li>
                    <p>
                      REGULATION (EU) 2016/679 OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL of 27 April 2016 on the protection of NATURAL PERSONS with regard to the processing of PERSONAL DATA and on the free movement of such data, and repealing Directive 95/46/EC (General Data Protection Regulation).
                    </p>
                    <ul>
                      <li>
                        <p>
                          <span>Consent:</span>
                          CONSENT means a clear affirmative act establishing a freely given, specific, informed and unambiguous indication of the website user’s agreement to the processing of personal data relating to him or her, such as by a <strong>written statement,</strong> including by <strong>electronic means,</strong> or an <strong>oral statement.</strong>
                        </p>
                      </li>
                      <li>
                        <p>
                          <span>Data Controller:</span>
                          A DATA CONTROLLER is a natural or legal person, public authority, agency or other body which, alone or jointly with others, determines the purposes and means of the processing of personal data for which and the manner in which any personal data are, or are to be, processed.
                        </p>
                      </li>
                      <li>
                        <p>
                          <span>Data Processor:</span>
                          A DATA PROCESSOR is a natural or legal person, public authority, agency or other body which processes personal data on behalf of the controller.
                        </p>
                      </li>
                      <li>
                        <p>
                          <span>General Data Protection Regulation (GDPR):</span>
                          A DATA PROCESSOR is a natural or legal person, public authority, agency or other body which processes personal data on behalf of the controller.
                        </p>
                        <ul>
                          <li>
                            <p>The processing of personal data should be designed to serve mankind.</p>
                          </li>
                          <li>
                            <p>Natural persons should have control of their own personal data.</p>
                          </li>
                          <li>
                            <p>The right to the protection of personal data is not an absolute right; it must be considered in relation to its function in society and be balanced against other fundamental rights, in accordance with the principle of proportionality.</p>
                          </li>
                        </ul>
                      </li>
                      <li>
                        <p>
                          <span>Legitimate Interest:</span>
                          Processing shall be lawful only if and to the extent that at least one of the following applies:
                        </p>
                        <ul>
                          <li>
                            <p>The data subject has given consent to the processing of his or her personal data for one or more specific purposes.</p>
                          </li>
                          <li>
                            <p>Processing is necessary for the performance of a contract to which the data subject is party or in order to take steps at the request of the data subject prior to entering into a contract.</p>
                          </li>
                          <li>
                            <p>Processing is necessary for compliance with a legal obligation to which the controller is subject.</p>
                          </li>
                          <li>
                            <p>Processing is necessary in order to protect the vital interests of the data subject or of another natural person.</p>
                          </li>
                          <li>
                            <p>Processing is necessary for the performance of a task carried out in the public interest or in the exercise of official authority vested in the controller.</p>
                          </li>
                          <li>
                            <p>Processing is necessary for the purposes of the legitimate interests pursued by the controller or by a third party, except where such interests are overridden by the interests or fundamental rights and freedoms of the data subject which require protection of personal data, in particular where the data subject is a child.</p>
                          </li>
                        </ul>
                      </li>
                      <li>
                        <p>
                          <span>Online Identifier:</span>
                          Natural persons may be associated with online identifiers provided by their devices, applications, tools and protocols, such as internet protocol addresses, cookie identifiers or other identifiers such as radio frequency identification tags. This may leave traces which, in particular when combined with unique identifiers and other information received by the servers, may be used to create profiles of the natural persons and identify them.
                        </p>
                      </li>
                      <li>
                        <p>
                          <span>Personal Data:</span>
                          Personal data means any information relating to an identified or identifiable natural person (‘data subject’); an identifiable natural person is one who can be identified, directly or indirectly, in particular by reference to an identifier such as a name, an identification number, location data, an online identifier or to one or more factors specific to the physical, physiological, genetic, mental, economic, cultural or social identity of that natural person.
                        </p>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>
              <li>
                <p>Other Definitions:</p>
                <ul>
                  <li>
                    <p><span>INT Group</span>
                    INT Group means M/s. Indus Net Technologies Pvt. Ltd. Having its registered office at Module No-532, SDF Building, Salt Lake, Kolkata-70091 and all its existing subsidiary / associate companies as of date being 11th June 2018 i.e.,
                    </p>
                    <ul>
                      <li>
                        <p>Indus Net Technologies Pte. Ltd. (Singapore)</p>
                      </li>
                      <li>
                        <p>Indus Net Technologies Pvt. Ltd. (UK)</p>
                      </li>
                      <li>
                        <p>Indus Net Technologies Inc. (United States of America)</p>
                      </li>
                      <li>
                        <p>Indus Net Techshu Digital Pvt. Ltd. (India)</p>
                      </li>
                      <li>
                        <p>Algo Energy Tech Ventures Pvt. Ltd. (India)</p>
                      </li>
                    </ul>
                    <p>And shall include all their successors, acquirers, administrators and executors etc. as may be applicable from time to time and duly updated on this website.</p>
                  </li>
                  <li>
                    <p className="mrgn-btm-lw"><span>Person</span></p>
                    <ul>
                      <li>
                        <p><span>Artificial/ Juristic/ Legal Person:</span>
                          An entity such as a corporation, created by law and given certain legal rights and duties of a human being; a being real or imaginary, who for the purpose of legal reasoning is treated more or less as a human being, but is not a citizen.
                        </p>
                      </li>
                      <li>
                        <p><span>Natural Person:</span>
                          A human being.
                        </p>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <p><span>Service Data</span>
                      Service Data is any Personal Data shared by a third party with INT Group of companies or with any of its officers or employees, to avail it’s services or during the course of availing INT services; pertaining to citizens/ residents of any EU based country, that is collected by any third party being the Data Controller. In other words, Service Data is data obtained by INT Group, as a Data Processor, from any third party and does not have any control over what, why, and how it is being collected or used.
                    </p>
                  </li>
                  <li>
                    <p><span>Websites</span>
                      Shall mean all websites as it may exist from time to time of INT Group and its officers being the following websites as of 11<sub>th</sub> June 2018:</p>
                    <ul>
                      <li>
                        <p><a href="https://www.indusnet.co.in/" target="_blank">https://www.indusnet.co.in/</a></p>
                      </li>
                      <li>
                        <p><a href="https://flexihiring.com/" target="_blank">https://www.flexihiring.com/</a></p>
                      </li>
                      <li>
                        <p><a href="https://www.swanvi.ca/" target="_blank">https://www.swanvi.ca/</a></p>
                      </li>
                      <li>
                        <p><a href="https://www.digitalmarketingdemystified.com" target="_blank">https://www.digitalmarketingdemystified.com/</a></p>
                      </li>
                      <li>
                        <p><a href="https://www.oomphbox.com/" target="_blank">https://www.oomphbox.com/</a></p>
                      </li>
                      <li>
                        <p><a href="https://www.breezeerp.in/" target="_blank">https://www.breezeerp.in/</a></p>
                      </li>
                      <li>
                        <p><a href="https://www.techshu.com/" target="_blank">https://www.techshu.com/</a></p>
                      </li>
                      <li>
                        <p><a href="https://www.entechventures.com/" target="_blank">https://www.entechventures.com</a></p>
                      </li>
                      <li>
                        <p><a href="http://www.boostenergyefficiency.com/" target="_blank">http://www.boostenergyefficiency.com/</a></p>
                      </li>
                      <li>
                        <p><a href="https://www.abhishekrungta.com/" target="_blank">https://www.abhishekrungta.com/</a></p>
                      </li>
                    </ul>
                    <p>Website mentioned in this policy hereinafter shall mean all or any combination of these websites</p>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
          <h2>Data Protection / Privacy Policy Scope</h2>
          <div className="policy-list">
            <ul className="mrgntplt list-content">
              <li>
                <p>This Policy is applicable to whosoever visits any of INT Group websites and/ or obtains any INT Group services.</p>
              </li>
              <li>
                <p>GDPR specific privacy measures and features will be available to all INT Group customers. However, GDPR specific rights like access to collected personal data, deletion/modification of stored data etc. shall be legally binding only for PERSONAL DATA collected of NATURAL PERSONS (not a company, partnership or any other kind of Legal Entity) of EU Subjects.</p>
              </li>
              <li>
                <p>This policy shall not create any legal right for data collected, by INT Group or any of its officers or agents, of any LEGAL / ARTIFICIAL PERSON who is not a human being or an EU Subject. (Example of such data: name and the form of the legal person and the contact details of the legal person).</p>
              </li>
              <li>
                <p>Nothing contained in this policy shall create any right for citizens or residents outside of EU countries regarding data control features like right to obtain collected Personal Data, Right to be Forgotten, Right to update or modify their Personal Data, Right to give consent for collection ore share of Personal /Service Data and similar rights which are not explicit rights provided under the Information Technology Act, 2000 of India (IT Act).</p>
              </li>
              <li>
                <p>All Data control and protection rights for subjects of all other countries except for EU countries shall be governed by IT Act and will be subject to reasonable security measures compliant to industry standard ethical practices.</p>
              </li>
              <li>
                <p>This Policy shall be legally binding on you and on INT Group, on different activities one our websites including but not limited to browsing through our websites, on obtaining different services whether paid or free, raising queries, consultations for whatsoever purposes as a click-wrap contract between you and INT Group.</p>
              </li>
            </ul>
          </div>
          <h2>Rights Of a Visitor / Licensee</h2>
          <div className="policy-list">
            <ul className="mrgntplt list-content">
              <li>
                <p>
                  <span>Right to Access, Rectify and/or Delete Personal Data</span>
                  If you are a Customer / Partner/ an interested party, providing your personal data to INT Group through its Website(s), applications or social media connects, blogs, corporate events, etc.; such data being collected for specific purposes mentioned hereunder. Being an EU Subject, you have the right to have access to your personal data provided to INT, and further you have the right to contact <a href="mailto:legal@indusnet.co.in">legal@indusnet.co.in</a> for rectification or deletion of your personal data, subject to other clauses of this policy, without affecting continuity of INT Group’s business.
                </p>
                <p>Thus, deleting an User does not delete business-specific organization-owned data created and contributed to; by the User including without limitation, knowledgebase articles, notes, forum topics/comments, support calls, surveys, canned responses, ticket templates, contacts, companies, tags, conversations in the tickets, comments or details provided on any third-party platform such as social media etc.</p>
                <p>Notwithstanding the foregoing, we will retain Service Data that may include your personal data as necessary to comply with our legal obligations, for litigation/defence purposes, maintain accurate financial and other records, resolve disputes, and enforce our agreements.</p>
              </li>
              <li>
                <p>
                  <span>Sharing Personal Data</span>
                  You have full control over your personal data provided to INT Group through various means. We share your personal data with your consent or as necessary to complete any transaction or provide any product/ services you have requested or authorised. We also share data with a third party; when required by law or to respond to legal process; to protect our customers; to protect lives; to maintain the security of our products; and to protect the rights or property of INT Group and its brand or Intellectual Properties.
                </p>
                <p>
                  In the event an entity belonging to INT Group of Companies goes through a business transition, such as a merger or acquisition by another company; or sale of all or a portion of its assets, Customer’s Account, all collected data and Service Data that may include your personal data; will likely be among the assets transferred. A prominent notice will be displayed on our Websites to intimate you of any such change in ownership or control; and Customers will be notified via an e-mail from <a href="mailto:legal@indusnet.co.in">legal@indusnet.co.in</a>. Nothing contained in this policy shall prevent INT Group to enter into any merger, acquisition, sale or any kind of restructuring of its entity(s).
                </p>
                <p>
                  We may share your data with independent third party(s) for consultations, support and technical services, implementation of solutions etc. depending on your services request, only after obtaining your consent regarding the same.
                </p>
              </li>
            </ul>
          </div>
          <h2>What Kind Of Personal Data Do We Collect?</h2>
          <div className="policy-list">
            <ul className="mrgntplt list-content">
              <li>
                <p>
                  When you visit our Websites, Browser Applications etc.; or participate in our events, blogs, contribution pages, social networking pages, surveys; INT Group of Companies may collect information, which may include your Personal Data. For the purposes of General Data Protection Regulation (GDPR), INT Group is the controller for personal data collected through INT owned systems and not for data collected on a platform owned by any third party, social medias etc.
                </p>
                <p>Different means through which we may collect your personal data for the following legitimate business purposes:</p>
              </li>
              <li>
                <p>
                  <span>Enquiry</span>
                  When you enquire about any Product or Service(s) or Partnership we may collect your (i) contact information such as name, e-mail address, mailing address, IP address, geographic location, or phone number, software environment etc. or any combination of such information.
                </p>
                <p>
                  Subject to this Notice and the Terms, we will use such data, including without limitation, (i) send you communication for the Service(s); (ii) assess needs of your business to determine or suggest suitable Service(s); (iii) send you requested information about the Service(s); (iv) respond to customer service requests, questions and concerns.
                </p>
              </li>
              <li>
                <p>
                  <span>Sign-up, Billing and Account information</span>
                  When you subscribe and sign-up to any of our Products/ Service(s)/ Trials, we may collect your (i) contact information such as name, e-mail address, mailing address, IP address, geographic location, or phone number, of the Account admin; (ii) billing information like billing address, etc; (iii) name and e-mail address when Account admin/Agent(s) provide feedback from within the Service(s); and (iv) unique identifiers, such as username, account number or password.
                </p>
                <p>
                  Subject to this Notice and the Terms, we will use such data, including without limitation, to (i) grant software license and other Service(s); (ii) send you communication from the Service(s); (iii) assess needs of your business to determine or suggest suitable Service(s); (iv) send you requested information about the Service(s); (v) respond to customer service requests, questions and concerns; (vi) administer your Account; (vii) send you promotional and marketing communications (where you have requested us to do so).
                </p>
              </li>
              <li>
                <p>
                  <span>Support Interactions</span>
                  When a customer/ Prospect interacts with an INT Group company support professional, we collect Device/ System and Usage data or error reports to diagnose and resolve problems.
                </p>
                <p>When a customer receives communications from INT Group, we may use your data to personalise the content of the communication.</p>
                <p>
                  When a customer engages with INT Group for professional services, we collect the name and contact data of the customer’s designated point of contact and use information provided by the customer to perform the services that the customer has requested and preserve all telephonic and written conversations for future reference, and such data, calls, chats or e-mails may be used for training and development purposes for INT professionals.
                </p>
              </li>
              <li>
                <p>
                  <span>Events</span>
                  When you attend an event conducted by us or any other third party where we exhibit or participate, including webinars or seminars, we may collect your contact information such as name, e-mail address, designation and company name.
                </p>
                <p>
                  Subject to this Notice, we will use such data, including without limitation, to (i) assess needs of your business to determine or suggest suitable Service(s); (ii) send you requested information about the Service(s); (iii) send you promotional and marketing communications (where you have requested us to do so); and (iv) respond to your questions and concerns.
                </p>
              </li>
              <li>
                <p>
                  <span>Analytics</span>
                  Apart from the aforementioned information collected by us, we automatically receive and record certain Personal Data of yours when you visit our Websites. This includes device model, IP address, the type of browser being used, usage pattern through cookies and browser settings. We also collect clicks, scrolls, conversion and drop-off on our Websites and Service(s) to render user journey at real-time. Subject to this Notice, we will use such data, including without limitation, to (i) assess needs of your business to determine or suggest suitable Service(s); (ii) send you requested information about the Service(s); (iii) respond to customer service requests, questions and concerns; and (iv) for analytical purposes.
                </p>
              </li>
              <li>
                <p>
                  <span>Testimonials</span>
                  We may post your testimonials/comments/reviews on our Websites which may contain your Personal Data. Prior to posting the testimonial, we will obtain your consent to post your name along with the testimonial. If you want your testimonial removed, please contact us at <a href="mailto:legal@indusnet.co.in">legal@indusnet.co.in</a>.
                </p>
              </li>
              <li>
                <p>
                  <span>Marketing Communications</span>
                  We may use your e-mail address, collected which may fall in to the category of Personal Data, to send our newsletters and/or marketing communications about our products and services. Where you have so requested, we will also send you marketing communications about our third-party partners. If you no longer wish to receive these communications, you can opt out by following the instructions contained in the e-mails you receive or by contacting us at <a href="mailto:legal@indusnet.co.in">legal@indusnet.co.in</a>.
                </p>
              </li>
              <li>
                <p>
                  <span>Job Portal/ Partnership Forms</span>
                  We may use and store your personal data, required for selection of your candidature for any job opening at our organization and may use the same to contact you for such purposes only as it will be specified at the time of filling up any candidature form.
                </p>
              </li>
            </ul>
          </div>
          <h2>Use Of Cookies</h2>
          <div className="policy-list">
            <ul className="mrgntplt list-content">
              <li>
                <p>
                  When you use the INT’s Group Service(s)/ website etc. we may store some information on your computer. This information will be in the form of a “cookie” or similar file. Cookies are small pieces of information stored on your hard drive, not on the INT Group website. We do not use cookies to spy on you or otherwise invade your privacy. They cannot invade your hard drive and steal information. We use cookies to help you navigate the INT Group website(s) and Service(s) as easily as possible, and to remember information about your current session. These are the Session Cookies and they are removed from your system when you close the web browser or turn off the computer. You must enable cookies on your web browser to browse INT Websites.
                </p>
                <p>
                  Various categories of cookies are used in the INT Group website(s) and are listed in the Cookie Policy with the options to manage them.
                </p>
              </li>
            </ul>
          </div>
          <h2>Children's Personal Data</h2>
          <div className="policy-list">
            <ul className="mrgntplt list-content">
              <li>
                <p>
                  INT Group does not knowingly collect any Personal Data from children under the age of 16. If you are under the age of 16, please do not submit any Personal Data through our Websites. If you have reason to believe that a child under the age of 16 has provided Personal Data to us through our Websites, please contact us and we will delete such personal information and terminate the child's account from our databases.
                </p>
              </li>
            </ul>
          </div>
          <h2>Security And Storage</h2>
          <div className="policy-list">
            <ul className="mrgntplt list-content">
              <li>
                <p>
                  The INT Group websites have industry standard security measures in place to protect the loss, misuse, and alteration of the information under our control. While there is no such thing as “perfect security” on the Internet, we will take all reasonable steps to ensure the safety of your information. Additionally, you retain all rights of ownership to the data you have stored with INT Group. We will not sell or share this data with any third parties or use this data to compete with you or advertise to your clients. Your privacy and the privacy of your clients are of utmost importance to us. Some general measures that we have taken for security of your personal data per ISO 27001 guidelines.
                </p>
              </li>
            </ul>
          </div>
          <h2>What Is Our Legal Basis For Processing Personal Data</h2>
          <div className="policy-list">
            <ul className="mrgntplt list-content">
              <li>
                <p>
                  If you are a visitor of INT website(s), our legal basis for collecting and using the Personal Data described above will depend on the Personal Data concerned and the specific context in which we collect it.
                </p>
                <p>
                  However, we will normally collect Personal Data from you only where we need the Personal Data to perform a contract with you, or where the processing is in our legitimate interests or rely upon your consent where we are legally required to do so and not overridden by your data protection interests or fundamental rights and freedoms. In some cases, we may also have a legal obligation to collect Personal Data from you or may otherwise need the Personal Data to protect your vital interests or those of another person.
                </p>
                <p>
                  If we ask you to provide Personal Data to comply with a legal requirement or to perform a contract with you, we will make this clear at the relevant time and advise you whether the provision of your Personal Data is mandatory or not (as well as of the possible consequences if you do not provide your Personal Data).
                </p>
                <p>
                  Similarly, if we collect and use your Personal Data in reliance on our legitimate interests (or those of any third party), we will make clear to you at the relevant time what those legitimate interests are.
                </p>
                <p>
                  If you have questions about or need further information concerning the legal basis on which we collect and use your personal data, please e-mail us at <a href="mailto:legal@indusnet.co.in">legal@indusnet.co.in</a>.
                </p>
              </li>
            </ul>
          </div>
          <h2>Links To Third Party Sites</h2>
          <div className="policy-list">
            <ul className="mrgntplt list-content">
              <li>
                <p>
                  Our Websites contain links to other websites that are not limited to but may be of our end customers, Partners, advertisements etc. that are not owned or controlled by INT Group Companies. Please be aware that we are not responsible for the privacy practices of such other websites or third parties. We encourage you to be aware when you leave our Websites and to read the privacy policies of that third-party website to understand their data collection, retention and process policy.
                </p>
              </li>
            </ul>
          </div>
          <h2>Amendments</h2>
          <div className="policy-list">
            <ul className="mrgntplt list-content">
              <li>
                <p>
                  Amendments to this Notice will be directly updated on this page and will be effective as may be notified. If we make any material changes, we will notify you by means of a notice on this Website prior to the change becoming effective and if you are our Licensee/ Partner, via e-mail (specified in your Account). Provided we will not be notifying you if we amend the Notice to make addition, deletions or modifications to the list of cookies from time to time to keep the list of cookies current and accurate. You should frequently visit this Notice to check for amendments. Your continued use of our Websites or the Service(s) following the posting of any amendment, modification, or change to this Notice shall constitute your acceptance of the amendments to this Notice. You can choose to discontinue use of the Websites or Service(s), if you do not accept the terms of this Notice, or any modified version of this Notice.
                </p>
              </li>
            </ul>
          </div>
          <h2>Data Retention Policy</h2>
          <div className="policy-list">
            <ul className="mrgntplt list-content">
              <li>
                <p>
                  We will retain your information for as long as your account is active or as needed to provide you services. We will retain and use your information as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements.
                </p>
              </li>
            </ul>
          </div>
          <h2>Rights of European Union Country Specific Citizens / Residents (EU Subjects):</h2>
          <div className="policy-list">
            <ul className="mrgntplt list-content">
              <li>
                <p>
                  If you are a citizen or resident of European Union (EU) based countries; then you have the right to ask INT Group for your personal data collected by it, by writing us as <a href="mailto:legal@indusnet.co.in">legal@indusnet.co.in</a>.
                </p>
              </li>
              <li>
                <p>
                  You have the right to raise tickets for objection or grievance redressal, if there is any collection of data by INT Group of Companies’ officers, agents, or employees, which is believed to be in nature of Personal data and not serving any justifiable purpose for which you have been requested to provide the same.
                </p>
              </li>
              <li>
                <p>
                  You have the right to delete or modify any part or all of your personal data collected by INT Group of Companies subject to other provisions of this Policy.
                </p>
              </li>
              <li>
                <p>
                  You have the right to be forgotten, with all your personal data footprints kept with INT Group of Companies in what so ever manner, if so opted by you. However, no data required for enforcement of our legal rights, obligations, legal proceedings, protection of our IP rights or protection against any claims shall be deleted and such purposes of data retention shall be clarified by INT Group, if requested for.
                </p>
              </li>
              <li>
                <p>
                  Third Party Data Processor of INT Group Companies: INT Group Companies may shares your data with third party organizations listed below as of date and may be updated from time to time. We may share your data with only those third party entities, which have declared to be GDPR compliant and maintains a similar data protection policy:
                </p>
                <ul>
                  <li>
                    <p>
                      Google (<a href="https://policies.google.com/privacy" target="_blank">Privacy Policy</a>)
                    </p>
                  </li>
                  <li>
                    <p>
                      Mautic (<a href="https://www.mautic.org/privacy-policy/" target="_blank">Privacy Policy</a>)
                    </p>
                  </li>
                  <li>
                    <p>
                      Mailer Lite (<a href="https://www.mailerlite.com/privacy-policy" target="_blank">Privacy Policy</a>)
                    </p>
                  </li>
                  <li>
                    <p>
                      Intercom (<a href="https://www.intercom.com/terms-and-policies" target="_blank">Terms and Policies</a>)
                    </p>
                  </li>
                  <li>
                    <p>
                      Facebook (<a href="https://www.facebook.com/policy.php?CAT_VISITOR_SESSION=c7b73ebc78d1681ade25473632eae199" target="_blank">Data Policy</a>)
                    </p>
                  </li>
                  <li>
                    <p>
                      Quora (<a href="https://www.quora.com/about/privacy" target="_blank">Privacy Policy</a>)
                    </p>
                  </li>
                  <li>
                    <p>
                      LinkedIn (<a href="https://www.linkedin.com/legal/privacy-policy?trk=" target="_blank">Privacy Policy</a>)
                    </p>
                  </li>
                </ul>
                <p>These rights are exclusively available for EU country specific subjects either being a Citizen/ Resident of such countries, and not for citizens/ residents of any other country not protected under GDPR guidelines.</p>
              </li>
            </ul>
          </div>
          <h2>Data Breaches</h2>
          <div className="policy-list">
            <ul className="mrgntplt list-content">
              <li>
                <p>
                  We will report any unlawful data breach of this website’s database or the database(s) of any of our third party data processors to any and all relevant persons and authorities within 72 hours of the breach if it is apparent that personal data stored in an identifiable manner has been stolen.
                </p>
              </li>
            </ul>
          </div>
          <h2>Resolving Grievances Pertaining To Personal Data Protection Of Natural Persons</h2>
          <div className="policy-list">
            <ul className="mrgntplt list-content">
              <li>
                <p>
                  If you face any difficulty in availing any of the data control features, want to update or delete your data, and any other related grievances, please write us at <a href="mailto:legal@indusnet.co.in">legal@indusnet.co.in</a>.
                </p>
              </li>
            </ul>
          </div>
        </Row>
      </Col>
    </section>
  );
}

export default PolicyContent;