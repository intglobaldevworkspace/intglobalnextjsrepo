import React from "react";
import { Col, Row } from "reactstrap";
import Fade from "react-reveal/Fade";

const Content = (props) => {
  // console.log(props);
  return (
    <section className="privacy-content">
      <Col md="12">
        <Row>
          <Fade bottom>
            {props.cntdata.map((cntObj) => {
              return <p>{cntObj.contenttext}</p>;
            })}
          </Fade>
        </Row>
      </Col>
    </section>
  );
};

export default Content;
