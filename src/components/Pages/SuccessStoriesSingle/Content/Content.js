import React from "react";
import { Col, Row } from "reactstrap";
import Fade from "react-reveal/Fade";

const Content = (props) => {
  // console.log(props);
  return (
    <section className="successstory-content">
      <Col md="10">
        <Row>
          <Fade top>
            <h2>{props.strydet.customercolldet.customertitle}</h2>
            <h4>{props.strydet.customercolldet.customersubtitle}</h4>
          </Fade>
        </Row>
      </Col>
      <Col md="9">
        <Row>
          <Fade top>
            <p>{props.strydet.customercolldet.customerdescrip}</p>
          </Fade>
        </Row>
      </Col>
    </section>
  );
};

export default Content;
