import React, { Component } from "react";
import { Col, Row, FormGroup } from "reactstrap";
import Fade from "react-reveal/Fade";
import { Formik, Form, Field, ErrorMessage } from "formik";
import axios from "axios";

class CategoryList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      catlist: this.props.categorydropdown,
    };
  }
  fetchSpecificCompanies = (e) => {
    const urlprefix = process.env.servUploadImg;
    console.log(e.target.value);
    if (e.target.value !== 0 || e.target.value !== "NA") {
      const headers = {
        "Content-Type": "application/json",
      };
      axios
        .post(
          urlprefix + "/customercolls/companiesbydropdown",
          { cid: e.target.value },
          headers
        )
        .then((response) => {
          console.log(response);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };
  // console.log(props.categorydropdown);

  render() {
    const { catlist } = this.state;
    // console.log(catlist);
    return (
      <section className="category-list">
        <Col md="5">
          <Row>
            <div className="catg-row">
              <Fade top>
                <a className="button link active">All</a>
              </Fade>
              <Fade top>
                <Formik
                  initialValues={{
                    category: "",
                  }}
                  onChange={(values, actions) => {
                    console.log(values);
                    actions.resetForm();
                  }}
                >
                  <Form className="catgform">
                    <FormGroup>
                      <Field
                        as="select"
                        name="category"
                        id="category"
                        onChange={(e) => this.fetchSpecificCompanies(e)}
                      >
                        <option value="NA">Select Category</option>
                        {catlist.map((catlistObj) => {
                          {
                            return catlistObj.catstatus ? (
                              <option
                                value={
                                  catlistObj.customercolls.length > 0
                                    ? catlistObj.customercolls.join()
                                    : 0
                                }
                                key={catlistObj.id}
                              >
                                {catlistObj.successstorycatname}
                              </option>
                            ) : null;
                          }
                        })}
                      </Field>
                    </FormGroup>
                  </Form>
                </Formik>
              </Fade>
            </div>
          </Row>
        </Col>
      </section>
    );
  }
}

export default CategoryList;
