import React from "react";
import { Row, Col } from "reactstrap";
import Link from "next/link";
import Fade from "react-reveal/Fade";

import CategoryList from "../CategoryList/CategoryList";
// import listsData from "./listsData";

export default class List extends React.Component {
  constructor(props) {
    super(props);
    // console.log(this.props);
    let thumbnaildet = this.props.clientfull.map((clientfullObj) => {
      return {
        customercolldet: {
          id: clientfullObj.customercolldet.id,
          customertitle: clientfullObj.customercolldet.customertitle,
          customerlogo: {
            url: clientfullObj.customercolldet.customerlogo.url,
          },
          successstorysingleurl: clientfullObj.successstorydetailpageurl,
        },
      };
    });
    // console.log(thumbnaildet);
    this.state = {
      items: thumbnaildet,
      visible: 9,
      error: false,
      catlist: this.props.customercat,
      headertxt: this.props.ssheader,
    };

    this.loadMore = this.loadMore.bind(this);
  }

  loadMore() {
    this.setState((prev) => {
      return { visible: prev.visible + 3 };
    });
  }

  // componentDidMount() {
  //   fetch("https://jsonplaceholder.typicode.com/posts").then(
  //     res => res.json()
  //   ).then(res => {
  //     this.setState({
  //       items: res
  //     });
  //   }).catch(error => {
  //     console.error(error);
  //     this.setState({
  //       error: true
  //     });
  //   });
  // }

  render() {
    // console.log(this.state);
    const urlprefix = process.env.servUploadImg;
    const { catlist, headertxt } = this.state;
    return (
      <section className="successstory-list">
        <Fade top>
          <h2>{headertxt}</h2>
        </Fade>
        <Row>
          <Col md="12">
            <CategoryList categorydropdown={catlist} />
          </Col>
          <div className="successstory-wrapper" aria-live="polite">
            {this.state.items.slice(0, this.state.visible).map((item) => {
              return (
                <Col
                  md="4"
                  className="successstory fade-in"
                  key={item.customercolldet.id}
                >
                  <Fade top>
                    <div className="successstory_box">
                      <figure>
                        <Link
                          href="/success-stories/[successdetail]"
                          as={`/success-stories/${item.customercolldet.successstorysingleurl}`}
                        >
                          <a title={item.customercolldet.customertitle}>
                            <img
                              src={
                                urlprefix +
                                item.customercolldet.customerlogo.url
                              }
                              className="img-fluid"
                              alt={item.customercolldet.customertitle}
                            />
                          </a>
                        </Link>
                      </figure>
                    </div>
                  </Fade>
                </Col>
              );
            })}
            <Fade top>
              {this.state.visible < this.state.items.length && (
                <button
                  onClick={this.loadMore}
                  type="button"
                  className="load-more"
                >
                  Load more
                </button>
              )}
            </Fade>
          </div>
        </Row>
      </section>
    );
  }
}
