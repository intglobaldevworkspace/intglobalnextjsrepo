/* eslint max-len: 0 */

export const successbanner = [
  {
    id: '1',
    bannerUrl: '/static/images/success-stories/case-study/banner/banner-ciplamed.png',
    bannerTitle: 'CiplaMed',
    bannerLogo: '/static/images/success-stories/case-study/logo/logo-ciplamed.png',
    bannerContent: 'An App for Cipla, a leading pharmaceutical company in India, which provides real time scientific information about drugs and medicines to the doctor community.',
    bannerReadMore: 'Read More'
  },
  {
    id: '2',
    bannerUrl: '/static/images/success-stories/case-study/banner/banner-guulpay.png',
    bannerTitle: 'Guulpay',
    bannerLogo: '/static/images/success-stories/case-study/logo/logo-guulpay.png',
    bannerContent: 'The Guulpay App to deliver an new age mobile & web app that could allow its users to recharge with ease, pay bills, receive and send money across borders.',
    bannerReadMore: 'Read More'
  },
  {
    id: '3',
    bannerUrl: '/static/images/success-stories/case-study/banner/banner-cashpoint.png',
    bannerTitle: 'Cashpoint',
    bannerLogo: '/static/images/success-stories/case-study/logo/logo-cashpoint.png',
    bannerContent: 'Cashpoint is a secured platform of data exchange for Europe’s fastest growing sports betting platform.',
    bannerReadMore: 'Read More'
  },
  {
    id: '4',
    bannerUrl: '/static/images/success-stories/case-study/banner/banner-somax.png',
    bannerTitle: 'Somax',
    bannerLogo: '/static/images/success-stories/case-study/logo/logo-somax.png',
    bannerContent: 'The Somax App is a faster Time to market by building IoT based scalable solution on Cloud.',
    bannerReadMore: 'Read More'
  },
  {
    id: '5',
    bannerUrl: '/static/images/success-stories/case-study/banner/banner-indusind.png',
    bannerTitle: 'IndusInd',
    bannerLogo: '/static/images/success-stories/case-study/logo/logo-indusind.png',
    bannerContent: 'An integrated virtual digital platform that streamlines core banking with API-fication of assets & transforming forex transaction.',
    bannerReadMore: 'Read More'
  },
];

export default successbanner;