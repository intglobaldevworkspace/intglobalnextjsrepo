import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const About = (props) => {
  return (
    <section className="about">
      <Fade bottom>
        <div
          dangerouslySetInnerHTML={{
            __html: props.aboutusdetails.goodideaheader,
          }}
        ></div>
      </Fade>
      <Row>
        <ul className="idea-list">
          {props.aboutusdetails.goodidearepeatabletext.map(
            (aboutusdet, index) => {
              // let leftorright = '';
              const leftorright =
                (parseInt(index) + 1) % 2 != 0 ? "left" : "right";
              // console.log(leftorright);
              return leftorright == "left" ? (
                <Fade left key={parseInt(index)}>
                  <li
                    dangerouslySetInnerHTML={{
                      __html: aboutusdet.goodideacontent,
                    }}
                  ></li>
                </Fade>
              ) : (
                <Fade right key={parseInt(index)}>
                  <li
                    dangerouslySetInnerHTML={{
                      __html: aboutusdet.goodideacontent,
                    }}
                  ></li>
                </Fade>
              );
            }
          )}
        </ul>
      </Row>
    </section>
  );
};

export default About;
