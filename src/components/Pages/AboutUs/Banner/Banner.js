import React from "react";
import { Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const Banner = (props) => {
  return (
    <section className="hdBan_inner">
      <div className="hdBan_inner__teaser">
        <Col md="7">
          <Fade left>
            <div dangerouslySetInnerHTML={{ __html: props.hbanner }}></div>
          </Fade>
        </Col>
      </div>
    </section>
  );
};

export default Banner;
