import React, { Component } from "react";
import { Row, Col, Button, Modal, ModalBody } from "reactstrap";
import Fade from "react-reveal/Fade";

export default class MeetTeam extends Component {
  constructor(props) {
    super(props);
    this.state = Object.assign({}, { ...props.meetInt }, { modal: false });
    // console.log(this.props);
  }

  toggle = () => {
    this.setState((prevState) => ({
      modal: !prevState.modal,
    }));
  };
  render() {
    // console.log(this.state);
    const urlprefix = process.env.servUploadImg;
    const {
      meetTheIntianFooter,
      meetTheIntianHeader,
      mtIntianCareers,
      mtIntianMoreButton,
      mtVideoAndImage,
    } = this.state;
    return (
      <section className="meet-the-team">
        <Fade bottom>
          <div
            dangerouslySetInnerHTML={{
              __html: meetTheIntianHeader,
            }}
          ></div>
          <div className="team-wrapper">
            <ul className="team_teaser__list">
              {mtVideoAndImage.map((teamDetail, index) => {
                return (
                  <li
                    id={teamDetail.id}
                    key={teamDetail.id}
                    className={
                      teamDetail.mtMediaType == "Image" ? "pic" : "vid"
                    }
                  >
                    <figure>
                      <img
                        src={urlprefix + teamDetail.intfrontImage.url}
                        className="img-fluid"
                        alt={teamDetail.mediaCaption}
                      />
                    </figure>
                    <span>
                      <a
                        href={teamDetail.meMediaLinkUrl}
                        target={
                          teamDetail.mtMediaType == "Image" ? "_blank" : ""
                        }
                      >
                        {teamDetail.mediaCaption}
                      </a>
                      {teamDetail.mtMediaType == "Video" ? (
                        <>
                          <Button onClick={this.toggle}></Button>
                          <Modal
                            isOpen={this.state.modal}
                            toggle={this.toggle}
                            className={
                              teamDetail.mtMediaType == "Image" ? "pic" : "vid"
                            }
                          >
                            <ModalBody>
                              <iframe
                                width="100%"
                                height="315"
                                src={teamDetail.mtVideoLink}
                                frameBorder="0"
                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                allowFullScreen
                              ></iframe>
                            </ModalBody>
                          </Modal>
                        </>
                      ) : (
                        ""
                      )}
                    </span>
                  </li>
                );
              })}
            </ul>
            <div
              dangerouslySetInnerHTML={{
                __html: meetTheIntianFooter,
              }}
            ></div>
            {mtIntianCareers.careersBtnStatus ? (
              <a
                href={mtIntianCareers.careersBtnUrl}
                className="btn link"
                title="More"
                target="_blank"
              >
                {mtIntianCareers.careersBtnText}
              </a>
            ) : (
              ""
            )}
            {mtIntianMoreButton.moreBtnStatus ? (
              <a
                href={mtIntianMoreButton.moreBtnUrl}
                className="btn link"
                title="Careers"
                target="_blank"
              >
                {mtIntianMoreButton.moreBtnText}
              </a>
            ) : (
              ""
            )}
          </div>
        </Fade>
      </section>
    );
  }
}
