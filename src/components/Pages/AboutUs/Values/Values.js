import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const Values = (props) => {
  const urlprefix = process.env.servUploadImg;
  return (
    <section className="values">
      <Fade bottom>
        <h2
          dangerouslySetInnerHTML={{
            __html: props.ourvaluedetails.ourvalueHeader,
          }}
        ></h2>
        <Row>
          {props.ourvaluedetails.ourvalueDetails.map((valueDetail) => {
            return (
              <Col
                id={valueDetail.id}
                md="4"
                className="values-item"
                key={valueDetail.id}
              >
                <figure>
                  <img
                    src={urlprefix + valueDetail.ourvaluesimg.url}
                    className="img-fluid"
                    alt={valueDetail.ourvaluestitle}
                  />
                </figure>
                <h4>{valueDetail.ourvaluestitle}</h4>
                <p>{valueDetail.ourvaluescontent}</p>
              </Col>
            );
          })}
        </Row>
      </Fade>
    </section>
  );
};

export default Values;
