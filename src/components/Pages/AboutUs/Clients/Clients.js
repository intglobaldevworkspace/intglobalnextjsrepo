import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const Clients = (props) => {
  const urlprefix = process.env.servUploadImg;
  return (
    <section className="clients">
      <Col md="12">
        <Row>
          <div className="clients-wrapper">
            <Fade bottom>
              <h3
                dangerouslySetInnerHTML={{
                  __html: props.fgclients.fgclientsHeader,
                }}
              ></h3>
            </Fade>
            <ul>
              {props.fgclients.fgclientDetails.map((clientDetail, index) => {
                return (
                  <li id={clientDetail.id} key={clientDetail.id}>
                    <figure>
                      <img
                        src={urlprefix + clientDetail.clientimage.url}
                        className="img-fluid"
                        alt={clientDetail.clientname}
                      />
                    </figure>
                  </li>
                );
              })}
            </ul>
            <Fade bottom>
              <h3
                dangerouslySetInnerHTML={{
                  __html: props.fgclients.fgclientsDetailBottom,
                }}
              ></h3>
            </Fade>
          </div>
          <Fade bottom>
            <a
              href={props.fgclients.fgclientseemorestoryLink}
              className="btn link"
              target="_blank"
            >
              {props.fgclients.fgclientsseemorestoryText}
            </a>
          </Fade>
        </Row>
      </Col>
    </section>
  );
};

export default Clients;
