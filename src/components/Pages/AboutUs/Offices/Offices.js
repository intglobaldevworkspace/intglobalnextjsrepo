import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";
import { useMediaQuery } from "react-responsive";

import OfficesTab from "./OfficesTab";
import OfficesAccord from "./OfficesAccord";

const Offices = (props) => {
  const isDesktopOrLaptop = useMediaQuery({ minDeviceWidth: 1024 });
  const isTabletOrMobile = useMediaQuery({ maxDeviceWidth: 1023 });

  return (
    <section className="offices">
      <Col md="12">
        <Row>
          <Fade bottom>
            <h2
              dangerouslySetInnerHTML={{
                __html: props.companyaddr.compAddressheader,
              }}
            ></h2>
          </Fade>
          <div className="office-wrapper">
            {isDesktopOrLaptop && (
              <OfficesTab companyaddress={props.companyaddr.compAddress} />
            )}
            {isTabletOrMobile && (
              <OfficesAccord companyaddress={props.companyaddr.compAddress} />
            )}
          </div>
        </Row>
      </Col>
    </section>
  );
};

export default Offices;
