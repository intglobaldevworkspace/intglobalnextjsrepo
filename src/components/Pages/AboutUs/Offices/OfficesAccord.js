import React, { Component } from "react";
import { Accordion, Card } from "react-bootstrap";
import { Col } from "reactstrap";
import Iframe from "react-iframe";

export default class OfficesAccord extends Component {
  constructor(props) {
    super(props);

    // this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: this.props.companyaddress[0].companygmapdrr.id,
      // isClicked: true,
      companyGmapAddress: this.props.companyaddress,
    };
  }
  render() {
    const urlprefix = process.env.servUploadImg;
    const { companyGmapAddress, activeTab } = this.state;
    return (
      <Col md="12">
        <Accordion defaultActiveKey={activeTab}>
          {companyGmapAddress.map((gmapaddr, index) => {
            return (
              <Card key={gmapaddr.companygmapdrr.id}>
                <Accordion.Toggle
                  as={Card.Header}
                  eventKey={gmapaddr.companygmapdrr.id}
                >
                  <figure>
                    <img
                      src={
                        urlprefix +
                        gmapaddr.companygmapdrr.gmaplocationimagehover.url
                      }
                      alt={gmapaddr.companygmapdrr.gmapcityplace}
                    />
                  </figure>
                  <h4>{gmapaddr.companygmapdrr.gmapcityplace}</h4>
                  <i className="creat"></i>
                </Accordion.Toggle>
                <Accordion.Collapse eventKey={gmapaddr.companygmapdrr.id}>
                  <Card.Body>
                    <i className="creat"></i>
                    <div className="office-add">
                      <div className="map-wrapper">
                        <Iframe
                          url={gmapaddr.companygmapdrr.gmapiframeurl}
                          width={gmapaddr.companygmapdrr.gmapwidth}
                          height={gmapaddr.companygmapdrr.gmapheight}
                          id={gmapaddr.companygmapdrr.gmapid}
                          className="officmap"
                          display="initial"
                          position="relative"
                        />
                      </div>
                      <h3>{gmapaddr.companygmapdrr.gmapcityplace}</h3>
                      <p
                        dangerouslySetInnerHTML={{
                          __html: gmapaddr.companygmapdrr.gmapplaceaddress,
                        }}
                      ></p>
                    </div>
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
            );
          })}
        </Accordion>
      </Col>
    );
  }
}
