import React, { Component } from "react";
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Row,
  Col,
} from "reactstrap";
import Fade from "react-reveal/Fade";
import classnames from "classnames";
import Iframe from "react-iframe";

export default class OfficesTab extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: this.props.companyaddress[0].companygmapdrr.id,
      isClicked: true,
      companyGmapAddress: this.props.companyaddress,
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  render() {
    const urlprefix = process.env.servUploadImg;
    const { companyGmapAddress } = this.state;
    // console.log(companyGmapAddress);
    return (
      <div className="offices-tab">
        <Col md="6">
          <Row>
            <Col md="11">
              <Row>
                <TabContent activeTab={this.state.activeTab}>
                  {companyGmapAddress.map((gmapaddr, index) => {
                    return (
                      <TabPane
                        tabId={gmapaddr.companygmapdrr.id}
                        key={gmapaddr.companygmapdrr.id}
                      >
                        <Fade left>
                          <div className="office-add">
                            <div className="map-wrapper">
                              <Iframe
                                url={gmapaddr.companygmapdrr.gmapiframeurl}
                                width={gmapaddr.companygmapdrr.gmapwidth}
                                height={gmapaddr.companygmapdrr.gmapheight}
                                id={gmapaddr.companygmapdrr.gmapid}
                                className="officmap"
                                display="initial"
                                position="relative"
                              />
                            </div>
                            <h3>{gmapaddr.companygmapdrr.gmapcityplace}</h3>
                            <p
                              dangerouslySetInnerHTML={{
                                __html:
                                  gmapaddr.companygmapdrr.gmapplaceaddress,
                              }}
                            ></p>
                          </div>
                        </Fade>
                      </TabPane>
                    );
                  })}
                </TabContent>
              </Row>
            </Col>
          </Row>
        </Col>
        <Col md="6">
          <Row>
            <Fade right>
              <Nav tabs vertical pills>
                {companyGmapAddress.map((gmapaddr) => {
                  return (
                    <NavItem key={gmapaddr.companygmapdrr.id}>
                      <NavLink
                        className={classnames({
                          active:
                            this.state.activeTab === gmapaddr.companygmapdrr.id,
                        })}
                        onClick={() => {
                          this.toggle(gmapaddr.companygmapdrr.id);
                        }}
                      >
                        <figure>
                          <img
                            src={
                              this.state.activeTab ===
                              gmapaddr.companygmapdrr.id
                                ? urlprefix +
                                  gmapaddr.companygmapdrr.gmaplocationimagehover
                                    .url
                                : urlprefix +
                                  gmapaddr.companygmapdrr.gmapimageplaceholder
                                    .url
                            }
                            alt={gmapaddr.companygmapdrr.gmapcityplace}
                          />
                        </figure>
                        <h4>{gmapaddr.companygmapdrr.gmapcityplace}</h4>
                      </NavLink>
                    </NavItem>
                  );
                })}
              </Nav>
            </Fade>
          </Row>
        </Col>
      </div>
    );
  }
}
