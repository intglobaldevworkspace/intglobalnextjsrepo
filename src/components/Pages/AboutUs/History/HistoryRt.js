import React, { Component } from "react";
import { Nav, NavItem, NavLink, Col } from "reactstrap";
import classnames from "classnames";
import Fade from "react-reveal/Fade";
import { Scrollbars } from "react-custom-scrollbars";

export default class HistoryRt extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: props.historydata.aboutuswhyandwhats[0].id,
      historyDataRight: props.historydata.aboutuswhyandwhats,
      historyDataSlider: props.historydata.historyRightScrollerSlider,
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  render() {
    console.log(this.state);
    const { historyDataSlider, historyDataRight } = this.state;
    return (
      <Col md="5">
        <Scrollbars
          onScroll={this.handleScroll}
          autoHide
          autoHideTimeout={1000}
          autoHideDuration={200}
          autoHeight
          autoHeightMin={0}
          autoHeightMax={historyDataSlider.autoHeightMax}
          universal={true}
          {...this.props}
        >
          <Fade right>
            <Nav tabs vertical pills>
              {historyDataRight.map((histDataRt) => {
                {
                  return histDataRt.thewhywhatStatus ? (
                    <NavItem key={histDataRt.id}>
                      <NavLink
                        className={classnames({
                          active: this.state.activeTab === histDataRt.id,
                        })}
                        onClick={() => {
                          this.toggle(histDataRt.id);
                        }}
                      >
                        <h3>
                          <strong>{histDataRt.indusAchieveYear}</strong>{" "}
                          <span
                            dangerouslySetInnerHTML={{
                              __html: histDataRt.whywhatContent,
                            }}
                          ></span>
                        </h3>
                      </NavLink>
                    </NavItem>
                  ) : null;
                }
              })}
            </Nav>
          </Fade>
        </Scrollbars>
      </Col>
    );
  }
}
