import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

export default class HistoryLt extends Component {
  constructor(props) {
    super(props);

    this.state = {
      historyLeftdata: props.historydata,
    };
  }
  render() {
    return (
      <Col md="7">
        <Row>
          <Fade left>
            <Col md="11" className="hist-cont">
              <div
                dangerouslySetInnerHTML={{
                  __html: this.state.historyLeftdata.aboutushistcontent,
                }}
              ></div>
              <div className="profiles">
                <ul>
                  {Object.entries(
                    this.state.historyLeftdata.aboutusyearsbox
                  ).map(([key, value], index) => {
                    if (
                      key === "founded" ||
                      key === "clients" ||
                      key === "teamnumber" ||
                      key === "repeatbusiness" ||
                      key === "hoursofcode"
                    ) {
                      // console.log("key -", key);
                      // console.log("Value -", value);
                      return (
                        <li
                          dangerouslySetInnerHTML={{
                            __html: value,
                          }}
                          key={index}
                        ></li>
                      );
                    }

                    if (key === "corporateprofilebuttonText") {
                      return (
                        <li key={index}>
                          <a
                            href={
                              this.state.historyLeftdata.aboutusyearsbox
                                .corporateprofilebuttonurl
                            }
                            className="btn link"
                          >
                            {value}
                          </a>
                        </li>
                      );
                    }
                  })}
                </ul>
              </div>
            </Col>
          </Fade>
        </Row>
      </Col>
    );
  }
}
