import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

import HistoryLt from "./HistoryLt";
import HistoryRt from "./HistoryRt";

export default class History extends Component {
  constructor(props) {
    super(props);

    this.state = {
      historyHeader: props.historydetails.historyHeader,
      historyOtherDetails: props.historydetails.historyDet,
      historyRight: props.historydetails.historyRightScroller,
    };
  }
  render() {
    const { historyHeader, historyOtherDetails, historyRight } = this.state;
    return (
      <section className="history">
        <Col md="12">
          <Row>
            <Fade bottom>
              <h2 dangerouslySetInnerHTML={{ __html: historyHeader }}></h2>
            </Fade>
            <div className="history-wrapper">
              <HistoryLt historydata={historyOtherDetails} />
              <HistoryRt historydata={historyRight} />
            </div>
          </Row>
        </Col>
      </section>
    );
  }
}
