import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

import Awards from "./Awards";
import Media from "./Media";

const MediaAwards = (props) => {
  // console.log(props.mediaAwd);
  return (
    <section className="media-awards">
      <Col md="12">
        <Row>
          <Fade bottom>
            <h2
              dangerouslySetInnerHTML={{
                __html: props.mediaAwd.awardsAndMediaHeader,
              }}
            ></h2>
          </Fade>
          <div className="media-wrapper">
            <Awards intawards={props.mediaAwd.awardsAndaccolades} />
            <Media medianews={props.mediaAwd.mediaAwardsNewsAndEvents} />
          </div>
          {props.mediaAwd.awardsAndMediaViewAll.viewallBtnStatus ? (
            <Fade bottom>
              <a
                href={props.mediaAwd.awardsAndMediaViewAll.viewAllBtnUrl}
                target="_blank"
                className="btn link"
              >
                {props.mediaAwd.awardsAndMediaViewAll.viewallBtnText}
              </a>
            </Fade>
          ) : (
            ""
          )}
        </Row>
      </Col>
    </section>
  );
};

export default MediaAwards;
