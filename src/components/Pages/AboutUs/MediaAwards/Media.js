import React, { Component } from "react";
import { Nav, NavItem, NavLink, Row, Col } from "reactstrap";
import classnames from "classnames";
import Fade from "react-reveal/Fade";
import { Scrollbars } from "react-custom-scrollbars";
import Moment from "react-moment";
import Swal from "sweetalert2";

export default class Media extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: props.medianews[0].id,
      newsContent: props.medianews,
    };
  }
  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  render() {
    // console.log(this.state);
    const urlprefix = process.env.servUploadImg;
    return (
      <Col md="5">
        <Scrollbars
          onScroll={this.handleScroll}
          autoHide
          autoHideTimeout={1000}
          autoHideDuration={200}
          autoHeight
          autoHeightMin={0}
          autoHeightMax={670}
          universal={true}
          {...this.props}
        >
          <Fade right>
            <Nav tabs vertical pills>
              {this.state.newsContent.map((newsEvents) => {
                return (
                  <NavItem key={newsEvents.id}>
                    {newsEvents.linkType != "popup" ? (
                      <NavLink
                        className={classnames({
                          active: this.state.activeTab === newsEvents.id,
                        })}
                        onClick={() => {
                          this.toggle(newsEvents.id);
                        }}
                        href={newsEvents.newsurl}
                        target="_blank"
                      >
                        <h3>
                          <strong>
                            {newsEvents.newsEventContentType}
                            <small>
                              <Moment format="MMM D, YYYY">
                                {newsEvents.newsEventDateTime}
                              </Moment>
                            </small>
                          </strong>{" "}
                          <span>{newsEvents.shortTitle}</span>
                        </h3>
                      </NavLink>
                    ) : (
                      <NavLink
                        className={classnames({
                          active: this.state.activeTab === newsEvents.id,
                        })}
                        onClick={() => {
                          this.toggle(newsEvents.id);
                        }}
                      >
                        <h3>
                          <strong>
                            {newsEvents.newsEventContentType}
                            <small>
                              <Moment format="MMM D, YYYY">
                                {newsEvents.newsEventDateTime}
                              </Moment>
                            </small>
                          </strong>{" "}
                          <span
                            style={{ cursor: "pointer" }}
                            onClick={() => {
                              Swal.fire({
                                padding: "0",
                                showCloseButton: true,
                                showCancelButton: false,
                                html: `<div>${newsEvents.shortTitle}
                              </div>`,
                                showConfirmButton: false,
                              });
                            }}
                          >
                            {newsEvents.shortTitle}
                          </span>
                        </h3>
                      </NavLink>
                    )}
                  </NavItem>
                );
              })}
            </Nav>
          </Fade>
        </Scrollbars>
      </Col>
    );
  }
}
