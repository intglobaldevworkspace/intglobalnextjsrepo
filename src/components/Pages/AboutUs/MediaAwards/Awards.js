import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";
import Swal from "sweetalert2";

export default class Awards extends Component {
  constructor(props) {
    super(props);

    this.state = {
      awardsDetails: this.props.intawards,
    };
  }

  render() {
    // console.log(this.state);
    const { awardsDetails } = this.state;
    const urlprefix = process.env.servUploadImg;
    return (
      <Col md="7">
        <Row>
          <Col md="11" className="media-cont">
            <Fade left>
              <ul>
                {awardsDetails.map((newsEventDetail) => {
                  return (
                    <li id={newsEventDetail.id} key={newsEventDetail.id}>
                      <div className="midawards-block">
                        <h6
                          onClick={() => {
                            Swal.fire({
                              width: 550,
                              padding: "0",
                              showCloseButton: true,
                              showCancelButton: false,
                              html: `<div class="popup-container">
                                <div class="popup-container_inr">
                                  <figure><img src=${
                                    urlprefix + newsEventDetail.awardImage.url
                                  } className="img-fluid" alt=${
                                newsEventDetail.awardstitle
                              } /></figure>
                                </div>
                              </div>`,
                              showConfirmButton: false,
                            });
                          }}
                        >
                          {newsEventDetail.awardsdescrip}
                        </h6>
                      </div>
                    </li>
                  );
                })}
              </ul>
            </Fade>
          </Col>
        </Row>
      </Col>
    );
  }
}
