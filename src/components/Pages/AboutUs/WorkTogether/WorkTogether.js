import React, { useState } from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";
import Contactus from "../../../shared/Contactus";
const WorkTogether = (props) => {
  const [workTogetherState, setworkTogetherState] = useState({
    isOpen: false,
    showContactusForm: true,
    showContactusSubmitted: false,
    ContactusSubmitfailed: false,
    showSubmitLoader: false,
    //----
    workTogetherHeaderText: props.work.workTogetherHeaderText,
    workTogetherContactButtonUrl: props.work.workTogetherContactButtonUrl,
    workTogetherContactButtonText: props.work.workTogetherContactButtonText,
    contactuspopup: props.work.contactuspopup,
  });
  const urlprefix = process.env.servUploadImg;

  const toggleModal = () => {
    setworkTogetherState((prevState) => ({
      ...prevState,
      isOpen: !prevState.isOpen,
      showContactusForm: true,
      showContactusSubmitted: false,
      ContactusSubmitfailed: false,
      showSubmitLoader: false,
    }));
  };

  const cntpopup = {
    isOpen: workTogetherState.isOpen,
    showContactusForm: workTogetherState.showContactusForm,
    showContactusSubmitted: workTogetherState.showContactusSubmitted,
    ContactusSubmitfailed: workTogetherState.ContactusSubmitfailed,
    showSubmitLoader: workTogetherState.showSubmitLoader,
    contactusPopup: workTogetherState.contactuspopup,
  };
  return (
    <section className="work-together">
      <Col md="12">
        <Row>
          <Fade bottom>
            <h2
              dangerouslySetInnerHTML={{
                __html: workTogetherState.workTogetherHeaderText,
              }}
            ></h2>
            <div className="works-wrapper">
              <a
                href={workTogetherState.workTogetherContactButtonUrl}
                className="btn link"
                onClick={(e) => {
                  e.preventDefault();
                  // toggle();
                  toggleModal();
                }}
              >
                {workTogetherState.workTogetherContactButtonText}
              </a>
            </div>
          </Fade>
        </Row>
      </Col>
      {workTogetherState.isOpen ? (
        <Contactus
          cntpopup={{
            ...cntpopup,
            contactText: workTogetherState.workTogetherContactButtonText,
          }}
          handler={toggleModal}
        />
      ) : null}
    </section>
  );
};

export default WorkTogether;
