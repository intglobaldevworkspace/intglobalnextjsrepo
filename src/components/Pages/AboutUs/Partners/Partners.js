import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const Partners = (props) => {
  // console.log(props);
  const urlprefix = process.env.servUploadImg;

  return (
    <section className="partners">
      <Col md="12">
        <Row>
          <Fade bottom>
            <h2
              dangerouslySetInnerHTML={{ __html: props.apartners.apheader }}
            ></h2>
          </Fade>
          <div className="partner-wrapper">
            <ul>
              {props.apartners.apdetails.map((partnerDetail, index) => {
                return (
                  <li id={partnerDetail.id} key={partnerDetail.id}>
                    <figure>
                      <img
                        src={
                          urlprefix +
                          partnerDetail.associateparnerdetail
                            .associatepartnerimg.url
                        }
                        className="img-fluid"
                        alt={
                          partnerDetail.associateparnerdetail
                            .associatepartnername
                        }
                      />
                    </figure>
                  </li>
                );
              })}
            </ul>
          </div>
          <Fade bottom>
            <a
              href={props.apartners.apviewall.apviewalllink}
              className="btn link"
            >
              {props.apartners.apviewall.apviewalltext}
            </a>
          </Fade>
        </Row>
      </Col>
    </section>
  );
};

export default Partners;
