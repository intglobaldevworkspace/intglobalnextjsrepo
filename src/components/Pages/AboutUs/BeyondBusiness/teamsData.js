/* eslint max-len: 0 */

export const teams = [
  {
    id: '1',
    imgUrl: '/static/images/teams/about-us/pic-02.jpg',
    caption: 'Meet the Team',
    type: 'pic',
    url: 'https://www.indusnet.co.in/team.html'
  },
  {
    id: '2',
    imgUrl: '/static/images/teams/about-us/pic-01.jpg',
    caption: 'Meet the Team',
    type: 'pic',
    url: 'https://www.indusnet.co.in/team.html'
  },
  {
    id: '3',
    imgUrl: '/static/images/teams/about-us/pic-04.jpg',
    caption: 'Meet the Team',
    type: 'pic',
    url: 'https://www.indusnet.co.in/team.html'
  },
  {
    id: '4',
    imgUrl: '/static/images/teams/about-us/vid-pic-03.jpg',
    caption: 'Meet the Team',
    type: 'vid',
    url: '/'
  },
];

export default teams;
