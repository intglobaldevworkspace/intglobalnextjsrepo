import React, { Component } from "react";
import { Row, Col, Button, Modal, ModalBody } from "reactstrap";
import Fade from "react-reveal/Fade";

export default class BeyondBusiness extends Component {
  constructor(props) {
    super(props);
    this.state = Object.assign({}, { ...props.beyond }, { modal: false });
  }

  toggle = () => {
    this.setState((prevState) => ({
      modal: !prevState.modal,
    }));
  };
  render() {
    // console.log(this.state);
    const urlprefix = process.env.servUploadImg;
    const {
      beyBfooter,
      beyBheader,
      beyBpartnerWithusbuttonStatus,
      beyBpartnerWithusbuttonText,
      beyBpartnerWithusbuttonUrl,
      beyBusinessVideoAndImage,
      beyondBusinessInstitutes,
    } = this.state;
    return (
      <section className="beyond-business">
        <Fade bottom>
          <div dangerouslySetInnerHTML={{ __html: beyBheader }}></div>
          <div className="brand-products">
            <Col md="12">
              <Row>
                {beyondBusinessInstitutes.map((businessDetail, index) => {
                  return (
                    <Col id={businessDetail.id} key={businessDetail.id} md="4">
                      <Col md="9">
                        <Row>
                          <figure>
                            <img
                              src={
                                urlprefix + businessDetail.instituteImage.url
                              }
                              className="img-fluid"
                              alt={businessDetail.instituteName}
                            />
                          </figure>
                          <h3>{businessDetail.instituteName}</h3>
                          <p>{businessDetail.instituteContent}</p>
                          {businessDetail.instituteMorebuttonStatus ? (
                            <a
                              href={businessDetail.instituteMorebuttonUrl}
                              className="btn link"
                              title={businessDetail.instituteMorebuttonText}
                            >
                              {businessDetail.instituteMorebuttonText}
                            </a>
                          ) : (
                            ""
                          )}
                        </Row>
                      </Col>
                    </Col>
                  );
                })}
              </Row>
            </Col>
          </div>
          <div className="team-wrapper">
            <ul className="team_teaser__list">
              {beyBusinessVideoAndImage.map((teamDetail, index) => {
                return (
                  <li
                    id={teamDetail.id}
                    key={teamDetail.id}
                    className={
                      teamDetail.beyBMediaOptions == "Image" ? "pic" : "vid"
                    }
                  >
                    <figure>
                      <img
                        src={urlprefix + teamDetail.beyBfrontImage.url}
                        className="img-fluid"
                        alt={teamDetail.beyBmediaCaption}
                      />
                    </figure>
                    <span>
                      <a
                        href={teamDetail.beyBmediaLinkURL}
                        target={
                          teamDetail.beyBMediaOptions == "Image" ? "_blank" : ""
                        }
                      >
                        {teamDetail.beyBmediaCaption}
                      </a>
                      {teamDetail.beyBMediaOptions == "Video" ? (
                        <>
                          <Button onClick={this.toggle}></Button>
                          <Modal
                            isOpen={this.state.modal}
                            toggle={this.toggle}
                            className={
                              teamDetail.beyBMediaOptions == "Image"
                                ? "pic"
                                : "vid"
                            }
                          >
                            <ModalBody>
                              <iframe
                                width="100%"
                                height="315"
                                src={teamDetail.beyBVideoLink}
                                frameBorder="0"
                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                allowFullScreen
                              ></iframe>
                            </ModalBody>
                          </Modal>
                        </>
                      ) : (
                        ""
                      )}
                    </span>
                  </li>
                );
              })}
            </ul>
            <div dangerouslySetInnerHTML={{ __html: beyBfooter }}></div>
            {beyBpartnerWithusbuttonStatus ? (
              <a
                href={beyBpartnerWithusbuttonUrl}
                className="btn link"
                title={beyBpartnerWithusbuttonText}
              >
                {beyBpartnerWithusbuttonText}
              </a>
            ) : (
              ""
            )}
          </div>
        </Fade>
      </section>
    );
  }
}
