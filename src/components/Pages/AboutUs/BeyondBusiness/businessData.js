/* eslint max-len: 0 */

export const business = [
  {
    id: '1',
    logoUrl: '/static/images/int-logo.png',
    name: 'Indus Net Lab',
    content: 'Digital acceleration lab. The first of its kind in Kolkata',
    urlText: 'More',
    url: 'https://indusnetlabs.com/'
  },
  {
    id: '2',
    logoUrl: '/static/images/int-logo.png',
    name: 'Indus Net Academy',
    content: 'Professional courses and training on new upcoming technologies',
    urlText: 'More',
    url: 'https://indusnethub.com/'
  },
  {
    id: '3',
    logoUrl: '/static/images/int-logo.png',
    name: 'Indus Net Foundation',
    content: 'Bridging the major skills gap in 1st generation learners',
    urlText: 'More',
    url: 'https://www.indusnetfoundation.org/'
  },
];

export default business;
