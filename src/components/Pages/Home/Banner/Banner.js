import React, { Component } from "react";
import Typed from "react-typed";
import Iframe from "react-iframe";

export default class Banner extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      bannerApiContents: this.props.bannercontent,
    };

    // console.log(this.state);
  }

  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        this.setState({ loading: !this.state.loading });
      }.bind(this),
      1000
    );
  }
  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const urlprefix = process.env.servUploadImg;
    const globeLink = process.env.globeLink;
    const {
      digitalSuccessTxt,
      globeImg,
      headingstarttxt,
    } = this.state.bannerApiContents;
    const banneramintxt_setone = {
      ...this.state.bannerApiContents.bannerAnimationTxts[0].banneranimco,
    };
    const banneramintxt_two = {
      ...this.state.bannerApiContents.bannerAnimationTxts[1].banneranimco,
    };
    // const bannerTxts = this.state.bannerApiContents.bannerAnimationTxts.map(
    //   (animatedTxt, index) => {
    //     return animatedTxt.banner_text;
    //   }
    // );

    // const banneramintxt = this.state.bannerApiContents.bannerAnimationTxts.map(animTxtObj=>{
    //    animTxtObj.banneranimco.map(animobj=>{
    //      return
    //   })
    // })

    // console.log(banneramintxt);
    return (
      <section className="hdBan">
        <div className="hdBan_inner">
          <div className={`anim ${this.state.loading ? "dragged" : ""}`}>
            <Iframe
              url={globeLink}
              width="100%"
              height="570"
              id="glober"
              className="officmap"
              display="block"
              dataShowTabBar="no"
              position="relative"
            />
          </div>
          <div className="hdBan__row-teaser">
            {/* <h1>
              <span>
                We <strong>Deliver</strong>
              </span>
              {bannerTxtOne} <strong>{bannerTxttwo}</strong>
            </h1> */}
            <h1>
              <b dangerouslySetInnerHTML={{ __html: headingstarttxt }}></b>
              {/* Digital <strong>Success.</strong> */}
              <Typed
                loop
                typeSpeed={120}
                backSpeed={120}
                strings={[
                  banneramintxt_setone.btextone,
                  banneramintxt_setone.btexttwo,
                  banneramintxt_two.btextone,
                  banneramintxt_two.btexttwo,
                ]}
                smartBackspace
                shuffle={false}
                backDelay={1}
                fadeOut={false}
                fadeOutDelay={100}
                loopCount={0}
                showCursor
                cursorChar="|"
              />
            </h1>
            <p>{digitalSuccessTxt}</p>
          </div>
        </div>
      </section>
    );
  }
}
