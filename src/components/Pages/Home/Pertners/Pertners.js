import React, { useState } from "react";
import { Row, Col } from "reactstrap";
import { useMediaQuery } from "react-responsive";

import ModelsDesk from "./ModelsDesk";
import ModelsMob from "./ModelsMob";
import Contactus from "../../../shared/Contactus";

const Pertners = (props) => {
  const [partnerState, setPartnerState] = useState({
    isClicked: true,

    partnerDetails: props.partnerdetails.partnerSlider.map((partnerDetail) => {
      return {
        partnerId: partnerDetail.partnerslide.id,
        partnerTitle: partnerDetail.partnerslide.partnertitle,
        partnerContent: partnerDetail.partnerslide.partnercontent,
        partnerImgUrl: partnerDetail.partnerslide.hasOwnProperty("partnerlogo")
          ? partnerDetail.partnerslide.partnerlogo.url
          : "",
      };
    }),
    partnerContTxt: props.partnerdetails.partnerContractTxt,
    partnerContDescription: props.partnerdetails.partnerContractDescription,
    contactusButton: props.partnerdetails.partnerContactusButton,
    contactusMessages: props.partnerdetails.contactuspopup,
    //----
    isOpen: false,
    showContactusForm: true,
    showContactusSubmitted: false,
    ContactusSubmitfailed: false,
    showSubmitLoader: false,
  });
  const isDesktopOrLaptop = useMediaQuery({ minDeviceWidth: 1024 });
  const isTabletOrMobile = useMediaQuery({ maxDeviceWidth: 1023 });

  const toggleModal = () => {
    setPartnerState((prevState) => ({
      ...prevState,
      isOpen: !prevState.isOpen,
      showContactusForm: true,
      showContactusSubmitted: false,
      ContactusSubmitfailed: false,
      showSubmitLoader: false,
    }));
  };

  const cntpopup = {
    isOpen: partnerState.isOpen,
    showContactusForm: partnerState.showContactusForm,
    showContactusSubmitted: partnerState.showContactusSubmitted,
    ContactusSubmitfailed: partnerState.ContactusSubmitfailed,
    showSubmitLoader: partnerState.showSubmitLoader,
    contactusPopup: partnerState.contactusMessages,
  };
  // console.log(partnerState);
  return (
    <section className="pertners">
      <Row suppressHydrationWarning={true}>
        <Col md="6">
          <div
            dangerouslySetInnerHTML={{
              __html: partnerState.partnerContTxt,
            }}
          ></div>
          <div
            dangerouslySetInnerHTML={{
              __html: partnerState.partnerContDescription,
            }}
          ></div>
          <a
            href={partnerState.contactusButton.letustalkurl}
            className="btn link"
            onClick={(e) => {
              e.preventDefault();
              // toggle();
              toggleModal();
            }}
          >
            {partnerState.contactusButton.letustalktext}
          </a>
        </Col>
        {partnerState.isOpen ? (
          <Contactus
            cntpopup={{
              ...cntpopup,
              contactText: partnerState.contactusButton.letustalktext,
            }}
            handler={toggleModal}
          />
        ) : null}
        {isDesktopOrLaptop && (
          <ModelsDesk partners={partnerState.partnerDetails} />
        )}
        {isTabletOrMobile && (
          <ModelsMob partners={partnerState.partnerDetails} />
        )}
      </Row>
    </section>
  );
};

export default Pertners;
