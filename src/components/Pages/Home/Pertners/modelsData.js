/* eslint max-len: 0 */

export const models = [
  {
    id: '1',
    icoUrl: '/static/images/ic-revenue.svg',
    name: 'Revenue Sharing',
    content: 'A part development fee and part revenue-sharing deal to engineer and scale your digital solution. We earn when you earn.'
  },
  {
    id: '2',
    icoUrl: '/static/images/ic-equity.svg',
    name: 'Equity Sharing',
    content: 'We take a small portion of your equity to realise your idea and engineer it to a successful launch. In this model, we pair our responsibilities together for your products’ success.'
  },
  {
    id: '3',
    icoUrl: '/static/images/ic-success.svg',
    name: 'Success Fee',
    content: 'For futuristic solutions with a risk of success, we invest our technology prowess by absorbing a substantial part of the cost component 50 %) against a success fee.'
  },
  {
    id: '4',
    icoUrl: '/static/images/ic-revenue.svg',
    name: 'Revenue Sharing',
    content: 'We take a small portion of your equity to realise your idea and engineer it to a successful launch. In this model, we pair our responsibilities together for your products’ success.'
  },
];

export default models;
