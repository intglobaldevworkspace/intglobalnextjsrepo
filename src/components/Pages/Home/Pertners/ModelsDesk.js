import React, { Component } from "react";
import { Col } from "reactstrap";

export default class ModelsDesk extends Component {
  constructor(props) {
    super(props);
    let rotationArray = [];
    rotationArray[0] = [0, -45, -90];
    rotationArray[1] = [45, 0, -45];
    rotationArray[2] = [90, 45, 0];

    let ulrotationClass = [];
    ulrotationClass[0] = "pron";
    ulrotationClass[1] = "prtw";
    ulrotationClass[2] = "prth";

    this.state = {
      isClicked: true,
      partnerDetails: this.props.partners,
      transform: 0,
      ptrSetTo: 1,
      automaticCounter: 0,
      // liRotation: [
      //   { rotateOne: -50, translate: 43.5, rotateTwo: 50 },
      //   { transform: 0, translate: 43.5, rotateTwo: -0 },
      //   { transform: 50, translate: 44, rotateTwo: -50 }
      // ]
      liRotation: [
        {
          rotate: "rotate(-50deg) translate(43.5vh) rotate(50deg)",
        },
        {
          rotate: "rotate(0deg) translate(44vh) rotate(-0deg)",
        },
        {
          rotate: "rotate(50deg) translate(44vh) rotate(-50deg)",
        },
      ],
      ulrotateClass: ulrotationClass,
      ulClass: ulrotationClass[1],
      fullRotationArray: rotationArray,
      presentArray: rotationArray[1],
    };
  }

  partnerSliderAutomatic = (id) => {
    if (this.state.automaticCounter == 2) {
      let rotateByDeg = this.state.presentArray[id];

      this.setState(
        (prevState) => {
          return {
            transform: prevState.transform + rotateByDeg,
            ptrSetTo: id,
            presentArray: this.state.fullRotationArray[id],
            ulClass: this.state.ulrotateClass[id],
            automaticCounter: 0,
          };
        },
        () => {
          // console.log("state transform -", this.state.transform);
          // console.log("state ptrSetTo -", this.state.ptrSetTo);
          // console.log("state presentArray -", this.state.presentArray);
        }
      );
    } else {
      let rotateByDeg = this.state.presentArray[id];
      this.setState(
        (prevState) => {
          return {
            transform: prevState.transform + rotateByDeg,
            ptrSetTo: id,
            presentArray: this.state.fullRotationArray[id],
            ulClass: this.state.ulrotateClass[id],
            automaticCounter: id + 1,
          };
        },
        () => {
          // console.log("state transform -", this.state.transform);
          // console.log("state ptrSetTo -", this.state.ptrSetTo);
          // console.log("state presentArray -", this.state.presentArray);
        }
      );
    }
  };

  changePartnerDetails = (event, id) => {
    event.preventDefault();
    // console.log("id -", id);

    // console.log("presentArray -", this.state.presentArray);
    let rotateByDeg = this.state.presentArray[id];

    if (id == 2) {
      this.setState({ automaticCounter: 0 });
    } else {
      this.setState({ automaticCounter: id + 1 });
    }

    this.setState(
      (prevState) => {
        return {
          transform: prevState.transform + rotateByDeg,
          ptrSetTo: id,
          presentArray: this.state.fullRotationArray[id],
          ulClass: this.state.ulrotateClass[id],
        };
      },
      () => {
        // console.log("state transform -", this.state.transform);
        // console.log("state ptrSetTo -", this.state.ptrSetTo);
        // console.log("state presentArray -", this.state.presentArray);
      }
    );
  };

  componentDidMount() {
    this.slidetimerid = setInterval(() => {
      this.partnerSliderAutomatic(this.state.automaticCounter);
    }, 6000);
  }
  componentWillUnmount() {
    clearInterval(this.slidetimerid);
  }

  render() {
    //const isClick = this.state.isClicked;
    const urlprefix = process.env.servUploadImg;
    const { partnerDetails } = this.state;
    return (
      <Col md="6">
        <div className="part-vertical-carousel-nav">
          <ul
            className={`part-vertical-carousel-nav__list ${this.state.ulClass}`}
            style={{
              transform: `rotate(${this.state.transform}deg)`,
            }}
          >
            {partnerDetails.map((partnerDet, index) => {
              return (
                <li
                  style={{
                    transform: this.state.liRotation[index].rotate,
                  }}
                  className={
                    index == this.state.ptrSetTo
                      ? "part-vertical-carousel-nav__list-item active"
                      : "part-vertical-carousel-nav__list-item"
                  }
                  key={index}
                >
                  <a
                    href={`#` + index}
                    title="Skip to first slide"
                    onClick={(e) => {
                      this.changePartnerDetails(e, index);
                    }}
                  >
                    <img
                      src={urlprefix + partnerDet.partnerImgUrl}
                      className="img-fluid"
                      alt={partnerDet.partnerTitle}
                    />
                  </a>
                  <div className="part-vertical-carousel-nav__list-item_label">
                    {partnerDet.partnerTitle}
                  </div>
                </li>
              );
            })}
          </ul>
          <div id="1" className="part-contnt-visible active">
            <h3>
              {this.state.partnerDetails[this.state.ptrSetTo].partnerTitle}
            </h3>
            <p
              dangerouslySetInnerHTML={{
                __html: this.state.partnerDetails[this.state.ptrSetTo]
                  .partnerContent,
              }}
            ></p>
          </div>
        </div>
      </Col>
    );
  }
}
