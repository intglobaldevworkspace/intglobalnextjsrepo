import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import Slider from "react-slick";

import modelsData from "./modelsData";

export default class ModelsMob extends Component {
  constructor(props) {
    super(props);
    this.state = {
      partnerDetails: this.props.partners,
    };
  }
  render() {
    const { partnerDetails } = this.state;
    const settings = {
      dots: true,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 4,
      autoplay: true,
      autoplaySpeed: 8000,
      classNames: "slides",
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: true,
            dots: true,
          },
        },
        {
          breakpoint: 800,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            initialSlide: 1,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
      infinite: false,
    };
    const urlprefix = process.env.servUploadImg;
    return (
      <Col md="6">
        <Slider {...settings}>
          {partnerDetails.map((modelDetail, index) => {
            return (
              <div className="model-item" key={index}>
                <figure>
                  <img
                    src={urlprefix + modelDetail.partnerImgUrl}
                    className="img-fluid"
                    alt={modelDetail.partnerTitle}
                  />
                </figure>
                <Col sm="12">
                  <Row>
                    <div className="model-det">
                      <h4>{modelDetail.partnerTitle}</h4>
                      <p
                        dangerouslySetInnerHTML={{
                          __html: modelDetail.partnerContent,
                        }}
                      ></p>
                    </div>
                  </Row>
                </Col>
              </div>
            );
          })}
        </Slider>
      </Col>
    );
  }
}
