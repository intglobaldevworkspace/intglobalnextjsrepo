import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const FastFlexible = (props) => {
  return (
    <section className="fast-flexible">
      <Fade bottom>
        <Row>
          <Col md="10">
            <div
              dangerouslySetInnerHTML={{
                __html: props.ff.fastflexOne,
              }}
            ></div>
            <div
              dangerouslySetInnerHTML={{
                __html: props.ff.fastflexTwo,
              }}
            ></div>
          </Col>
        </Row>
      </Fade>
    </section>
  );
};

export default FastFlexible;
