import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";
import Counters from "./Counters";

export default class Profit extends Component {
  constructor(props) {
    super(props);

    this.state = {
      byProfitDetails: this.props.profit,
    };

    // console.log(this.state);
  }

  render() {
    const urlprefix = process.env.servUploadImg;
    const {
      byProfitHeader,
      byProfitImgURl,
      byProfitContent,
      byNumberScroller,
    } = this.state.byProfitDetails;
    return (
      <section className="profits">
        <Fade bottom>
          <Row>
            <Col
              md="12"
              dangerouslySetInnerHTML={{ __html: byProfitHeader }}
            ></Col>
            <Col md="5">
              <figure className="profimg">
                <img src={urlprefix + byProfitImgURl} alt="INT Beyond Profit" />
              </figure>
            </Col>
            <Col md="7">
              <div dangerouslySetInnerHTML={{ __html: byProfitContent }}></div>
              <Counters counterContent={byNumberScroller} />
            </Col>
          </Row>
        </Fade>
      </section>
    );
  }
}
