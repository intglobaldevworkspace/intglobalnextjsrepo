import React, { useState } from "react";
import CountUp from "react-countup";
import VisibilitySensor from "react-visibility-sensor";
import { Col } from "reactstrap";

export default function Counter(props) {
  const [focus, setFocus] = useState(false);
  // console.log(props.counterContent);
  return (
    <div className="prof-counter">
      {props.counterContent.map((counterEle, index) => (
        <Col md="6" key={index}>
          <div className="c-block">
            <CountUp
              start={focus ? 0 : null}
              end={counterEle.scrollupto}
              delay={1}
              duration={4}
              redraw={true}
              useEasing={true}
              useGrouping={true}
            >
              {({ countUpRef }) => (
                <h4>
                  <span ref={countUpRef} />
                  <VisibilitySensor
                    onChange={(isVisible) => {
                      if (isVisible) {
                        setFocus(true);
                      }
                    }}
                  >
                    <a>+</a>
                  </VisibilitySensor>
                </h4>
              )}
            </CountUp>
            <span className="det">{counterEle.highlightedword}</span>
          </div>
        </Col>
      ))}
    </div>
  );
}
