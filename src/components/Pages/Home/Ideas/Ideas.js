import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const Ideas = (props) => {
  return (
    <section className="ideas">
      <Row>
        <Col md="6">
          <Fade left>
            <div
              className="ideas-title"
              dangerouslySetInnerHTML={{
                __html: props.ideas.transformIdeas,
              }}
            ></div>
          </Fade>
        </Col>
        <Col md="6">
          <Fade right>
            <div className="ideas-cont">
              <div
                dangerouslySetInnerHTML={{
                  __html: props.ideas.collaborative,
                }}
              ></div>
              <a
                href={props.ideas.scheduleacallbutton.schedulecallurl}
                className="btn link"
                target="_blank"
              >
                {props.ideas.scheduleacallbutton.scheduleacalltxt}
              </a>
            </div>
          </Fade>
        </Col>
      </Row>
    </section>
  );
};

export default Ideas;
