import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import Slider from "react-slick";

// import tabData from './tabData';

export default class CustomerAccord extends Component {
  constructor(props) {
    super(props);

    this.state = {
      CustomerContents: this.props.clients,
    };

    // console.log(this.state);
  }
  render() {
    const settings = {
      dots: false,
      arrow: true,
      infinte: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 8000,
      classNames: "slides",
    };
    const urlprefix = process.env.servUploadImg;
    const { CustomerContents } = this.state;
    return (
      <div className="cust-carousl">
        <Slider {...settings}>
          {CustomerContents.map((tabDetail, index) => {
            return (
              <div className="cust-item" key={tabDetail.custID}>
                <figure>
                  <img
                    src={urlprefix + tabDetail.custlogoUrl}
                    className="img-fluid"
                    alt="logo"
                  />
                </figure>
                <Col sm="12">
                  {/* <figure>
                            <img src={tabDetail.imgUrl} className="img-fluid" alt="logo" />
                          </figure> */}
                  <div className="cust-det">
                    <h4>{tabDetail.custTitle}</h4>
                    <h5>{tabDetail.custSubTitle}</h5>
                    <p>{tabDetail.cust_desc}</p>
                  </div>
                </Col>
              </div>
            );
          })}
        </Slider>
      </div>
    );
  }
}
