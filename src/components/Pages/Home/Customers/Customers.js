import React from "react";
import { Row, Col } from "reactstrap";
import { useMediaQuery } from "react-responsive";
import Fade from "react-reveal/Fade";

import CustomerTab from "./CustomerTab";
import CustomerAccord from "./CustomerAccord";

const Customers = (props) => {
  const isDesktopOrLaptop = useMediaQuery({ minDeviceWidth: 1024 });
  const isTabletOrMobile = useMediaQuery({ maxDeviceWidth: 1023 });

  return (
    <section className="customers" suppressHydrationWarning={true}>
      <Fade bottom>
        <div className="cust-innr">
          <Row>
            <Col
              sm="10"
              dangerouslySetInnerHTML={{
                __html: props.customer.customerDescription,
              }}
            ></Col>
          </Row>
          {isDesktopOrLaptop && (
            <CustomerTab clients={props.customer.arrCustomer} />
          )}
          {isTabletOrMobile && (
            <CustomerAccord clients={props.customer.arrCustomer} />
          )}
        </div>
      </Fade>
    </section>
  );
};

export default Customers;
