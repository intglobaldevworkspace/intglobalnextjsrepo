import React, { useState } from "react";
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Card,
  Button,
  CardTitle,
  CardText,
  Row,
  Col,
} from "reactstrap";
//import { Accordion } from 'react-bootstrap';
import classnames from "classnames";

const CustomerTab = (props) => {
  const [activeTab, setActiveTab] = useState(props.clients[0].custID);

  const toggle = (tab) => {
    if (activeTab !== tab) setActiveTab(tab);
  };
  const urlprefix = process.env.servUploadImg;
  return (
    <>
      <div className="cust-tab">
        <Nav tabs>
          {props.clients.map((tabDetail, index) => {
            return (
              <NavItem key={tabDetail.custID}>
                <NavLink
                  className={classnames({
                    active: activeTab === tabDetail.custID,
                  })}
                  onClick={() => {
                    toggle(tabDetail.custID);
                  }}
                >
                  <img
                    src={urlprefix + tabDetail.custlogoUrl}
                    className="img-fluid"
                    alt="logo"
                  />
                </NavLink>
              </NavItem>
            );
          })}
        </Nav>
        <TabContent activeTab={activeTab}>
          {props.clients.map((tabDetail, index) => {
            return (
              <TabPane key={tabDetail.custID} tabId={tabDetail.custID}>
                <Row>
                  <Col sm="11">
                    <Col sm="5">
                      <img
                        src={urlprefix + tabDetail.custbodylogo}
                        className="img-fluid"
                        alt="logo"
                      />
                    </Col>
                    <Col sm="7">
                      <h4>{tabDetail.custTitle}</h4>
                      <h5>{tabDetail.custSubTitle}</h5>
                      <p>{tabDetail.cust_desc}</p>
                    </Col>
                  </Col>
                </Row>
              </TabPane>
            );
          })}
        </TabContent>
      </div>
    </>
  );
};

export default CustomerTab;
