/* eslint max-len: 0 */

export const testimonials = [
  {
    id: '1',
    tech: 'Fast & Flexible',
    imgUrl: '/static/images/img-simon-constantine.jpg',
    name: 'Simon Constantine',
    desig: 'CIO, ALMIZAN Natural Trading Company',
    content: 'Our commitment with Indus Net relies on the fact of their dedication. We have competitors at this hour who are growing faster and better in their eCommerce platform.<br/>Indus Net team is willing to help us to level the playing field with a full understanding of our business to reconstruct and enhance our site to not only be looking pretty, but also to function at the speed of the light.'
  },
  {
    id: '2',
    tech: 'Reliable & Trusted',
    imgUrl: '/static/images/img-nora-bergin.jpg',
    name: 'Nora Bergin',
    desig: 'Head of Project Management, Every1Mobile Limited',
    content: 'The app delivered by Indus Net Technologies Pvt. Ltd. has received extremely positive feedback from stakeholders and garnered significant interest from investors. Their team worked hard to manage time zone differences, demonstrating business prowess as well as great interpersonal skills.'
  },
  {
    id: '3',
    tech: 'Robust Process',
    imgUrl: '/static/images/img-ankit-gupta.jpg',
    name: 'Ankit Gupta',
    desig: 'IndusInd Bank',
    content: 'The entire team of Indus Net has worked very hard to take this project to this stage despite the fact that the team has faced various challenges. Indus Net team has stretched multiple times and have done a lot of work with great attitude, motivation and calmness.'
  },
  {
    id: '4',
    tech: 'Quick & Agile',
    imgUrl: '/static/images/img-malcolm-mclean.jpg',
    name: 'Malcolm Mclean',
    desig: 'Head of Technology, Ageas',
    content: 'In Indus Net technologies, we are supported by a partner which goes that extra mile to solve your problem. Its about sharing a good vibe while you work so that we are on the same page and innovate something unique for the mass.'
  },
  {
    id: '5',
    tech: 'Robust Process',
    imgUrl: '/static/images/img-romi-kohli.jpg',
    name: 'Romi Kohli',
    desig: 'Co-Founder/CTO, Fintainium (formerly known as ePayRails)',
    content: 'Delivering robust product features on an accelerated timeline, the team helped its clients earn the investments needed to grow. Their work increased the partner’s internal headcount and revenue. The project manager’s leadership abilities made a strong impression.'
  },
];

export default testimonials;
