import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import Slider from "react-slick";

import testimonialsData from "./testimonialsData";

export default class TestimonialsMob extends Component {
  constructor(props) {
    super(props);

    this.state = {
      clientTestimonial: this.props.testimonialDetails,
      isClicked: true,
    };

    // console.log(this.state);
  }
  render() {
    const urlprefix = process.env.servUploadImg;
    const settings = {
      dots: true,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 4,
      autoplay: true,
      autoplaySpeed: 8000,
      classNames: "slides",
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: true,
            dots: true,
          },
        },
        {
          breakpoint: 800,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            initialSlide: 1,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
      infinite: true,
    };
    const { clientTestimonial } = this.state;
    return (
      <>
        <Col md="6">
          <h2
            dangerouslySetInnerHTML={{
              __html: clientTestimonial.testimonialHeader,
            }}
          ></h2>
          <div className="testi-content">
            <small>
              <span>
                {clientTestimonial.clientTestimonials[0].custhighlightword}
              </span>
            </small>
            <p
              dangerouslySetInnerHTML={{
                __html:
                  clientTestimonial.clientTestimonials[0].custappreciation,
              }}
            ></p>
          </div>
        </Col>
        <Col md="6">
          <Slider {...settings}>
            {clientTestimonial.clientTestimonials.map((testimonialDetail) => {
              return (
                <div className="testi-item" key={testimonialDetail.id}>
                  <small>
                    <span>{testimonialDetail.custhighlightword}</span>
                  </small>
                  <figure>
                    <img
                      src={urlprefix + testimonialDetail.custimage}
                      className="img-fluid"
                      alt={testimonialDetail.custname}
                    />
                  </figure>
                  <Col sm="12">
                    <Row>
                      <div className="testi-det">
                        <h4>{testimonialDetail.custname}</h4>
                        <h5>{testimonialDetail.custdesignation}</h5>
                        <p>{testimonialDetail.custappreciation}</p>
                      </div>
                    </Row>
                  </Col>
                </div>
              );
            })}
          </Slider>
        </Col>
      </>
    );
  }
}
