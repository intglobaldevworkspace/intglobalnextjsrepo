import React, { useState } from "react";
import { Row } from "reactstrap";
import { useMediaQuery } from "react-responsive";

import TestimonialsDesk from "./TestimonialsDesk";
import TestimonialsMob from "./TestimonialsMob";

const Testimonials = (props) => {
  const isDesktopOrLaptop = useMediaQuery({ minDeviceWidth: 1024 });
  const isTabletOrMobile = useMediaQuery({ maxDeviceWidth: 1023 });
  const [sliderfirst, setSliderfirst] = useState(props.testimonial);
  return (
    <section className="testimonials">
      <Row>
        {isDesktopOrLaptop && (
          <TestimonialsDesk testimonialDetails={sliderfirst} />
        )}
        {isTabletOrMobile && (
          <TestimonialsMob testimonialDetails={sliderfirst} />
        )}
      </Row>
    </section>
  );
};

export default Testimonials;
