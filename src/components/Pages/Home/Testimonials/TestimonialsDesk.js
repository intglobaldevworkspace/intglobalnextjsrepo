import React from "react";
import { Col } from "reactstrap";

export default class TestimonialsDesk extends React.Component {
  constructor(props) {
    super(props);

    const testimonialinit = this.props.testimonialDetails.clientTestimonials.map(
      (testiDet, index) => {
        return {
          id: testiDet.id,
          custappreciation: testiDet.custappreciation,
          custcompany: testiDet.custcompany,
          custdesignation: testiDet.custdesignation,
          custhighlightword: testiDet.custhighlightword,
          custimage: testiDet.custimage,
          custname: testiDet.custname,
          transDeg: 0,
          highlightcls: index == 2 ? "active" : "",
        };
      }
    );
    // console.log("New testi -", testimonialinit);
    let clientTestimonial = {
      ...this.props.testimonialDetails,
    };
    clientTestimonial.clientTestimonials = testimonialinit;

    let rotationArray = [];
    rotationArray[0] = [0, 25, 50, 75, 100];
    rotationArray[1] = [-25, 0, 25, 50, 75];
    rotationArray[2] = [-50, -25, 0, 25, 50];
    rotationArray[3] = [-75, -50, -25, 0, 25];
    rotationArray[4] = [-100, -75, -50, -25, 0];

    let ulrotationClass = [];
    ulrotationClass[0] = "tron";
    ulrotationClass[1] = "trtw";
    ulrotationClass[2] = "trth";
    ulrotationClass[3] = "trfr";
    ulrotationClass[4] = "trfv";

    this.state = {
      clientTestimonial: this.props.testimonialDetails,
      isClicked: true,
      transform: 0,
      ptrSetTo: 2,
      automaticCounter: 0,
      liRotation: [
        {
          transformOne: 50,
          translate: -43,
          transformTwo: -50,
        },
        {
          transformOne: 25,
          translate: -43,
          transformTwo: -25,
        },
        {
          transformOne: 0,
          translate: -43,
          transformTwo: -0,
        },
        {
          transformOne: -25,
          translate: -43.5,
          transformTwo: 25,
        },
        {
          transformOne: -50,
          translate: -43.5,
          transformTwo: 50,
        },
      ],

      ulrotateClass: ulrotationClass,
      ulClass: ulrotationClass[2],
      fullRotationArray: rotationArray,
      presentArray: rotationArray[2],
    };
  }

  //-- Automatic slider
  clientTestimonialAutomatic = (id) => {
    // console.log("id -", id);
    if (this.state.automaticCounter == 4) {
      // this.setState({ automaticCounter: 0 });
      let rotateByDeg = this.state.presentArray[id];
      this.setState(
        (prevState) => {
          return {
            transform: prevState.transform + rotateByDeg,
            ptrSetTo: id,
            presentArray: this.state.fullRotationArray[id],
            ulClass: this.state.ulrotateClass[id],
            automaticCounter: 0,
          };
        },
        () => {
          // console.log("state transform -", this.state.transform);
          // console.log("state ptrSetTo -", this.state.ptrSetTo);
          // console.log("state presentArray -", this.state.presentArray);
        }
      );
    } else {
      // this.setState(prevState => ({
      //   automaticCounter: prevState.automaticCounter + id
      // }));
      // this.setState({ automaticCounter: id + 1 });
      let rotateByDeg = this.state.presentArray[id];
      this.setState(
        (prevState) => {
          return {
            transform: prevState.transform + rotateByDeg,
            ptrSetTo: id,
            presentArray: this.state.fullRotationArray[id],
            ulClass: this.state.ulrotateClass[id],
            automaticCounter: id + 1,
          };
        },
        () => {
          // console.log("state transform -", this.state.transform);
          // console.log("state ptrSetTo -", this.state.ptrSetTo);
          // console.log("state presentArray -", this.state.presentArray);
        }
      );
    }
    // console.log(this.state.automaticCounter);
  };
  //-- Automatic slider

  changeClientTestimonial = (event, id) => {
    event.preventDefault();
    // console.log("id -", id);
    // console.log("presentArray -", this.state.presentArray);

    let rotateByDeg = this.state.presentArray[id];
    if (id == 4) {
      this.setState({ automaticCounter: 0 });
    } else {
      this.setState({ automaticCounter: id + 1 });
    }
    // console.log("automaticCounter -", this.state.automaticCounter);
    this.setState(
      (prevState) => {
        return {
          transform: prevState.transform + rotateByDeg,
          ptrSetTo: id,
          presentArray: this.state.fullRotationArray[id],
          ulClass: this.state.ulrotateClass[id],
        };
      },
      () => {
        // console.log("state transform -", this.state.transform);
        // console.log("state ptrSetTo -", this.state.ptrSetTo);
        // console.log("state presentArray -", this.state.presentArray);
      }
    );
    // console.log(this.state);
  };

  componentDidMount() {
    this.slidetimerid = setInterval(() => {
      this.clientTestimonialAutomatic(this.state.automaticCounter);
    }, 6000);
  }
  componentWillUnmount() {
    clearInterval(this.slidetimerid);
  }
  render() {
    //const isClick = this.state.isClicked;
    const urlprefix = process.env.servUploadImg;
    // console.log(this.state);
    const { clientTestimonial } = this.state;
    return (
      <>
        <Col md="6">
          <h2
            dangerouslySetInnerHTML={{
              __html: clientTestimonial.testimonialHeader,
            }}
          ></h2>
          <div className="testi-content">
            <div id="1" className="testi-content_inr active">
              <small>
                <span>
                  {
                    clientTestimonial.clientTestimonials[this.state.ptrSetTo]
                      .custhighlightword
                  }
                </span>
              </small>
              <p
                dangerouslySetInnerHTML={{
                  __html:
                    clientTestimonial.clientTestimonials[this.state.ptrSetTo]
                      .custappreciation,
                }}
              ></p>
            </div>
          </div>
        </Col>
        <Col md="6">
          <div className="testi-vertical-carousel-nav">
            <ul
              className={`testi-vertical-carousel-nav__list ${this.state.ulClass}`}
              style={{
                transform: `rotate(${this.state.transform}deg)`,
              }}
            >
              {/** ---------- Upper part -------------- */}
              {clientTestimonial.clientTestimonials.map(
                (testmonialDet, index) => {
                  return (
                    <li
                      key={index}
                      className={
                        index == this.state.ptrSetTo
                          ? "testi-vertical-carousel-nav__list-item active"
                          : "testi-vertical-carousel-nav__list-item"
                      }
                      style={{
                        transform: `rotate(${this.state.liRotation[index].transformOne}deg) translate(${this.state.liRotation[index].translate}vh) rotate(${this.state.liRotation[index].transformTwo}deg)`,
                      }}
                    >
                      <a
                        href={`#` + index}
                        title="Skip to first slide"
                        onClick={(e) => {
                          this.changeClientTestimonial(e, index);
                        }}
                      >
                        <img
                          src={urlprefix + testmonialDet.custimage}
                          className="img-fluid"
                          alt={testmonialDet.custname}
                        />
                      </a>
                      <div className="testi-vertical-carousel-nav__list-item_label">
                        {testmonialDet.custname}
                      </div>
                    </li>
                  );
                }
              )}

              {/** ---------- Upper part -------------- */}
            </ul>
            <div id="1" className="testi-contnt-visible active">
              <h3>
                {
                  clientTestimonial.clientTestimonials[this.state.ptrSetTo]
                    .custname
                }
              </h3>
              <p>
                {
                  clientTestimonial.clientTestimonials[this.state.ptrSetTo]
                    .custdesignation
                }
                {", "}
                {
                  clientTestimonial.clientTestimonials[this.state.ptrSetTo]
                    .custcompany
                }
              </p>
            </div>
          </div>
        </Col>
      </>
    );
  }
}
