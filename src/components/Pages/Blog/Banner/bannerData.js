/* eslint max-len: 0 */

export const blogbanner = [
  {
    id: '1',
    bannerUrl: '/static/images/banner/blog/banner-blog.jpg',
    bannerTitle: 'INT Blog'
  },
];

export default blogbanner;