import React from "react";
import { Col, Row } from "reactstrap";

// import bannerData from './bannerData';

const Banner = (props) => {
  const urlprefix = process.env.servUploadImg;
  return (
    <section className="hdBan_inner">
      <div className="hdBan_inner__teaser">
        <Col md="12">
          <Row>
            <div className="blog-ban">
              <figure>
                <img
                  src={urlprefix + props.blogbanner.bannerimageurl}
                  className="img-fluid"
                  alt={props.blogbanner.bannerimgtitle}
                />
              </figure>
            </div>
          </Row>
        </Col>
      </div>
    </section>
  );
};

export default Banner;
