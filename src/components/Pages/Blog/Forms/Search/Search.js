import React from 'react';

const Search = (props) => {
  return (
    <section className="blog-search">
      <form name="go_search" className="search">
        <input type="checkbox" id="toggleSearch" className="search__toggle" hidden />
        <div className="search__field">
          <label htmlFor="toggleSearch" className="search__label">
            <span className="search__close"></span>
          </label>
          <input type="text" className="search__input" placeholder="What are you looking for?" />
          <label htmlFor="toggleSearch" className="search__label">
            <div className="search__button">
              <div className="search__icon search__button--toggle"></div>
            </div>
            <button type="submit" name="go_search" className="search__button search__button--submit">
              <div className="search__icon"></div>
            </button>
          </label>
        </div>
      </form>
    </section>
  );
}

export default Search;