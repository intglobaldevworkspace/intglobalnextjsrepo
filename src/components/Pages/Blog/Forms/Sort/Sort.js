import React from 'react';

const Sort = (props) => {
  return (
    <section className="blog-sort">
      <form name="sorting" className="sort">
        <select>
          <option value="2017">2017</option>
          <option value="2018">2018</option>
          <option value="2019">2020</option>
          <option selected value="2020">2020</option>
        </select>
      </form>
    </section>
  );
}

export default Sort;