/* eslint max-len: 0 */

export const bloglist = [
  {
    id: '1',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-01.png',
    blogtitle: 'Emerging Digital Transformation Being Witnessed By The BFSI Sector',
    author: 'Abhishek Rungta',
    date: '02',
    month: 'May',
    catgry: ['FinTech']
  },
  {
    id: '2',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-02.png',
    blogtitle: 'How Banks Can Tackle the New Wave Of Digital Disruption With Managed Remote Teams?',
    author: 'Souvik Chaki',
    date: '04',
    month: 'May',
    catgry: ['FinTech', 'General']
  },
  {
    id: '3',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-03.png',
    blogtitle: 'Will Banks Live Up To Customer Convenience With A Remote Workforce In Future',
    author: 'Santanu Mukherjee',
    date: '06',
    month: 'May',
    catgry: ['FinTech', 'General']
  },
  {
    id: '4',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-04.png',
    blogtitle: 'Emerging Digital Transformation Being Witnessed By The BFSI Sector',
    author: 'Abhishek Rungta',
    date: '08',
    month: 'May',
    catgry: ['FinTech']
  },
  {
    id: '5',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-02.png',
    blogtitle: 'How Banks Can Tackle the New Wave Of Digital Disruption With Managed Remote Teams?',
    author: 'Souvik Chaki',
    date: '10',
    month: 'May',
    catgry: ['FinTech', 'General']
  },
  {
    id: '6',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-03.png',
    blogtitle: 'Will Banks Live Up To Customer Convenience With A Remote Workforce In Future',
    author: 'Santanu Mukherjee',
    date: '12',
    month: 'May',
    catgry: ['FinTech', 'General']
  },
  {
    id: '7',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-04.png',
    blogtitle: 'Emerging Digital Transformation Being Witnessed By The BFSI Sector',
    author: 'Abhishek Rungta',
    date: '14',
    month: 'May',
    catgry: ['FinTech']
  },
  {
    id: '8',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-02.png',
    blogtitle: 'How Banks Can Tackle the New Wave Of Digital Disruption With Managed Remote Teams?',
    author: 'Souvik Chaki',
    date: '16',
    month: 'May',
    catgry: ['FinTech', 'General']
  },
  {
    id: '9',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-03.png',
    blogtitle: 'Will Banks Live Up To Customer Convenience With A Remote Workforce In Future',
    author: 'Santanu Mukherjee',
    date: '18',
    month: 'May',
    catgry: ['FinTech', 'General']
  },
  {
    id: '10',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-01.png',
    blogtitle: 'Emerging Digital Transformation Being Witnessed By The BFSI Sector',
    author: 'Abhishek Rungta',
    date: '02',
    month: 'May',
    catgry: ['FinTech']
  },
  {
    id: '11',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-02.png',
    blogtitle: 'How Banks Can Tackle the New Wave Of Digital Disruption With Managed Remote Teams?',
    author: 'Souvik Chaki',
    date: '04',
    month: 'May',
    catgry: ['FinTech', 'General']
  },
  {
    id: '12',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-03.png',
    blogtitle: 'Will Banks Live Up To Customer Convenience With A Remote Workforce In Future',
    author: 'Santanu Mukherjee',
    date: '06',
    month: 'May',
    catgry: ['FinTech', 'General']
  },
  {
    id: '13',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-04.png',
    blogtitle: 'Emerging Digital Transformation Being Witnessed By The BFSI Sector',
    author: 'Abhishek Rungta',
    date: '08',
    month: 'May',
    catgry: ['FinTech']
  },
  {
    id: '14',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-02.png',
    blogtitle: 'How Banks Can Tackle the New Wave Of Digital Disruption With Managed Remote Teams?',
    author: 'Souvik Chaki',
    date: '10',
    month: 'May',
    catgry: ['FinTech', 'General']
  },
  {
    id: '15',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-03.png',
    blogtitle: 'Will Banks Live Up To Customer Convenience With A Remote Workforce In Future',
    author: 'Santanu Mukherjee',
    date: '12',
    month: 'May',
    catgry: ['FinTech', 'General']
  },
  {
    id: '16',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-04.png',
    blogtitle: 'Emerging Digital Transformation Being Witnessed By The BFSI Sector',
    author: 'Abhishek Rungta',
    date: '14',
    month: 'May',
    catgry: ['FinTech']
  },
  {
    id: '17',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-02.png',
    blogtitle: 'How Banks Can Tackle the New Wave Of Digital Disruption With Managed Remote Teams?',
    author: 'Souvik Chaki',
    date: '16',
    month: 'May',
    catgry: ['FinTech', 'General']
  },
  {
    id: '18',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-03.png',
    blogtitle: 'Will Banks Live Up To Customer Convenience With A Remote Workforce In Future',
    author: 'Santanu Mukherjee',
    date: '18',
    month: 'May',
    catgry: ['FinTech', 'General']
  },
  {
    id: '19',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-01.png',
    blogtitle: 'Emerging Digital Transformation Being Witnessed By The BFSI Sector',
    author: 'Abhishek Rungta',
    date: '02',
    month: 'May',
    catgry: ['FinTech']
  },
  {
    id: '20',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-02.png',
    blogtitle: 'How Banks Can Tackle the New Wave Of Digital Disruption With Managed Remote Teams?',
    author: 'Souvik Chaki',
    date: '04',
    month: 'May',
    catgry: ['FinTech', 'General']

  },
  {
    id: '21',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-03.png',
    blogtitle: 'Will Banks Live Up To Customer Convenience With A Remote Workforce In Future',
    author: 'Santanu Mukherjee',
    date: '06',
    month: 'May',
    catgry: ['FinTech', 'General']
  },
  {
    id: '22',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-04.png',
    blogtitle: 'Emerging Digital Transformation Being Witnessed By The BFSI Sector',
    author: 'Abhishek Rungta',
    date: '08',
    month: 'May',
    catgry: ['FinTech']
  },
  {
    id: '23',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-02.png',
    blogtitle: 'How Banks Can Tackle the New Wave Of Digital Disruption With Managed Remote Teams?',
    author: 'Souvik Chaki',
    date: '10',
    month: 'May',
    catgry: ['FinTech', 'General']
  },
  {
    id: '24',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-03.png',
    blogtitle: 'Will Banks Live Up To Customer Convenience With A Remote Workforce In Future',
    author: 'Santanu Mukherjee',
    date: '12',
    month: 'May',
    catgry: ['FinTech', 'General']
  },
  {
    id: '25',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-04.png',
    blogtitle: 'Emerging Digital Transformation Being Witnessed By The BFSI Sector',
    author: 'Abhishek Rungta',
    date: '14',
    month: 'May',
    catgry: ['FinTech']
  },
  {
    id: '26',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-02.png',
    blogtitle: 'How Banks Can Tackle the New Wave Of Digital Disruption With Managed Remote Teams?',
    author: 'Souvik Chaki',
    date: '16',
    month: 'May',
    catgry: ['FinTech', 'General']
  },
  {
    id: '27',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-03.png',
    blogtitle: 'Will Banks Live Up To Customer Convenience With A Remote Workforce In Future',
    author: 'Santanu Mukherjee',
    date: '18',
    month: 'May',
    catgry: ['FinTech', 'General']
  },
  {
    id: '28',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-01.png',
    blogtitle: 'Emerging Digital Transformation Being Witnessed By The BFSI Sector',
    author: 'Abhishek Rungta',
    date: '02',
    month: 'May',
    catgry: ['FinTech']
  },
  {
    id: '29',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-02.png',
    blogtitle: 'How Banks Can Tackle the New Wave Of Digital Disruption With Managed Remote Teams?',
    author: 'Souvik Chaki',
    date: '04',
    month: 'May',
    catgry: ['FinTech', 'General']
  },
  {
    id: '30',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-03.png',
    blogtitle: 'Will Banks Live Up To Customer Convenience With A Remote Workforce In Future',
    author: 'Santanu Mukherjee',
    date: '06',
    month: 'May',
    catgry: ['FinTech', 'General']
  },
  {
    id: '31',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-04.png',
    blogtitle: 'Emerging Digital Transformation Being Witnessed By The BFSI Sector',
    author: 'Abhishek Rungta',
    date: '08',
    month: 'May',
    catgry: ['FinTech']
  },
  {
    id: '32',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-02.png',
    blogtitle: 'How Banks Can Tackle the New Wave Of Digital Disruption With Managed Remote Teams?',
    author: 'Souvik Chaki',
    date: '10',
    month: 'May',
    catgry: ['FinTech', 'General']
  },
  {
    id: '33',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-03.png',
    blogtitle: 'Will Banks Live Up To Customer Convenience With A Remote Workforce In Future',
    author: 'Santanu Mukherjee',
    date: '12',
    month: 'May',
    catgry: ['FinTech', 'General']
  },
  {
    id: '34',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-04.png',
    blogtitle: 'Emerging Digital Transformation Being Witnessed By The BFSI Sector',
    author: 'Abhishek Rungta',
    date: '14',
    month: 'May',
    catgry: ['FinTech']
  },
  {
    id: '35',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-02.png',
    blogtitle: 'How Banks Can Tackle the New Wave Of Digital Disruption With Managed Remote Teams?',
    author: 'Souvik Chaki',
    date: '16',
    month: 'May',
    catgry: ['FinTech', 'General']
  },
  {
    id: '36',
    thumbUrl: '/static/images/banner/blog/thumbnail/blog-thumb-03.png',
    blogtitle: 'Will Banks Live Up To Customer Convenience With A Remote Workforce In Future',
    author: 'Santanu Mukherjee',
    date: '18',
    month: 'May',
    catgry: ['FinTech', 'General']
  },
];

export default bloglist;