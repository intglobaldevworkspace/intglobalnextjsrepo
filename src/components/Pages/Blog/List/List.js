import React from "react";
import { Row, Col } from "reactstrap";
import Link from "next/link";
// import Pagination from 'react-bootstrap/Pagination'
import Fade from "react-reveal/Fade";

// import listsData from "./listsData";

export default class List extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      items: this.props.bloglist,
      visible: 9,
      error: false,
    };
    console.log(this.props);
    this.loadMore = this.loadMore.bind(this);
  }

  loadMore() {
    this.setState((prev) => {
      return { visible: prev.visible + 3 };
    });
  }

  // componentDidMount() {
  //   fetch("https://jsonplaceholder.typicode.com/posts").then(
  //     res => res.json()
  //   ).then(res => {
  //     this.setState({
  //       items: res
  //     });
  //   }).catch(error => {
  //     console.error(error);
  //     this.setState({
  //       error: true
  //     });
  //   });
  // }

  render() {
    const urlprefix = process.env.servUploadImg;
    const { items, visible } = this.state;
    const blogSortedDESC = items.sort((a, b) => {
      let dateA = new Date(a.postaddedon);
      let dateB = new Date(b.postaddedon);
      return dateB - dateA;
    });
    return (
      <section className="blog-list">
        <Row>
          <div className="blog-wrapper" aria-live="polite">
            {blogSortedDESC.slice(0, visible).map((blogListDetail) => {
              return blogListDetail.blogarticlestatus ? (
                <Col
                  id={blogListDetail.id}
                  md="4"
                  key={blogListDetail.id}
                  className="blog fade-in"
                >
                  <Fade top>
                    <div className="blog-list__box">
                      <figure>
                        <img
                          src={urlprefix + blogListDetail.bloglistthumbnail.url}
                          className="img-fluid"
                          alt={blogListDetail.blogarticletitle}
                        />
                        {/* <figcaption className="event-date">
                            <h5><span>{listDetail.date}</span> {listDetail.month}</h5>
                          </figcaption> */}
                      </figure>
                      <div className="blog-info">
                        <Fade top>
                          <ul>
                            {blogListDetail.blogcategories.map((catgry) => {
                              return catgry.blogcatstatus ? (
                                <li key={catgry.id}>
                                  <span className="cat">
                                    {catgry.blogcatname}
                                  </span>
                                </li>
                              ) : null;
                            })}
                          </ul>
                          <h3>
                            <Link
                              href="/blog/[blogdet]"
                              as={`/blog/${blogListDetail.blogdetailurl}`}
                            >
                              <a title={blogListDetail.blogarticletitle}>
                                {blogListDetail.blogarticletitle}
                              </a>
                            </Link>
                          </h3>
                          <span className="author">
                            {blogListDetail.blogarticleauthor
                              .articleauthorstatus
                              ? blogListDetail.blogarticleauthor
                                  .articleauthorname
                              : null}
                          </span>
                        </Fade>
                      </div>
                    </div>
                  </Fade>
                </Col>
              ) : null;
            })}
          </div>
          <Fade top>
            {this.state.visible < this.state.items.length && (
              <button
                onClick={this.loadMore}
                type="button"
                className="load-more"
              >
                Load more
              </button>
            )}
          </Fade>
        </Row>
      </section>
    );
  }
}
