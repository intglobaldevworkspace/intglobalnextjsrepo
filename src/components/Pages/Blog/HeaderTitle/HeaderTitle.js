import React from "react";
import { Col, Row } from "reactstrap";
import Fade from "react-reveal/Fade";

const HeaderTitle = (props) => {
  // console.log(props);
  return (
    <section className="blog-title">
      <Col md="5">
        <Row>
          <Fade left>
            <h1>{props.titledetail}</h1>
          </Fade>
        </Row>
      </Col>
    </section>
  );
};

export default HeaderTitle;
