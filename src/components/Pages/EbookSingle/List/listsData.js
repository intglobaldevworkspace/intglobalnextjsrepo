/* eslint max-len: 0 */

export const ebooklist = [
  {
    id: '1',
    thumbUrl: '/static/images/ebook/thumb/insurance-digital-claim-journey-analytics-overlay_ebook.jpg',
    ebooktitle: 'Insurance Digital Claim Journey – Analytics Overlay'
  },
  {
    id: '2',
    thumbUrl: '/static/images/ebook/thumb/twelve-pointer-action-plan-for-during-and-post-covid-19_ebook.jpg',
    ebooktitle: '12 Pointer Action Plan Before, During And After Covid-19'
  },
  {
    id: '3',
    thumbUrl: '/static/images/ebook/thumb/leaving-the-office-behind_the-new-normal-ebook.jpg',
    ebooktitle: 'Leaving The Office Behind : The New Normal'
  },
  {
    id: '4',
    thumbUrl: '/static/images/ebook/thumb/comparison-of-mobile-applications-of-top-indian-banks-ebook.jpg',
    ebooktitle: 'Comparison of Mobile Applications of Top Indian Banks'
  },
  {
    id: '5',
    thumbUrl: '/static/images/ebook/thumb/outsourcing-101-ebook.jpg',
    ebooktitle: 'Outsourcing 101'
  },
  {
    id: '6',
    thumbUrl: '/static/images/ebook/thumb/deep-learning-101-ebook.jpg',
    ebooktitle: 'Deep Learning 101'
  },
  {
    id: '7',
    thumbUrl: '/static/images/ebook/thumb/current-trends-in-CMS-platforms-2019-ebook.jpg',
    ebooktitle: 'Current Trends in CMS Platforms 2019'
  },
  {
    id: '8',
    thumbUrl: '/static/images/ebook/thumb/ten-mobile-app-development-technologies-to-watch-out-in-2019-ebook.jpg',
    ebooktitle: '10 Mobile App Development Technologies to Watch Out in 2019'
  },
  {
    id: '9',
    thumbUrl: '/static/images/ebook/thumb/outsourcing-trends-in-insurance-industry-ebook.jpg',
    ebooktitle: 'Outsourcing Trends in Insurance Industry'
  },
  {
    id: '10',
    thumbUrl: '/static/images/ebook/thumb/migrating-application-from-zend-2.X-to-3.0-ebook.jpg',
    ebooktitle: 'Migrating an Application from Zend 2.X to 3.0'
  },
  {
    id: '11',
    thumbUrl: '/static/images/ebook/thumb/top-ten-digital-trends-in-banking-ebook.jpg',
    ebooktitle: 'Top Ten Digital Trends in Banking – 2018 And Beyond'
  },
  {
    id: '12',
    thumbUrl: '/static/images/ebook/thumb/ecm-ebook.jpg',
    ebooktitle: 'A Detailed Study on Enterprise Content Management (ECM)'
  },
  {
    id: '13',
    thumbUrl: '/static/images/ebook/thumb/a-quick-guide-to-android-instant-apps-ebook.jpg',
    ebooktitle: 'A Quick Guide to Android Instant Apps'
  },
  {
    id: '14',
    thumbUrl: '/static/images/ebook/thumb/socia-media-not-just-a-digital-marketing-channel-ebook.jpg',
    ebooktitle: 'Social media – Not just a digital marketing channel'
  },
  {
    id: '15',
    thumbUrl: '/static/images/ebook/thumb/mobile-app-development-trends-in-2018-ebook.jpg',
    ebooktitle: 'Mobile App Development Trends in 2018'
  },
  {
    id: '16',
    thumbUrl: '/static/images/ebook/thumb/insurance-digital-claim-journey-analytics-overlay_ebook.jpg',
    ebooktitle: 'The Big CMS Comparison Report'
  },
  {
    id: '17',
    thumbUrl: '/static/images/ebook/thumb/twelve-pointer-action-plan-for-during-and-post-covid-19_ebook.jpg',
    ebooktitle: 'State of Digital: 2018'
  },
  {
    id: '18',
    thumbUrl: '/static/images/ebook/thumb/leaving-the-office-behind_the-new-normal-ebook.jpg',
    ebooktitle: 'Social Media Tools'
  },
  {
    id: '19',
    thumbUrl: '/static/images/ebook/thumb/comparison-of-mobile-applications-of-top-indian-banks-ebook.jpg',
    ebooktitle: 'Register As A Seller in Top 50 Marketplaces of The World'
  },
  {
    id: '20',
    thumbUrl: '/static/images/ebook/thumb/outsourcing-101-ebook.jpg',
    ebooktitle: 'Top 100 InsurTech Influencers'
  },
  {
    id: '21',
    thumbUrl: '/static/images/ebook/thumb/deep-learning-101-ebook.jpg',
    ebooktitle: 'Digital Marketing Audit'
  },
  {
    id: '22',
    thumbUrl: '/static/images/ebook/thumb/current-trends-in-CMS-platforms-2019-ebook.jpg',
    ebooktitle: 'Top 30 Influencers in Insurtech'
  },
  {
    id: '23',
    thumbUrl: '/static/images/ebook/thumb/ten-mobile-app-development-technologies-to-watch-out-in-2019-ebook.jpg',
    ebooktitle: 'Fintech and Insurtech Innovations'
  },
  {
    id: '24',
    thumbUrl: '/static/images/ebook/thumb/outsourcing-trends-in-insurance-industry-ebook.jpg',
    ebooktitle: 'Top 35 Digital Innovations in FMCG Industry'
  },
  {
    id: '25',
    thumbUrl: '/static/images/ebook/thumb/migrating-application-from-zend-2.X-to-3.0-ebook.jpg',
    ebooktitle: 'Checklist For Improving Your Website Speed and Performance'
  },
  {
    id: '26',
    thumbUrl: '/static/images/ebook/thumb/top-ten-digital-trends-in-banking-ebook.jpg',
    ebooktitle: 'How to Improve Your Website Security Checklist'
  },
  {
    id: '27',
    thumbUrl: '/static/images/ebook/thumb/ecm-ebook.jpg',
    ebooktitle: 'How Can Predictive Analysis Help You Grow Your Business?'
  },
  {
    id: '28',
    thumbUrl: '/static/images/ebook/thumb/a-quick-guide-to-android-instant-apps-ebook.jpg',
    ebooktitle: 'Agile Methodology'
  },
  {
    id: '29',
    thumbUrl: '/static/images/ebook/thumb/socia-media-not-just-a-digital-marketing-channel-ebook.jpg',
    ebooktitle: 'Magento 2.0 Vs Magento 1.9'
  },
  {
    id: '30',
    thumbUrl: '/static/images/ebook/thumb/mobile-app-development-trends-in-2018-ebook.jpg',
    ebooktitle: 'A Quick Guide To Website Maintenance'
  },
];

export default ebooklist;