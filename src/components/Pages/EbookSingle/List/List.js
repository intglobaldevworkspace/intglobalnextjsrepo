import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import Link from "next/link";
import Fade from "react-reveal/Fade";

import CategoryList from "../CategoryList/CategoryList";
// import listsData from "./listsData";
import axios from "axios";

export default class List extends Component {
  constructor(props) {
    super(props);

    this.state = {
      items: this.props.listpage.ebooklisting,
      visible: 9,
      error: false,
      ebookcats: this.props.listpage.ebookcat,
      listpageheader: this.props.listpage.ebooklistheader,
    };

    this.loadMore = this.loadMore.bind(this);
  }

  loadMore() {
    this.setState((prev) => {
      return { visible: prev.visible + 3 };
    });
  }

  // componentDidMount() {
  //   fetch("https://jsonplaceholder.typicode.com/posts").then(
  //     res => res.json()
  //   ).then(res => {
  //     this.setState({
  //       items: res
  //     });
  //   }).catch(error => {
  //     console.error(error);
  //     this.setState({
  //       error: true
  //     });
  //   });
  // }

  fetchSpecificEbook = (e) => {
    const urlprefix = process.env.servUploadImg;
    // console.log(e.target.value);
    if (e.target.value !== 0 || e.target.value !== "") {
      const headers = {
        "Content-Type": "application/json",
      };

      axios
        .get(urlprefix + "/ebookcats/" + e.target.value)
        .then((response) => {
          // console.log(response.data);
          let fullinfodet = [];
          fullinfodet = response.data.ebookcatstatus
            ? response.data.ebooklistcolls.map((catwiseitems) => {
                return {
                  id: catwiseitems.id,
                  ebookshorttitle: catwiseitems.ebookshorttitle,
                  ebooksingleurl: catwiseitems.ebooksingleurl,
                  ebookstatus: catwiseitems.ebookstatus,
                  ebooklistpageimgthumb: catwiseitems.ebooklistpageimg.url,
                };
              })
            : [];

          this.setState({ items: fullinfodet, selval: response.data.id });
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };

  fetchallrecords = (e) => {
    const urlprefix = process.env.servUploadImg;
    // console.log(e.target.value);
    e.preventDefault();
    axios
      .get(urlprefix + "/ebooklistcolls")
      .then((response) => {
        // console.log("RESP-", response.data);
        let ebookdet = response.data
          .filter((respObj) => respObj.ebookstatus)
          .map((ebkObj) => {
            return {
              ebookstatus: ebkObj.ebookstatus,
              ebookcats: ebkObj.ebookcats,
              ebookshorttitle: ebkObj.ebookshorttitle,
              ebooksingleurl: ebkObj.ebooksingleurl,
              ebooklistpageimgthumb: ebkObj.ebooklistpageimg.url,
            };
          });

        this.setState({ items: ebookdet, selval: "" });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  render() {
    const { ebookcats, listpageheader, items, selval } = this.state;
    // console.log(items);
    const urlprefix = process.env.servUploadImg;
    return (
      <section className="ebook-list">
        <Fade top>
          <h2>{listpageheader}</h2>
        </Fade>
        <Row>
          <Col md="12">
            <CategoryList
              catl={ebookcats}
              selval={selval}
              catSpecificValues={this.fetchSpecificEbook}
              getallrecordsforcat={this.fetchallrecords}
            />
          </Col>
          <div className="ebook-wrapper" aria-live="polite">
            {items && items.length > 0 ? (
              items.slice(0, this.state.visible).map((item, index) => {
                return (
                  <Col md="4" className="resource-ebook fade-in" key={index}>
                    <Fade top>
                      <div className="resource-ebook_box">
                        <figure>
                          <Link
                            href="/ebook/ebookdetail"
                            as={`/ebook/${item.ebooksingleurl}`}
                          >
                            <a title={item.ebookshorttitle}>
                              <img
                                src={urlprefix + item.ebooklistpageimgthumb}
                                className="img-fluid"
                                alt={item.ebookshorttitle}
                              />
                            </a>
                          </Link>
                        </figure>
                      </div>
                    </Fade>
                  </Col>
                );
              })
            ) : (
              <Col md="4" className="resource-infographic fade-in">
                <Fade bottom>
                  <div className="resource-infographic_box">
                    <div className="text-center">No Records Found</div>
                  </div>
                </Fade>
              </Col>
            )}
            <Fade top>
              {this.state.visible < items.length && (
                <button
                  onClick={this.loadMore}
                  type="button"
                  className="load-more"
                >
                  Load more
                </button>
              )}
            </Fade>
          </div>
        </Row>
      </section>
    );
  }
}
