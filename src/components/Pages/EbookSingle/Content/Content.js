import React, { useState } from "react";
import { Col, Row } from "reactstrap";
import Fade from "react-reveal/Fade";

const Content = (props) => {
  const [contState, setContState] = useState(props.ebookdet[0]);
  // console.log("CONT -", contState.ebookdetailpagecontent);
  return (
    <section className="ebook-content">
      <Col md="10">
        <Row>
          <Fade top>
            <h2>{contState.ebookdetailpagecontent.ebkdetailpageheader}</h2>
          </Fade>
        </Row>
      </Col>
      <Col md="9">
        <Row>
          <Fade top>
            <h3>{contState.ebookdetailpagecontent.ebkdetailsubheadertwo}</h3>
            <p>{contState.ebookdetailpagecontent.ebkdetailsubheaderthree}</p>
            <h4>{contState.ebookdetailpagecontent.ebkdetailsubheaderfour}</h4>
            <ul>
              {contState.ebookdetailpagecontent.ebkdetailexpect &&
              contState.ebookdetailpagecontent.ebkdetailexpect > 0
                ? contState.ebookdetailpagecontent.ebkdetailexpect.map(
                    (ebkObj) => {
                      return (
                        <li key={ebkObj.id}>{ebkObj.expectationbulletpoint}</li>
                      );
                    }
                  )
                : null}
            </ul>
          </Fade>
        </Row>
      </Col>
    </section>
  );
};

export default Content;
