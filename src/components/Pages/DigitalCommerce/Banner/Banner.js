import React from "react";
import { Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const Banner = (props) => {
  // console.log(props.dchdrban);
  const { dchdrban } = props;
  return (
    <section className="hdBan_inner">
      <div className="hdBan_inner__teaser">
        <Col md="7">
          <h1>
            <Fade left>
              <div
                dangerouslySetInnerHTML={{
                  __html: dchdrban.headerbannertxtone,
                }}
              ></div>
            </Fade>
            <Fade left>
              <div>{dchdrban.headerbannertxttwo}</div>
            </Fade>
            <Fade left>
              <strong>{dchdrban.headerbannertextthree}</strong>
            </Fade>
          </h1>
        </Col>
      </div>
    </section>
  );
};

export default Banner;
