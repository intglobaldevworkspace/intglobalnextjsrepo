/* eslint max-len: 0 */

export const platform = [
  {
    id: '1',
    iconUrlP: '/static/images/ecommerce/dig-commerce/platforms/big-commerce.png',
    iconUrlW: '/static/images/ecommerce/dig-commerce/platforms/big-commerce.webp',
    title: 'Big Commerce'
  },
  {
    id: '2',
    iconUrlP: '/static/images/ecommerce/dig-commerce/platforms/shopify.png',
    iconUrlW: '/static/images/ecommerce/dig-commerce/platforms/shopify.webp',
    title: 'Shopify'
  },
  {
    id: '3',
    iconUrlP: '/static/images/ecommerce/dig-commerce/platforms/3d-cart.png',
    iconUrlW: '/static/images/ecommerce/dig-commerce/platforms/3d-cart.webp',
    title: '3d Cart'
  },
  {
    id: '4',
    iconUrlP: '/static/images/ecommerce/dig-commerce/platforms/woo-commerce.png',
    iconUrlW: '/static/images/ecommerce/dig-commerce/platforms/woo-commerce.webp',
    title: 'Woo Commerce'
  },
  {
    id: '5',
    iconUrlP: '/static/images/ecommerce/dig-commerce/platforms/volusion.png',
    iconUrlW: '/static/images/ecommerce/dig-commerce/platforms/volusion.webp',
    title: 'Volusion'
  },
  {
    id: '6',
    iconUrlP: '/static/images/ecommerce/dig-commerce/platforms/presta-shop.png',
    iconUrlW: '/static/images/ecommerce/dig-commerce/platforms/presta-shop.webp',
    title: 'Presta Shop'
  },
  {
    id: '7',
    iconUrlP: '/static/images/ecommerce/dig-commerce/platforms/weebly.png',
    iconUrlW: '/static/images/ecommerce/dig-commerce/platforms/weebly.webp',
    title: 'Weebly'
  },
  {
    id: '8',
    iconUrlP: '/static/images/ecommerce/dig-commerce/platforms/squarespace.png',
    iconUrlW: '/static/images/ecommerce/dig-commerce/platforms/squarespace.webp',
    title: 'Square Space'
  },
  {
    id: '9',
    iconUrlP: '/static/images/ecommerce/dig-commerce/platforms/magento.png',
    iconUrlW: '/static/images/ecommerce/dig-commerce/platforms/magento.webp',
    title: 'Magento'
  },
];

export default platform;
