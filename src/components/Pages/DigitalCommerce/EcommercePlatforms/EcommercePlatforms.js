import React from "react";
import { Row } from "reactstrap";
import Fade from "react-reveal/Fade";

// import ecommercePlatformData from './ecommercePlatformData';

const EcommercePlatforms = (props) => {
  const { ecommplatform } = props;
  // console.log(ecommplatform);
  const urlprefix = process.env.servUploadImg;
  return (
    <section className="ecom-plattforms">
      <Fade bottom>
        <h2>{ecommplatform.ecommplatform}</h2>
      </Fade>
      <Row>
        <ul className="plattform-item">
          {ecommplatform.ecommplatformcontent.map((ecommercePlatDetail) => {
            return (
              <li id={ecommercePlatDetail.id} key={ecommercePlatDetail.id}>
                <Fade bottom>
                  <figure>
                    <img
                      src={urlprefix + ecommercePlatDetail.ecommerceimage.url}
                      className="img-fluid"
                      alt={ecommercePlatDetail.ecommercetitle}
                    />
                  </figure>
                </Fade>
              </li>
            );
          })}
        </ul>
      </Row>
    </section>
  );
};

export default EcommercePlatforms;
