import React, { useState } from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";
import Contactus from "../../../shared/Contactus";
// import featureEcomData from './featureEcomData';

const FeaturesEcommerce = (props) => {
  const [featureecommState, setfeatureecommState] = useState({
    isOpen: false,
    showContactusForm: true,
    showContactusSubmitted: false,
    ContactusSubmitfailed: false,
    showSubmitLoader: false,

    contactuspopup: props.featuresecomm.contactuspopup,
  });

  const { featuresecomm } = props;
  // console.log(featuresecomm);
  const urlprefix = process.env.servUploadImg;

  const toggleModal = () => {
    setfeatureecommState((prevState) => ({
      ...prevState,
      isOpen: !prevState.isOpen,
    }));
  };

  const cntpopup = {
    isOpen: featureecommState.isOpen,
    showContactusForm: featureecommState.showContactusForm,
    showContactusSubmitted: featureecommState.showContactusSubmitted,
    ContactusSubmitfailed: featureecommState.ContactusSubmitfailed,
    showSubmitLoader: featureecommState.showSubmitLoader,
    contactusPopup: featureecommState.contactuspopup,
  };
  return (
    <section className="ecommerce">
      <Row>
        <Fade bottom>
          <h2>{featuresecomm.featureecommheader}</h2>
        </Fade>
        <Col md="12">
          <ul className="ecommerce-item">
            {featuresecomm.featureecommcontent.map((featureEcomDetail) => {
              return (
                <li id={featureEcomDetail.id} md="3" key={featureEcomDetail.id}>
                  <Fade bottom>
                    <div className="hvr-blocks-out">
                      <figure>
                        <img
                          src={
                            urlprefix + featureEcomDetail.featureecommimage.url
                          }
                          className="img-fluid"
                          alt={featureEcomDetail.featurecommtitle}
                        />
                      </figure>
                      <h6>{featureEcomDetail.featurecommtitle}</h6>
                      <div>
                        <p>{featureEcomDetail.featurecommtooltiptext}</p>
                      </div>
                      <a href="#"></a>
                    </div>
                  </Fade>
                </li>
              );
            })}
          </ul>
          <Fade bottom>
            {featuresecomm.featureecommletustalk.letustalkstatus ? (
              <a
                href={featuresecomm.featureecommletustalk.letustalkurl}
                className="btn link"
                onClick={(e) => {
                  e.preventDefault();
                  // toggle();
                  toggleModal();
                }}
              >
                {featuresecomm.featureecommletustalk.letustalktext}
              </a>
            ) : null}
          </Fade>
        </Col>
      </Row>
      {featureecommState.isOpen ? (
        <Contactus
          cntpopup={{
            ...cntpopup,
            contactText: featuresecomm.featureecommletustalk.letustalktext,
          }}
          handler={toggleModal}
        />
      ) : null}
    </section>
  );
};

export default FeaturesEcommerce;
