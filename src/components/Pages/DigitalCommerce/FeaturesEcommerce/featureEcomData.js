/* eslint max-len: 0 */

export const featureecommerce = [
  {
    id: '1',
    iconUrl: '/static/images/icons/dig-commerce/feature-ecommerce/third-party-integrations.svg',
    tooltipID: 'Toolti01',
    tooltipText: 'Become easy to reach with enterprise mobility so your content and information can be availed anywhere and at anytime.',
    title: '3rd Party Integrations - Payment, Shipping'
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/dig-commerce/feature-ecommerce/multiple-payment.svg',
    tooltipID: 'Toolti02',
    tooltipText: 'Hinders any unauthorized access protecting sensitive information while also clearing any malware from the system.',
    title: 'Multiple payment options (Credit card, PayPal, PO, Terms, etc.)'
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/dig-commerce/feature-ecommerce/easy-to-use-checkout.svg',
    tooltipID: 'Toolti03',
    tooltipText: 'The key office applications can be extended to employees’ devices, allowing them to securely carry out important business functions anytime and anywhere.',
    title: 'An easy-to-use checkout'
  },
  {
    id: '4',
    iconUrl: '/static/images/icons/dig-commerce/feature-ecommerce/content-management-capab.svg',
    tooltipID: 'Toolti04',
    tooltipText: 'Easy accessibility anywhere irrespective of time and place making content sharing possible with just a few clicks.',
    title: 'Content management capabilities'
  },
  {
    id: '5',
    iconUrl: '/static/images/icons/dig-commerce/feature-ecommerce/promotion-discount-code.svg',
    tooltipID: 'Toolti05',
    tooltipText: 'With the whole business working as a single unit, the ability to deliver great customer experience is magnified.',
    title: 'Promotion and discount code tools'
  },
  {
    id: '6',
    iconUrl: '/static/images/icons/dig-commerce/feature-ecommerce/search-engine-optimized-code.svg',
    tooltipID: 'Toolti06',
    tooltipText: 'Using enterprise mobility services, big enterprises and companies can optimize their workflows, and broaden their reach effectively increasing ROI',
    title: 'Search engine optimized code and layout'
  },
  {
    id: '7',
    iconUrl: '/static/images/icons/dig-commerce/feature-ecommerce/analytics-reporting-tools.svg',
    tooltipID: 'Toolti06',
    tooltipText: 'Using enterprise mobility services, big enterprises and companies can optimize their workflows, and broaden their reach effectively increasing ROI',
    title: 'Analytics and Reporting tools'
  },
  {
    id: '8',
    iconUrl: '/static/images/icons/dig-commerce/feature-ecommerce/integrated-blog.svg',
    tooltipID: 'Toolti06',
    tooltipText: 'Using enterprise mobility services, big enterprises and companies can optimize their workflows, and broaden their reach effectively increasing ROI',
    title: 'An integrated blog or articles section'
  },
  {
    id: '9',
    iconUrl: '/static/images/icons/dig-commerce/feature-ecommerce/email-marketing.svg',
    tooltipID: 'Toolti06',
    tooltipText: 'Using enterprise mobility services, big enterprises and companies can optimize their workflows, and broaden their reach effectively increasing ROI',
    title: 'Email marketing integration'
  },
];

export default featureecommerce;
