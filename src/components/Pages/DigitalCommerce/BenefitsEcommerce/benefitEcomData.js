/* eslint max-len: 0 */

export const benefitecommerce = [
  {
    id: '1',
    iconUrl: '/static/images/icons/dig-commerce/benefits-ecommerce/global-reach.svg',
    tooltipID: 'Toolti01',
    tooltipText: 'Become easy to reach with enterprise mobility so your content and information can be availed anywhere and at anytime.',
    title: 'Global Reach and Improved Customer Base'
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/dig-commerce/benefits-ecommerce/new-customers.svg',
    tooltipID: 'Toolti02',
    tooltipText: 'Hinders any unauthorized access protecting sensitive information while also clearing any malware from the system.',
    title: 'Gain New Customers with Search Engine Visibility'
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/dig-commerce/benefits-ecommerce/lower-costs.svg',
    tooltipID: 'Toolti03',
    tooltipText: 'The key office applications can be extended to employees’ devices, allowing them to securely carry out important business functions anytime and anywhere.',
    title: 'Lower Costs'
  },
  {
    id: '4',
    iconUrl: '/static/images/icons/dig-commerce/benefits-ecommerce/eliminate-travel-time.svg',
    tooltipID: 'Toolti04',
    tooltipText: 'Easy accessibility anywhere irrespective of time and place making content sharing possible with just a few clicks.',
    title: 'Eliminate Travel Time and Cost'
  },
  {
    id: '5',
    iconUrl: '/static/images/icons/dig-commerce/benefits-ecommerce/provide-comparison-shopping.svg',
    tooltipID: 'Toolti05',
    tooltipText: 'With the whole business working as a single unit, the ability to deliver great customer experience is magnified.',
    title: 'Provide Comparison Shopping'
  },
  {
    id: '6',
    iconUrl: '/static/images/icons/dig-commerce/benefits-ecommerce/enable-deals.svg',
    tooltipID: 'Toolti06',
    tooltipText: 'Using enterprise mobility services, big enterprises and companies can optimize their workflows, and broaden their reach effectively increasing ROI',
    title: 'Enable Deals, Bargains, Coupons, and Group Buying'
  },
  {
    id: '7',
    iconUrl: '/static/images/icons/dig-commerce/benefits-ecommerce/create-targeted-communication.svg',
    tooltipID: 'Toolti06',
    tooltipText: 'Using enterprise mobility services, big enterprises and companies can optimize their workflows, and broaden their reach effectively increasing ROI',
    title: 'Create Targeted Communication'
  },
  {
    id: '8',
    iconUrl: '/static/images/icons/dig-commerce/benefits-ecommerce/open-all-times.svg',
    tooltipID: 'Toolti06',
    tooltipText: 'Using enterprise mobility services, big enterprises and companies can optimize their workflows, and broaden their reach effectively increasing ROI',
    title: 'Remain Open All the Time 24x7x365'
  },
  {
    id: '9',
    iconUrl: '/static/images/icons/dig-commerce/benefits-ecommerce/create-markets.svg',
    tooltipID: 'Toolti06',
    tooltipText: 'Using enterprise mobility services, big enterprises and companies can optimize their workflows, and broaden their reach effectively increasing ROI',
    title: 'Create Markets for Niche Products'
  },
];

export default benefitecommerce;
