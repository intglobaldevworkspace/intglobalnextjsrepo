import React, { useState } from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";
import Contactus from "../../../shared/Contactus";
// import benefitEcomData from './benefitEcomData';

const BenefitsEcommerce = (props) => {
  const { benefits } = props;
  // console.log(benefits);

  const [benefitsecommstate, setbenefitecommstate] = useState({
    isOpen: false,
    showContactusForm: true,
    showContactusSubmitted: false,
    ContactusSubmitfailed: false,
    showSubmitLoader: false,

    contactuspopup: props.benefits.contactuspopup,
  });
  const urlprefix = process.env.servUploadImg;

  const toggleModal = () => {
    setbenefitecommstate((prevState) => ({
      ...prevState,
      isOpen: !prevState.isOpen,
      showContactusForm: true,
      showContactusSubmitted: false,
      ContactusSubmitfailed: false,
      showSubmitLoader: false,
    }));
  };

  const cntpopup = {
    isOpen: benefitsecommstate.isOpen,
    showContactusForm: benefitsecommstate.showContactusForm,
    showContactusSubmitted: benefitsecommstate.showContactusSubmitted,
    ContactusSubmitfailed: benefitsecommstate.ContactusSubmitfailed,
    showSubmitLoader: benefitsecommstate.showSubmitLoader,
    contactusPopup: benefitsecommstate.contactuspopup,
  };
  return (
    <section className="ecommerce">
      <Row>
        <Fade bottom>
          <h2>{benefits.beneheader}</h2>
        </Fade>
        <Col md="12">
          <ul className="ecommerce-item">
            {benefits.benecontent.map((benefitEcomDetail) => {
              return (
                <li id={benefitEcomDetail.id} md="3" key={benefitEcomDetail.id}>
                  <Fade bottom>
                    <div className="hvr-blocks-out">
                      <figure>
                        <img
                          src={
                            urlprefix + benefitEcomDetail.benefitsecommimage.url
                          }
                          className="img-fluid"
                          alt={benefitEcomDetail.benefitsecommtitle}
                        />
                      </figure>
                      <h6>{benefitEcomDetail.benefitsecommtitle}</h6>
                      <div>
                        <p>{benefitEcomDetail.benefitsecommtooltiptext}</p>
                      </div>
                      <a href="#"></a>
                    </div>
                  </Fade>
                </li>
              );
            })}
          </ul>
          <Fade bottom>
            {benefits.beneletustalk.benefitsletustalkstatus ? (
              <a
                href={benefits.beneletustalk.benefitsletustalkurl}
                className="btn link"
                onClick={(e) => {
                  e.preventDefault();
                  // toggle();
                  toggleModal();
                }}
              >
                {benefits.beneletustalk.benefitsletustalktext}
              </a>
            ) : null}
          </Fade>
        </Col>
      </Row>
      {benefitsecommstate.isOpen ? (
        <Contactus
          cntpopup={{
            ...cntpopup,
            contactText: benefits.beneletustalk.benefitsletustalktext,
          }}
          handler={toggleModal}
        />
      ) : null}
    </section>
  );
};

export default BenefitsEcommerce;
