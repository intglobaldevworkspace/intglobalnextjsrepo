/* eslint max-len: 0 */

export const services = [
  {
    id: '1',
    iconUrl: '/static/images/icons/dig-commerce/services/consulting.svg',
    title: 'Consulting',
    tooltipID: 'Toolti01',
    tooltipText: 'Our Strategy & Consulting experts will guide you through a comprehensive growth strategy that includes (but not limited to) sales automation, brand positioning, technology, PR and content strategy.'
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/dig-commerce/services/custom-ecommerce.svg',
    title: 'Custom E-Commerce',
    tooltipID: 'Toolti02',
    tooltipText: 'Our B2B and B2C Enterprise-grade ecommerce development service is Custom-built to give your store a unique edge that stands apart from competitors.'
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/dig-commerce/services/mobile-commerce.svg',
    title: 'Mobile Commerce',
    tooltipID: 'Toolti03',
    tooltipText: 'Our Ecommerce solutions are built for Mobile Commerce Enabled with Sticky Experiences both on iOS, Android platforms.'
  },
  {
    id: '4',
    iconUrl: '/static/images/icons/dig-commerce/services/intelligence.svg',
    title: 'Intelligence',
    tooltipID: 'Toolti04',
    tooltipText: 'We provide intelligent Analytics that helps to optimize operation, maximise Revenue, improve usability, and increase conversion.'
  },
  {
    id: '5',
    iconUrl: '/static/images/icons/dig-commerce/services/loyalty-program.svg',
    title: 'Loyalty Program',
    tooltipID: 'Toolti05',
    tooltipText: 'Our custom Loyalty Management system helps you create Discounts and Offers seamlessly and distribute it through Email Marketing and Personalize your Shopping experience.'
  },
  {
    id: '6',
    iconUrl: '/static/images/icons/dig-commerce/services/integrations.svg',
    title: 'Integrations',
    tooltipID: 'Toolti06',
    tooltipText: 'Our system is built to integrate with any solutions be it you ERP, CRM, PMS, or your CMS.'
  },
  {
    id: '7',
    iconUrl: '/static/images/icons/dig-commerce/services/migration.svg',
    title: 'Migration',
    tooltipID: 'Toolti07',
    tooltipText: 'We offer data Migration services to precisely relocate your data with complete integrity and security, without any disruption to your online business.'
  },
  {
    id: '8',
    iconUrl: '/static/images/icons/dig-commerce/services/content-creation.svg',
    title: 'Content Creation',
    tooltipID: 'Toolti08',
    tooltipText: 'Our services include creating content and information your users need, in a way that is easy for them to understand and relate to, and improve conversion.'
  },
  {
    id: '9',
    iconUrl: '/static/images/icons/dig-commerce/services/cloud.svg',
    title: 'Cloud',
    tooltipID: 'Toolti09',
    tooltipText: 'Our e-commerce systems can be hosted on Cloud as well as on-premise servers.'
  },
];

export default services;
