import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

// import servicesData from "./servicesData";

const Services = (props) => {
  // const [tooltipOpen, setTooltipOpen] = useState(false);
  // const toggle = () => setTooltipOpen(!tooltipOpen);
  const { servicecontent } = props;
  // console.log(servicecontent);
  const urlprefix = process.env.servUploadImg;
  return (
    <section className="our-services">
      <Fade bottom>
        <h2>{servicecontent.servheader}</h2>
      </Fade>
      <Row>
        <Col md="12">
          {servicecontent.servcontent.map((serviceDetail) => {
            return (
              <Col
                id={serviceDetail.id}
                md="4"
                className="srvc-item"
                key={serviceDetail.id}
              >
                <Fade bottom>
                  <div className="hvr-blocks-out">
                    <figure>
                      <img
                        src={urlprefix + serviceDetail.servicecontentimage.url}
                        className="img-fluid"
                        alt={serviceDetail.servicecontenttitle}
                      />
                    </figure>
                    <h6>{serviceDetail.servicecontenttitle}</h6>
                    <p>{serviceDetail.content}</p>
                    <div>
                      <p>{serviceDetail.servicecontenttooltiptext}</p>
                    </div>
                    <a href="#"></a>
                  </div>
                </Fade>
              </Col>
            );
          })}
        </Col>
      </Row>
    </section>
  );
};

export default Services;
