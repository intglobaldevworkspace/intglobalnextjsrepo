import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const Solutions = (props) => {
  const { solcont } = props;
  return (
    <section className="solutions">
      <Fade bottom>
        <h2>{solcont.solutiontextone}</h2>
        <Row>
          <Col md="12">
            <p>{solcont.solutiontexttwo}</p>
          </Col>
        </Row>
      </Fade>
    </section>
  );
};

export default Solutions;
