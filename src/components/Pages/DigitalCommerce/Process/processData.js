/* eslint max-len: 0 */

export const process = [
  {
    id: 'p1',
    numb: '01',
    title: 'Technology',
    listings: ['Own Website', 'Marketplace']
  },
  {
    id: 'p2',
    numb: '02',
    title: 'Merchandising',
    listings: ['Content Creation', 'Cataloging']
  },
  {
    id: 'p3',
    numb: '03',
    title: 'Channel Management & Optimization',
    listings: ['Inventory', 'Pricing', 'Promotions']
  },
  {
    id: 'p4',
    numb: '04',
    title: 'Fulfillment',
    listings: ['Warehouse Management', 'Logistics', 'Order Management', 'Omnichannel']
  },
  {
    id: 'p5',
    numb: '05',
    title: 'Reconcilation',
    listings: ['Receivables & Payables', 'Intelligent Matching']
  },
  {
    id: 'p6',
    numb: '06',
    title: 'Customer Service',
    listings: ['Call Support', 'Email Queries', 'Chat Suport']
  },
  {
    id: 'p7',
    numb: '07',
    title: 'Analytics',
    listings: ['Business Intelligence', 'Advance Analytics']
  },
];

export default process;
