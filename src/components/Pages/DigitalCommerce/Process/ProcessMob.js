import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import Slider from "react-slick";

// import processData from "./processData";

export default class ProcessMob extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pc: this.props,
    };
  }
  render() {
    const { pc } = this.state.pc;
    // console.log(pc);
    const settings = {
      dots: true,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 4,
      autoplay: true,
      autoplaySpeed: 8000,
      classNames: "slides",
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true,
          },
        },
        {
          breakpoint: 800,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 1,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
      infinite: false,
    };

    return (
      <Col md="12">
        <h3
          dangerouslySetInnerHTML={{
            __html: pc.processcontentglobemiddlecontent,
          }}
        ></h3>
        <Slider {...settings}>
          {pc.processcontentglobecontent.map((processDetail) => {
            return (
              <div
                id={processDetail.id}
                className="process-items"
                key={processDetail.id}
              >
                <h4>
                  <span>{processDetail.dcommpointernumber}</span>
                </h4>
                <Col sm="12">
                  <Row>
                    <h5>{processDetail.dcommpointerheader}</h5>
                    <ul>
                      {processDetail.digitalcommglobesubcontent &&
                      processDetail.digitalcommglobesubcontent.length
                        ? processDetail.digitalcommglobesubcontent.map(
                            (listing, j) => {
                              return (
                                <li key={j}>
                                  <p>{listing.dcommsubheadertext}</p>
                                </li>
                              );
                            }
                          )
                        : null}
                    </ul>
                  </Row>
                </Col>
              </div>
            );
          })}
        </Slider>
      </Col>
    );
  }
}
