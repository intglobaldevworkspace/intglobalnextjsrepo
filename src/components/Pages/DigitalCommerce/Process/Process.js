import React, { useState } from "react";
import { Row } from "reactstrap";
import Fade from "react-reveal/Fade";
import { useMediaQuery } from "react-responsive";

import ProcessDesk from "./ProcessDesk";
import ProcessMob from "./ProcessMob";
import Contactus from "../../../shared/Contactus";

const Process = (props) => {
  const isDesktopOrLaptop = useMediaQuery({ minDeviceWidth: 1024 });
  const isTabletOrMobile = useMediaQuery({ maxDeviceWidth: 1023 });
  const { processcont } = props;
  // console.log(processcont);

  const [dcprocessstate, setdcprocessstate] = useState({
    isOpen: false,
    showContactusForm: true,
    showContactusSubmitted: false,
    ContactusSubmitfailed: false,
    showSubmitLoader: false,

    contactuspopup: props.processcont.contactuspopup,
  });

  const toggleModal = () => {
    setdcprocessstate((prevState) => ({
      ...prevState,
      isOpen: !prevState.isOpen,
      showContactusForm: true,
      showContactusSubmitted: false,
      ContactusSubmitfailed: false,
      showSubmitLoader: false,
    }));
  };

  const cntpopup = {
    isOpen: dcprocessstate.isOpen,
    showContactusForm: dcprocessstate.showContactusForm,
    showContactusSubmitted: dcprocessstate.showContactusSubmitted,
    ContactusSubmitfailed: dcprocessstate.ContactusSubmitfailed,
    showSubmitLoader: dcprocessstate.showSubmitLoader,
    contactusPopup: dcprocessstate.contactuspopup,
  };
  return (
    <section className="our-process">
      <Fade bottom>
        <h2>{processcont.processcontentheader}</h2>
      </Fade>
      <Row>
        <div className="process">
          {isDesktopOrLaptop && (
            <ProcessDesk pc={processcont.processcontentglobe} />
          )}
          {isTabletOrMobile && (
            <ProcessMob pc={processcont.processcontentglobe} />
          )}
          <Fade bottom>
            {processcont.processcontentletustalk
              .processcontentletustalkstatus ? (
              <a
                href={
                  processcont.processcontentletustalk.processcontentletustalkurl
                }
                className="btn link"
                onClick={(e) => {
                  e.preventDefault();
                  // toggle();
                  toggleModal();
                }}
              >
                {
                  processcont.processcontentletustalk
                    .processcontentletustalktext
                }
              </a>
            ) : null}
          </Fade>
        </div>
      </Row>
      {dcprocessstate.isOpen ? (
        <Contactus
          cntpopup={{
            ...cntpopup,
            contactText:
              processcont.processcontentletustalk.processcontentletustalktext,
          }}
          handler={toggleModal}
        />
      ) : null}
    </section>
  );
};

export default Process;
