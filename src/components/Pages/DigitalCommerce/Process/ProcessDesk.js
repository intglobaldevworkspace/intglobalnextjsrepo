import React, { useRef, useEffect } from "react";

export default function ProcessDesk(props) {
  const graph = useRef(null);
  const { pc } = props;
  useEffect(() => {
    const circle = setTimeout(() => {
      const ciclegraph = graph.current;
      const circleElements = ciclegraph.childNodes;
      let angle = 360 - 90;
      let dangle = 360 / circleElements.length;
      for (let i = 0; i < circleElements.length; i++) {
        let circle = circleElements[i];
        angle += dangle;

        circle.style.transform = `rotate(${angle}deg) translate(${
          ciclegraph.clientWidth / 2
        }px) rotate(-${angle}deg)`;
        // console.log(graph.current.clientWidth / 2);
      }
    }, 2000);
    return () => clearTimeout(circle);
  }, []);
  // console.log(pc);
  const urlprefix = process.env.servUploadImg;
  return (
    <div id="processflow" className="process-flow">
      <figure>
        <img
          src={urlprefix + pc.processcontentglobebackground.url}
          className="img-fluid"
        />
      </figure>
      <ul className="ciclegraph" ref={graph}>
        {pc.processcontentglobecontent.map((globeulcontent, index) => {
          return (
            <li key={index}>
              <h4>{globeulcontent.dcommpointernumber}</h4>
              <ul>
                <li>
                  <h5>{globeulcontent.dcommpointerheader}</h5>
                </li>
                <li>
                  <ul>
                    {globeulcontent.digitalcommglobesubcontent &&
                    globeulcontent.digitalcommglobesubcontent.length
                      ? globeulcontent.digitalcommglobesubcontent.map(
                          (subcontentobj, index) => {
                            return (
                              <li key={index}>
                                <p>{subcontentobj.dcommsubheadertext}</p>
                              </li>
                            );
                          }
                        )
                      : null}
                  </ul>
                </li>
              </ul>
            </li>
          );
        })}
      </ul>
      <div className="center">
        <p
          dangerouslySetInnerHTML={{
            __html: pc.processcontentglobemiddlecontent,
          }}
        ></p>
      </div>
    </div>
  );
}
