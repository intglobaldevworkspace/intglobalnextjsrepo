/* eslint max-len: 0 */

export const whyus = [
  {
    id: '1',
    iconUrl: '/static/images/icons/dig-commerce/why-us/product-engineering-mindset.svg',
    tooltipID: 'Toolti01',
    tooltipText: 'We do iterative software development using cutting-edge technologies for robustness, security, and scale.',
    title: 'Product Engineering Mindset'
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/dig-commerce/why-us/seamless-experience.svg',
    tooltipID: 'Toolti02',
    tooltipText: "We don't limit UI/UX to just your website or storefront. Our engineers deploy seamless experience through your responsive website, site architecture, payment system, Single-click Checkout",
    title: 'Seamless Experience'
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/dig-commerce/why-us/enhanced-optimized.svg',
    tooltipID: 'Toolti03',
    tooltipText: 'We build SEO Optimization, Speed Optimization and Conversion Optimization system to provide you the extra edge over your competitors.',
    title: 'Enhanced & Optimized'
  },
  {
    id: '4',
    iconUrl: '/static/images/icons/dig-commerce/why-us/marketing-ready.svg',
    tooltipID: 'Toolti04',
    tooltipText: 'Our systems provide for Marketing Automation, Re-Marketing, Smart Targeting and Omni Channel marketing to maximise your reach in the market.',
    title: 'Marketing Ready'
  },
  {
    id: '5',
    iconUrl: '/static/images/icons/dig-commerce/why-us/winning-team.svg',
    tooltipID: 'Toolti05',
    tooltipText: 'Our team of technology Geeks, Digital Marketing Experts & Growth Hackers works in sync to power your ecommerce engine at its full throttle.',
    title: 'Winning Team'
  },
  {
    id: '6',
    iconUrl: '/static/images/icons/dig-commerce/why-us/full-service.svg',
    tooltipID: 'Toolti06',
    tooltipText: 'Design, Development, Maintenance, Product Management, Support.',
    title: 'Full Service'
  },
  {
    id: '7',
    iconUrl: '/static/images/icons/dig-commerce/why-us/world-class-support.svg',
    tooltipID: 'Toolti07',
    tooltipText: 'To keep your business working 24/7 through our world class support team so that you can sit back and enjoy increased revenue through sales.',
    title: 'World-Class Support'
  },
  {
    id: '8',
    iconUrl: '/static/images/icons/dig-commerce/why-us/flexible-engagement-model.svg',
    tooltipID: 'Toolti08',
    tooltipText: 'We engage you through our fixed cost or our skin-in-the-game partnership model to digital commerce development and services.',
    title: 'Flexible Engagement Model'
  },
  {
    id: '9',
    iconUrl: '/static/images/icons/dig-commerce/why-us/remote-team.svg',
    tooltipID: 'Toolti09',
    tooltipText: 'Our remote team is quick to set up, with zero set-up fees providing you with efficient & effective execution.',
    title: 'Remote Team'
  },
];

export default whyus;
