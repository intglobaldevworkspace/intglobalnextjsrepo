import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

// import whyusData from './whyusData';

const WhyUS = (props) => {
  // const [tooltipOpen, setTooltipOpen] = useState(false);
  // const toggle = () => setTooltipOpen(!tooltipOpen);
  const { whyuscont } = props;
  // console.log(whyuscont);
  const urlprefix = process.env.servUploadImg;
  return (
    <section className="whyus">
      <Row>
        <Fade bottom>
          <h2>{whyuscont.whyusheader}</h2>
        </Fade>
        <Col md="12">
          <ul className="whyus-item">
            {whyuscont.whyuscontent.map((whyusDetail) => {
              return (
                <li id={whyusDetail.id} md="3" key={whyusDetail.id}>
                  <Fade bottom>
                    <div className="hvr-blocks-out">
                      <figure>
                        <img
                          src={urlprefix + whyusDetail.whyuscontentimage.url}
                          className="img-fluid"
                          alt={whyusDetail.whyustitle}
                        />
                      </figure>
                      <h6>{whyusDetail.whyustitle}</h6>
                      <div>
                        <p>{whyusDetail.whyustooltiptext}</p>
                      </div>
                      <a href="#"></a>
                    </div>
                  </Fade>
                </li>
              );
            })}
          </ul>
        </Col>
      </Row>
    </section>
  );
};

export default WhyUS;
