import React from 'react';
import { Col } from 'reactstrap';
import Fade from 'react-reveal/Fade';

const Banner = (props) => {

  return (
    <section className="hdBan_inner">
        <div className="hdBan_inner__teaser">
          <Col md="7">
            <h1>
              <Fade left>
                <b>
                  We <u>Deliver.</u>
                </b>
              </Fade>
              <Fade left>Creative</Fade>
              <Fade left>Resources <strong>Infographic.</strong></Fade>
            </h1>
          </Col>
        </div>
    </section>
  );
}

export default Banner;