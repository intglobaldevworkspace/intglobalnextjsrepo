import React from "react";
import { Row, Col } from "reactstrap";
import Link from "next/link";
import Fade from "react-reveal/Fade";

import CategoryList from "../CategoryList/CategoryList";
import listsData from "./listsData";
import axios from "axios";

export default class List extends React.Component {
  constructor(props) {
    super(props);
    console.log(this.props);
    this.state = {
      // items: listsData,
      items: this.props.allcatdet.infogdetailrecord,
      visible: 12,
      error: false,
      selval: "",
      infocats: this.props.allcatdet.infogcat,
      infogheadertitle: this.props.allcatdet.infogheader,
    };

    this.loadMore = this.loadMore.bind(this);
  }

  loadMore() {
    this.setState((prev) => {
      return { visible: prev.visible + 3 };
    });
  }

  // componentDidMount() {
  //   fetch("https://jsonplaceholder.typicode.com/posts").then(
  //     res => res.json()
  //   ).then(res => {
  //     this.setState({
  //       items: res
  //     });
  //   }).catch(error => {
  //     console.error(error);
  //     this.setState({
  //       error: true
  //     });
  //   });
  // }

  fetchSpecificInfographic = (e) => {
    const urlprefix = process.env.servUploadImg;
    // console.log(e.target.value);
    if (e.target.value !== 0 || e.target.value !== "") {
      const headers = {
        "Content-Type": "application/json",
      };

      axios
        .get(urlprefix + "/infographiccats/" + e.target.value)
        .then((response) => {
          // console.log(response.data);
          let fullinfodet = [];
          fullinfodet = response.data.infographiccatstatus
            ? response.data.infographiclistcolls.map((catwiseitems) => {
                return {
                  id: catwiseitems.id,
                  graphicurl: catwiseitems.graphicurl,
                  infogstatus: catwiseitems.infogstatus,
                  infogdetailimg: {
                    url: catwiseitems.infogdetailimg.url,
                  },
                  infographicbigimg: catwiseitems.infographicbigimg,
                  shorttitle: catwiseitems.shorttitle,
                  infographicdetailtitle: catwiseitems.infographicdetailtitle,
                };
              })
            : [];

          this.setState({ items: fullinfodet, selval: response.data.id });
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };

  fetchallrecords = (e) => {
    const urlprefix = process.env.servUploadImg;
    // console.log(e.target.value);
    e.preventDefault();
    axios
      .get(urlprefix + "/infographiclistcolls")
      .then((response) => {
        // console.log("RESP-", response.data);

        let fullinfodet = response.data
          .filter((respObj) => respObj.infogstatus)
          .map((ifgcolldet) => {
            return {
              id: ifgcolldet.id,
              graphicurl: ifgcolldet.graphicurl,
              shorttitle: ifgcolldet.shorttitle,
              infogdetailimg: { url: ifgcolldet.infogdetailimg.url },
            };
          });

        this.setState({ items: fullinfodet, selval: "" });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  render() {
    const { infocats, infogheadertitle, items, selval } = this.state;
    const urlprefix = process.env.servUploadImg;
    return (
      <section className="infographic-list">
        <Fade bottom>
          <h2>{infogheadertitle}</h2>
        </Fade>
        <Row>
          <Col md="12">
            <CategoryList
              catl={infocats}
              catSpecificValues={this.fetchSpecificInfographic}
              selval={selval}
              getallrecordsforcat={this.fetchallrecords}
            />
          </Col>
          <div className="infographic-wrapper" aria-live="polite">
            {this.state.items && this.state.items.length > 0 ? (
              items.slice(0, this.state.visible).map((item) => {
                return (
                  <Col
                    md="4"
                    className="resource-infographic fade-in"
                    key={item.id}
                  >
                    <Fade bottom>
                      <div className="resource-infographic_box">
                        <figure>
                          {/* <Link
                            href="/infographic/[infographicdetail]"
                            as={`/infographic/${item.graphicurl}`}
                          > */}
                            <a title={item.shorttitle} href={`/infographic/${item.graphicurl}`}>
                              <img
                                src={urlprefix + item.infogdetailimg.url}
                                className="img-fluid"
                                alt={item.shorttitle}
                              />
                            </a>
                          {/* </Link> */}
                        </figure>
                      </div>
                    </Fade>
                  </Col>
                );
              })
            ) : (
              <Col md="4" className="resource-infographic fade-in">
                <Fade bottom>
                  <div className="resource-infographic_box">
                    <div className="text-center">No Records Found</div>
                  </div>
                </Fade>
              </Col>
            )}
            <Fade bottom>
              {this.state.visible < this.state.items.length && (
                <button
                  onClick={this.loadMore}
                  type="button"
                  className="load-more"
                >
                  Load More
                </button>
              )}
            </Fade>
          </div>
        </Row>
      </section>
    );
  }
}
