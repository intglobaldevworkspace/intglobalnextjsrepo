import React, { Component } from "react";
import { Col, Row } from "reactstrap";
import Fade from "react-reveal/Fade";
import Lightbox from "react-image-lightbox";
// import 'react-image-lightbox/style.css'; // This only needs to be imported once in your app

const images = [
  "/static/images/infographic/large/challenger-banks-in-UK-to-look-out-for.jpg",
];

export default class Content extends Component {
  constructor(props) {
    super(props);

    this.state = {
      photoIndex: 0,
      isOpen: false,
      infogCatDet: this.props.imgcontentDet,
    };
  }

  render() {
    const { photoIndex, isOpen, infogCatDet } = this.state;
    // console.log("---", infogCatDet);
    let singleInfographicdet = null;
    // singleInfographicdet =
    //   infogCatDet.infographiclistcolls &&
    //   infogCatDet.infographiclistcolls.length > 0
    //     ? infogCatDet.infographiclistcolls[0]
    //     : infogCatDet.infographiclistcolls;

    singleInfographicdet =
      infogCatDet && infogCatDet.length > 0 ? infogCatDet[0] : infogCatDet;
    // console.log(singleInfographicdet);
    const urlprefix = process.env.servUploadImg;
    return (
      <section className="infographic-content">
        <Col md="10">
          <Row>
            <Fade top>
              <h2>{singleInfographicdet.infographicdetailtitle}</h2>
            </Fade>
          </Row>
        </Col>
        <Col md="9">
          <Fade bottom>
            <div>
              <figure className="single-img-wrapper">
                <a
                  onClick={() => this.setState({ isOpen: true })}
                  title={singleInfographicdet.infographicdetailtitle}
                >
                  <img
                    src={urlprefix + singleInfographicdet.infogdetailimg.url}
                    className="img-fluid"
                    alt={singleInfographicdet.infographicdetailtitle}
                  />
                </a>
              </figure>

              {isOpen && (
                <Lightbox
                  mainSrc={
                    urlprefix + singleInfographicdet.infographicbigimg.url
                  }
                  //nextSrc={images[(photoIndex + 1) % images.length]}
                  //prevSrc={images[(photoIndex + images.length - 1) % images.length]}
                  onCloseRequest={() => this.setState({ isOpen: false })}
                />
              )}
            </div>
          </Fade>
        </Col>
      </section>
    );
  }
}
