import React from 'react';
import {
  Col,
  Row
} from 'reactstrap';
import Slider from "react-slick";
import Fade from 'react-reveal/Fade';

import bannerData from './bannerData';

export default class Banner extends React.Component {
  render() {
    const settings = {
      dots: false,
      arrows: true,
      infinite: true,
      fade: true,
      speed: 500,
      autoplay: true,
      autoplaySpeed: 6000,
      draggable: true,
      cssEase: 'ease-in-out',
      slidesToShow: 1,
      slidesToScroll: 1
    };

    return (
      <section className="hdBan_inner">
        <div className="hdBan_inner__teaser">
          <div className="successstory-ban">
          <Slider {...settings}>
        {bannerData.map((bannerDetail, index) => {
          return (
            <div id={bannerDetail.id} className="successstory-items" key={bannerDetail.id}>
              <Row>
                <Col md="6">
                  <div className="story-logo">
                    <Fade left>
                      <figure>
                        <img src={bannerDetail.bannerLogo} className="img-fluid" alt={bannerDetail.bannerTitle} />
                      </figure>
                    </Fade>
                  </div>
                  <div className="story-content">
                    <Fade left>
                      <p>{bannerDetail.bannerContent}</p>
                    </Fade>
                  </div>
                  <div className="story-action">
                    <Fade left>
                      <a href="#" className="read-more">{bannerDetail.bannerReadMore}</a>
                    </Fade>
                  </div>
                </Col>
                <Col md="6">
                  <figure className="story-banr">
                    <Fade right>
                      <span>
                        <img src={bannerDetail.bannerUrl} className="img-fluid" alt={bannerDetail.bannerTitle} />
                      </span>
                    </Fade>
                  </figure>
                </Col>
              </Row>
            </div>
          )
        })}
      </Slider>
          </div>
        </div>
      </section>
    )
  }
}

