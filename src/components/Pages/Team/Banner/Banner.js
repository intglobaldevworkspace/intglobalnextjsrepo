import React from "react";
import { Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const Banner = (props) => {
  // console.log(props);
  return (
    <section className="hdBan_inner">
      <div className="hdBan_inner__teaser">
        <Col md="7">
          <Fade left>
            <h1>
              <b>
                {props.hban.bText1} <u>{props.hban.bText2}</u>
              </b>
              {props.hban.bText3} <strong>{props.hban.bText4}</strong>
            </h1>
          </Fade>
        </Col>
      </div>
    </section>
  );
};

export default Banner;
