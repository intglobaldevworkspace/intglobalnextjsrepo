/* eslint max-len: 0 */

export const boardfaces = [
  {
    id: '1',
    imgUrl: '/static/images/img-ar.png',
    name: 'Abhishek Rungta',
    skill: 'Founder & CEO',
    expnce: 'A techie turned entrepreneur in his teens, Abhishek Rungta, the CEO of INT. is a seasoned Digital Strategy Consultant, who provides organisation a strategic road map to attain Digital Success.',
    fbUrl: '//facebook.com/abhishekrungta/',
    twUrl: '//twitter.com/abhishekrungta/',
    linUrl: '//linkedin.com/in/abhishekrungta/',
    instUrl: '//www.instagram.com/abhishekrungta/',
    mail: 'talash@indusnet.co.in'
    // socials: [
    //   {
    //     fbUrl: 'https://www.facebook.com/abhishekrungta',
    //     twUrl: 'https://twitter.com/abhishekrungta',
    //     linUrl: 'https://www.linkedin.com/in/abhishekrungta',
    //     instUrl: 'https://www.instagram.com/abhishekrungta',
    //     mail: 'talash@indusnet.co.in'
    //   },
    // ],
  },
  {
    id: '2',
    imgUrl: '/static/images/img-bb.png',
    name: 'Bharat Berlia',
    skill: 'CIO',
    expnce: 'The CIO of INT., Bharat Berlia brings two decades of hands-on experience in identifying new opportunities, developing business strategy & delivery road-maps in the BFSI, Retail, Healthcare and Entertainment sectors.',
    fbUrl: '//facebook.com/bharat.berlia/',
    twUrl: '//twitter.com/bharatberlia/',
    linUrl: '//linkedin.com/in/bharatberlia/',
    instUrl: '//instagram.com/brat.man/',
    mail: 'bberlia@indusnet.co.in'
    // socials: [
    //   {
    //     fbUrl: 'https://www.facebook.com/bharat.berlia',
    //     twUrl: 'https://twitter.com/bharatberlia',
    //     linUrl: 'https://www.linkedin.com/in/bharatberlia',
    //     instUrl: 'https://www.instagram.com/brat.man',
    //     mail: 'bberlia@indusnet.co.in'
    //   },
    // ],
  },
  {
    id: '3',
    imgUrl: '/static/images/img-ai.png',
    name: 'Aji Issac Mathew',    
    skill: 'CEO & Co-Founder of Indus Net TechShu',
    expnce: 'A Digital Strategist for a decade, Aji Issac Matthew, the CEO  and cofounder of Indus Net Techshu is a seasoned Marketing Professional crafting ROI driven digital solutions for the global enterprises.',
    fbUrl: '//facebook.com/Aji.Issac/',
    twUrl: '//twitter.com/AjiTechShu/',
    linUrl: '//linkedin.com/in/ajinimc/',
    instUrl: '//instagram.com/ajitechshu/',
    mail: 'aji@techshu.org'
    // socials: [
    //   {
    //     fbUrl: 'https://www.facebook.com/Aji.Issac',
    //     twUrl: 'https://twitter.com/AjiTechShu',
    //     linUrl: 'https://www.linkedin.com/in/ajinimc',
    //     instUrl: 'https://www.instagram.com/ajitechshu',
    //     mail: 'aji@techshu.org'
    //   },
    // ],
  },
  {
    id: '4',
    imgUrl: '/static/images/img-sc.png',
    name: 'Souvik Chaki',    
    skill: 'Business Head, India',
    expnce: 'A digital innovator for a decade, Souvik Chaki has been a key player in growth acceleration of  global enterprises through unique digital products and redefined customer journey.',
    fbUrl: '//facebook.com/Aji.Issac/',
    twUrl: '//twitter.com/AjiTechShu/',
    linUrl: '//linkedin.com/in/souvikchaki',
    instUrl: '//instagram.com/souvikchaki/',
    mail: 'souvik.chaki@indusnet.co.in'
    // socials: [
    //   {
    //     fbUrl: 'https://www.facebook.com/Aji.Issac',
    //     twUrl: 'https://twitter.com/AjiTechShu',
    //     linUrl: 'https://www.linkedin.com/in/ajinimc',
    //     instUrl: 'https://www.instagram.com/ajitechshu',
    //     mail: 'aji@techshu.org'
    //   },
    // ],
  },
];

export default boardfaces;
