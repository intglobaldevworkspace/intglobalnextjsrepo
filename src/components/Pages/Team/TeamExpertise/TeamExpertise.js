import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const TeamExpertise = (props) => {
  return (
    <section className="team_expertise">
      <Fade bottom>
        <Row>
          <Col md="11">
            <p dangerouslySetInnerHTML={{ __html: props.teamxp }}></p>
          </Col>
        </Row>
      </Fade>
    </section>
  );
};

export default TeamExpertise;
