import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";
import Chart from "react-google-charts";

export default class TeamStatistics extends Component {
  constructor(props) {
    super(props);
    // console.log(this.props);
    this.state = {
      statH: this.props.teamstat.teamstatHeader,
      statSettings: this.props.teamstat.statPiechartSettings,
      statData: this.props.teamstat.statPiechartData,
      firstTworecords: this.props.teamstat.teamStatFirst2records,
    };
  }

  render() {
    const urlprefix = process.env.servUploadImg;
    const { statH, statSettings, statData, firstTworecords } = this.state;
    // console.log(statSettings);
    // console.log(statData);

    // console.log(firstTworecords);
    return (
      <section className="team_statistics">
        <Fade bottom>
          <Row>
            <Col md="10">
              <h2>{statH}</h2>
            </Col>
          </Row>
        </Fade>
        <Row>
          <div className="statistics-data">
            <Col md="4" className="stat-box">
              <div className="stat-box_col fade-in">
                <ul>
                  {firstTworecords.map((firsttworecObj) => (
                    <li key={firsttworecObj.id}>
                      <figure>
                        <img
                          src={
                            urlprefix + firsttworecObj.firsttwostaticdataimg.url
                          }
                          className="img-fluid"
                          alt={firsttworecObj.tsfirsttwotitle}
                        />
                      </figure>
                      <h3>{firsttworecObj.tsfirsttwotitle}</h3>
                    </li>
                  ))}
                </ul>
              </div>
            </Col>
            {statData.map((statDataObj) => {
              let piechartData = [];
              piechartData = statDataObj.teamstatdynamicArr.map(
                (keyValPairObj, index) => {
                  return index > 0
                    ? [keyValPairObj.tskeynam, parseInt(keyValPairObj.tsKeyVal)]
                    : [keyValPairObj.tskeynam, keyValPairObj.tsKeyVal];
                }
              );
              // console.log("piechartData -", piechartData);
              let pieItemsColor = [];
              pieItemsColor = statDataObj.teamstatdataColor.map(
                (pieItemcolorObj) => {
                  return pieItemcolorObj.colorhexformat;
                }
              );

              // console.log("pieItemsColor -", pieItemsColor);
              // console.log(statSettings);

              let settingsConfigObj = {
                title: statDataObj.keyvalIdentifier,
                titleTextStyle: {
                  position: statSettings.titleTextPosition,
                  color: statSettings.titleTextStyleColor,
                  fontName: statSettings.titleTextStyleFontname,
                  fontSize: statDataObj.titleTextStylefontSize, //different value
                },
                legend: "none",
                width: "100%",
                height: "100%",
                pieSliceText: statSettings.pieSliceText,
                colors: pieItemsColor,
                pieHole: statSettings.pieHole,
                pieSliceTextStyle: {
                  fontSize: statSettings.pieSliceTextStyleFontSize,
                },
                chartArea: {
                  left: "5%",
                  top: "5%",
                  height: "90%",
                  width: "90%",
                },
                responsive: true,
                legend: {
                  position: "right",
                  alignment: "center",
                  textStyle: {
                    color: statSettings.legendtextStyleColor,
                    fontSize: statSettings.legendtextStylefontSize,
                  },
                },
                tooltip: {
                  showColorCode: statSettings.tooltipshowColorCode,
                },
                is3D: true,
              };
              // console.log(settingsConfigObj);
              return (
                <Col md="4" className="stat-box" key={statDataObj.id}>
                  <div className="stat-box_col fade-in">
                    <h4>{statDataObj.keyvalIdentifier}</h4>
                    <Chart
                      chartType="PieChart"
                      width="100%"
                      height="200px"
                      loader={<div>Loading Chart</div>}
                      data={piechartData}
                      options={settingsConfigObj}
                      legendToggle
                    />
                  </div>
                </Col>
              );
            })}
          </div>
        </Row>
      </section>
    );
  }
}
