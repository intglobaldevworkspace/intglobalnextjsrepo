import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

// import mangmenteamsData from "./mangmenteamsData";

const ManagementTeam = (props) => {
  const urlprefix = process.env.servUploadImg;
  // console.log(props);
  return (
    <section className="management-team">
      <Fade bottom>
        <Row>
          <Col md="10">
            <h2>{props.mgmt.mgmtteamheader}</h2>
          </Col>
        </Row>
      </Fade>
      <Row>
        {/* <div className="management-team_row">
            <Col md="6">
              <Fade right>
                <figure className="mangimg">
                  <img src="/static/images/img-management-team.png" alt="INT. Management Team" />
                </figure>
              </Fade>
            </Col>
            <Col md="6">
              <Fade left>
                <h3>Management Team <strong>of INT.</strong></h3>
              </Fade>
            </Col>
          </div> */}
        <div className="managmnt-teams">
          {props.mgmt.mgmtteam.map((mangmenteamDetail) => {
            return (
              <Col md="3" key={mangmenteamDetail.id}>
                <Fade bottom>
                  <div className="teams-block">
                    <figure>
                      <img
                        src={urlprefix + mangmenteamDetail.memberImg.url}
                        className="img-fluid"
                        alt={mangmenteamDetail.teamMemberName}
                      />
                    </figure>
                    <h4>{mangmenteamDetail.teamMemberName}</h4>
                    <h5>{mangmenteamDetail.teamMemberDesig}</h5>
                  </div>
                </Fade>
              </Col>
            );
          })}
        </div>
      </Row>
    </section>
  );
};

export default ManagementTeam;
