/* eslint max-len: 0 */

export const teams = [
  {
    id: '1',
    imgUrl: '/static/images/teams/about-us/management/anindya.png',
    name: 'Anindya Sengupta',
    desig: 'Sr. Account Relationship Manager'
  },
  {
    id: '2',
    imgUrl: '/static/images/teams/about-us/management/krishnendu.png',
    name: 'Krishnendu Chatterjee',
    desig: 'Sr. Account Relationship Manager'
  },
  {
    id: '3',
    imgUrl: '/static/images/teams/about-us/management/santanu.png',
    name: 'Santanu Mukherjee',
    desig: 'Sr. Account Relationship Manager'
  },
  {
    id: '4',
    imgUrl: '/static/images/teams/about-us/management/souvik.png',
    name: 'Souvik Chaki',
    desig: 'Business Head, India'
  },
  {
    id: '5',
    imgUrl: '/static/images/teams/about-us/management/debopam.png',
    name: 'Debopam Majilya',
    desig: 'Senior Project Manager'
  },
  {
    id: '6',
    imgUrl: '/static/images/teams/about-us/management/mainak.png',
    name: 'Mainak Biswas',
    desig: 'Sr. Account Relationship Manager'
  },
  {
    id: '7',
    imgUrl: '/static/images/teams/about-us/management/amit.png',
    name: 'Amit Singha',
    desig: 'Account Relationship Manager'
  },
  {
    id: '8',
    imgUrl: '/static/images/teams/about-us/management/swarnali.jpg',
    name: 'Swarnali Nandy',
    desig: 'Senior Manager – Delivery'
  }, 
];

export default teams;
