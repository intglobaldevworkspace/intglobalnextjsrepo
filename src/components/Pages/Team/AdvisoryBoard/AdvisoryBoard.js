import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

import advisoryboardsData from "./advisoryboardsData";

export default class AdvisoryBoard extends Component {
  render() {
    const urlprefix = process.env.servUploadImg;
    // console.log(this.props);
    // const {}=this.props
    return (
      <section className="advisory-board">
        <Fade bottom>
          <Row>
            <Col md="10">
              <h2>{this.props.advbrd.advHeader}</h2>
            </Col>
          </Row>
          <Row>
            {this.props.advbrd.advMembers.map((advisoryboardDetail) => {
              return (
                <div
                  className="advisory-board_memb"
                  key={advisoryboardDetail.id}
                >
                  <Col md="2">
                    <figure>
                      <img
                        src={urlprefix + advisoryboardDetail.advmemberimg.url}
                        className="img-fluid"
                        alt={advisoryboardDetail.advmembername}
                      />
                    </figure>
                  </Col>
                  <Col md="10">
                    <h3>
                      {advisoryboardDetail.advmembername}{" "}
                      <span>{advisoryboardDetail.advmemberdesignation}</span>
                    </h3>
                    <p>{advisoryboardDetail.advmemberdescription}</p>
                  </Col>
                </div>
              );
            })}
          </Row>
        </Fade>
      </section>
    );
  }
}
