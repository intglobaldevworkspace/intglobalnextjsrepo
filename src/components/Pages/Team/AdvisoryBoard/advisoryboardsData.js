/* eslint max-len: 0 */

export const advisoryboard = [
  {
    id: '1',
    imgUrl: '/static/images/img-malcolm-mclean.jpg',
    name: 'Malcolm McLean',
    desig: 'ADVISOR',
    content: 'Entrepreneur and technology innovator with over 17 years experience in designing, building and maintaining secure, cloud-based platforms – particularly in financial services. Extensive, successful track record of building and retaining offshore development teams.',
  },
  {
    id: '2',
    imgUrl: '/static/images/img-alkesh-agarwal.jpg',
    name: 'Alkesh Agarwal',
    desig: 'ADVISOR',
    content: 'A young entrepreneur and a seasoned growth innovator, Alkesh Agarwal is known for transforming challenges into lucrative business opportunities for next-gen performance agencies globally.',
  },
];

export default advisoryboard;
