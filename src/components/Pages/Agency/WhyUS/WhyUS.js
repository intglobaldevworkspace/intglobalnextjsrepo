import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

// import whyusData from "./whyusData";

const WhyUS = (props) => {
  // console.log(props.why);
  const urlprefix = process.env.servUploadImg;
  return (
    <section className="whyus">
      <Col md="10">
        <Row>
          <Fade bottom>
            <h2>{props.why.header}</h2>
          </Fade>
          <ul className="whyus-item">
            {props.why.whyusdata.map((whyusDetail) => {
              return (
                <li id={whyusDetail.id} md="3" key={whyusDetail.id}>
                  <Fade bottom>
                    <div className="hvr-blocks-out">
                      <figure>
                        <img
                          src={urlprefix + whyusDetail.whyusimg.url}
                          className="img-fluid"
                          alt={whyusDetail.whyustitle}
                        />
                      </figure>
                      <h6>{whyusDetail.whyustitle}</h6>
                      <div>
                        <p>{whyusDetail.whyustooltiptext}</p>
                      </div>
                      <a></a>
                    </div>
                  </Fade>
                </li>
              );
            })}
          </ul>
        </Row>
      </Col>
    </section>
  );
};

export default WhyUS;
