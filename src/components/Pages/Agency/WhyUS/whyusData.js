/* eslint max-len: 0 */

export const whyus = [
  {
    id: '1',
    iconUrl: '/static/images/icons/agency/why-us/strategic-approach.png',
    title: 'Strategic Approach',
    tooltipText: 'From research to vision to implementation, everything is strategized by the industry expert, from the scratch.'
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/agency/why-us/seamless-experience.svg',
    title: 'Specialized Solution',
    tooltipText: 'We simplify your digital transformation journey with simple, minimalistic, and customised solution instead of one-size-fits-all solutions.'
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/agency/why-us/dedicated-hiring.png',
    title: 'Dedicated Hiring',
    tooltipText: 'Hire resources who are constantly upskilled and trained as per the evolving industry trends.'
  },
  {
    id: '4',
    iconUrl: '/static/images/icons/agency/why-us/flexible-contracts.png',
    title: 'Flexible contracts',
    tooltipText: 'Forget long term contracts. Hire experts on demand, based on your requirements.'
  },
  {
    id: '5',
    iconUrl: '/static/images/icons/agency/why-us/agile-and-focused-teams.png',
    title: 'Agile & Focussed Teams',
    tooltipText: 'Agile process delivers business value with customer-centric solutions.'
  },
  {
    id: '6',
    iconUrl: '/static/images/icons/agency/why-us/years-of-experience.png',
    title: '22+ Years of Experience',
    tooltipText: 'We bring you two decades of proven experience in serving global agencies.'
  },
];

export default whyus;
