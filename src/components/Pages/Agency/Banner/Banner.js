import React from "react";
import { Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const Banner = (props) => {
  // console.log(props);
  return (
    <section className="hdBan_inner">
      <div className="hdBan_inner__teaser">
        <Col md="6">
          <Fade left>
            <h1>
              <b>
                {props.btext.bannertextone} <u>{props.btext.bannertexttwo}</u>{" "}
                <br />
                {props.btext.bannertextthree}
                <u>{props.btext.bannertextfour}</u>
              </b>
              {props.btext.bannertextfive} <br />
              <strong>
                {props.btext.bannertextsix} <br />
                {props.btext.bannertextseven} <br />
                {props.btext.bannertexteight}
              </strong>
            </h1>
          </Fade>
        </Col>
      </div>
    </section>
  );
};

export default Banner;
