/* eslint max-len: 0 */

export const goals = [
  {
    id: '1',
    iconUrl: '/static/images/icons/agency/achieve-goals/multiple-engagement-touchpoints.png',
    title: 'Multiple Engagement Touchpoints',
    tooltipID: 'Toolti01',
    tooltipText: 'From consultation, campaign designing, product planning and development to marketing and customer support, leverage full stack solution from your tech partner.'
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/agency/achieve-goals/top-notch-consultation.png',
    title: 'Top-notch Consultation',
    tooltipID: 'Toolti02',
    tooltipText: 'Top-notch consulting from industry experts’ help agencies navigate through all facets of their digital landscape, identifying growth opportunities.'
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/agency/achieve-goals/seamless-customer-success-journey.png',
    title: 'Seamless Customer-Success Journey',
    tooltipID: 'Toolti03',
    tooltipText: 'Create interesting digital assets, manage them and amplify its usage while growing your client’s businesses using digital technology.'
  },
  {
    id: '4',
    iconUrl: '/static/images/icons/agency/achieve-goals/fast-and-flexible.png',
    title: 'Fast and Flexible',
    tooltipID: 'Toolti04',
    tooltipText: 'Digital solutions curated in a fast, agile, and robust format to keep in sync with the fast evolving digital landscape.'
  },
  {
    id: '5',
    iconUrl: '/static/images/icons/agency/achieve-goals/partnership-beyond-contracts.png',
    title: 'Partnership Beyond Contracts',
    tooltipID: 'Toolti05',
    tooltipText: 'As a technology partner we share your incentives as well as loss with equal intensity.'
  },
];

export default goals;
