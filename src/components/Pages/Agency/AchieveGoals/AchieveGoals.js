import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

// import goalsData from "./goalsData";

const AchieveGoals = (props) => {
  // console.log(props);
  const urlprefix = process.env.servUploadImg;
  return (
    <section className="achieve-goals">
      <Fade bottom>
        <h2>
          {props.ach.achtextone} <br />
          {props.ach.achtexttwo}
        </h2>
      </Fade>
      <Row>
        <Col md="5">
          <Fade left>
            <h5>{props.ach.achtextthree}</h5>
          </Fade>
        </Col>
        <Col md="7">
          <Row>
            {props.ach.achtooltipdata.map((goalDetail) => {
              return (
                <Col
                  id={goalDetail.id}
                  md="4"
                  className="goal-item"
                  key={goalDetail.id}
                >
                  <Fade bottom>
                    <div className="hvr-blocks-out">
                      <figure>
                        <img
                          src={urlprefix + goalDetail.achieveimg.url}
                          className="img-fluid"
                          alt={goalDetail.achievetooltiptitle}
                        />
                      </figure>
                      <h6>{goalDetail.achievetooltiptitle}</h6>
                      <div>
                        <p>{goalDetail.achievettooltiptext}</p>
                      </div>
                      <a></a>
                    </div>
                  </Fade>
                </Col>
              );
            })}
          </Row>
        </Col>
      </Row>
    </section>
  );
};

export default AchieveGoals;
