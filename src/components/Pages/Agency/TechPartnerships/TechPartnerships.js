import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const TechPartnerships = (props) => {
  // console.log(props);
  return (
    <section className="tech_partnr">
      <Fade bottom>
        <h2>{props.tp.tptextone}</h2>
        <Row>
          <Col md="11">
            <p>{props.tp.tptexttwo}</p>
          </Col>
        </Row>
      </Fade>
    </section>
  );
};

export default TechPartnerships;
