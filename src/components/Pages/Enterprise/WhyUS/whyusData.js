/* eslint max-len: 0 */

export const whyus = [
  {
    id: '1',
    iconUrl: '/static/images/icons/enterprise/why-us/strategic-approach.png',
    title: 'Strategic Approach',
    tooltipText: 'From research to vision to implementation, everything is strategized by the industry expert, from the scratch.'
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/enterprise/why-us/seamless-experience.svg',
    title: 'Dedicated Hiring',
    tooltipText: 'We simplify your digital transformation journey with simple, minimalistic, and customised solution instead of one-size-fits-all solutions.'
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/enterprise/why-us/dedicated-hiring.png',
    title: 'Flexible Contracts',
    tooltipText: 'Hire resources who are constantly upskilled and trained as per the evolving industry trends.'
  },
  {
    id: '4',
    iconUrl: '/static/images/icons/enterprise/why-us/flexible-contracts.png',
    title: 'Agile & Focused Teams',
    tooltipText: 'Forget long term contracts. Hire experts on demand, based on your requirements.'
  },
  {
    id: '5',
    iconUrl: '/static/images/icons/enterprise/why-us/years-of-experience.png',
    title: '22+ Years of Experience',
    tooltipText: 'We bring you two decades of proven experience in serving global agencies.'
  },
];

export default whyus;
