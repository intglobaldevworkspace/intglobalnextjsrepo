import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const TechExpertise = (props) => {
  return (
    <section className="tech_expertise">
      <Fade bottom>
        <Row>
          <Col md="12">
            <p dangerouslySetInnerHTML={{ __html: props.texptext }}></p>
          </Col>
        </Row>
      </Fade>
    </section>
  );
};

export default TechExpertise;
