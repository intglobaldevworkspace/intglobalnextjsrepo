import React from "react";
import { Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const Banner = (props) => {
  // console.log(props);
  const {
    hbantext1,
    hbantext2,
    hbantext3,
    hbantext4,
    hbantext5,
    hbantext6,
  } = props.hbtext;
  return (
    <section className="hdBan_inner">
      <div className="hdBan_inner__teaser">
        <Col md="7">
          <Fade left>
            <h1>
              <b>
                {hbantext1} <u>{hbantext2}</u>
              </b>
              {hbantext3} <br />
              {hbantext4} <br />
              {hbantext5} <br />
              <strong>{hbantext6}</strong>
            </h1>
          </Fade>
        </Col>
      </div>
    </section>
  );
};

export default Banner;
