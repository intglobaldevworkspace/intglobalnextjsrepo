/* eslint max-len: 0 */

export const solutions = [
  {
    id: '1',
    iconUrl: '/static/images/icons/enterprise/tech-solution/multiple-engagement-touchpoints.png',
    title: 'Technology Consultation',
    tooltipID: 'Toolti01',
    tooltipText: 'From consultation, campaign designing, product planning and development to marketing and customer support, leverage full stack solution from your tech partner.'
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/enterprise/tech-solution/top-notch-consultation.png',
    title: 'Business-Centric Planning',
    tooltipID: 'Toolti02',
    tooltipText: 'Top-notch consulting from industry experts’ help agencies navigate through all facets of their digital landscape, identifying growth opportunities.'
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/enterprise/tech-solution/seamless-customer-success-journey.png',
    title: 'Product Engineering Mindset',
    tooltipID: 'Toolti03',
    tooltipText: 'Create interesting digital assets, manage them and amplify its usage while growing your client’s businesses using digital technology.'
  },
  {
    id: '4',
    iconUrl: '/static/images/icons/enterprise/tech-solution/fast-and-flexible.png',
    title: 'Analytical Approach',
    tooltipID: 'Toolti04',
    tooltipText: 'Digital solutions curated in a fast, agile, and robust format to keep in sync with the fast evolving digital landscape.'
  },
  {
    id: '5',
    iconUrl: '/static/images/icons/enterprise/tech-solution/partnership-beyond-contracts.png',
    title: 'Robust & Remote Support System',
    tooltipID: 'Toolti05',
    tooltipText: 'As a technology partner we share your incentives as well as loss with equal intensity.'
  },
];

export default solutions;
