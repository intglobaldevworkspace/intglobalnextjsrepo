import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

// import solutionsData from "./solutionsData";

const TechSolutions = (props) => {
  // console.log(props.tsol);
  const urlprefix = process.env.servUploadImg;
  const { tsoltext1, tsoltext2, tsoldata } = props.tsol;
  return (
    <section className="tech-solution">
      <Fade bottom>
        <h2>{tsoltext1}</h2>
      </Fade>
      <Row>
        <Col md="5">
          <Fade left>
            <h5>{tsoltext2}</h5>
          </Fade>
        </Col>
        <Col md="7">
          <Row>
            {tsoldata.map((solutionDetail) => {
              return (
                <Col
                  id={solutionDetail.id}
                  md="4"
                  className="sol-item"
                  key={solutionDetail.id}
                >
                  <Fade bottom>
                    <div className="hvr-blocks-out">
                      <figure>
                        <img
                          src={urlprefix + solutionDetail.techsolimg.url}
                          className="img-fluid"
                          alt={solutionDetail.techsoltitle}
                        />
                      </figure>
                      <h6>{solutionDetail.techsoltitle}</h6>
                      <div>
                        <p>{solutionDetail.techsoltooltip}</p>
                      </div>
                      <a></a>
                    </div>
                  </Fade>
                </Col>
              );
            })}
          </Row>
        </Col>
      </Row>
    </section>
  );
};

export default TechSolutions;
