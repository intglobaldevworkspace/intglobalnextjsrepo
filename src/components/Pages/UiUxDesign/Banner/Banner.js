import React from "react";
import { Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const Banner = (props) => {
  // console.log(props.hban);
  const {
    headerbannertextone,
    headerbannertexttwo,
    headerbannertextthree,
    headerbannertextfour,
  } = props.hban;
  return (
    <section className="hdBan_inner">
      <div className="hdBan_inner__teaser">
        <Col md="7">
          <h1>
            <Fade left>
              <b dangerouslySetInnerHTML={{ __html: headerbannertextone }}></b>
            </Fade>
            <Fade left>{headerbannertexttwo}</Fade>
            <Fade left>{headerbannertextthree}</Fade>
            <Fade left>
              <strong
                dangerouslySetInnerHTML={{ __html: headerbannertextfour }}
              ></strong>
            </Fade>
          </h1>
        </Col>
      </div>
    </section>
  );
};

export default Banner;
