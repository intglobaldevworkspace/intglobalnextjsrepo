import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

// import servicesData from "./servicesData";

const Services = (props) => {
  // console.log(props.servicecont);
  const { servicedata, serviceheader, servicesubheader } = props.servicecont;
  const urlprefix = process.env.servUploadImg;
  return (
    <section className="our-services">
      <Fade bottom>
        <h2>{serviceheader}</h2>
      </Fade>
      <Fade bottom>
        <Row>
          <Col md="11">
            <p>{servicesubheader}</p>
          </Col>
        </Row>
      </Fade>
      <Row>
        <Col md="12">
          {servicedata.map((serviceDetail) => {
            return (
              <Col
                id={serviceDetail.id}
                md="4"
                className="srvc-item"
                key={serviceDetail.id}
              >
                <Fade bottom>
                  <div className="hvr-blocks-out">
                    <figure>
                      <img
                        src={urlprefix + serviceDetail.servicesimg.url}
                        className="img-fluid"
                        alt={serviceDetail.servicestitle}
                      />
                    </figure>
                    <h6>{serviceDetail.servicestitle}</h6>
                    {/* <p>{serviceDetail.content}</p> */}
                    <div>
                      <p>{serviceDetail.servicesdescription}</p>
                    </div>
                    <a></a>
                  </div>
                </Fade>
              </Col>
            );
          })}
        </Col>
      </Row>
    </section>
  );
};

export default Services;
