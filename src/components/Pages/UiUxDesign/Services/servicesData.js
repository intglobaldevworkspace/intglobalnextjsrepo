/* eslint max-len: 0 */

export const services = [
  {
    id: '1',
    iconUrl: '/static/images/icons/uiux-design/services/usability-testing.svg',
    title: 'Usability Testing',
    tooltipID: 'Toolti01',
    tooltipText: 'Analyzing users’ behaviours while using a product and providing insights on how effective, efficient and satisfying a product is.'
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/uiux-design/services/interface-design.svg',
    title: 'Interface Design',
    tooltipID: 'Toolti02',
    tooltipText: 'Having a user-centric approach while bringing together concepts from interaction design, visual design and information architecture is critical when designing interfaces.'
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/uiux-design/services/ux-consultant.svg',
    title: 'UX Consultant',
    tooltipID: 'Toolti03',
    tooltipText: 'Our seasoned consultants provide expert analysis on how mature a specific product is in terms of UX and usability.'
  },
  {
    id: '4',
    iconUrl: '/static/images/icons/uiux-design/services/information-architecture.svg',
    title: 'Information Architecture',
    tooltipID: 'Toolti04',
    tooltipText: 'Building an experience to simplify the complex navigation of all the content in the website and improve discoverability.'
  },
  {
    id: '5',
    iconUrl: '/static/images/icons/uiux-design/services/service-design.svg',
    title: 'Service Design',
    tooltipID: 'Toolti05',
    tooltipText: 'Service design is a human-centered and creative service concept that brings together the teams that make up a service ecosystem and integrates them with design thinking, co-working and creative thinking.'
  },
  {
    id: '6',
    iconUrl: '/static/images/icons/uiux-design/services/customer-journey-mapping.svg',
    title: 'Customer Journey Mapping',
    tooltipID: 'Toolti06',
    tooltipText: 'For each decision of the customer, the different steps of customer experience are different. Mapping or wireframing the touchpoints is essential in providing the UX that affects the perception of the brand.'
  },
  {
    id: '7',
    iconUrl: '/static/images/icons/uiux-design/services/user-research.svg',
    title: 'User Research',
    tooltipID: 'Toolti07',
    tooltipText: 'Getting to know more about current or potential users, their behaviours, attitudes and expectations can open up opportunities for improving the product and experience.'
  },
];

export default services;
