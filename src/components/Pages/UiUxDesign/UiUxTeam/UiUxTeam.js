import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

// import uiuxTeamData from "./uiuxTeamData";

const UiUxTeam = (props) => {
  // console.log(props);
  return (
    <section className="uiux-team">
      <Row>
        <Fade bottom>
          <h2
            dangerouslySetInnerHTML={{ __html: props.uiuxdata.teamdataheader }}
          ></h2>
        </Fade>
        <Fade bottom></Fade>
        <Col md="12">
          <Row>
            {props.uiuxdata.teamdata.map((uiuxTeamDetail) => {
              return (
                <Col
                  id={uiuxTeamDetail.id}
                  md="6"
                  sm="6"
                  className="team-item"
                  key={uiuxTeamDetail.id}
                >
                  <Fade bottom>
                    <div className="team-row">
                      <h6>{uiuxTeamDetail.teamdatadescription}</h6>
                    </div>
                  </Fade>
                </Col>
              );
            })}
          </Row>
        </Col>
      </Row>
    </section>
  );
};

export default UiUxTeam;
