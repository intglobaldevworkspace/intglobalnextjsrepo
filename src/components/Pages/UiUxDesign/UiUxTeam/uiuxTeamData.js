/* eslint max-len: 0 */

export const teamdata = [
  {
    id: 'l1',
    listcontent: 'Gain a richer understanding of your audience – from service requirements to brand perception.'
  },
  {
    id: 'l2',
    listcontent: 'Grow your service offering by pinpointing audience desires and expectations.'
  },
  {
    id: 'l3',
    listcontent: 'Boost conversions on your existing website by identifying the barriers your audience face and optimising the user journey accordingly.'
  },
  {
    id: 'l4',
    listcontent: 'Deliver a seamless, user-focused digital experience that will help you achieve organisational goals for growth, retention and more.'
  },
];

export default teamdata;
