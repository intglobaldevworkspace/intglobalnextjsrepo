import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

// import howWeDoData from "./howWeDoData";

const HowWeDo = (props) => {
  // console.log(props.howwedo);
  const { howwedoheader, howwedosubheader, howwedocontent } = props.howwedo;
  const urlprefix = process.env.servUploadImg;
  return (
    <section className="how-we-do">
      <Row>
        <Fade bottom>
          <h2>{howwedoheader}</h2>
        </Fade>
        <Fade bottom>
          <Row>
            <Col md="11">
              <p>{howwedosubheader}</p>
            </Col>
          </Row>
        </Fade>
        <Col md="12">
          <ul className="howwedo-item">
            {howwedocontent.map((howWeDoDetail) => {
              return (
                <li id={howWeDoDetail.id} md="3" key={howWeDoDetail.id}>
                  <Fade bottom>
                    <div className="hvr-blocks-out">
                      <figure>
                        <img
                          src={urlprefix + howWeDoDetail.howwedoimg.url}
                          className="img-fluid"
                          alt={howWeDoDetail.howwedotitle}
                        />
                      </figure>
                      <h6>{howWeDoDetail.howwedotitle}</h6>
                      <div>
                        <ul>
                          {howWeDoDetail.howwedouiuxlistingdetail.map(
                            (listing, j) => {
                              return (
                                <li key={j}>
                                  <p>{listing.uiuxlistingtitle}</p>
                                </li>
                              );
                            }
                          )}
                        </ul>
                      </div>
                      <a></a>
                    </div>
                  </Fade>
                </li>
              );
            })}
          </ul>
        </Col>
      </Row>
    </section>
  );
};

export default HowWeDo;
