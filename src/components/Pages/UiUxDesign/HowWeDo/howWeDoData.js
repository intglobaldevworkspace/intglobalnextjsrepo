/* eslint max-len: 0 */

export const howwedo = [
  {
    id: '1',
    iconUrl: '/static/images/icons/uiux-design/how-do-we-do/research.svg',
    tooltipID: 'Toolti01',
    title: 'Research',
    listings: ['Generative User Research', 'Expert Reviews, Heuristic Analysis & Competitor Analysis', 'Usability Testing', 'User Experience Testing', 'Emerging trends & Behaviour', 'Ethnographic Research']
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/uiux-design/how-do-we-do/design.svg',
    tooltipID: 'Toolti02',
    title: 'Design',
    listings: ['Experience Design', 'Design Audit (UX/UI)', 'UX/UI Design (Digital product and Services)', 'Digital Innovation Consulting', 'Digital Branding', 'Interaction Design and Prototyping']
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/uiux-design/how-do-we-do/build.svg',
    tooltipID: 'Toolti03',
    title: 'Build',
    listings: ['Front End Development', 'Web Application', 'Mobile Application', 'Custom Application', 'SaaS Implementation']
  },
];

export default howwedo;
