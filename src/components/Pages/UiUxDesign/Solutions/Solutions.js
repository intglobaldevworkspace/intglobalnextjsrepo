import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const Solutions = (props) => {
  // console.log(props.solcont);
  const { solcontheaderone, solcontheadertwo } = props.solcont;
  return (
    <section className="solutions">
      <Row>
        <Col md="9">
          <Fade bottom>
            <h2>{solcontheaderone}</h2>
          </Fade>
        </Col>
        <Col md="7">
          <Fade bottom>
            <p>{solcontheadertwo}</p>
          </Fade>
        </Col>
      </Row>
    </section>
  );
};

export default Solutions;
