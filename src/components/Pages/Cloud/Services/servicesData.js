/* eslint max-len: 0 */

export const services = [
  {
    id: '1',
    iconUrl: '/static/images/icons/cloud/services/cloud-managed-services.svg',
    title: 'Cloud managed services',
    tooltipID: 'Toolti01',
    tooltipText: 'Focus on your core business, leave the cloud management to us.'
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/cloud/services/cloud-devops.svg',
    title: 'Cloud DevOps',
    tooltipID: 'Toolti02',
    tooltipText: 'Our cloud DevOps services are based on a reliable framework that automates routine, activities and design applications with simplified tasks for greater stability and standardization.'
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/cloud/services/cloud-sec-ops-compliance.svg',
    title: 'Cloud and SecOps and compliance',
    tooltipID: 'Toolti03',
    tooltipText: 'End-to-End security, vulnerabilities, and cloud-specific attack vectors monitored, self-healing deployments with compliance guardrails baked in.'
  },
  {
    id: '4',
    iconUrl: '/static/images/icons/cloud/services/security-strategy-risks.svg',
    title: 'Security strategy and Risks',
    tooltipID: 'Toolti04',
    tooltipText: 'Better manage your risks, compliance and governance by teaming with our services experts.'
  },
  {
    id: '5',
    iconUrl: '/static/images/icons/cloud/services/cio-advisory-services.svg',
    title: 'CIO advisory services',
    tooltipID: 'Toolti05',
    tooltipText: 'Maximize investments with the right hybrid multi- cloud strategy using the best open technology and platforms that scale.'
  },
  {
    id: '6',
    iconUrl: '/static/images/icons/cloud/services/cloud-adoption-advisory-services.svg',
    title: 'Cloud adoption advisory services',
    tooltipID: 'Toolti06',
    tooltipText: 'Today cloud adoption is the way in which businesses implement digital transformation. We help you drive your transformation to achieve the greatest impact.'
  },
  {
    id: '7',
    iconUrl: '/static/images/icons/cloud/services/cloud-engineering-solutions.svg',
    title: 'Cloud Engineering solutions',
    tooltipID: 'Toolti07',
    tooltipText: 'Streamlined and continuous development operations in the cloud.'
  },
  {
    id: '8',
    iconUrl: '/static/images/icons/cloud/services/cloud-re-engineering-solutions.svg',
    title: 'Cloud Re-Engineering solutions',
    tooltipID: 'Toolti08',
    tooltipText: 'Build, Lift-and-shift, Replatform, or refactor a portion of the application.'
  },
  {
    id: '9',
    iconUrl: '/static/images/icons/cloud/services/cloud-migration-services.svg',
    title: 'Cloud migration services',
    tooltipID: 'Toolti09',
    tooltipText: 'A planned iterative approach ensures minimum disruption to your business operations.'
  },
  {
    id: '10',
    iconUrl: '/static/images/icons/cloud/services/hybrid-multi-cloud-solutions.svg',
    title: 'Hybrid Multi-Cloud Solutions',
    tooltipID: 'Toolti10',
    tooltipText: 'Enabling end-to-end visibility into the Hybrid Multi-cloud environment, Hybrid Multi-Cloud Migration and  Hybrid Multi-Cloud Orchestration.'
  },
  {
    id: '11',
    iconUrl: '/static/images/icons/cloud/services/legacy-systems-transformations.svg',
    title: 'Legacy systems transformations',
    tooltipID: 'Toolti11',
    tooltipText: 'Free your business from experiencing scalability and operations issues and enable your company to add critical business functionality quickly.'
  },
];

export default services;
