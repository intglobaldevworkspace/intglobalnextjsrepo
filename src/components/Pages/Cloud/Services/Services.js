import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

// import servicesData from "./servicesData";

const Services = (props) => {
  // console.log(props.servicescont);
  const {
    servicescloudheader,
    servicescloudsubheader,
    servicescontdata,
  } = props.servicescont;
  const urlprefix = process.env.servUploadImg;
  return (
    <section className="our-services">
      <Fade bottom>
        <h2>{servicescloudheader}</h2>
      </Fade>
      <Fade bottom>
        <Row>
          <Col md="11">
            <p>{servicescloudsubheader}</p>
          </Col>
        </Row>
      </Fade>
      <Row>
        <Col md="12">
          {servicescontdata.map((serviceDetail) => {
            return (
              <Col
                id={serviceDetail.id}
                md="4"
                className="srvc-item"
                key={serviceDetail.id}
              >
                <Fade bottom>
                  <div className="hvr-blocks-out">
                    <figure>
                      <img
                        src={urlprefix + serviceDetail.clouddataimg.url}
                        className="img-fluid"
                        alt={serviceDetail.clouddatatitle}
                      />
                    </figure>
                    <h6>{serviceDetail.clouddatatitle}</h6>
                    {/* <p>{serviceDetail.content}</p> */}
                    <div>
                      <p>{serviceDetail.clouddatacontent}</p>
                    </div>
                    <a></a>
                  </div>
                </Fade>
              </Col>
            );
          })}
        </Col>
      </Row>
    </section>
  );
};

export default Services;
