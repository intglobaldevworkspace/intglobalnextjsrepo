import React from "react";
import { Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const Banner = (props) => {
  // console.log(props.hban);
  const { bantextone, bantexttwo, bantextthree, bantextfour } = props.hban;
  return (
    <section className="hdBan_inner">
      <div className="hdBan_inner__teaser">
        <Col md="7">
          <h1>
            <Fade left>
              <b>
                {bantextone} <u>{bantexttwo}</u>
              </b>
            </Fade>
            <Fade left>{bantextthree}</Fade>
            <Fade left>
              <strong>{bantextfour}</strong>
            </Fade>
          </h1>
        </Col>
      </div>
    </section>
  );
};

export default Banner;
