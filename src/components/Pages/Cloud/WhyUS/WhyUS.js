import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

// import whyusData from "./whyusData";

const WhyUS = (props) => {
  // console.log(props.whyusdata);
  const urlprefix = process.env.servUploadImg;
  const { whyusdatacontent, whyusheader, whyussubheader } = props.whyusdata;
  return (
    <section className="whyus">
      <Row>
        <Fade bottom>
          <h2>{whyusheader}</h2>
        </Fade>
        <Fade bottom>
          <Row>
            <Col md="11">
              <p>{whyussubheader}</p>
            </Col>
          </Row>
        </Fade>
        <Col md="12">
          <ul className="whyus-item">
            {whyusdatacontent.map((whyusDetail) => {
              return (
                <li id={whyusDetail.id} md="3" key={whyusDetail.id}>
                  <Fade bottom>
                    <div className="hvr-blocks-out">
                      <figure>
                        <img
                          src={urlprefix + whyusDetail.servcloudwhyusimg.url}
                          className="img-fluid"
                          alt={whyusDetail.servcloudtitle}
                        />
                      </figure>
                      <h6>{whyusDetail.servcloudtitle}</h6>
                      <div>
                        <p>{whyusDetail.servcloudwhyustooltiptext}</p>
                      </div>
                      <a></a>
                    </div>
                  </Fade>
                </li>
              );
            })}
          </ul>
        </Col>
      </Row>
    </section>
  );
};

export default WhyUS;
