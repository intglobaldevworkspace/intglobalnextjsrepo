/* eslint max-len: 0 */

export const whyus = [
  {
    id: '1',
    iconUrl: '/static/images/icons/cloud/why-us/design-think.svg',
    tooltipID: 'Toolti01',
    tooltipText: 'Our experimental product engineering approach helps enterprises to innovate, enhance performance and productivity with agility.',
    title: 'Design Thinking'
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/cloud/why-us/product-development.svg',
    tooltipID: 'Toolti02',
    tooltipText: "Our strong application development team delivers enterprise agility based solutions for organizations to lead business agility transformation waves.",
    title: 'End-to-End product development'
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/cloud/why-us/experienced-developers.svg',
    tooltipID: 'Toolti03',
    tooltipText: 'Our developers are skilled in new and emerging technologies & experienced in building best-in-class and industry leading applications.',
    title: 'Experienced developers'
  },
  {
    id: '4',
    iconUrl: '/static/images/icons/cloud/why-us/innovation.svg',
    tooltipID: 'Toolti04',
    tooltipText: 'Global enterprises considers us their innovation and R&D partner in digital business transformation.',
    title: 'Innovation'
  },
  {
    id: '5',
    iconUrl: '/static/images/icons/cloud/why-us/premium-cloud-provider-partners.svg',
    tooltipID: 'Toolti05',
    tooltipText: 'Our partners are premium cloud providers like AWS, Azure and Digital Ocean.',
    title: 'Premium Cloud provider partners'
  },
  {
    id: '6',
    iconUrl: '/static/images/icons/cloud/why-us/remote-teams.svg',
    tooltipID: 'Toolti06',
    tooltipText: 'We provide you with an extra arm of operations through our remote team services. No matter what the challenge, we always have the right person to do the job.',
    title: 'Remote Teams'
  },
];

export default whyus;
