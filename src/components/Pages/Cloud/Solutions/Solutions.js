import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const Solutions = (props) => {
  // console.log(props);
  return (
    <section className="solutions">
      <Row>
        <Col md="12">
          <Fade bottom>
            <h2>{props.solcont}</h2>
          </Fade>
        </Col>
      </Row>
    </section>
  );
};

export default Solutions;
