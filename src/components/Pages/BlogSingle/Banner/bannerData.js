/* eslint max-len: 0 */

export const singleblogbanner = [
  {
    id: '1',
    bannerUrl: '/static/images/banner/blog/single/banner-single-blog.jpg',
    bannerTitle: 'Emerging Digital Transformation Being Witnessed By The BFSI Sector'
  },
];

export default singleblogbanner;