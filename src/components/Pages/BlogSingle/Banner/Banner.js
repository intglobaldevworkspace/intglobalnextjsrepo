import React from "react";
import { Col, Row } from "reactstrap";

// import bannerData from "./bannerData";

const Banner = (props) => {
  // console.log(props);
  const { blogarticletitle, blogdetailbanner } = props.bandet;
  const urlprefix = process.env.servUploadImg;
  return (
    <section className="hdBan_inner">
      <div className="hdBan_inner__teaser">
        <Col md="12">
          <Row>
            <div className="blog-ban">
              <figure>
                <img
                  src={urlprefix + blogdetailbanner.url}
                  className="img-fluid"
                  alt={blogarticletitle}
                />
              </figure>
            </div>
          </Row>
        </Col>
      </div>
    </section>
  );
};

export default Banner;
