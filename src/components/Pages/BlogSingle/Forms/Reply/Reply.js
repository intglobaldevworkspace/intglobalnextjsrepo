import React from 'react';
import {
  Col,
  Row
} from 'reactstrap';

const Reply = (props) => {
  return (
    <section className="comment-form">
      <Col md="12">
        <Row>
          <h4>Leave a Reply</h4>
          <form method="post">
            <div className="form-group">
              <label>Full Name </label>
              <input type="text" className="form-control" placeholder="Full Name" name="fname" />
            </div>
            <div className="form-group">
              <label>Email <span className="text-danger">*</span> </label>
              <input type="email" className="form-control" placeholder="Email Address" name="email" />
            </div>
            <div className="form-group">
              <label>Comment <span className="text-danger">*</span></label>
              <textarea className="form-control" placeholder="Type your comment..." name="comment" rows="5"></textarea>
            </div>
            <button type="submit" className="btn btn-primary" name="button">Post comment</button>
          </form>
        </Row>
      </Col>
    </section>
  );
}

export default Reply;