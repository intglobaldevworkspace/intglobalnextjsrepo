import React from 'react';
import {
  Col,
  Row
} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faReply } from '@fortawesome/free-solid-svg-icons';

const Comment = (props) => {
  return (
    <section className="comments-list">
      <Col md="12">
        <Row>
          <h4>2 Comments</h4>
          <ul>
            <li className="media">
              <div className="media-body">
                <h5>John Doe</h5>
                <span>Posted on: April 14 2020</span>
                <p>Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition.</p>
                <a href="#" className="btn-link"> <FontAwesomeIcon icon={faReply} /> Reply </a>
              </div>
            </li>
            <li className="media">
                <ul>
                  <li className="media">
                    <div className="media-body">
                      <h5>John Doe</h5>
                      <span>Posted on: July 20 2020</span>
                      <p>Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches</p>
                      <a href="#" className="btn-link"> <FontAwesomeIcon icon={faReply} /> Reply </a>
                    </div>
                  </li>
                </ul>
            </li>
          </ul>
        </Row>
      </Col>
    </section>
  );
}

export default Comment;