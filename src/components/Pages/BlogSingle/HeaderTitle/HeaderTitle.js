import React, { useState } from "react";
import { Col, Row } from "reactstrap";
//import Fade from 'react-reveal/Fade';
import moment from "moment";
// import headerTitleData from "./headerTitleData";

const SingleBlogTitle = (props) => {
  // console.log(props.headerdet);
  const [blogHeaderState, setblogHeaderState] = useState(props.headerdet);
  // console.log(blogHeaderState);
  return (
    <section className="blog-title">
      <Col md="12">
        <Row>
          <div>
            <ul>
              {blogHeaderState.blogcategories &&
                blogHeaderState.blogcategories.length &&
                blogHeaderState.blogcategories.map((catgry, j) => {
                  return (
                    <li key={catgry.id}>
                      <span className="cat">{catgry.blogcatname}</span>
                    </li>
                  );
                })}
            </ul>
            <h1>{blogHeaderState.blogarticletitle}</h1>
            <ul>
              {blogHeaderState.blogarticleauthor.articleauthorstatus ? (
                <li>
                  <span className="author">
                    By{" "}
                    <u>{blogHeaderState.blogarticleauthor.articleauthorname}</u>
                  </span>
                </li>
              ) : null}
              <li>
                <span className="mo">
                  {moment(blogHeaderState.blogarticleauthor.postaddedon).format(
                    "MMM"
                  )}
                </span>{" "}
                <span className="dt">
                  {moment(blogHeaderState.blogarticleauthor.postaddedon).format(
                    "DD"
                  )}
                </span>
                ,{" "}
                <span className="yr">
                  {moment(blogHeaderState.blogarticleauthor.postaddedon).format(
                    "YYYY"
                  )}
                </span>{" "}
                -
              </li>
              {/* <li>
                <span className="views">{headerTitleDetail.view} views</span>
              </li> */}
            </ul>
          </div>
        </Row>
      </Col>
    </section>
  );
};

export default SingleBlogTitle;
