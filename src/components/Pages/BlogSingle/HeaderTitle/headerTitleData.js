/* eslint max-len: 0 */

export const singleblogtitle = [
  {
    id: '1',
    blogtitle: 'Emerging Digital Transformation Being Witnessed By The BFSI Sector',
    author: 'Abhishek Rungta',
    date: '17',
    month: 'July',
    year: '2020',
    view: '119',
    catgry: ['FinTech']
  },
];

export default singleblogtitle;