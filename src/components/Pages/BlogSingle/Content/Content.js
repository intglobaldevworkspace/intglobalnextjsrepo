import React from "react";
import { Col, Row } from "reactstrap";
import {
  FacebookShareButton,
  FacebookIcon,
  TwitterShareButton,
  TwitterIcon,
  LinkedinShareButton,
  LinkedinIcon,
  WhatsappShareButton,
  WhatsappIcon,
  EmailShareButton,
  EmailIcon,
} from "react-share";
import Fade from "react-reveal/Fade";

const Content = (props) => {
  console.log(props);
  const urlprefix = process.env.servUploadImg;
  return (
    <section className="blog-content">
      <ul className="social-share">
        {props.cont.social.map((socialObj) => {
          switch (socialObj.socialplatformname.toUpperCase()) {
            case "FACEBOOK":
              {
                {
                  return socialObj.socialiconshowstatus ? (
                    <li key={socialObj.id}>
                      <FacebookShareButton
                        url={`${urlprefix}/blog/${socialObj.socialneturl}`}
                        quote={socialObj.socialnetquote}
                        hashtag={socialObj.socialnethashtag}
                        className="shfb"
                      >
                        <FacebookIcon size={26} round={true} />
                      </FacebookShareButton>
                    </li>
                  ) : null;
                }
              }
              break;
            case "TWITTER":
              {
                {
                  return socialObj.socialiconshowstatus ? (
                    <li key={socialObj.id}>
                      <TwitterShareButton
                        url={`${urlprefix}/blog/${socialObj.socialneturl}`}
                        quote={socialObj.socialnetquote}
                        hashtag={socialObj.socialnethashtag}
                        className="shtw"
                      >
                        <TwitterIcon size={26} round={true} />
                      </TwitterShareButton>
                    </li>
                  ) : null;
                }
              }
              break;
            case "LINKEDIN":
              {
                {
                  return socialObj.socialiconshowstatus ? (
                    <li key={socialObj.id}>
                      <LinkedinShareButton
                        url={`${urlprefix}/blog/${socialObj.socialneturl}`}
                        quote={socialObj.socialnetquote}
                        hashtag={socialObj.socialnethashtag}
                        className="shlin"
                      >
                        <LinkedinIcon size={26} round={true} />
                      </LinkedinShareButton>
                    </li>
                  ) : null;
                }
              }
              break;
            case "WHATSAPP": {
              {
                return socialObj.socialiconshowstatus ? (
                  <li key={socialObj.id}>
                    <WhatsappShareButton
                      url={`${urlprefix}/blog/${socialObj.socialneturl}`}
                      quote={socialObj.socialnetquote}
                      hashtag={socialObj.socialnethashtag}
                      className="shwap"
                    >
                      <WhatsappIcon size={26} round={true} />
                    </WhatsappShareButton>
                  </li>
                ) : null;
              }
            }
            case "EMAILSHARE":
              {
                {
                  return socialObj.socialiconshowstatus ? (
                    <li key={socialObj.id}>
                      <EmailShareButton
                        url={`${urlprefix}/blog/${socialObj.socialneturl}`}
                        quote={socialObj.socialnetquote}
                        hashtag={socialObj.socialnethashtag}
                        className="shmail"
                      >
                        <EmailIcon size={26} round={true} />
                      </EmailShareButton>
                    </li>
                  ) : null;
                }
              }
              break;
          }
        })}
      </ul>
      <Col md="12">
        <Row>
          <div
            dangerouslySetInnerHTML={{ __html: props.cont.contentbody }}
          ></div>
        </Row>
      </Col>
    </section>
  );
};

export default Content;
