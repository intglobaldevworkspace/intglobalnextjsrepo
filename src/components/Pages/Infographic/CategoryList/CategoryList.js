import React, { Component } from "react";
import { Col, Row, FormGroup } from "reactstrap";
import Fade from "react-reveal/Fade";
import { Formik, Form, Field, ErrorMessage } from "formik";

class CategoryList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      catlist: this.props.catl,
    };
  }

  render() {
    const { catlist } = this.state;
    // console.log(catlist);
    return (
      <section className="category-list">
        <Col md="5">
          <Row>
            <div className="catg-row">
              <Fade bottom>
                <a
                  className="button link active"
                  onClick={(e) => this.props.getallrecordsforcat(e)}
                >
                  All
                </a>
              </Fade>
              <Fade bottom>
                <Formik
                  initialValues={{
                    category: "",
                  }}
                  onChange={(values, actions) => {
                    // console.log(values);
                    // actions.resetForm();
                  }}
                >
                  <Form className="catgform">
                    <FormGroup>
                      <Field
                        as="select"
                        name="category"
                        id="category"
                        value={this.props.selval}
                        onChange={(e) => this.props.catSpecificValues(e)}
                      >
                        <option value="">Categories</option>
                        {catlist.map((catlistObj) => {
                          {
                            return catlistObj.infographiccatstatus ? (
                              <option value={catlistObj.id} key={catlistObj.id}>
                                {catlistObj.infographiccatname}
                              </option>
                            ) : null;
                          }
                        })}
                      </Field>
                    </FormGroup>
                  </Form>
                </Formik>
              </Fade>
            </div>
          </Row>
        </Col>
      </section>
    );
  }
}

export default CategoryList;
