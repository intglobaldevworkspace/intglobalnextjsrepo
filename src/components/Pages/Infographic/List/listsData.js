/* eslint max-len: 0 */

export const infographicslist = [
  {
    id: '1',
    thumbUrl: '/static/images/infographic/thumb/challenger-banks-in-UK-to-look-out-for.jpg',
    infographictitle: '5 Challenger Banks in UK to Look Out For!'
  },
  {
    id: '2',
    thumbUrl: '/static/images/infographic/thumb/how-banks-can-thrive-as-digital-payments-grow.jpg',
    infographictitle: 'How Banks Can Thrive as Digital Payments Grow'
  },
  {
    id: '3',
    thumbUrl: '/static/images/infographic/thumb/mobile-app-development-trends-in-2018.jpg',
    infographictitle: 'Mobile App Development Trends in 2018'
  },
];

export default infographicslist;