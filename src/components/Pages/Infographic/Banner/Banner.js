import React from "react";
import { Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const Banner = (props) => {
  // console.log(props);
  return (
    <section className="hdBan_inner">
      <div className="hdBan_inner__teaser">
        <Col md="7">
          <h1>
            <Fade left>
              <b>
                {props.bann.htextone} <u>{props.bann.htexttwo}</u>
              </b>
            </Fade>
            <Fade left>{props.bann.htextthree}</Fade>
            <Fade left>
              {props.bann.htextfour} <strong>{props.bann.htextfive}</strong>
            </Fade>
          </h1>
        </Col>
      </div>
    </section>
  );
};

export default Banner;
