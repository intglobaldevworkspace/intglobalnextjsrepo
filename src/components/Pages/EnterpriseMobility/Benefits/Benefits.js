import React, { useState } from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

// import benefitsData from './benefitsData';

const Benefits = (props) => {
  // const [tooltipOpen, setTooltipOpen] = useState(false);
  // const toggle = () => setTooltipOpen(!tooltipOpen);
  const urlprefix = process.env.servUploadImg;
  const { bene } = props;
  // console.log(bene);
  return (
    <section className="benefits">
      <Row>
        <Fade bottom>
          <h2>{bene.benefitheader}</h2>
        </Fade>
        <Col md="12">
          <ul className="benefit-item">
            {bene.embenefitscolls.map((benefitDetail) => {
              return (
                <li id={benefitDetail.id} md="3" key={benefitDetail.id}>
                  <Fade bottom>
                    <div className="hvr-blocks-out">
                      <figure>
                        <img
                          src={urlprefix + benefitDetail.benefitimage.url}
                          className="img-fluid"
                          alt={benefitDetail.benefittitle}
                        />
                      </figure>
                      <h6>{benefitDetail.benefittitle}</h6>
                      <div>
                        <p>{benefitDetail.benefittooltipText}</p>
                      </div>
                      <a href="#"></a>
                    </div>
                  </Fade>
                </li>
              );
            })}
          </ul>
        </Col>
      </Row>
    </section>
  );
};

export default Benefits;
