/* eslint max-len: 0 */

export const benefits = [
  {
    id: '1',
    iconUrl: '/static/images/icons/entp-mobility/benefits/availability.svg',
    tooltipID: 'Toolti01',
    tooltipText: 'Become easy to reach with enterprise mobility so your content and information can be availed anywhere and at anytime.',
    title: 'Availability'
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/entp-mobility/benefits/secure.svg',
    tooltipID: 'Toolti02',
    tooltipText: 'Hinders any unauthorized access protecting sensitive information while also clearing any malware from the system.',
    title: 'Secure'
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/entp-mobility/benefits/improve-employee-productivity.svg',
    tooltipID: 'Toolti03',
    tooltipText: 'The key office applications can be extended to employees’ devices, allowing them to securely carry out important business functions anytime and anywhere.',
    title: 'Improve Employee Productivity'
  },
  {
    id: '4',
    iconUrl: '/static/images/icons/entp-mobility/benefits/content-sharing.svg',
    tooltipID: 'Toolti04',
    tooltipText: 'Easy accessibility anywhere irrespective of time and place making content sharing possible with just a few clicks.',
    title: 'Content Sharing'
  },
  {
    id: '5',
    iconUrl: '/static/images/icons/entp-mobility/benefits/customer-experience.svg',
    tooltipID: 'Toolti05',
    tooltipText: 'With the whole business working as a single unit, the ability to deliver great customer experience is magnified.',
    title: 'Customer Experience'
  },
  {
    id: '6',
    iconUrl: '/static/images/icons/entp-mobility/benefits/increased-roi.svg',
    tooltipID: 'Toolti06',
    tooltipText: 'Using enterprise mobility services, big enterprises and companies can optimize their workflows, and broaden their reach effectively increasing ROI',
    title: 'Increased ROI'
  },
];

export default benefits;
