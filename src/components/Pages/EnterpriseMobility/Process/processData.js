/* eslint max-len: 0 */

export const process = [
  {
    id: '1',
    iconUrlJ: '/static/images/process/entp-mobility/mobility-assessment.png',
    iconUrlW: '/static/images/process/entp-mobility/mobility-assessment.webp',
    title: 'Mobility Assessment'
  },
  {
    id: '2',
    iconUrlJ: '/static/images/process/entp-mobility/planning-strategy.png',
    iconUrlW: '/static/images/process/entp-mobility/planning-strategy.webp',
    title: 'Planning & Strategy'
  },
  {
    id: '3',
    iconUrlJ: '/static/images/process/entp-mobility/ui-ux-design.png',
    iconUrlW: '/static/images/process/entp-mobility/ui-ux-design.webp',
    title: 'UI/UX Design'
  },
  {
    id: '4',
    iconUrlJ: '/static/images/process/entp-mobility/engineering-development.png',
    iconUrlW: '/static/images/process/entp-mobility/engineering-development.webp',
    title: 'Engineering and Development'
  },
  {
    id: '5',
    iconUrlJ: '/static/images/process/entp-mobility/support.png',
    iconUrlW: '/static/images/process/entp-mobility/support.webp',
    title: 'Support'
  },
  {
    id: '6',
    iconUrlJ: '/static/images/process/entp-mobility/enterprise-on-mobile.png',
    iconUrlW: '/static/images/process/entp-mobility/enterprise-on-mobile.webp',
    title: 'Enterprise On Mobile'
  },
  {
    id: '7',
    iconUrlJ: '/static/images/process/entp-mobility/managed-application-distribution.png',
    iconUrlW: '/static/images/process/entp-mobility/managed-application-distribution.webp',
    title: 'Managed Application Distribution'
  },
  {
    id: '8',
    iconUrlJ: '/static/images/process/entp-mobility/testing-qa.png',
    iconUrlW: '/static/images/process/entp-mobility/testing-qa.webp',
    title: 'Testing & QA'
  },
];

export default process;
