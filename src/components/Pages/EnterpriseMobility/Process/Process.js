import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

// import processData from './processData';

const Process = (props) => {
  const urlprefix = process.env.servUploadImg;
  const { processcont } = props;
  // console.log(processcont);
  return (
    <section className="our-process">
      <Fade bottom>
        <h2>{processcont.emprocessheader}</h2>
      </Fade>
      <Row>
        {processcont.emprocesscolls.map((processDetail) => {
          return (
            <Col
              id={processDetail.id}
              md="3"
              className="process-item"
              key={processDetail.id}
            >
              <Fade bottom>
                <figure>
                  <img
                    src={urlprefix + processDetail.processimage.url}
                    className="img-fluid"
                    alt={processDetail.processtitle}
                  />
                </figure>
                <h4>{processDetail.processtitle}</h4>
              </Fade>
            </Col>
          );
        })}
      </Row>
    </section>
  );
};

export default Process;
