/* eslint max-len: 0 */

export const whyus = [
  {
    id: '1',
    iconUrl: '/static/images/icons/entp-mobility/why-us/strategic-thinking.svg',
    tooltipID: 'Toolti01',
    tooltipText: 'We implement strategy using Design Thinking and product engineering mindset giving us the edge to build innovative out-of-the-box solutions.',
    title: 'Strategic Thinking'
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/entp-mobility/why-us/specialisation-mobility-solutions.svg',
    tooltipID: 'Toolti02',
    tooltipText: 'We build solutions involving multiple digital touchpoints and connected devices for clients globally that have led to customer success.',
    title: 'Specialisation in Mobility Solutions'
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/entp-mobility/why-us/handpicked-developers.svg',
    tooltipID: 'Toolti03',
    tooltipText: 'Our experienced mobile developers are constantly upskilled and trained for the latest Security practices, SDKs and frameworks as well as mobility paradigms.',
    title: 'Handpicked Developers'
  },
  {
    id: '4',
    iconUrl: '/static/images/icons/entp-mobility/why-us/agile-processes-focussed.svg',
    tooltipID: 'Toolti04',
    tooltipText: 'We provide rigorous training of best engineering practices and processes like Agile or LEAN methodology.',
    title: 'Agile Processes Focussed'
  },
  {
    id: '5',
    iconUrl: '/static/images/icons/entp-mobility/why-us/enterprise-delivery-practices.svg',
    tooltipID: 'Toolti05',
    tooltipText: 'Our development process uses best development practices like Agile methodologies, Continuous Integration (CI) and Continuous Deployment (CD).',
    title: 'Enterprise Delivery Practices'
  },
  {
    id: '6',
    iconUrl: '/static/images/icons/entp-mobility/why-us/years-of-experience.svg',
    tooltipID: 'Toolti06',
    tooltipText: 'with over two decades in servicing clients globally and 300+ years of combined individual expertise you know you are in good hands when you sign up with us.',
    title: '22+ Years of Experience'
  },
  {
    id: '7',
    iconUrl: '/static/images/icons/entp-mobility/why-us/focus-on-standards-quality.svg',
    tooltipID: 'Toolti07',
    tooltipText: 'Our developers are trained against and follow best-in-class coding standards that are enforced through automated & manual code reviews.',
    title: 'Focus On Standards & Quality'
  },
  {
    id: '8',
    iconUrl: '/static/images/icons/entp-mobility/why-us/flexible-contracts.svg',
    tooltipID: 'Toolti08',
    tooltipText: 'Recognising the varied situations that can exist for our clients, we have very flexible terms of engagement like time and material contracts, our skin in your game model and so on.',
    title: 'Flexible Contracts'
  },
];

export default whyus;
