import React, { useState } from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

// import whyusData from "./whyusData";

const WhyUS = (props) => {
  // const [tooltipOpen, setTooltipOpen] = useState(false);
  // const toggle = () => setTooltipOpen(!tooltipOpen);
  const { whyuscont } = props;
  const urlprefix = process.env.servUploadImg;
  // console.log(whyuscont);
  return (
    <section className="whyus">
      <Row>
        <Fade bottom>
          <h2>{whyuscont.whyusheadertxt}</h2>
        </Fade>
        <Col md="12">
          <ul className="whyus-item">
            {whyuscont.emwhyuscolls.map((whyusDetail) => {
              return (
                <li id={whyusDetail.id} md="3" key={whyusDetail.id}>
                  <Fade bottom>
                    <div className="hvr-blocks-out">
                      <figure>
                        <img
                          src={urlprefix + whyusDetail.whyusimage.url}
                          className="img-fluid"
                          alt={whyusDetail.whyustooltiptitle}
                        />
                      </figure>
                      <h6>{whyusDetail.whyustooltiptitle}</h6>
                      <div>
                        <p>{whyusDetail.whyustooltiptxt}</p>
                      </div>
                      <a href="#"></a>
                    </div>
                  </Fade>
                </li>
              );
            })}
          </ul>
        </Col>
      </Row>
    </section>
  );
};

export default WhyUS;
