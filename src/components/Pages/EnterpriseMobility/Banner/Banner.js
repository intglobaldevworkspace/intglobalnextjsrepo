import React from "react";
import { Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const Banner = (props) => {
  const { emhdrban } = props;
  return (
    <section className="hdBan_inner">
      <div className="hdBan_inner__teaser">
        <Col md="6">
          <h1>
            <Fade left>
              <div
                dangerouslySetInnerHTML={{ __html: emhdrban.hdrbannertxtone }}
              ></div>
            </Fade>
            <Fade left>
              <div>{emhdrban.hdrbannertxttwo}</div>
            </Fade>
            <strong>
              <Fade left>
                <div>{emhdrban.hdrbannertxtthree}</div>
              </Fade>
              <Fade left>
                <div>{emhdrban.hdrbannertextfour}</div>
              </Fade>
            </strong>
          </h1>
        </Col>
      </div>
    </section>
  );
};

export default Banner;
