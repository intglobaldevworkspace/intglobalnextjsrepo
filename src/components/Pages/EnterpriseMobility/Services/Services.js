import React, { useState } from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

// import servicesData from "./servicesData";

const Services = (props) => {
  // const [tooltipOpen, setTooltipOpen] = useState(false);
  // const toggle = () => setTooltipOpen(!tooltipOpen);
  const { servcont } = props;
  // console.log(servcont);
  const urlprefix = process.env.servUploadImg;
  return (
    <section className="our-services">
      <Fade bottom>
        <h2>{servcont.servicesheader}</h2>
      </Fade>
      <Row>
        <Col md="12">
          {servcont.emservicescolls.map((serviceDetail) => {
            return (
              <Col
                id={serviceDetail.id}
                md="4"
                className="srvc-item"
                key={serviceDetail.id}
              >
                <Fade bottom>
                  <div className="hvr-blocks-out">
                    <figure>
                      <img
                        src={
                          urlprefix +
                          serviceDetail.emservicescmp.serviceimage.url
                        }
                        className="img-fluid"
                        alt={serviceDetail.emservicescmp.servicetitle}
                      />
                    </figure>
                    <h6>{serviceDetail.emservicescmp.servicetitle}</h6>
                    <div>
                      <p>{serviceDetail.emservicescmp.tooltiptext}</p>
                    </div>
                    <a href="#"></a>
                  </div>
                </Fade>
              </Col>
            );
          })}
        </Col>
      </Row>
    </section>
  );
};

export default Services;
