/* eslint max-len: 0 */

export const services = [
  {
    id: '1',
    iconUrl: '/static/images/icons/entp-mobility/services/product-engineering-mindset.svg',
    title: 'Product Engineering Mindset',
    tooltipID: 'Toolti01',
    tooltipText: 'Our team specialises in Enterprise Web Development, Mobile Apps, B2B Mobile App Development, Cross-platform Enterprise Apps to meet all your neads in enterprise mobility roadmap.'
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/entp-mobility/services/engaging-ui-ux.svg',
    title: 'Engaging UI/UX',
    tooltipID: 'Toolti02',
    tooltipText: 'We deliver engaging and feature-rich experiences that are tailored to your end-users (employees, customers or partners).'
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/entp-mobility/services/application-development.svg',
    title: 'Application Development',
    tooltipID: 'Toolti03',
    tooltipText: 'Our team specialises in Enterprise Web Development, Mobile Apps, B2B Mobile App Development, Cross-platform Enterprise Apps to meet all your neads in enterprise mobility roadmap.'
  },
  {
    id: '4',
    iconUrl: '/static/images/icons/entp-mobility/services/analytics.svg',
    title: 'Analytics',
    tooltipID: 'Toolti04',
    tooltipText: 'Our analytics solutions allow collection, consolidation, and analysis of your enterprise solution data to assist with defining performance metrics and assessing health status for smooth operations.'
  },
  {
    id: '5',
    iconUrl: '/static/images/icons/entp-mobility/services/testing-maintenance-support.svg',
    title: 'Testing, Maintenance & Support',
    tooltipID: 'Toolti05',
    tooltipText: 'Rest easier by transferring testing, support & maintenance of your solutions to our highly experienced robust support systems and Remote Teams'
  },
  {
    id: '6',
    iconUrl: '/static/images/icons/entp-mobility/services/backend-implementation-integration.svg',
    title: 'Back-end Implementation & Integration',
    tooltipID: 'Toolti06',
    tooltipText: 'Expand and integrate 3rd party solutions and services like CMS, CRM, ERPs, and even social media & CXM platforms to extract the full potential of your mobility solution'
  },
  {
    id: '7',
    iconUrl: '/static/images/icons/entp-mobility/services/security.svg',
    title: 'Security',
    tooltipID: 'Toolti07',
    tooltipText: 'Wireframing, responsive design, user experience testing, prototyping, customer journey mapping, and UX optimizations.'
  },
  {
    id: '8',
    iconUrl: '/static/images/icons/entp-mobility/services/legacy-app-modernization.svg',
    title: 'Legacy App Modernization',
    tooltipID: 'Toolti08',
    tooltipText: 'Drive high-LTV traffic and conversions through audience targeting, ad creative, and optimizations on channels such as Facebook, Google, Pinterest, etc.'
  },
  {
    id: '9',
    iconUrl: '/static/images/icons/entp-mobility/services/cloudstorage-hosting-migration.svg',
    title: 'Cloud Storage, Hosting Migration',
    tooltipID: 'Toolti09',
    tooltipText: 'Move your mobility solution across existing Cloud hosting providers or on to your private cloud.'
  },
];

export default services;
