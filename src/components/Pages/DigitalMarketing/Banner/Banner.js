import React from "react";
import { Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const Banner = (props) => {
  // console.log(props);
  return (
    <section className="hdBan_inner">
      <div className="hdBan_inner__teaser">
        <Col md="6">
          <h1>
            <Fade left>
              <b
                dangerouslySetInnerHTML={{
                  __html: props.hdrbanner.dmheaderbanner.headerbannertxtone,
                }}
              ></b>
            </Fade>
            <Fade left>
              {props.hdrbanner.dmheaderbanner.headerbannertxttwo}
            </Fade>
            <strong>
              <Fade left>
                {props.hdrbanner.dmheaderbanner.headerbannertxtthree}
              </Fade>
              <Fade left>
                {props.hdrbanner.dmheaderbanner.headerbannertxtfour}
              </Fade>
            </strong>
          </h1>
        </Col>
      </div>
    </section>
  );
};

export default Banner;
