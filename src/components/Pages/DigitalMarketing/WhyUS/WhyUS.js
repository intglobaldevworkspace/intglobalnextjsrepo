import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

// import whyusData from "./whyusData";

export default class WhyUS extends Component {
  // const [tooltipOpen, setTooltipOpen] = useState(false);
  // const toggle = () => setTooltipOpen(!tooltipOpen);
  constructor(props) {
    super(props);

    this.state = {
      tooltipOpen: false,
      whyusfullcontent: props.whyuscont,
    };
  }

  render() {
    const { whyusfullcontent } = this.state;
    const urlprefix = process.env.servUploadImg;
    // console.log(whyusfullcontent);
    return (
      <section className="why-us">
        <Row>
          <Fade bottom>
            <h2>{whyusfullcontent.whyusheader}</h2>
          </Fade>
          <Fade bottom>
            <p>{whyusfullcontent.whyussubheader}</p>
          </Fade>
          <Col md="12">
            <ul className="whyus-item">
              {whyusfullcontent.whyuscontentdetails.map((whyusDetail) => {
                return (
                  <li id={whyusDetail.id} md="3" key={whyusDetail.id}>
                    <Fade bottom>
                      <div className="hvr-blocks-out">
                        <figure>
                          <img
                            src={urlprefix + whyusDetail.dmwhyusimageicon.url}
                            className="img-fluid"
                            alt={whyusDetail.dmwhyustitle}
                          />
                        </figure>
                        <h6>{whyusDetail.dmwhyustitle}</h6>
                        <div>
                          <p>{whyusDetail.dmwhyustooltiptext}</p>
                        </div>
                        <a href="#"></a>
                      </div>
                    </Fade>
                  </li>
                );
              })}
            </ul>
          </Col>
        </Row>
      </section>
    );
  }
}
