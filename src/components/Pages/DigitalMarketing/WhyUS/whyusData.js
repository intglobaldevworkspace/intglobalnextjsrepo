/* eslint max-len: 0 */

export const whyus = [
  {
    id: '1',
    iconUrl: '/static/images/icons/dig-marketing/why-us/hourly-rate.svg',
    tooltipID: 'Toolti01',
    tooltipText: 'Monthly retainer based on hourly rate',
    title: 'Monthly retainer based on hourly rate'
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/dig-marketing/why-us/agile-methodology.svg',
    tooltipID: 'Toolti02',
    tooltipText: 'Agile methodology incorporating creatives, strategists, designers and developers as needed',
    title: 'Agile methodology incorporating creatives, strategists, designers and developers as needed'
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/dig-marketing/why-us/integrated-company-goals.svg',
    tooltipID: 'Toolti03',
    tooltipText: 'Integrated approach based around your company’s goals and audience, not marketing tactics or channels',
    title: 'Integrated approach based around your company’s goals and audience, not marketing tactics or channels'
  },
  {
    id: '4',
    iconUrl: '/static/images/icons/dig-marketing/why-us/cost-per-acquisition.svg',
    tooltipID: 'Toolti04',
    tooltipText: 'Cost per acquisition and ROI focus',
    title: 'Cost per acquisition and ROI focus'
  },
  {
    id: '5',
    iconUrl: '/static/images/icons/dig-marketing/why-us/analytics-dashboard.svg',
    tooltipID: 'Toolti05',
    tooltipText: 'Analytics dashboard customized for your business',
    title: 'Analytics dashboard customized for your business'
  },
  {
    id: '6',
    iconUrl: '/static/images/icons/dig-marketing/why-us/ad-spend-management.svg',
    tooltipID: 'Toolti06',
    tooltipText: 'No-commission ad spend management',
    title: 'No-commission ad spend management'
  },
  {
    id: '7',
    iconUrl: '/static/images/icons/dig-marketing/why-us/longterm-sustainable-growth.svg',
    tooltipID: 'Toolti07',
    tooltipText: 'Long-term orientation that creates consistent, sustainable growth',
    title: 'Long-term orientation that creates consistent, sustainable growth'
  },
  {
    id: '8',
    iconUrl: '/static/images/icons/dig-marketing/why-us/performance-reviews.svg',
    tooltipID: 'Toolti08',
    tooltipText: 'Weekly collaboration, monthly reporting and quarterly performance reviews',
    title: 'Weekly collaboration, monthly reporting and quarterly performance reviews'
  },
];

export default whyus;
