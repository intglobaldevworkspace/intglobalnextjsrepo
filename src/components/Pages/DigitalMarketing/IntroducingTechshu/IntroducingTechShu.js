import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

class IntroducingTechShu extends Component {
  // constructor(props) {
  //   super(props);

  //   this.state = {
  //     techshuContent: this.props.techshu,
  //   };
  // }

  render() {
    const urlprefix = process.env.servUploadImg;
    // const { techshuContent } = this.state;
    const techshuContent = this.props.techshu;
    // console.log(techshuContent);
    return (
      <section className="intro_techshu">
        <Fade bottom>
          <div
            dangerouslySetInnerHTML={{ __html: techshuContent.techshuHeader }}
          ></div>
        </Fade>
        <Row>
          <Col md="12">
            <Fade bottom>
              <div
                dangerouslySetInnerHTML={{
                  __html: techshuContent.techshuSubheader,
                }}
              ></div>
            </Fade>
          </Col>
          <Col md="4">
            <Fade left>
              <figure>
                <img
                  src={urlprefix + techshuContent.techshuLogo.url}
                  className="img-fluid"
                  alt="Introducing - Techshu"
                />
              </figure>
            </Fade>
          </Col>
          <Col md="8">
            <Fade right>
              <p
                dangerouslySetInnerHTML={{
                  __html: techshuContent.techshuDescrip,
                }}
              ></p>
            </Fade>
          </Col>
        </Row>
        <Fade bottom>
          {techshuContent.knowmorelinkstatus ? (
            <a href={techshuContent.knowmorelinkurl} className="btn link">
              {techshuContent.techshumore}
            </a>
          ) : null}
        </Fade>
      </section>
    );
  }
}

export default IntroducingTechShu;
