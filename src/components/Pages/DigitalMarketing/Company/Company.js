import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const Company = (props) => {
  const { dmcompanyprof } = props.companydet;
  // console.log(dmcompanyprof);
  return (
    <section className="company">
      <Fade bottom>
        <div
          dangerouslySetInnerHTML={{ __html: dmcompanyprof.companyprofheader }}
        ></div>
        <Row>
          <Col md="12">
            <p
              dangerouslySetInnerHTML={{
                __html: dmcompanyprof.companyprofsubheader,
              }}
            ></p>
          </Col>
        </Row>
      </Fade>
    </section>
  );
};

export default Company;
