/* eslint max-len: 0 */

export const services = [
  {
    id: '1',
    iconUrl: '/static/images/icons/dig-marketing/services/growth-strategy.svg',
    title: 'Growth Strategy',
    tooltipID: 'Toolti01',
    tooltipText: 'Design & execute integrated growth strategies grounded in cross-channel data and analytics.',
    content: 'Design & execute integrated growth strategies grounded in cross-channel data and analytics.'
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/dig-marketing/services/fullstack-development.svg',
    title: 'Full-Stack Development',
    tooltipID: 'Toolti02',
    tooltipText: 'Conversion-focused website development, re-platforming, e-commerce & customization.',
    content: 'Conversion-focused website development, re-platforming, e-commerce & customization.'
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/dig-marketing/services/email-marketing.svg',
    title: 'Email Marketing',
    tooltipID: 'Toolti03',
    tooltipText: 'Scale email channel revenue through campaign audit and strategic email automation overhaul.',
    content: 'Scale email channel revenue through campaign audit and strategic email automation overhaul.'
  },
  {
    id: '4',
    iconUrl: '/static/images/icons/dig-marketing/services/conversion-rate-optimization.svg',
    title: 'Conversion Rate Optimization',
    tooltipID: 'Toolti04',
    tooltipText: 'A/B test website elements to improve e-commerce performance and convert customers.',
    content: 'A/B test website elements to improve e-commerce performance and convert customers.'
  },
  {
    id: '5',
    iconUrl: '/static/images/icons/dig-marketing/services/search-engine-optimization.svg',
    title: 'Search Engine Optimization',
    tooltipID: 'Toolti05',
    tooltipText: 'Increasing organic search traffic through content and code.',
    content: 'Increasing organic search traffic through content and code.'
  },
  {
    id: '6',
    iconUrl: '/static/images/icons/dig-marketing/services/reporting-analytics.svg',
    title: 'Reporting & Analytics',
    tooltipID: 'Toolti06',
    tooltipText: 'Analytical reviews of cross channel-performance and tests to surface actionable takeaways.',
    content: 'Analytical reviews of cross channel-performance and tests to surface actionable takeaways.'
  },
  {
    id: '7',
    iconUrl: '/static/images/icons/dig-marketing/services/ux-ui-design.svg',
    title: 'UX/UI Design',
    tooltipID: 'Toolti07',
    tooltipText: 'Wireframing, responsive design, user experience testing, prototyping, customer journey mapping, and UX optimizations.',
    content: 'Wireframing, responsive design, user experience testing, prototyping, customer journey mapping, and UX optimizations.'
  },
  {
    id: '8',
    iconUrl: '/static/images/icons/dig-marketing/services/paid-media-ppc.svg',
    title: 'Paid Media (PPC)',
    tooltipID: 'Toolti08',
    tooltipText: 'Drive high-LTV traffic and conversions through audience targeting, ad creative, and optimizations on channels such as Facebook, Google, Pinterest, etc.',
    content: 'Drive high-LTV traffic and conversions through audience targeting, ad creative, and optimizations on channels such as Facebook, Google, Pinterest, etc.'
  },
  {
    id: '9',
    iconUrl: '/static/images/icons/dig-marketing/services/creatives-content.svg',
    title: 'Creatives & Content',
    tooltipID: 'Toolti09',
    tooltipText: 'Provide content tailored to specific channels with a focus on storytelling, creative direction, brand guidelines, brand collateral design, animation, photography, video, messaging, and illustration.',
    content: 'Provide content tailored to specific channels with a focus on storytelling, creative direction, brand guidelines, brand collateral design, animation, photography, video, messaging, and illustration.'
  },
];

export default services;
