import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

export default class Services extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tooltipOpen: false,
      companyservices: this.props.dmsrv,
    };
  }

  toggle = () => {
    this.setState({ tooltipOpen: !this.tooltipOpen });
  };

  render() {
    // console.log(this.state);
    const { companyservices } = this.state;
    // console.log(companyservices);
    const urlprefix = process.env.servUploadImg;
    return (
      <section className="services">
        <Fade bottom>
          <h2>{companyservices.header}</h2>
        </Fade>
        <Row>
          <Fade bottom>
            <Col md="12">
              <div
                dangerouslySetInnerHTML={{ __html: companyservices.subheader }}
              ></div>
            </Col>
          </Fade>
          <Col md="12">
            {companyservices.servicerecords.map((serviceDetail) => {
              return (
                <Col
                  id={serviceDetail.id}
                  md="4"
                  className="srvc-item"
                  key={serviceDetail.id}
                >
                  <Fade bottom>
                    <div className="hvr-blocks-out">
                      <figure>
                        <img
                          src={urlprefix + serviceDetail.dmreciconimage.url}
                          className="img-fluid"
                          alt={serviceDetail.dmrecdatatitle}
                        />
                      </figure>
                      <h6>{serviceDetail.dmrecdatatitle}</h6>
                      <p>{serviceDetail.dmreccontent}</p>
                      <div>
                        <p>{serviceDetail.dmrectooltiptext}</p>
                      </div>
                      <a href="#"></a>
                    </div>
                  </Fade>
                </Col>
              );
            })}
          </Col>
        </Row>
      </section>
    );
  }
}
