import React from "react";
import { Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const Banner = (props) => {
  // console.log(props);
  return (
    <section className="hdBan_inner">
      <div className="hdBan_inner__teaser">
        <Col md="7">
          <h1>
            <Fade left>
              <b
                dangerouslySetInnerHTML={{
                  __html: props.bannerdet.applicationdevbannerone,
                }}
              ></b>
            </Fade>
            <Fade left>
              <div
                dangerouslySetInnerHTML={{
                  __html: props.bannerdet.applicationdevbannertwo,
                }}
              ></div>
            </Fade>
            <Fade left>
              <div
                dangerouslySetInnerHTML={{
                  __html: props.bannerdet.applicationdevbannerthree,
                }}
              ></div>
            </Fade>
          </h1>
        </Col>
      </div>
    </section>
  );
};

export default Banner;
