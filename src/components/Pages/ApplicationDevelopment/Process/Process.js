import React, { useState } from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";
import { useMediaQuery } from "react-responsive";

import ProcessDesk from "./ProcessDesk";
import ProcessMob from "./ProcessMob";
import Contactus from "../../../shared/Contactus";

export default function Process(props) {
  const isDesktopOrLaptop = useMediaQuery({ minDeviceWidth: 1024 });
  const isTabletOrMobile = useMediaQuery({ maxDeviceWidth: 1023 });
  // console.log(props.processcont);

  const [appdevprocessstate, setappdevprocessstate] = useState({
    isOpen: false,
    showContactusForm: true,
    showContactusSubmitted: false,
    ContactusSubmitfailed: false,
    showSubmitLoader: false,

    contactuspopup: props.processcont.contactuspopup,
  });

  const toggleModal = () => {
    setappdevprocessstate((prevState) => ({
      ...prevState,
      isOpen: !prevState.isOpen,
      showContactusForm: true,
      showContactusSubmitted: false,
      ContactusSubmitfailed: false,
      showSubmitLoader: false,
    }));
  };

  const cntpopup = {
    isOpen: appdevprocessstate.isOpen,
    showContactusForm: appdevprocessstate.showContactusForm,
    showContactusSubmitted: appdevprocessstate.showContactusSubmitted,
    ContactusSubmitfailed: appdevprocessstate.ContactusSubmitfailed,
    showSubmitLoader: appdevprocessstate.showSubmitLoader,
    contactusPopup: appdevprocessstate.contactuspopup,
  };
  const {
    processcontheader,
    processdataone,
    processdatatwo,
    processletustalk,
  } = props.processcont;

  return (
    <section className="our-process">
      <Fade bottom>
        <h2>{processcontheader}</h2>
      </Fade>
      <Row>
        <div className="process">
          {isDesktopOrLaptop && (
            <ProcessDesk
              pdone={processdataone}
              pdtwo={processdatatwo}
              footer={props.processcont.processfooter}
            />
          )}
          {isTabletOrMobile && (
            <ProcessMob
              pdone={processdataone}
              pdtwo={processdatatwo}
              footer={props.processcont.processfooter}
            />
          )}
          <Fade bottom>
            {processletustalk.appdevletustalkbuttonstatus ? (
              <a
                href={processletustalk.appdevletustalkurl}
                className="btn link"
                onClick={(e) => {
                  e.preventDefault();
                  // toggle();
                  toggleModal();
                }}
              >
                {processletustalk.appdevletustalktext}
              </a>
            ) : null}
          </Fade>
        </div>
      </Row>
      {appdevprocessstate.isOpen ? (
        <Contactus
          cntpopup={{
            ...cntpopup,
            contactText: processletustalk.appdevletustalktext,
          }}
          handler={toggleModal}
        />
      ) : null}
    </section>
  );
}
