import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import Slider from "react-slick";
import Fade from "react-reveal/Fade";

// import processData from "./processData";
// import processNextData from "./processNextData";

export default class ProcessMob extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pdone: this.props.pdone,
      pdtwo: this.props.pdtwo,
      footer: this.props.footer,
    };
  }

  render() {
    const settings = {
      dots: true,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 4,
      autoplay: true,
      autoplaySpeed: 8000,
      classNames: "slides",
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true,
          },
        },
        {
          breakpoint: 800,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            initialSlide: 1,
          },
        },
        {
          breakpoint: 700,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
      infinite: false,
    };

    const urlprefix = process.env.servUploadImg;
    const { pdone, pdtwo, footer } = this.state;
    return (
      <Col md="12">
        <div id="processflow" className="process-flow">
          <Slider {...settings}>
            {pdone.map((processDetail) => {
              return (
                <Fade bottom key={processDetail.id}>
                  <div id={processDetail.id} className="process-items">
                    <div className="process-flex">
                      <figure>
                        <img
                          src={
                            urlprefix + processDetail.appdevprocessoneimg.url
                          }
                          className="img-fluid"
                          alt={processDetail.appdevprocessonetitle}
                        />
                      </figure>
                      <h3>{processDetail.appdevprocessonetitle}</h3>
                    </div>
                  </div>
                </Fade>
              );
            })}
          </Slider>
          <ul className="process-list_next">
            {pdtwo.map((processNextDetail) => {
              return (
                <Fade bottom key={processNextDetail.id}>
                  <li id={processNextDetail.id}>
                    <figure>
                      <img
                        src={
                          urlprefix + processNextDetail.appdevprocesstwoimg.url
                        }
                        className="img-fluid"
                        alt={processNextDetail.appdevprocesstwotitle}
                      />
                    </figure>
                    <h4>{processNextDetail.appdevprocesstwotitle}</h4>
                  </li>
                </Fade>
              );
            })}
          </ul>
          <Fade bottom>
            <Row>
              <Col md="8">
                <div dangerouslySetInnerHTML={{ __html: footer }}></div>
              </Col>
            </Row>
          </Fade>
        </div>
      </Col>
    );
  }
}
