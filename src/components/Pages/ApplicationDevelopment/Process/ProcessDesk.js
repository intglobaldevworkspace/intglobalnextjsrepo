import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

// import processData from "./processData";
// import processNextData from "./processNextData";

export default function ProcessDesk(props) {
  // console.log(props);
  const urlprefix = process.env.servUploadImg;
  return (
    <div id="processflow" className="process-flow">
      <ul className="process-list">
        {props.pdone.map((processDetail) => {
          return (
            <Fade bottom key={processDetail.id}>
              <li id={processDetail.id}>
                <div className="process-item">
                  <figure>
                    <img
                      src={urlprefix + processDetail.appdevprocessoneimg.url}
                      className="img-fluid"
                      alt={processDetail.appdevprocessonetitle}
                    />
                  </figure>
                  <h3>{processDetail.appdevprocessonetitle}</h3>
                </div>
              </li>
            </Fade>
          );
        })}
      </ul>
      <ul className="process-list_next">
        {props.pdtwo.map((processNextDetail) => {
          return (
            <Fade bottom key={processNextDetail.id}>
              <li id={processNextDetail.id}>
                <figure>
                  <img
                    src={urlprefix + processNextDetail.appdevprocesstwoimg.url}
                    className="img-fluid"
                    alt={processNextDetail.appdevprocesstwotitle}
                  />
                </figure>
                <h4>{processNextDetail.appdevprocesstwotitle}</h4>
              </li>
            </Fade>
          );
        })}
      </ul>
      <Fade bottom>
        <Row>
          <Col md="8">
            <div dangerouslySetInnerHTML={{ __html: props.footer }}></div>
          </Col>
        </Row>
      </Fade>
    </div>
  );
}
