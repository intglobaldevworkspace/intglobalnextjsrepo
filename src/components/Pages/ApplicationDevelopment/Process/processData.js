/* eslint max-len: 0 */

export const process = [
  {
    id: 'p1',
    title: 'Research',
    iconUrl: '/static/images/process/application-development/research.svg',
  },
  {
    id: 'p2',
    title: 'Strategy & Plan',
    iconUrl: '/static/images/process/application-development/strategy-plan.svg',
  },
  {
    id: 'p3',
    title: 'Prototyping',
    iconUrl: '/static/images/process/application-development/prototyping.svg',
  },
  {
    id: 'p4',
    title: 'Design & Development',
    iconUrl: '/static/images/process/application-development/design-development.svg',
  },
  {
    id: 'p5',
    title: 'Testing & QA',
    iconUrl: '/static/images/process/application-development/testing-qa.svg',
  },
  {
    id: 'p6',
    title: 'Deployment & Launch',
    iconUrl: '/static/images/process/application-development/deployment-launch.svg',
  },
];

export default process;
