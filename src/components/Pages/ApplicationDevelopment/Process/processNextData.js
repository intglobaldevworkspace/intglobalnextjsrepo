/* eslint max-len: 0 */

export const processnext = [
  {
    id: 'pn1',
    title: 'Growth',
    iconUrl: '/static/images/process/application-development/growth.svg',
  },
  {
    id: 'pn2',
    title: 'Maintenance',
    iconUrl: '/static/images/process/application-development/maintenance.svg',
  },
  {
    id: 'pn3',
    title: 'Handover',
    iconUrl: '/static/images/process/application-development/handover.svg',
  },
];

export default processnext;
