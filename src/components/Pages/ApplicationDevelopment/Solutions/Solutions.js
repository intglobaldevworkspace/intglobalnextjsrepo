import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const Solutions = (props) => {
  const { sol } = props;
  // console.log(sol);
  return (
    <section className="solutions">
      <Fade bottom>
        <div dangerouslySetInnerHTML={{ __html: sol.solheaderone }}></div>
        <Row>
          <Col md="11">
            <div dangerouslySetInnerHTML={{ __html: sol.solheadertwo }}></div>
          </Col>
        </Row>
      </Fade>
    </section>
  );
};

export default Solutions;
