/* eslint max-len: 0 */

export const whyus = [
  {
    id: '1',
    iconUrl: '/static/images/icons/application-development/why-us/product-engineering-mindset.svg',
    tooltipID: 'Toolti01',
    tooltipText: 'We do iterative software development using cutting-edge technologies for robustness, security, and scale.',
    title: 'Product Engineering Mindset'
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/application-development/why-us/seamless-experience.svg',
    tooltipID: 'Toolti02',
    tooltipText: "We don't limit UI/UX to just your website or store front. Our engineers deploy seamless experience through your responsive website, site architecture, payment system, Single-click Checkout.",
    title: 'Seamless Experience'
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/application-development/why-us/winning-devops-team.svg',
    tooltipID: 'Toolti03',
    tooltipText: 'Our tem of technology Geeks, Product Experts, cloud architect & system administrator works in sync to power your application development and solutioning.',
    title: 'Winning Devops Team'
  },
  {
    id: '4',
    iconUrl: '/static/images/icons/application-development/why-us/world-class-support.svg',
    tooltipID: 'Toolti04',
    tooltipText: 'To keep your business working 24/7 through our world class support team so that you can sit back and enjoy increased operational output.',
    title: 'World-class Support'
  },
  {
    id: '5',
    iconUrl: '/static/images/icons/application-development/why-us/remote-team.svg',
    tooltipID: 'Toolti05',
    tooltipText: 'Our remote team is quick to set up, with zero set-up fees providing you with efficient & effective execution.',
    title: 'Remote Team'
  },
  {
    id: '6',
    iconUrl: '/static/images/icons/application-development/why-us/costs-timings-upfront.svg',
    tooltipID: 'Toolti06',
    tooltipText: 'We build what you need on time, while you stay in budget leveraging our flexible engagement models.',
    title: 'Costs and timings upfront'
  },
];

export default whyus;
