import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

// import whyusData from "./whyusData";

const WhyUS = (props) => {
  // console.log(props.whyuscont);
  const { whyusheader, whyussubheader, whyusoptions } = props.whyuscont;
  // const [tooltipOpen, setTooltipOpen] = useState(false);
  // const toggle = () => setTooltipOpen(!tooltipOpen);
  const urlprefix = process.env.servUploadImg;
  return (
    <section className="whyus">
      <Row>
        <Fade bottom>
          <h2>{whyusheader}</h2>
        </Fade>
        <Fade bottom>
          <Row>
            <Col md="11">
              <p>{whyussubheader}</p>
            </Col>
          </Row>
        </Fade>
        <Col md="12">
          <ul className="whyus-item">
            {whyusoptions.map((whyusDetail) => {
              return (
                <li id={whyusDetail.id} md="3" key={whyusDetail.id}>
                  <Fade bottom>
                    <div className="hvr-blocks-out">
                      <figure>
                        <img
                          src={urlprefix + whyusDetail.whyusimg.url}
                          className="img-fluid"
                          alt={whyusDetail.whyustitle}
                        />
                      </figure>
                      <h6>{whyusDetail.whyustitle}</h6>
                      <div>
                        <p>{whyusDetail.whyustooltiptext}</p>
                      </div>
                      <a></a>
                    </div>
                  </Fade>
                </li>
              );
            })}
          </ul>
        </Col>
      </Row>
    </section>
  );
};

export default WhyUS;
