import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

// import technologiesData from './technologiesData';

const Technologies = (props) => {
  const {
    techstackheader,
    techstacksubheader,
    techstackdetails,
    viewmorebutton,
  } = props.techstack;
  // console.log(techstackdetails, viewmorebutton);

  const urlprefix = process.env.servUploadImg;
  return (
    <section className="technologies">
      <Fade bottom>
        <h2>{techstackheader}</h2>
      </Fade>
      <Fade bottom>
        <Row>
          <Col md="11">
            <p>{techstacksubheader}</p>
          </Col>
        </Row>
      </Fade>
      <Row>
        <ul className="tech-item">
          {techstackdetails.map((technologyDetail) => {
            return (
              <li id={technologyDetail.id} key={technologyDetail.id}>
                <Fade bottom>
                  <figure>
                    <img
                      src={urlprefix + technologyDetail.technologyimg.url}
                      className="img-fluid"
                      alt={technologyDetail.technologytitle}
                    />
                  </figure>
                </Fade>
              </li>
            );
          })}
        </ul>
        <Fade bottom>
          {viewmorebutton.viewmorebuttonstatus ? (
            <a href={viewmorebutton.viewmorebuttonurl} className="btn link">
              {viewmorebutton.viewmorebuttontext}
            </a>
          ) : null}
        </Fade>
      </Row>
    </section>
  );
};

export default Technologies;
