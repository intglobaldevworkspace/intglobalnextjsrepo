/* eslint max-len: 0 */

export const technologies = [
  {
    id: '1',
    iconUrlP: '/static/images/technology/application-development/laravel.png',
    iconUrlW: '/static/images/technology/application-development/laravel.webp',
    title: 'Laravel'
  },
  {
    id: '2',
    iconUrlP: '/static/images/technology/application-development/cake-php.png',
    iconUrlW: '/static/images/technology/application-development/cake-php.webp',
    title: 'CakePHP'
  },
  {
    id: '3',
    iconUrlP: '/static/images/technology/application-development/php.png',
    iconUrlW: '/static/images/technology/application-development/php.webp',
    title: 'PHP'
  },
  {
    id: '4',
    iconUrlP: '/static/images/technology/application-development/nette-framework.png',
    iconUrlW: '/static/images/technology/application-development/nette-framework.webp',
    title: 'Nette Framework'
  },
  {
    id: '5',
    iconUrlP: '/static/images/technology/application-development/codeigniter.png',
    iconUrlW: '/static/images/technology/application-development/codeigniter.webp',
    title: 'CodeIgniter'
  },
  {
    id: '6',
    iconUrlP: '/static/images/technology/application-development/slim.png',
    iconUrlW: '/static/images/technology/application-development/slim.webp',
    title: 'Slim'
  },
  {
    id: '7',
    iconUrlP: '/static/images/technology/application-development/zend-framework.png',
    iconUrlW: '/static/images/technology/application-development/zend-framework.webp',
    title: 'Zend Framework'
  },
  {
    id: '8',
    iconUrlP: '/static/images/technology/application-development/phpixie.png',
    iconUrlW: '/static/images/technology/application-development/phpixie.webp',
    title: 'PHPixie'
  },
  {
    id: '9',
    iconUrlP: '/static/images/technology/application-development/asp-net.png',
    iconUrlW: '/static/images/technology/application-development/asp-net.webp',
    title: 'ASP .Net'
  },
  {
    id: '10',
    iconUrlP: '/static/images/technology/application-development/ajax-amsterdam.png',
    iconUrlW: '/static/images/technology/application-development/ajax-amsterdam.webp',
    title: 'Ajax Amsterdam'
  },
  {
    id: '11',
    iconUrlP: '/static/images/technology/application-development/dev-express.png',
    iconUrlW: '/static/images/technology/application-development/dev-express.webp',
    title: 'DevExpress'
  },
  {
    id: '12',
    iconUrlP: '/static/images/technology/application-development/application-development.png',
    iconUrlW: '/static/images/technology/application-development/application-development.webp',
    title: 'DevExpress'
  },
  {
    id: '13',
    iconUrlP: '/static/images/technology/application-development/infragistics.png',
    iconUrlW: '/static/images/technology/application-development/infragistics.webp',
    title: 'Infragistics'
  },
  {
    id: '14',
    iconUrlP: '/static/images/technology/application-development/cms.png',
    iconUrlW: '/static/images/technology/application-development/cms.webp',
    title: 'CMS'
  },
  {
    id: '15',
    iconUrlP: '/static/images/technology/application-development/drupal.png',
    iconUrlW: '/static/images/technology/application-development/drupal.webp',
    title: 'Drupal'
  },
  {
    id: '16',
    iconUrlP: '/static/images/technology/application-development/telerik.png',
    iconUrlW: '/static/images/technology/application-development/telerik.webp',
    title: 'Telerik'
  },
];

export default technologies;
