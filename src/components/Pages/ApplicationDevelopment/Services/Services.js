import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

// import servicesData from "./servicesData";

const Services = (props) => {
  // console.log(props);
  // const [tooltipOpen, setTooltipOpen] = useState(false);
  // const toggle = () => setTooltipOpen(!tooltipOpen);
  const { servicesdetails, servicesheader, servicessubheader } = props.serv;
  const urlprefix = process.env.servUploadImg;
  return (
    <section className="our-services">
      <Fade bottom>
        <h2>{servicesheader}</h2>
      </Fade>
      <Fade bottom>
        <Row>
          <Col md="11">
            <p>{servicessubheader}</p>
          </Col>
        </Row>
      </Fade>
      <Row>
        <Col md="12">
          {servicesdetails.map((serviceDetail) => {
            return (
              <Col
                id={serviceDetail.id}
                md="4"
                className="srvc-item"
                key={serviceDetail.id}
              >
                <Fade bottom>
                  <div className="hvr-blocks-out">
                    <figure>
                      <img
                        src={urlprefix + serviceDetail.appdevimg.url}
                        className="img-fluid"
                        alt={serviceDetail.appdevtitle}
                      />
                    </figure>
                    <h6>{serviceDetail.appdevtitle}</h6>
                    {/* <p>{serviceDetail.content}</p> */}
                    <div>
                      <p>{serviceDetail.appdevtooltiptext}</p>
                    </div>
                    <a></a>
                  </div>
                </Fade>
              </Col>
            );
          })}
        </Col>
      </Row>
    </section>
  );
};

export default Services;
