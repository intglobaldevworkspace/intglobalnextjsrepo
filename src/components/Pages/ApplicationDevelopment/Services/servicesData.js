/* eslint max-len: 0 */

export const services = [
  {
    id: '1',
    iconUrl: '/static/images/icons/application-development/services/web-mobile-app-development.svg',
    title: 'Web or Mobile Application Development',
    tooltipID: 'Toolti01',
    tooltipText: 'Our interactive Web Application Development integrates your business to enhance the user experience (UX) that is engaging, efficient and elegant.'
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/application-development/services/ecommerce-app-development.svg',
    title: 'eCommerce App Development',
    tooltipID: 'Toolti02',
    tooltipText: 'We provide companies with web & mobile e-commerce solutions implemented with wallets solutions that are W3C compliant using the latest payment options.'
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/application-development/services/api-integration-development.svg',
    title: 'API Integration Development',
    tooltipID: 'Toolti03',
    tooltipText: 'Our API Integration modifies existing software used in cross-platform solutions for desktop, mobile and cloud configurations.'
  },
  {
    id: '4',
    iconUrl: '/static/images/icons/application-development/services/cross-platform-app-services.svg',
    title: 'Cross-Platform App Development Services',
    tooltipID: 'Toolti04',
    tooltipText: 'We offer the best app development solutions for mobile apps. We leverage native features for iOS, Android and Windows for cross-platform development.'
  },
  {
    id: '5',
    iconUrl: '/static/images/icons/application-development/services/enterprise-app-developers.svg',
    title: 'Enterprise App Developers',
    tooltipID: 'Toolti05',
    tooltipText: 'Our apps help automate workflows involving Enterprise Resource Planning (ERP), Customer Relationship Management (CRM) and Supply Chain Management (SCM).'
  },
  {
    id: '6',
    iconUrl: '/static/images/icons/application-development/services/enterprise-software-integration-sol.svg',
    title: 'Enterprise Software Integration Solutions',
    tooltipID: 'Toolti06',
    tooltipText: 'Clean, organized back ends and dynamic, scalable front ends, developing web portals, enterprise automation apps, microsites, and e-commerce solutions.'
  },
];

export default services;
