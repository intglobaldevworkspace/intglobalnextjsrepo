import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

const CustomerExperience = (props) => {
  return (
    <section className="experiences">
      <Fade bottom>
        <Row>
          <Col md="11">
            <p>{props.cexp}</p>
          </Col>
        </Row>
      </Fade>
    </section>
  );
};

export default CustomerExperience;
