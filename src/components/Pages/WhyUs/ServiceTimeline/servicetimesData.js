/* eslint max-len: 0 */

export const timeline = [
  {
    id: '1',
    iconUrl: '/static/images/icons/about-us/values/integrity.svg',
    title: 'Since',
    content: '<strong>1997</strong>'
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/about-us/values/commitment.svg',
    title: 'Team',
    content: '<strong>750+</strong> Passionate Professionals.'
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/about-us/values/respect.svg',
    title: 'Offices',
    content: '<strong>40,000</strong> square feet.'
  },
  {
    id: '4',
    iconUrl: '/static/images/icons/about-us/values/integrity.svg',
    title: 'Presence in',
    content: 'India, UK, USA, Brazil, Canada, Singapore, UAE, Saudi Arabia.'
  },
  {
    id: '5',
    iconUrl: '/static/images/icons/about-us/values/commitment.svg',
    title: 'Core Services',
    content: 'Digital Consulting, Web Apps, Mobile Apps, Digital Marketing, Analytics, Product Engineering, Cloud Infra.'
  },
  {
    id: '6',
    iconUrl: '/static/images/icons/about-us/values/respect.svg',
    title: 'Globally Distributed Revenue',
    content: '<strong>25%</strong> UK, <strong>25%</strong> Rest of Europe, <strong>25%</strong> USA, <strong>15%</strong> India, <strong>10%</strong> Rest of the world.'
  },
  {
    id: '7',
    iconUrl: '/static/images/icons/about-us/values/integrity.svg',
    title: 'Business Distribution',
    content: '<strong>75%</strong> Tech, <strong>25%</strong> Marketing.'
  },
  {
    id: '8',
    iconUrl: '/static/images/icons/about-us/values/commitment.svg',
    title: '#Hours Delivered',
    content: '<strong>6,250,000</strong> Hours and counting...'
  },
  {
    id: '9',
    iconUrl: '/static/images/icons/about-us/values/respect.svg',
    title: 'Industries Served',
    content: 'Financial Service, Media, Publishing, BFSI, Entertainment, Retail, Health, IT Services, Professional Services, Education, Others.'
  },
  {
    id: '10',
    iconUrl: '/static/images/icons/about-us/values/integrity.svg',
    title: 'Project Ticket Size',
    content: '<strong>$15</strong> - <strong>$100K</strong>'
  },
  {
    id: '11',
    iconUrl: '/static/images/icons/about-us/values/commitment.svg',
    title: 'Perceived as',
    content: 'Growth & Innovation Partner, Strategic Digital Partner.'
  },
  {
    id: '12',
    iconUrl: '/static/images/icons/about-us/values/respect.svg',
    title: 'Proud of',
    content: '<strong>85%</strong> Business comes from existing clients.'
  },
  {
    id: '13',
    iconUrl: '/static/images/icons/about-us/values/integrity.svg',
    title: 'Lean Organization',
    content: 'Only three levels of hierarchy including top management. Empowered team.'
  },
  {
    id: '14',
    iconUrl: '/static/images/icons/about-us/values/commitment.svg',
    title: 'Current Sales Channel',
    content: 'Inbound–Online, Events, Word of Mouth, References.'
  },
  {
    id: '15',
    iconUrl: '/static/images/icons/about-us/values/respect.svg',
    title: 'Agile Team Structure',
    content: 'At least <strong>3x</strong> more productive.'
  },
];

export default timeline;
