import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

export default class ServiceTimelineLt extends Component {
  constructor(props) {
    super(props);

    this.state = {
      servLimitLeft: this.props.servLT,
    };
  }

  render() {
    const { servLimitLeft } = this.state;
    // console.log(servLimitLeft);
    return (
      <Col md="6">
        <Row>
          <Fade left>
            <Col md="11" className="service-cont">
              <p>{servLimitLeft.st1}</p>
              <p>{servLimitLeft.st2}</p>
              <p>
                {servLimitLeft.st3}
                <br />
                {servLimitLeft.st4}
              </p>
            </Col>
          </Fade>
        </Row>
      </Col>
    );
  }
}
