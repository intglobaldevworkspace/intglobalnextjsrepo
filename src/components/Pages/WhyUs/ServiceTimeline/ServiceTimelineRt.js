import React, { Component } from "react";
import { Nav, NavItem, NavLink, Col } from "reactstrap";
import classnames from "classnames";
import Fade from "react-reveal/Fade";
import { Scrollbars } from "react-custom-scrollbars";

export default class ServiceTimelineRt extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: this.props.slidercont.sliderContent[0].id,
      sContent: this.props.slidercont.sliderContent,
      slidersetting: this.props.slidercont.slidersetting,
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  render() {
    const { sContent, slidersetting } = this.state;
    // console.log(sContent, slidersetting);
    return (
      <Col md="6">
        <Scrollbars
          onScroll={this.handleScroll}
          autoHide
          autoHideTimeout={slidersetting.autoHideTimeout}
          autoHideDuration={slidersetting.autoHideDuration}
          autoHeight
          autoHeightMin={slidersetting.autoHeightMin}
          autoHeightMax={slidersetting.autoHeightMax}
          universal={slidersetting.universal}
          {...this.props}
        >
          <Fade right>
            <Nav tabs vertical pills>
              {sContent.map((slidercontentObj) => (
                <NavItem key={slidercontentObj.id}>
                  <NavLink
                    className={classnames({
                      active: this.state.activeTab === slidercontentObj.id,
                    })}
                    onClick={() => {
                      this.toggle(slidercontentObj.id);
                    }}
                  >
                    <h3>
                      <strong>{slidercontentObj.titletext}</strong>{" "}
                      <span
                        dangerouslySetInnerHTML={{
                          __html: slidercontentObj.compHistContent,
                        }}
                      ></span>
                    </h3>
                  </NavLink>
                </NavItem>
              ))}
            </Nav>
          </Fade>
        </Scrollbars>
      </Col>
    );
  }
}
