import React from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

import ServiceTimelineLt from "./ServiceTimelineLt";
import ServiceTimelineRt from "./ServiceTimelineRt";

const ServiceTimeline = (props) => {
  // console.log(props);
  return (
    <section className="service-timeline">
      <Row>
        <Col md="11">
          <Fade bottom>
            <h2>{props.servTL.serviceTimeLineHeader}</h2>
          </Fade>
          <Row>
            <div className="service-wrapper">
              <ServiceTimelineLt servLT={props.servTL.serviceTimeLineLt} />
              <ServiceTimelineRt
                slidercont={props.servTL.serviceTimeLineLeftSlider}
              />
            </div>
          </Row>
        </Col>
      </Row>
    </section>
  );
};

export default ServiceTimeline;
