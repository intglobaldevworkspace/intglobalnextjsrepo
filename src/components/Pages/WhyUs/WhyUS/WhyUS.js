import React, { useState } from "react";
import { Row, Col } from "reactstrap";
import Fade from "react-reveal/Fade";

// import whyusData from "./whyusData";

const WhyUS = (props) => {
  // console.log(props);
  const urlprefix = process.env.servUploadImg;
  const { whyUsHeader, whyUsPoints } = props.whyus;
  return (
    <section className="whyus-models">
      <Fade bottom>
        <h2>{whyUsHeader}</h2>
      </Fade>
      <Row>
        <Col md="12">
          {whyUsPoints.map((whyusDetail) => {
            return (
              <Col md="4" className="model-item" key={whyusDetail.id}>
                <Fade bottom>
                  <div className="hvr-blocks-out">
                    <figure>
                      <img
                        src={urlprefix + whyusDetail.aboutusWhyUsImg.url}
                        className="img-fluid"
                        alt={whyusDetail.aboutusWhyusTitle}
                      />
                    </figure>
                    <h6>{whyusDetail.aboutusWhyusTitle}</h6>
                    <div className="awards-action">
                      <a title={whyusDetail.readmoreText}>
                        {whyusDetail.readmoreText}
                      </a>
                    </div>
                    <div>
                      <p>{whyusDetail.aboutUsWhyUsTooltipText}</p>
                    </div>
                    <a></a>
                  </div>
                </Fade>
              </Col>
            );
          })}
        </Col>
      </Row>
    </section>
  );
};

export default WhyUS;
