/* eslint max-len: 0 */

export const whyus = [
  {
    id: '1',
    iconUrl: '/static/images/icons/why-us/why-us/costs-timings-upfront.svg',
    title: 'Fast & Flexible Solutions',
    tooltipID: 'Toolti01',
    tooltipText: 'Technology and Digital Trends are rapidly transforming while unlocking a world of potential in the digital premise. INT. helps you uncover where the real value exists with fast, agile, and robust solutions.',
    readmore: 'Read More »'
  },
  {
    id: '2',
    iconUrl: '/static/images/icons/why-us/why-us/product-engineering-mindset.svg',
    title: 'Enterprise-Grade Solutions',
    tooltipID: 'Toolti02',
    tooltipText: 'From ideation to execution, INT. delivers mission-critical, scalable, and innovative digital solutions to enterprises across the world.',
    readmore: 'Read More »'
  },
  {
    id: '3',
    iconUrl: '/static/images/icons/why-us/why-us/remote-team.svg',
    title: 'Value-Added Consultation',
    tooltipID: 'Toolti04',
    tooltipText: 'Our value-added consultation is analytics driven. We help you navigate every facet of the digital landscape, to identify growth opportunities, reveal competitive advantages, and define engaging experiences.',
    readmore: 'Read More »'
  },
  {
    id: '4',
    iconUrl: '/static/images/icons/why-us/why-us/seamless-experience.svg',
    title: 'Skin-in-the-Game Partnerships',
    tooltipID: 'Toolti02',
    tooltipText: 'As your technology partner, INT. offers skin-in-the-game partnerships. We not only share the incentives, but as your partner shares the disincentives also, when you stand a loss.',
    readmore: 'Read More »'
  },
  {
    id: '5',
    iconUrl: '/static/images/icons/why-us/why-us/winning-devops-team.svg',
    title: 'Simplified Solution',
    tooltipID: 'Toolti03',
    tooltipText: 'From complexity to clarity, we help organizations to create, launch, engineer, re-engineer digital products and solutions that delight users.',
    readmore: 'Read More »'
  },
  {
    id: '6',
    iconUrl: '/static/images/icons/why-us/why-us/world-class-support.svg',
    title: 'Adaptability & Agility',
    tooltipID: 'Toolti04',
    tooltipText: 'Instead of one-size-fits-all approach, we focus on curating agile & customized solution synchronizing with our customer’s needs.',
    readmore: 'Read More »'
  },
];

export default whyus;
