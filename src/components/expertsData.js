/* eslint max-len: 0 */

export const experts = [
  {
    id: '1',
    imgUrl: '/static/images/img-ar.png',
    name: 'Abhishek Rungta',
    skill: 'Inusretech Expert',
    expnce: 'With over 2 decades of experience',
    fbUrl: '//facebook.com/abhishekrungta/',
    twUrl: '//twitter.com/abhishekrungta/',
    linUrl: '//linkedin.com/in/abhishekrungta/',
    instUrl: '//www.instagram.com/abhishekrungta/',
    mail: 'talash@indusnet.co.in'
    // socials: [
    //   {
    //     fbUrl: 'https://www.facebook.com/abhishekrungta',
    //     twUrl: 'https://twitter.com/abhishekrungta',
    //     linUrl: 'https://www.linkedin.com/in/abhishekrungta',
    //     instUrl: 'https://www.instagram.com/abhishekrungta',
    //     mail: 'talash@indusnet.co.in'
    //   },
    // ],
  },
  {
    id: '2',
    imgUrl: '/static/images/img-ai.png',
    name: 'Aji Issac Mathew',    
    skill: 'Digital Strategy Expert',
    expnce: 'With over 3 decades of experience',
    fbUrl: '//facebook.com/Aji.Issac/',
    twUrl: '//twitter.com/AjiTechShu/',
    linUrl: '//linkedin.com/in/ajinimc/',
    instUrl: '//instagram.com/ajitechshu/',
    mail: 'aji@techshu.com'
    // socials: [
    //   {
    //     fbUrl: 'https://www.facebook.com/Aji.Issac',
    //     twUrl: 'https://twitter.com/AjiTechShu',
    //     linUrl: 'https://www.linkedin.com/in/ajinimc',
    //     instUrl: 'https://www.instagram.com/ajitechshu',
    //     mail: 'aji@techshu.com'
    //   },
    // ],
  },
  {
    id: '3',
    imgUrl: '/static/images/img-bb.png',
    name: 'Bharat Berlia',
    skill: 'Building Business Expert',
    expnce: 'With over 2 decades of experience',
    fbUrl: '//facebook.com/bharat.berlia/',
    twUrl: '//twitter.com/bharatberlia/',
    linUrl: '//linkedin.com/in/bharatberlia/',
    instUrl: '//instagram.com/brat.man/',
    mail: 'bberlia@indusnet.co.in'
    // socials: [
    //   {
    //     fbUrl: 'https://www.facebook.com/bharat.berlia',
    //     twUrl: 'https://twitter.com/bharatberlia',
    //     linUrl: 'https://www.linkedin.com/in/bharatberlia',
    //     instUrl: 'https://www.instagram.com/brat.man',
    //     mail: 'bberlia@indusnet.co.in'
    //   },
    // ],
  },
  {
    id: '4',
    imgUrl: '/static/images/img-nt.png',
    name: 'Robert Mathew',    
    skill: 'Product Strategy Expert',
    expnce: 'With over 3 decades of experience',
    fbUrl: '//facebook.com/Aji.Issac/',
    twUrl: '//twitter.com/AjiTechShu/',
    linUrl: '//linkedin.com/in/ajinimc/',
    instUrl: '//instagram.com/ajitechshu/',
    mail: 'aji@techshu.com'
    // socials: [
    //   {
    //     fbUrl: 'https://www.facebook.com/Aji.Issac',
    //     twUrl: 'https://twitter.com/AjiTechShu',
    //     linUrl: 'https://www.linkedin.com/in/ajinimc',
    //     instUrl: 'https://www.instagram.com/ajitechshu',
    //     mail: 'aji@techshu.com'
    //   },
    // ],
  },
  {
    id: '5',
    imgUrl: '/static/images/img-ar.png',
    name: 'Abhishek Rungta',
    skill: 'Inusretech Expert',
    expnce: 'With over 2 decades of experience',
    fbUrl: '//facebook.com/abhishekrungta/',
    twUrl: '//twitter.com/abhishekrungta/',
    linUrl: '//linkedin.com/in/abhishekrungta/',
    instUrl: '//www.instagram.com/abhishekrungta/',
    mail: 'talash@indusnet.co.in'
    // socials: [
    //   {
    //     fbUrl: 'https://www.facebook.com/abhishekrungta',
    //     twUrl: 'https://twitter.com/abhishekrungta',
    //     linUrl: 'https://www.linkedin.com/in/abhishekrungta',
    //     instUrl: 'https://www.instagram.com/abhishekrungta',
    //     mail: 'talash@indusnet.co.in'
    //   },
    // ],
  },
  {
    id: '6',
    imgUrl: '/static/images/img-ai.png',
    name: 'Aji Issac Mathew',    
    skill: 'Digital Strategy Expert',
    expnce: 'With over 3 decades of experience',
    fbUrl: '//facebook.com/Aji.Issac/',
    twUrl: '//twitter.com/AjiTechShu/',
    linUrl: '//linkedin.com/in/ajinimc/',
    instUrl: '//instagram.com/ajitechshu/',
    mail: 'aji@techshu.com'
    // socials: [
    //   {
    //     fbUrl: 'https://www.facebook.com/Aji.Issac',
    //     twUrl: 'https://twitter.com/AjiTechShu',
    //     linUrl: 'https://www.linkedin.com/in/ajinimc',
    //     instUrl: 'https://www.instagram.com/ajitechshu',
    //     mail: 'aji@techshu.com'
    //   },
    // ],
  },
  {
    id: '7',
    imgUrl: '/static/images/img-bb.png',
    name: 'Bharat Berlia',
    skill: 'Building Business Expert',
    expnce: 'With over 2 decades of experience',
    fbUrl: '//facebook.com/bharat.berlia/',
    twUrl: '//twitter.com/bharatberlia/',
    linUrl: '//linkedin.com/in/bharatberlia/',
    instUrl: '//instagram.com/brat.man/',
    mail: 'bberlia@indusnet.co.in'
    // socials: [
    //   {
    //     fbUrl: 'https://www.facebook.com/bharat.berlia',
    //     twUrl: 'https://twitter.com/bharatberlia',
    //     linUrl: 'https://www.linkedin.com/in/bharatberlia',
    //     instUrl: 'https://www.instagram.com/brat.man',
    //     mail: 'bberlia@indusnet.co.in'
    //   },
    // ],
  },
  {
    id: '8',
    imgUrl: '/static/images/img-nt.png',
    name: 'Robert Mathew',    
    skill: 'Product Strategy Expert',
    expnce: 'With over 3 decades of experience',
    fbUrl: '//facebook.com/abhishekrungta/',
    twUrl: '//twitter.com/abhishekrungta/',
    linUrl: '//linkedin.com/in/abhishekrungta/',
    instUrl: '//www.instagram.com/abhishekrungta/',
    mail: 'talash@indusnet.co.in'
    // socials: [
    //   {
    //     fbUrl: 'https://www.facebook.com/abhishekrungta',
    //     twUrl: 'https://twitter.com/abhishekrungta',
    //     linUrl: 'https://www.linkedin.com/in/abhishekrungta',
    //     instUrl: 'https://www.instagram.com/abhishekrungta',
    //     mail: 'talash@indusnet.co.in'
    //   },
    // ],
  },
  {
    id: '9',
    imgUrl: '/static/images/img-ar.png',
    name: 'Abhishek Rungta',
    skill: 'Inusretech Expert',
    expnce: 'With over 2 decades of experience',
    fbUrl: '//facebook.com/abhishekrungta/',
    twUrl: '//twitter.com/abhishekrungta/',
    linUrl: '//linkedin.com/in/abhishekrungta/',
    instUrl: '//www.instagram.com/abhishekrungta/',
    mail: 'talash@indusnet.co.in'
    // socials: [
    //   {
    //     fbUrl: 'https://www.facebook.com/abhishekrungta',
    //     twUrl: 'https://twitter.com/abhishekrungta',
    //     linUrl: 'https://www.linkedin.com/in/abhishekrungta',
    //     instUrl: 'https://www.instagram.com/abhishekrungta',
    //     mail: 'talash@indusnet.co.in'
    //   },
    // ],
  },
  {
    id: '10',
    imgUrl: '/static/images/img-ai.png',
    name: 'Aji Issac Mathew',    
    skill: 'Digital Strategy Expert',
    expnce: 'With over 3 decades of experience',
    fbUrl: '//facebook.com/Aji.Issac/',
    twUrl: '//twitter.com/AjiTechShu/',
    linUrl: '//linkedin.com/in/ajinimc/',
    instUrl: '//instagram.com/ajitechshu/',
    mail: 'aji@techshu.com'
    // socials: [
    //   {
    //     fbUrl: 'https://www.facebook.com/Aji.Issac',
    //     twUrl: 'https://twitter.com/AjiTechShu',
    //     linUrl: 'https://www.linkedin.com/in/ajinimc',
    //     instUrl: 'https://www.instagram.com/ajitechshu',
    //     mail: 'aji@techshu.com'
    //   },
    // ],
  },
  {
    id: '11',
    imgUrl: '/static/images/img-bb.png',
    name: 'Bharat Berlia',
    skill: 'Building Business Expert',
    expnce: 'With over 2 decades of experience',
    fbUrl: '//facebook.com/bharat.berlia/',
    twUrl: '//twitter.com/bharatberlia/',
    linUrl: '//linkedin.com/in/bharatberlia/',
    instUrl: '//instagram.com/brat.man/',
    mail: 'bberlia@indusnet.co.in'
    // socials: [
    //   {
    //     fbUrl: 'https://www.facebook.com/bharat.berlia',
    //     twUrl: 'https://twitter.com/bharatberlia',
    //     linUrl: 'https://www.linkedin.com/in/bharatberlia',
    //     instUrl: 'https://www.instagram.com/brat.man',
    //     mail: 'bberlia@indusnet.co.in'
    //   },
    // ],
  },
  {
    id: '12',
    imgUrl: '/static/images/img-nt.png',
    name: 'Robert Mathew',    
    skill: 'Product Strategy Expert',
    expnce: 'With over 3 decades of experience',
    fbUrl: '//facebook.com/Aji.Issac/',
    twUrl: '//twitter.com/AjiTechShu/',
    linUrl: '//linkedin.com/in/ajinimc/',
    instUrl: '//instagram.com/ajitechshu/',
    mail: 'aji@techshu.com'
    // socials: [
    //   {
    //     fbUrl: 'https://www.facebook.com/Aji.Issac',
    //     twUrl: 'https://twitter.com/AjiTechShu',
    //     linUrl: 'https://www.linkedin.com/in/ajinimc',
    //     instUrl: 'https://www.instagram.com/ajitechshu',
    //     mail: 'aji@techshu.com'
    //   },
    // ],
  },
  {
    id: '13',
    imgUrl: '/static/images/img-ar.png',
    name: 'Abhishek Rungta',
    skill: 'Inusretech Expert',
    expnce: 'With over 2 decades of experience',
    fbUrl: '//facebook.com/abhishekrungta/',
    twUrl: '//twitter.com/abhishekrungta/',
    linUrl: '//linkedin.com/in/abhishekrungta/',
    instUrl: '//www.instagram.com/abhishekrungta/',
    mail: 'talash@indusnet.co.in'
    // socials: [
    //   {
    //     fbUrl: 'https://www.facebook.com/abhishekrungta',
    //     twUrl: 'https://twitter.com/abhishekrungta',
    //     linUrl: 'https://www.linkedin.com/in/abhishekrungta',
    //     instUrl: 'https://www.instagram.com/abhishekrungta',
    //     mail: 'talash@indusnet.co.in'
    //   },
    // ],
  },
  {
    id: '14',
    imgUrl: '/static/images/img-ai.png',
    name: 'Aji Issac Mathew',    
    skill: 'Digital Strategy Expert',
    expnce: 'With over 3 decades of experience',
    fbUrl: '//facebook.com/Aji.Issac/',
    twUrl: '//twitter.com/AjiTechShu/',
    linUrl: '//linkedin.com/in/ajinimc/',
    instUrl: '//instagram.com/ajitechshu/',
    mail: 'aji@techshu.com'
    // socials: [
    //   {
    //     fbUrl: 'https://www.facebook.com/Aji.Issac',
    //     twUrl: 'https://twitter.com/AjiTechShu',
    //     linUrl: 'https://www.linkedin.com/in/ajinimc',
    //     instUrl: 'https://www.instagram.com/ajitechshu',
    //     mail: 'aji@techshu.com'
    //   },
    // ],
  },
  {
    id: '15',
    imgUrl: '/static/images/img-bb.png',
    name: 'Bharat Berlia',
    skill: 'Building Business Expert',
    expnce: 'With over 2 decades of experience',
    fbUrl: '//facebook.com/bharat.berlia/',
    twUrl: '//twitter.com/bharatberlia/',
    linUrl: '//linkedin.com/in/bharatberlia/',
    instUrl: '//instagram.com/brat.man/',
    mail: 'bberlia@indusnet.co.in'
    // socials: [
    //   {
    //     fbUrl: 'https://www.facebook.com/bharat.berlia',
    //     twUrl: 'https://twitter.com/bharatberlia',
    //     linUrl: 'https://www.linkedin.com/in/bharatberlia',
    //     instUrl: 'https://www.instagram.com/brat.man',
    //     mail: 'bberlia@indusnet.co.in'
    //   },
    // ],
  },
  {
    id: '16',
    imgUrl: '/static/images/img-nt.png',
    name: 'Robert Mathew',    
    skill: 'Product Strategy Expert',
    expnce: 'With over 3 decades of experience',
    fbUrl: '//facebook.com/abhishekrungta/',
    twUrl: '//twitter.com/abhishekrungta/',
    linUrl: '//linkedin.com/in/abhishekrungta/',
    instUrl: '//www.instagram.com/abhishekrungta/',
    mail: 'talash@indusnet.co.in'
    // socials: [
    //   {
    //     fbUrl: 'https://www.facebook.com/abhishekrungta',
    //     twUrl: 'https://twitter.com/abhishekrungta',
    //     linUrl: 'https://www.linkedin.com/in/abhishekrungta',
    //     instUrl: 'https://www.instagram.com/abhishekrungta',
    //     mail: 'talash@indusnet.co.in'
    //   },
    // ],
  },
];

export default experts;
