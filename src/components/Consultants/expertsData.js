/* eslint max-len: 0 */

export const experts = [
  {
    id: '1',
    imgUrl: '/static/images/img-ar.png',
    name: 'Abhishek Rungta',
    skill: 'Inusretech Expert',
    expnce: 'With over 2 decades of experience'
  },
  {
    id: '2',
    imgUrl: '/static/images/img-ai.png',
    name: 'Aji Issac Mathew',    
    skill: 'Digital Strategy Expert',
    expnce: 'With over 3 decades of experience'
  },
  {
    id: '3',
    imgUrl: '/static/images/img-bb.png',
    name: 'Bharat Berlia',
    skill: 'Building Business Expert',
    expnce: 'With over 2 decades of experience'
  },
  {
    id: '4',
    imgUrl: '/static/images/img-nt.png',
    name: 'Robert Mathew',    
    skill: 'Product Strategy Expert',
    expnce: 'With over 3 decades of experience'
  },
  {
    id: '5',
    imgUrl: '/static/images/img-ar.png',
    name: 'Abhishek Rungta',
    skill: 'Inusretech Expert',
    expnce: 'With over 2 decades of experience'
  },
  {
    id: '6',
    imgUrl: '/static/images/img-ai.png',
    name: 'Aji Issac Mathew',    
    skill: 'Digital Strategy Expert',
    expnce: 'With over 3 decades of experience'
  },
  {
    id: '7',
    imgUrl: '/static/images/img-bb.png',
    name: 'Bharat Berlia',
    skill: 'Building Business Expert',
    expnce: 'With over 2 decades of experience'
  }, 
];

export default experts;
