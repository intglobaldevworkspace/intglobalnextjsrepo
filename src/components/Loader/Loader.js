import React from 'react';
 

const Loader = () => {

    return (
        <>
          <div className="preloader-wrap"></div>
          <img src="/static/images/logo-bl.png" className="img-fluid d-block" alt="logo" />
        </>
    )
}

export default Loader;