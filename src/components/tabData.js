/* eslint max-len: 0 */

export const customers = [
  {
    id: '1',
    imgUrl: 'https://www.indusnet.co.in/images/logo-ageas-01.png',
    title: 'Ageas',
    subtitle: 'Smart Terminals transforming Europe’s leading and fastest growing online betting platform.',
    content: 'Enhanced reach and experience by offering software product engineering through remote teams. Building the product from scratch,  regular upgrades using sprint-based planning and deployment.',
  },
  {
    id: '2',
    imgUrl: 'https://www.indusnet.co.in/images/logo-cashpoint-01.png',
    title: 'Cashpoint',    
    subtitle: 'Smart Terminals transforming Europe’s leading and fastest growing online betting platform.',
    content: 'Enhanced reach and experience by offering software product engineering through remote teams. Building the product from scratch,  regular upgrades using sprint-based planning and deployment.',
  },
  {
    id: '3',
    imgUrl: 'https://www.indusnet.co.in/images/logo-ageas-01.png',
    title: 'Ageas',
    subtitle: 'Smart Terminals transforming Europe’s leading and fastest growing online betting platform.',
    content: 'Enhanced reach and experience by offering software product engineering through remote teams. Building the product from scratch,  regular upgrades using sprint-based planning and deployment.',
  },
  {
    id: '4',
    imgUrl: 'https://www.indusnet.co.in/images/logo-cashpoint-01.png',
    title: 'Cashpoint',    
    subtitle: 'Smart Terminals transforming Europe’s leading and fastest growing online betting platform.',
    content: 'Enhanced reach and experience by offering software product engineering through remote teams. Building the product from scratch,  regular upgrades using sprint-based planning and deployment.',
  },
  {
    id: '5',
    imgUrl: 'https://www.indusnet.co.in/images/logo-ageas-01.png',
    title: 'Ageas',
    subtitle: 'Smart Terminals transforming Europe’s leading and fastest growing online betting platform.',
    content: 'Enhanced reach and experience by offering software product engineering through remote teams. Building the product from scratch,  regular upgrades using sprint-based planning and deployment.',
  },
  {
    id: '6',
    imgUrl: 'https://www.indusnet.co.in/images/logo-cashpoint-01.png',
    title: 'Cashpoint',    
    subtitle: 'Smart Terminals transforming Europe’s leading and fastest growing online betting platform.',
    content: 'Enhanced reach and experience by offering software product engineering through remote teams. Building the product from scratch,  regular upgrades using sprint-based planning and deployment.',
  },
];

export default customers;
