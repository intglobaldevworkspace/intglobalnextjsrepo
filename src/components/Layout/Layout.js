import React from 'react';

const Layout = (props) => {
    return ( 
        <main className='cover'>
            <div className='wrapper'>
                {props.children}
            </div>
        </main>
    )
}

export default Layout;