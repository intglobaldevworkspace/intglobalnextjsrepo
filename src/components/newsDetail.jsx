import React from 'react';
import Layout from '../src/components/layout/Layout';
import LayoutInner from '../src/components/layoutinner/LayoutInner';

import { Row, Col } from 'reactstrap';

export default class NewsDetail extends React.Component {

  static async getInitialProps({query}) {
    let news = {};
    const slug = query.slug;

    try {
      news = await getBlogBySlug(slug);
    } catch(err) {
      console.error(err);
    }

    return {news};
  }

  render() {
    const {news} = this.props;

    return (
      <Layout title="Indus Net Technologies - Show Dynamic News Tilte Here">
        <LayoutInner className="news-detail-page">
          <Row>
            <Col md="12">
              <div className="news-wrapper">
                <figure><img src={img_name} alt="" /></figure>
                <h2>
                  News <strong>Title</strong>
                </h2>
                <p>News Contents.</p>
              </div>
            </Col>
          </Row>
        </LayoutInner>
      </Layout>
    )
  }
}
