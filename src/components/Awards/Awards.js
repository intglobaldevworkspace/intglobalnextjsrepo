import React, { useState } from "react";
import { Row, Col } from "reactstrap";
import Slider from "react-slick";
import Fade from "react-reveal/Fade";
import Swal from "sweetalert2";

const Awards = (props) => {
  const [award, setAward] = useState({
    ...props.awards,
  });
  // console.log(award);
  const settings = {
    dots: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 8000,
    classNames: "slides",
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
    infinite: true,
  };

  let newset = { ...settings };
  newset.slidesToScroll = award.awardsslidersetting.slidestoscroll;
  newset.slidesToShow = award.awardsslidersetting.slidestoshow;
  const urlprefix = process.env.servUploadImg;
  // console.log(newset);
  return (
    <section className="awards-accolades">
      <Fade bottom>
        <div dangerouslySetInnerHTML={{ __html: award.awardsheadertxt }}></div>
        <Col md="12">
          <div className="awrd-carousl">
            <Slider {...newset}>
              {award.peawardsaccolades.map((awardDetail) => {
                return (
                  <div
                    className="award-item"
                    id={awardDetail.id}
                    key={awardDetail.id}
                    onClick={() => {
                      Swal.fire({
                        width: 700,
                        padding: "0",
                        showCloseButton: true,
                        showCancelButton: false,
                        html: `<div><img src=${
                          urlprefix + awardDetail.awardImage.url
                        } className="img-fluid" alt=${
                          awardDetail.awardsdescrip
                        }>
                              </div>`,
                        showConfirmButton: false,
                      });
                    }}
                  >
                    <div className="awards-block">
                      <h6 style={{ cursor: "pointer" }}>
                        {awardDetail.awardsdescrip}
                      </h6>
                    </div>
                  </div>
                );
              })}
            </Slider>
          </div>
        </Col>
      </Fade>
    </section>
  );
};

export default Awards;
