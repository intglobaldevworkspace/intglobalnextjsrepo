/* eslint max-len: 0 */

export const awards = [
  {
    id: '1',
    awardName: 'Deloitte Technology Fast 500 Asia Pacific in 2010, 2011, 2017'
  },
  {
    id: '2',
    awardName: 'Top 100 SMEs of India by Dun & Bradstreet in 2015'
  },
  {
    id: '3',
    awardName: 'ET Bengal Corporate Award - Nomination, 2017'
  },
  {
    id: '4',
    awardName: 'India SME 100 Awards, 2014'
  },
];

export default awards;
