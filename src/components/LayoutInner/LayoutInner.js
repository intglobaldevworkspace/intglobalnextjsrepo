import { Container } from 'reactstrap';

const LayoutInner = (props) => {
  const { className } = props;
  return (
      <div className={`inr-page ${className}`}>
          <Container>
              {props.children}
          </Container>
      </div>
  )
}

LayoutInner.defaultProps = {
    className: ''
}

export default LayoutInner;