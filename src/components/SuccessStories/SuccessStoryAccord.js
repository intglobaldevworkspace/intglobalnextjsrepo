import React, { Component } from "react";
import { Col } from "reactstrap";
import Slider from "react-slick";

// import storiesData from "./storiesData";

export default class SuccessStoryAccord extends Component {
  constructor(props) {
    super(props);

    this.state = {
      storiesData: this.props.succStories,
    };
  }
  render() {
    const settings = {
      dots: false,
      arrow: true,
      infinte: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 8000,
      classNames: "slides",
    };
    const { storiesData } = this.state;
    const urlprefix = process.env.servUploadImg;
    // console.log(storiesData);

    return (
      <div className="success-carousl">
        <Slider {...settings}>
          {storiesData.map((storyDetail, index) => {
            return (
              <div className="story-item" key={storyDetail.customercolldet.id}>
                <figure>
                  <img
                    src={
                      urlprefix + storyDetail.customercolldet.customerlogo.url
                    }
                    className="img-fluid"
                    alt="logo"
                  />
                </figure>
                <Col sm="12">
                  <div className="story-det">
                    <h4>{storyDetail.customercolldet.customertitle}</h4>
                    <h5>{storyDetail.customercolldet.customersubtitle}</h5>
                    <p>{storyDetail.customercolldet.customerdescrip}</p>
                  </div>
                </Col>
                <Col md="12" className="testi-content">
                  <p>
                    {
                      storyDetail.customercolldet.customerportfolio[0]
                        .custappreciation
                    }
                  </p>
                  <span className="t-author">
                    <h6>
                      {
                        storyDetail.customercolldet.customerportfolio[0]
                          .custname
                      }
                      <span>
                        {
                          storyDetail.customercolldet.customerportfolio[0]
                            .custdesignation
                        }
                      </span>
                    </h6>
                    <figure>
                      <img
                        src={
                          urlprefix +
                          storyDetail.customercolldet.customerportfolio[0]
                            .custimage.url
                        }
                        alt={
                          storyDetail.customercolldet.customerportfolio[0]
                            .custname
                        }
                        classnames="img-fluid"
                      />
                    </figure>
                  </span>
                </Col>
              </div>
            );
          })}
        </Slider>
      </div>
    );
  }
}
