import React, { useState } from "react";
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Row,
  Col,
} from "reactstrap";
import classnames from "classnames";
import Fade from "react-reveal/Fade";

import storiesData from "./storiesData";

const SuccessStoryTab = (props) => {
  const [activeTab, setActiveTab] = useState(
    props.succStories[0].customercolldet.id
  );

  const toggle = (tab) => {
    if (activeTab !== tab) setActiveTab(tab);
  };
  // console.log(props.succStories);
  const storiesData = props.succStories;
  const urlprefix = process.env.servUploadImg;
  // console.log(storiesData);

  return (
    <div className="success-tab">
      <Nav tabs>
        <Fade top>
          {storiesData.map((storyDetail, index) => {
            return (
              <NavItem
                id={storyDetail.customercolldet.id}
                key={storyDetail.customercolldet.id}
              >
                <NavLink
                  className={classnames({
                    active: activeTab === storyDetail.customercolldet.id,
                  })}
                  onClick={() => {
                    toggle(storyDetail.customercolldet.id);
                  }}
                >
                  <img
                    src={
                      urlprefix + storyDetail.customercolldet.customerlogo.url
                    }
                    className="img-fluid"
                    alt={storyDetail.customercolldet.customertitle}
                  />
                </NavLink>
              </NavItem>
            );
          })}
        </Fade>
      </Nav>
      <TabContent activeTab={activeTab}>
        {storiesData.map((storyDetail) => {
          return (
            <TabPane
              key={storyDetail.customercolldet.id}
              tabId={storyDetail.customercolldet.id}
            >
              <Row>
                <Col md="11">
                  <Col md="5">
                    <Fade left>
                      <img
                        src={
                          urlprefix +
                          storyDetail.customercolldet.customerlogo.url
                        }
                        className="img-fluid"
                        alt={storyDetail.customercolldet.customertitle}
                      />
                    </Fade>
                  </Col>
                  <Col md="7">
                    <Fade right>
                      <h4>{storyDetail.customercolldet.customertitle}</h4>
                      <h5>{storyDetail.customercolldet.customersubtitle}</h5>
                      <p>{storyDetail.customercolldet.customerdescrip}</p>
                    </Fade>
                  </Col>
                </Col>
                <Col md="12" className="testi-content">
                  <Fade bottom>
                    <p>
                      {storyDetail.customercolldet.customerportfolio &&
                      storyDetail.customercolldet.customerportfolio.length &&
                      storyDetail.customercolldet.customerportfolio[0]
                        .custappreciation !== ""
                        ? storyDetail.customercolldet.customerportfolio[0]
                            .custappreciation
                        : ""}
                    </p>
                    <span className="t-author">
                      <h6>
                        {storyDetail.customercolldet.customerportfolio &&
                        storyDetail.customercolldet.customerportfolio.length &&
                        storyDetail.customercolldet.customerportfolio[0]
                          .custname !== ""
                          ? storyDetail.customercolldet.customerportfolio[0]
                              .custname
                          : ""}
                        <span>
                          {storyDetail.customercolldet.customerportfolio &&
                          storyDetail.customercolldet.customerportfolio
                            .length &&
                          storyDetail.customercolldet.customerportfolio[0]
                            .custdesignation !== ""
                            ? storyDetail.customercolldet.customerportfolio[0]
                                .custdesignation
                            : ""}
                        </span>
                      </h6>
                      <figure>
                        <img
                          src={
                            storyDetail.customercolldet.customerportfolio &&
                            storyDetail.customercolldet.customerportfolio
                              .length &&
                            storyDetail.customercolldet.customerportfolio[0]
                              .custimage.url !== ""
                              ? urlprefix +
                                storyDetail.customercolldet.customerportfolio[0]
                                  .custimage.url
                              : ""
                          }
                          alt={
                            storyDetail.customercolldet.customerportfolio &&
                            storyDetail.customercolldet.customerportfolio
                              .length &&
                            storyDetail.customercolldet.customerportfolio[0]
                              .custname !== ""
                              ? storyDetail.customercolldet.customerportfolio[0]
                                  .custname
                              : ""
                          }
                          classnames="img-fluid"
                        />
                      </figure>
                    </span>
                  </Fade>
                </Col>
              </Row>
            </TabPane>
          );
        })}
      </TabContent>
    </div>
  );
};

export default SuccessStoryTab;
