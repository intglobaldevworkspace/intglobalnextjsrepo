import React, { useState } from "react";
import { Row, Col } from "reactstrap";
import { useMediaQuery } from "react-responsive";
import Fade from "react-reveal/Fade";

import SuccessStoryTab from "./SuccessStoryTab";
import SuccessStoryAccord from "./SuccessStoryAccord";
import Contactus from "../shared/Contactus";

const SuccessStories = (props) => {
  // console.log(props);
  const [successStoryState, setSuccessStoryState] = useState({
    isOpen: false,
    showContactusForm: true,
    showContactusSubmitted: false,
    ContactusSubmitfailed: false,
    showSubmitLoader: false,
    //----
    successstoryHeader: props.stories.successstoryHeader,
    customerColl: props.stories.customerColl,
    letustalkurl: props.stories.successstoryletustalk.letustalkurl,
    letustalktxt: props.stories.successstoryletustalk.letustalktxt,
    contactuspopup: props.stories.contactuspopup,
  });

  const isDesktopOrLaptop = useMediaQuery({ minDeviceWidth: 1024 });
  const isTabletOrMobile = useMediaQuery({ maxDeviceWidth: 1023 });

  const toggleModal = () => {
    setSuccessStoryState((prevState) => ({
      ...prevState,
      isOpen: !prevState.isOpen,
      showContactusForm: true,
      showContactusSubmitted: false,
      ContactusSubmitfailed: false,
      showSubmitLoader: false,
    }));
  };

  const cntpopup = {
    isOpen: successStoryState.isOpen,
    showContactusForm: successStoryState.showContactusForm,
    showContactusSubmitted: successStoryState.showContactusSubmitted,
    ContactusSubmitfailed: successStoryState.ContactusSubmitfailed,
    showSubmitLoader: successStoryState.showSubmitLoader,
    contactusPopup: successStoryState.contactuspopup,
  };

  return (
    <section className="success-stoty">
      <Row>
        <Col sm="10">
          <Fade bottom>
            <h2
              dangerouslySetInnerHTML={{
                __html: successStoryState.successstoryHeader,
              }}
            ></h2>
          </Fade>
        </Col>
      </Row>
      {isDesktopOrLaptop && (
        <SuccessStoryTab succStories={successStoryState.customerColl} />
      )}
      {isTabletOrMobile && (
        <SuccessStoryAccord succStories={successStoryState.customerColl} />
      )}
      <Fade bottom>
        <a
          href={successStoryState.letustalkurl}
          className="btn link"
          onClick={(e) => {
            e.preventDefault();
            // toggle();
            toggleModal();
          }}
        >
          {successStoryState.letustalktxt}
        </a>
      </Fade>
      {successStoryState.isOpen ? (
        <Contactus
          cntpopup={{
            ...cntpopup,
            contactText: successStoryState.letustalktxt,
          }}
          handler={toggleModal}
        />
      ) : null}
    </section>
  );
};

export default SuccessStories;
