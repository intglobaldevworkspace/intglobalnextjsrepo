import React, { Component } from "react";
import { Container, Button, Navbar } from "reactstrap";
import Link from "next/link";
import Contactus from "./Contactus";
import Headermenu from "./Headermenu";
import fetch from "node-fetch";
import { Modals, ModalBackground, ModalContent } from "./Modals";
import axios from "axios";

export default class Header extends Component {
  constructor(props) {
    super(props);

    // this.toggle = this.toggle.bind(this);
    // this.onMouseEnter = this.onMouseEnter.bind(this);
    // this.onMouseLeave = this.onMouseLeave.bind(this);

    // console.log(this.props);
    this.state = {
      showExit: false,
      // dropdownOpen: false,
      isLoading: false,
      isScrolling: false,
      isClicked: true,
      isOpen: false,
      contactText: this.props.header.contactus,
      ImgUrl: this.props.header.logonorm,
      ImgUrlInv: this.props.header.logoinv,
      topmenu: this.props.header.sitemenu,
      contactusPopup: this.props.header.contactuspopup,
      showContactusForm: true,
      showContactusSubmitted: false,
      ContactusSubmitfailed: false,
      showSubmitLoader: false,
      showexitintentStatus: true,
      intentcontent: "",
    };
    // console.log("header", this.props);
  }

  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        this.setState({ isLoading: !this.state.isLoading });
      }.bind(this),
      1000
    );

    window.addEventListener("scroll", this.touchScroll);
    window.addEventListener("mouseout", this.handleExit);

    //----------------------------
    const headers = {
      "Content-Type": "application/json",
    };
    const urlprefix = process.env.servUploadImg;
    axios
      .get(urlprefix + "/exitintentdetail")
      .then((response) => {
        // console.log(response.data);
        this.setState({
          showexitintentStatus: response.data.showexitintent,
          intentcontent: response.data.intentpopupcontent,
        });
      })
      .catch((error) => {
        console.log(error);
      });
    //----------------------------
  }

  handleExitClick = (e) => {
    this.setState({
      showExit: false,
    });
    sessionStorage.setItem("intentPopup", "true");
  };

  handleExit = (e) => {
    e = e ? e : window.event;
    var vpWidth = Math.max(
      document.documentElement.clientWidth,
      window.innerWidth || 0
    );

    if (e.clientX >= vpWidth - 50) return;
    if (e.clientY >= 50) return;

    var from = e.relatedTarget || e.toElement;
    if (!from) {
      //sessionStorage.getItem('intentPopup');
      if (sessionStorage.getItem("intentPopup") === null) {
        this.setState({
          showExit: true,
        });
      } else {
        this.setState({
          showExit: false,
        });
      }
    }
  };

  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }

    window.removeEventListener("scroll", this.touchScroll);
  }

  touchScroll = () => {
    if (window.scrollY > 10) {
      this.setState({ isScrolling: true });
    } else {
      this.setState({ isScrolling: false });
    }
  };

  toggleModal = () => {
    this.setState({
      isOpen: !this.state.isOpen,
      showContactusForm: true,
      showContactusSubmitted: false,
      ContactusSubmitfailed: false,
      showSubmitLoader: false,
    });
  };
  menucloseonclick = () => {
    // alert("hi");
    this.setState({ isClicked: !this.state.isClicked });
  };
  render() {
    const isClick = this.state.isClicked;
    const isLoad = this.state.isLoading;
    const isScroll = this.state.isScrolling;
    const {
      contactText,
      ImgUrl,
      ImgUrlInv,
      topmenu,
      contactusPopup,
    } = this.state;
    // console.log(topmenu);

    // console.log(this.state.isOpen);
    const urlprefix = process.env.servUploadImg;
    const { showExit } = this.state;
    return (
      <div
        className={isClick ? "header-wrapper" : "header-wrapper menu-clicked"}
      >
        <header
          id="header"
          className={isScroll ? "header _scrolled" : "header"}
        >
          <div className={isLoad ? "header_inner draggd" : "header_inner"}>
            <Container>
              <Navbar color="faded" light>
                <Link href="/">
                  <a className="logo navbar-brand">
                    <img
                      src={isClick ? urlprefix + ImgUrl : urlprefix + ImgUrlInv}
                      className="img-fluid"
                      alt="Indus Net Technologies 3.0"
                    />
                  </a>
                </Link>
                <Button color="link" onClick={this.toggleModal}>
                  {contactText}
                </Button>
                {this.state.isOpen ? (
                  <Contactus
                    cntpopup={{ ...this.state }}
                    handler={this.toggleModal}
                  />
                ) : null}
                <div
                  className={isClick ? "nav-but-wrap" : "nav-but-wrap active"}
                  onClick={() => this.setState({ isClicked: !isClick })}
                >
                  <div className="menu-icon hover-target">
                    <span className="menu-icon__line menu-icon__line-left"></span>
                    <span className="menu-icon__line"></span>
                    <span className="menu-icon__line menu-icon__line-right"></span>
                  </div>
                </div>
              </Navbar>
            </Container>
          </div>
        </header>
        {/* <Headermenu
          menudetails={{
            topmenu: this.state.topmenu,
            dropdownOpen: this.state.dropdownOpen,
          }}
          menuclosehandler={this.menucloseonclick}
        /> */}
        <Headermenu
          menudetails={{
            topmenu: this.state.topmenu,
          }}
          menuclosehandler={this.menucloseonclick}
        />

        {this.state.showexitintentStatus ? (
          <Modals id="exitpopup-modal" className={showExit ? "show" : "hide"}>
            <ModalBackground
              className="exitpopup-modal_overlay"
              onClick={this.handleExitClick}
            />
            <ModalContent className="exitpopup-modal_content">
              <div className="exitpopup-modal__box">
                <button type="button" className="close" aria-label="Close">
                  <span aria-hidden="true" onClick={this.handleExitClick}>
                    ×
                  </span>
                </button>
                <div
                  dangerouslySetInnerHTML={{
                    __html: this.state.intentcontent,
                  }}
                ></div>
              </div>
            </ModalContent>
          </Modals>
        ) : null}
      </div>
    );
  }
}
