import React, { Component } from "react";
import {
  Row,
  Col,
  FormGroup,
  FormText,
  Label,
  Button,
  Spinner,
} from "reactstrap";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import fetch from "node-fetch";
import PhoneInput from "react-phone-input-2";
// import "react-phone-input-2/lib/style.css";
// import "react-phone-input-2/lib/material.css";

import Modal from "react-bootstrap/Modal";

export default class Contactus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: this.props.cntpopup.isOpen,
      showContactusForm: this.props.cntpopup.showContactusForm,
      showContactusSubmitted: this.props.cntpopup.showContactusSubmitted,
      ContactusSubmitfailed: this.props.cntpopup.ContactusSubmitfailed,
      showSubmitLoader: this.props.cntpopup.showSubmitLoader,
      // contactUsModalHeader: this.props.cntpopup.contactUsModalHeader,
      contactusPopup: this.props.cntpopup.contactusPopup,
      contactText: this.props.cntpopup.contactText,
    };
    // console.log("const -", this.props.cntpopup);
  }

  // toggleModal = () => {
  //   // this.setState((prevState) => ({
  //   //   isOpen: !prevState.isOpen,
  //   // }));
  //   this.setState({ isOpen: !this.state.isOpen });
  // };

  componentDidMount() {
    // console.log("In didmount contact us -", this.state);
  }
  render() {
    const urlprefix = process.env.servUploadImg;
    const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
    const contactusFormSchema = Yup.object().shape({
      name: Yup.string().required("Please enter your name"),
      email: Yup.string()
        .email("enter correct email")
        .required("Email is required"),
      phone: Yup.string()
        .matches(phoneRegExp, "Phone number is not valid")
        .required("Phone is required"),
      service: Yup.mixed().oneOf(["1", "2"], "Select a service type"),
      requirement: Yup.string().required("Please enter comments"),
    });

    const { contactusPopup, contactText } = this.state;
    const serviceType = { 1: "Request For Quote", 2: "Application For Job" };
    // console.log("Inside contact us render -", contactusPopup);

    return (
      <Modal show={true} onHide={this.props.handler}>
        <Formik
          initialValues={{
            name: "",
            phone: "",
            email: "",
            service: 0,
            requirement: "",
          }}
          validationSchema={contactusFormSchema}
          onSubmit={async (values, actions) => {
            // handleSave(params, () => {
            //   resetForm(initialValues);
            // });
            // console.log(actions);
            console.log(values);
            actions.resetForm();
            actions.setSubmitting(false);

            try {
              this.setState({
                showSubmitLoader: !this.state.showSubmitLoader,
              });

              const res = await fetch(urlprefix + "/contactusenquiries", {
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                },
                body: JSON.stringify({
                  values,
                }),
              });
              const contactResp = await res.json();
              console.log(contactResp);
              if (contactResp.hasOwnProperty("id")) {
                this.setState({
                  showContactusForm: !this.state.showContactusForm,
                  showContactusSubmitted: !this.state.showContactusSubmitted,
                  showSubmitLoader: !this.state.showSubmitLoader,
                });
              }
              //.then(res => res.json());
            } catch (error) {
              //ContactusSubmitfailed
              this.setState({
                showContactusForm: !this.state.showContactusForm,
                ContactusSubmitfailed: !this.state.ContactusSubmitfailed,
                showSubmitLoader: !this.state.showSubmitLoader,
              });

              console.log(error);
            }

            // const opt = {
            //   url: urlprefix + "/contactusenquiries",
            //   method: "post",
            //   data: values,
            //   headers: {
            //     "Content-Type": "application/json",
            //     "Access-Control-Allow-Origin": "*"
            //   }
            // };

            // try {
            //   const response = await axios(opt);
            //   console.log(response);
            // } catch (error) {
            //   console.log(error);
            // }
          }}
        >
          <Form className="form contactPop">
            <Modal.Header closeButton>
              <Modal.Title>{contactText}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div
                style={{
                  display: this.state.showContactusForm ? "" : "none",
                }}
              >
                <Col>
                  <FormGroup>
                    <Label htmlFor="name">Name</Label>
                    <Field
                      name="name"
                      id="name"
                      placeholder="Enter your name"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="name"
                      className="text-danger"
                      component="small"
                    />
                  </FormGroup>
                </Col>
                <Col>
                  <FormGroup>
                    <Label htmlFor="servicetype">Select Service</Label>
                    <Field
                      as="select"
                      name="service"
                      id="servicetype"
                      className="form-control"
                    >
                      <option value="0" defaultValue>
                        Select Service
                      </option>
                      {Object.entries(serviceType).map((srvType, index) => (
                        <option value={srvType[0]} key={srvType[1]}>
                          {srvType[1]}
                        </option>
                      ))}
                    </Field>
                    <ErrorMessage
                      name="service"
                      className="text-danger "
                      component="small"
                    />
                  </FormGroup>
                </Col>
                <Col>
                  <FormGroup>
                    <Label htmlFor="email">Email</Label>
                    <Field
                      type="email"
                      name="email"
                      id="email"
                      placeholder="Enter your email"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="email"
                      className="text-danger"
                      component="small"
                    />
                  </FormGroup>
                </Col>
                {/* <Col>
                  <FormGroup>
                    <PhoneInput
                      placeholder="Insert your phone"
                      // country={'us'}
                      value={this.state.phone}
                      onChange={(phone) => this.setState({ phone })}
                    />
                    <ErrorMessage
                      name="phone"
                      className="text-danger alert alert-danger"
                      component="small"
                    />
                  </FormGroup>
                </Col> */}
                <Col>
                  <FormGroup>
                    <Label htmlFor="phone">Phone</Label>
                    <Field
                      type="tel"
                      name="phone"
                      id="phone"
                      placeholder="Enter your phone"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="phone"
                      className="text-danger"
                      component="small"
                    />
                  </FormGroup>
                </Col>
                <Col>
                  <FormGroup>
                    <Label htmlFor="requirement">Requirements</Label>
                    <Field
                      as="textarea"
                      name="requirement"
                      id="requirement"
                      placeholder="Enter your requirements"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="requirement"
                      className="text-danger"
                      component="small"
                    />
                  </FormGroup>
                </Col>
              </div>
              <Col>
                <FormGroup>
                  <FormText
                    color="muted"
                    dangerouslySetInnerHTML={{
                      __html: contactusPopup.contactusTimings,
                    }}
                  ></FormText>
                </FormGroup>
              </Col>
              <Col
                style={{
                  display: this.state.showContactusSubmitted ? "" : "none",
                }}
              >
                <FormGroup>
                  <FormText
                    color="text-success"
                    className="alert alert-success"
                    dangerouslySetInnerHTML={{
                      __html: contactusPopup.contactusSuccess,
                    }}
                  ></FormText>
                </FormGroup>
              </Col>
              <Col
                style={{
                  display: this.state.ContactusSubmitfailed ? "" : "none",
                }}
              >
                <FormGroup>
                  <FormText
                    color="text-danger"
                    className="alert alert-danger"
                    style={{
                      display: this.state.ContactusSubmitfailed ? "" : "none",
                    }}
                    dangerouslySetInnerHTML={{
                      __html: contactusPopup.contactusFailure,
                    }}
                  ></FormText>
                </FormGroup>
              </Col>
            </Modal.Body>
            <Modal.Footer>
              <Col
                style={{
                  display: this.state.showContactusForm ? "" : "none",
                }}
              >
                <Button type="submit" className="btn btn-secondary disabled">
                  Submit
                </Button>
              </Col>
              <Col
                style={{
                  display: this.state.showContactusSubmitted ? "" : "none",
                }}
              >
                <Button
                  type="button"
                  onClick={this.props.handler}
                  className="btn btn-secondary "
                >
                  OK
                </Button>
              </Col>
              <Col
                style={{
                  display: this.state.ContactusSubmitfailed ? "" : "none",
                }}
              >
                <Button
                  type="button"
                  onClick={this.props.handler}
                  className="btn btn-secondary "
                >
                  OK
                </Button>
              </Col>
            </Modal.Footer>
            <div
              className="loadr"
              style={{
                display: this.state.showSubmitLoader ? "" : "none",
              }}
            >
              <Spinner style={{ width: "3rem", height: "3rem" }} />
            </div>
          </Form>
        </Formik>
      </Modal>
    );
  }
}
