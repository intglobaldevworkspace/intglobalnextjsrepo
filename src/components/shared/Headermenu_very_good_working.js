import React, { useState, useEffect } from "react";
import {
  Container,
  Col,
  Navbar,
  Nav,
  NavItem,
  NavLink,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Button,
} from "reactstrap";
import Link from "next/link";
import { useMediaQuery } from "react-responsive";

const Headermenu = (props) => {
  let tempmenykeys,
    menukeys = {};

  tempmenykeys = props.menudetails.topmenu.map((topmenuObj) => topmenuObj.id);
  // console.log(tempmenykeys);
  tempmenykeys.forEach((menuObj) => {
    menukeys[menuObj] = false;
  });
  // console.log(menukeys);

  const [headdermenustate, setheadermenustate] = useState({
    topmenu: props.menudetails.topmenu,
    dropdownOpen: false,
    menuitems: menukeys,
  });

  // useEffect(() => {
  //   effect
  //   return () => {
  //     cleanup
  //   }
  // }, [input])

  const toggle = (id) => {
    // console.log(id);
    // setheadermenustate({[id]:!this.state[`${id}`]})
    setheadermenustate((prevState) => ({
      ...prevState,
      dropdownOpen: !prevState.dropdownOpen,
    }));

    // setheadermenustate((prevState) => ({
    //   ...prevState,
    //   [menuitems]: !prevState.menuitems[id],
    // }));
  };

  const onMouseEnter = (id) => {
    console.log(id);
    console.log(headdermenustate.menuitems[id]);

    // setheadermenustate((prevState) => ({
    //   ...prevState,
    //   dropdownOpen: true,
    // }));

    // setheadermenustate((prevState) => {
    //   return {
    //     ...prevState,
    //     dropdownOpen: true,
    //   };
    // });

    let { menuitems } = headdermenustate;
    for (const [key, value] of Object.entries(menuitems)) {
      if (key === id) {
        menuitems[key] = true;
      }
    }
    console.log(typeof menuitems);
    setheadermenustate((prevState) => {
      return {
        ...prevState,
        dropdownOpen: true,
        menuitems: Object.assign({}, { ...menuitems }),
      };
    });

    // setheadermenustate({
    //   topmenu: { ...headdermenustate.topmenu },
    //   dropdownOpen: true,
    //   menuitems: Object.assign({}, { ...menuitems }),
    // });

    // let newmenuObj=Object.assign({},{...headdermenustate.menuitems},{headdermenustate.menuitems.id:true})

    // setheadermenustate({menuitems:newmenuObj})
  };

  const onMouseLeave = (id) => {
    // console.log(id);
    // setheadermenustate((prevState) => ({
    //   ...prevState,
    //   dropdownOpen: false,
    // }));
    let { menuitems } = headdermenustate;
    for (const [key, value] of Object.entries(menuitems)) {
      if (key === id) {
        menuitems[key] = false;
      }
    }
    console.log(typeof menuitems);
    setheadermenustate((prevState) => {
      return {
        ...prevState,
        dropdownOpen: false,
        menuitems: Object.assign({}, { ...menuitems }),
      };
    });
  };
  // console.log(headdermenustate.topmenu);
  // console.log(headdermenustate);
  const isDesktopOrLaptop = useMediaQuery({ minDeviceWidth: 1024 });
  const isTabletOrMobile = useMediaQuery({ maxDeviceWidth: 1023 });
  //   console.log(isDesktopOrLaptop);
  //   console.log(isTabletOrMobile);
  // console.log(headdermenustate);
  return isDesktopOrLaptop ? (
    <div className="nav">
      <div className="nav__content">
        <div className="container">
          <Nav navbar className="nav__list">
            {headdermenustate.topmenu.map((menuitem, Index) => {
              {
                return menuitem.menustatus ? (
                  <NavItem className="nav__list-item" key={Index}>
                    {menuitem.submenuitem.length > 0 ? (
                      <Dropdown
                        className=""
                        onMouseOver={() => onMouseEnter(menuitem.id)}
                        onMouseLeave={() => onMouseLeave(menuitem.id)}
                        // isOpen={headdermenustate.dropdownOpen}
                        isOpen={headdermenustate.menuitems[menuitem.id]}
                        toggle={() => toggle(menuitem.id)}
                      >
                        <DropdownToggle className="hover-target cus-cur caret">
                          {menuitem.menuname}
                        </DropdownToggle>
                        <DropdownMenu>
                          {menuitem.submenuitem.map((submenuitem, index) => {
                            if (submenuitem.topsubmenustatus) {
                              return (
                                <Link href={submenuitem.submenuurl} key={index}>
                                  <a
                                    className="hover-target cus-cur"
                                    title={submenuitem.submenuname}
                                    onClick={props.menuclosehandler}
                                  >
                                    <DropdownItem>
                                      {submenuitem.submenuname}
                                    </DropdownItem>
                                  </a>
                                </Link>
                              );
                            }
                          })}
                        </DropdownMenu>
                      </Dropdown>
                    ) : (
                      <Link href={menuitem.menuurl}>
                        <a
                          className="hover-target cus-cur"
                          title={menuitem.menuname}
                          onClick={props.menuclosehandler}
                        >
                          {menuitem.menuname}
                        </a>
                      </Link>
                    )}
                  </NavItem>
                ) : null;
              }
            })}
          </Nav>
        </div>
      </div>
    </div>
  ) : (
    <div className="nav">
      <div className="nav__content">
        <div className="container">
          <Nav navbar className="nav__list">
            {headdermenustate.topmenu.map((menuitem, Index) => {
              if (menuitem.menustatus) {
                return (
                  <NavItem className="nav__list-item" key={Index}>
                    {menuitem.submenuitem.length > 0 ? (
                      <Dropdown
                        className=""
                        // isOpen={headdermenustate.dropdownOpen}
                        isOpen={headdermenustate.menuitems[menuitem.id]}
                        toggle={() => toggle(menuitem.id)}
                      >
                        <DropdownToggle className="hover-target cus-cur caret">
                          {menuitem.menuname}
                        </DropdownToggle>
                        <DropdownMenu>
                          {menuitem.submenuitem.map((submenuitem, index) => {
                            if (submenuitem.topsubmenustatus) {
                              return (
                                <Link href={submenuitem.submenuurl} key={index}>
                                  <a
                                    className="hover-target cus-cur"
                                    title={submenuitem.submenuname}
                                  >
                                    <DropdownItem>
                                      {submenuitem.submenuname}
                                    </DropdownItem>
                                  </a>
                                </Link>
                              );
                            }
                          })}
                        </DropdownMenu>
                      </Dropdown>
                    ) : (
                      <NavLink
                        href={menuitem.menuurl}
                        className="hover-target cus-cur"
                        title="Segments"
                      >
                        {menuitem.menuname}
                      </NavLink>
                    )}
                  </NavItem>
                );
              }
            })}
          </Nav>
        </div>
      </div>
    </div>
  );
};

export default Headermenu;
