import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFacebookF,
  faTwitter,
  faLinkedinIn,
  faInstagram,
} from "@fortawesome/free-brands-svg-icons";
import Contactus from "./Contactus";
import { Container, Row, Col, Nav, NavItem, NavLink } from "reactstrap";
import Link from "next/link";

import Swal from "sweetalert2";

export default class Footer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false,
      fullFooterContent: this.props.footer,
      showContactusForm: true,
      showContactusSubmitted: false,
      ContactusSubmitfailed: false,
      showSubmitLoader: false,
    };
  }

  toggleModal = () => {
    this.setState({
      isOpen: !this.state.isOpen,
      showContactusForm: true,
      showContactusSubmitted: false,
      ContactusSubmitfailed: false,
      showSubmitLoader: false,
    });
  };

  render() {
    const {
      socialLinks,
      footerfourthcol,
      footersecondcol,
      footerthirdcol,
      footerfirstcol,
      footercopyright,
      contactuspopup,
      contactus,
      footermenu,
      footerSocialLinks,
    } = this.state.fullFooterContent;
    const cntpopup = {
      isOpen: this.state.isOpen,
      showContactusForm: this.state.showContactusForm,
      showContactusSubmitted: this.state.showContactusSubmitted,
      ContactusSubmitfailed: this.state.ContactusSubmitfailed,
      showSubmitLoader: this.state.showSubmitLoader,
      contactusPopup: this.state.fullFooterContent.contactuspopup,
    };
    const urlprefix = process.env.servUploadImg;
    return (
      <footer className="footer">
        <section className="ft-links">
          <Container>
            <Row>
              <Col md="3">
                <h3>{footerfirstcol.footerfirstcolheader}</h3>
                <p>{footerfirstcol.footerfirstcolsubheader}</p>
                <Nav navbar>
                  {footerfirstcol.footerfirstcolmenudetails.map(
                    (firstcolmenuDetailObj) => {
                      return firstcolmenuDetailObj.menuidentify ==
                        "location" ? (
                        <NavItem key={firstcolmenuDetailObj.id}>
                          <NavLink
                            title={
                              firstcolmenuDetailObj.firstcolsubmenuitemtitle
                            }
                            onClick={(e) => {
                              e.preventDefault();
                              Swal.fire({
                                width: 600,
                                padding: "0",
                                showCloseButton: true,
                                showCancelButton: false,
                                html: `<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d29473.97160515756!2d88.428356!3d22.569884!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xccb71b5b9a840f8a!2sIndus%20Net%20Technologies!5e0!3m2!1sen!2sin!4v1593613164992!5m2!1sen!2sin" width="100%" height="450"></iframe>`,
                                showConfirmButton: false,
                              });
                            }}
                          >
                            <figure>
                              <img
                                src={
                                  urlprefix +
                                  firstcolmenuDetailObj.firstcolsubmenuitemimg
                                    .url
                                }
                                alt={
                                  firstcolmenuDetailObj.firstcolsubmenuitemtitle
                                }
                              />
                            </figure>
                            <span>
                              {firstcolmenuDetailObj.firstcolsubmenuitemtext}
                            </span>
                          </NavLink>
                        </NavItem>
                      ) : (
                        <NavItem key={firstcolmenuDetailObj.id}>
                          <NavLink
                            href={firstcolmenuDetailObj.firstcolsubmenuitemlink}
                            target="_blank"
                            title={
                              firstcolmenuDetailObj.firstcolsubmenuitemtitle
                            }
                          >
                            <figure>
                              <img
                                src={
                                  urlprefix +
                                  firstcolmenuDetailObj.firstcolsubmenuitemimg
                                    .url
                                }
                                alt={
                                  firstcolmenuDetailObj.firstcolsubmenuitemtitle
                                }
                              />
                            </figure>
                            <span>
                              {firstcolmenuDetailObj.firstcolsubmenuitemtext}
                            </span>
                          </NavLink>
                        </NavItem>
                      );
                    }
                  )}
                </Nav>
              </Col>
              <Col md="3">
                <h3>{footersecondcol.secondcolheader}</h3>
                <Nav navbar className="footer_nav__list">
                  {footersecondcol.footersecondcols.map((footercolObj) => {
                    if (
                      footercolObj.secondcolmenuitem.toUpperCase() ===
                      "CONTACT US"
                    ) {
                      return (
                        <NavItem key={footercolObj.id}>
                          <NavLink
                            onClick={this.toggleModal}
                            title={footercolObj.secondcolmenuitem}
                          >
                            {footercolObj.secondcolmenuitem}
                          </NavLink>
                          {this.state.isOpen ? (
                            <Contactus
                              cntpopup={{
                                ...cntpopup,
                                contactText: footercolObj.secondcolmenuitem,
                              }}
                              handler={this.toggleModal}
                            />
                          ) : null}
                        </NavItem>
                      );
                    } else {
                      return (
                        <NavItem key={footercolObj.id}>
                          <Link href={footercolObj.secondcolmenulink}>
                            <a title={footercolObj.secondcolmenuitem}>
                              {footercolObj.secondcolmenuitem}
                            </a>
                          </Link>
                        </NavItem>
                      );
                    }
                  })}
                </Nav>
              </Col>

              <Col md="3">
                <h3>{footerthirdcol.thirdcolumnheader}</h3>
                <Nav navbar className="footer_nav__list">
                  {footerthirdcol.footerthirdcols.map((footerthirdcolObj) => {
                    return (
                      <NavItem key={footerthirdcolObj.id}>
                        <Link href={footerthirdcolObj.footerthirdcollink}>
                          <a title={footerthirdcolObj.footerthirdcolheader}>
                            {footerthirdcolObj.footerthirdcolheader}
                          </a>
                        </Link>
                      </NavItem>
                    );
                  })}
                </Nav>
              </Col>

              <Col md="3">
                <h3>{footerfourthcol.footercolheader}</h3>
                <Nav navbar className="footer_news__list">
                  {footerfourthcol.footercolfourmenuitems.map(
                    (fourthcolObj) => {
                      return (
                        <NavItem key={fourthcolObj.id}>
                          <NavLink
                            href={fourthcolObj.footercolfourmenulink}
                            target="_blank"
                            title={fourthcolObj.footercolmenuheader}
                          >
                            {fourthcolObj.footercolmenuheader}
                          </NavLink>
                        </NavItem>
                      );
                    }
                  )}
                </Nav>
              </Col>
            </Row>
          </Container>
        </section>

        <section className="ft-copy">
          <Container>
            <Row>
              <Col md="4">
                <div className="socials">
                  <Nav navbar className="social_list">
                    {socialLinks.map((socialnet, index) => {
                      let socialIcon = {
                        Facebook: faFacebookF,
                        Twitter: faTwitter,
                        LinkedIn: faLinkedinIn,
                        Instagram: faInstagram,
                      };
                      return (
                        <NavItem key={index}>
                          <NavLink
                            href={socialnet.socialnetlink}
                            title={`Like in ${socialnet.socialnetname}`}
                            target="_blank"
                          >
                            <FontAwesomeIcon
                              icon={socialIcon[socialnet.socialnetname]}
                            />
                          </NavLink>
                        </NavItem>
                      );
                    })}
                  </Nav>
                </div>
              </Col>
              <Col md="8">
                <p>
                  Copyright &copy; {new Date().getFullYear()} {footercopyright}
                </p>
              </Col>
            </Row>
          </Container>
        </section>
      </footer>
    );
  }
}
