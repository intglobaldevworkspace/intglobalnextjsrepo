import React, { Component } from "react";
import {
  Row,
  Col,
  FormGroup,
  FormText,
  Label,
  Button,
  Spinner,
} from "reactstrap";
import { Formik, Form, Field, ErrorMessage } from "formik";
import IntlTelInput from "react-intl-tel-input";
import * as Yup from "yup";
import fetch from "node-fetch";
import Select from "react-select";
import axios from "axios";

import Modal from "react-bootstrap/Modal";

export default class Contactus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: this.props.cntpopup.isOpen,
      showContactusForm: this.props.cntpopup.showContactusForm,
      showContactusSubmitted: this.props.cntpopup.showContactusSubmitted,
      ContactusSubmitfailed: this.props.cntpopup.ContactusSubmitfailed,
      showSubmitLoader: this.props.cntpopup.showSubmitLoader,
      // contactUsModalHeader: this.props.cntpopup.contactUsModalHeader,
      contactusPopup: this.props.cntpopup.contactusPopup,
      contactText: this.props.cntpopup.contactText,
      phone: "",
      // service: { value: "0", label: "Select Service" },
      selvalue: "Select a Service",
      servOptions: {
        0: "Select a Service",
        1: "Request For Quote",
        2: "Application For Job",
      },
      selectedOption: null,
      options: [
        { value: "1", label: "Request For Quote" },
        { value: "2", label: "Application For Job" },
      ],
    };
    // console.log("const -", this.props.cntpopup);
  }

  componentDidMount() {
    // console.log("In didmount contact us -", this.state);
  }

  // validateName = (value) => {
  //   let error;
  //   if (!value) {
  //     error = "Required";
  //   } else if (value === "") {
  //     error = "Enter your name!";
  //   }
  //   return error;
  // };

  // validatePhone = (value) => {
  //   let error;
  //   if (!value) {
  //     error = "Required";
  //   } else if (value === "") {
  //     error = "Please enter a valid Phone/mobile number";
  //   }
  //   return error;
  // };

  // validateEmail = (value) => {
  //   let error;
  //   if (!value) {
  //     error = "Required";
  //   } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
  //     error = "Invalid email address";
  //   }
  //   return error;
  // };

  formatPhoneNumberOutput(
    isValid,
    newNumber,
    countryData,
    fullNumber,
    isExtension
  ) {
    if (isValid && fullNumber) {
      return fullNumber.replace(/(\s|-)/g, "");
    }
    return "invalid_phone_number"; // caught by validator
  }

  // validateRequirement = (value) => {
  //   let error;
  //   if (!value) {
  //     error = "Required";
  //   } else if (value === "") {
  //     error = "Enter Requirement!";
  //   }
  //   return error;
  // };

  selectHandleChange(selectedOption) {
    console.log(`Option selected:`, selectedOption);
    this.setState({ selectedOption: selectedOption });
  }

  chkSelectedOption = (e) => {
    console.log(e.target.value);
    this.setState({ selvalue: e.target.value });
    // let error;
    // error = "Enter select!";
    // return error;
  };

  putPhoneNumber = (e) => {
    this.setState({ ...this.state, [e.target.name]: e.target.value });
  };

  render() {
    const urlprefix = process.env.servUploadImg;

    const {
      contactusPopup,
      contactText,
      servOptions,
      selvalue,
      selectedOption,
    } = this.state;

    // const serviceType = {
    //   1: "Request For Quote",
    //   2: "Application For Job",
    // };

    //{ value: "0", label: "Select Service" },
    // const serviceType = [
    //   { value: "1", label: "Request For Quote" },
    //   { value: "2", label: "Application For Job" },
    // ];

    const serviceType = [
      "Select a Service",
      "Request For Quote",
      "Application For Job",
    ];

    const contactusFormSchema = Yup.object().shape({
      name: Yup.string().required("Please enter your name"),
      email: Yup.string()
        .email("enter correct email")
        .required("Email is required"),
      countrycode: Yup.string()
        .matches(
          /^\+{1}[0-9]{1,5}$/,
          "Country code must be minimum 1 digit, maximum 5 digits and a (+) symbol"
        )
        .required(),
      phone: Yup.string()
        .min(10, "Phone number must a minimum of 10 digits")
        .max(12, "Phone number must a maximum of 12 digits")
        .required("Phone is required"),
      service: Yup.string()
        .oneOf(["Request For Quote", "Application For Job"], "Select a Service")
        .required(),
      requirement: Yup.string().required("Please enter requirements"),

      // countrycode: Yup.string()
      //   .min(2, "Country code must be minimum 1 digit and a (+) symbol")
      //   .max(5, "Country code must be maximum 5 digits and a (+) symbol")
      //   .required("Country code is required"),
      // service: Yup.string().ensure().required("Select service"),
      // service: Yup.object()
      //   .shape({
      //     service: Yup.object().shape({
      //       label: Yup.string().required(),
      //       value: Yup.string().required(),
      //     }),
      //   })
      //   .required("Select service"),
      // //  service: Yup.mixed().oneOf(["1", "2"], "Select a service type"),
      // requirement: Yup.string().required("Please enter requirements"),
    });
    return (
      <Modal show={true} onHide={this.props.handler}>
        <Formik
          initialValues={{
            name: "",
            phone: "",
            countrycode: "",
            email: "",
            service: "Select a Service",
            requirement: "",
          }}
          validationSchema={contactusFormSchema}
          onSubmit={async (values, actions) => {
            console.log("form submit", values);
            // actions.resetForm();
            actions.setSubmitting(false);
            const {
              countrycode,
              email,
              name,
              phone,
              requirement,
              service,
            } = values;

            // console.log(countrycode, email, name, phone, requirement, service);
            // console.log(servOptions[service]);
            // Object.entries(values).map((valObj) => {
            //   console.log("prop1", valObj[0]);
            //   console.log("prop2", valObj[1]);
            // });

            // const freshmarketerObj = {
            //   id: "9208ac3a-3274-404f-8f1e-2ee139b6217c",
            //   tokens: {
            //     user_name: name,
            //     user_service: servOptions[service],
            //     user_email: email,
            //     user_phone: countrycode + phone,
            //     user_requirements: requirement,
            //   },
            //   recipient: "utpalendu@indusnet.co.in",
            // };

            // const headers = {
            //   "Content-Type": "application/json",
            //   accept: "application/json",
            //   "fm-token": "7u6emrs2hjm79j26knfvvs2mugn5dfprkmoa4c75",
            // };

            // axios
            //   .post(
            //     "https://indusnet.freshmarketer.com/mas/api/v1/mail/transactional",
            //     freshmarketerObj,
            //     headers
            //   )
            //   .then((response) => {
            //     console.log(response);
            //   })
            //   .catch((error) => {
            //     console.log(error);
            //   });

            // const res = await fetch(
            //   "https://indusnet.freshmarketer.com/mas/api/v1/mail/transactional",
            //   {
            //     method: "POST",
            //     headers: {
            //       "Content-Type": "application/json",
            //       accept: "application/json",
            //       "fm-token": "7u6emrs2hjm79j26knfvvs2mugn5dfprkmoa4c75",
            //     },
            //     body: JSON.stringify({
            //       freshmarketerObj,
            //     }),
            //   }
            // );
            // const contactResp = await res.json();
            // console.log(contactResp);

            // try {
            //   this.setState({
            //     showSubmitLoader: !this.state.showSubmitLoader,
            //   });

            //   const res = await fetch(urlprefix + "/contactusenquiries", {
            //     method: "POST",
            //     headers: {
            //       "Content-Type": "application/json",
            //     },
            //     body: JSON.stringify({
            //       values,
            //     }),
            //   });
            //   const contactResp = await res.json();
            //   console.log(contactResp);
            //   if (contactResp.hasOwnProperty("id")) {
            //     this.setState({
            //       showContactusForm: !this.state.showContactusForm,
            //       showContactusSubmitted: !this.state.showContactusSubmitted,
            //       showSubmitLoader: !this.state.showSubmitLoader,
            //     });
            //   }
            //   //.then(res => res.json());
            // } catch (error) {
            //   //ContactusSubmitfailed
            //   this.setState({
            //     showContactusForm: !this.state.showContactusForm,
            //     ContactusSubmitfailed: !this.state.ContactusSubmitfailed,
            //     showSubmitLoader: !this.state.showSubmitLoader,
            //   });

            //   console.log(error);
            // }
          }}
        >
          {({
            errors,
            touched,
            validateField,
            validateForm,
            setFieldValue,
          }) => {
            console.log("Top -", errors);
            return (
              <Form className="form contactPop">
                <Modal.Header closeButton>
                  <Modal.Title>{contactText}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <div
                    style={{
                      display: this.state.showContactusForm ? "" : "none",
                    }}
                  >
                    <Col>
                      <FormGroup>
                        <Label htmlFor="name">Name</Label>
                        <Field
                          name="name"
                          id="name"
                          placeholder="Enter your name"
                          className="form-control"
                        />
                        {errors.name && touched.name && (
                          <small className="text-danger">{errors.name}</small>
                        )}
                      </FormGroup>
                    </Col>
                    {/* <Col>
                      <FormGroup>
                        <Label htmlFor="servicetype">Select Service</Label>
                        <Field
                          name="service"
                          id="service"
                          as="select"
                          className="form-control"
                          value={selvalue}
                          onChange={(e) => {
                            this.setState({
                              ...this.state,
                              selvalue: e.target.value,
                            });
                          }}
                        >
                          {Object.entries(servOptions).map((srvType) => (
                            <option value={srvType[0]} key={srvType[1]}>
                              {srvType[1]}
                            </option>
                          ))}
                        </Field>
                        {errors.service && touched.service && (
                          <small className="text-danger">
                            {errors.service}
                          </small>
                        )}
                      </FormGroup>
                    </Col> */}
                    <Col>
                      <FormGroup>
                        <Label htmlFor="servicetype">Service</Label>
                        <Field
                          name="service"
                          id="service"
                          as="select"
                          className="form-control"
                          value={this.state.selvalue}
                          onChange={(e) => this.chkSelectedOption(e)}
                        >
                          {/* <option value="">Select a service</option> */}
                          {/* {Object.entries(serviceType).map((servObj) => (
                            <option value={servObj[0]} key={servObj[1]}>
                              {servObj[1]}
                            </option>
                          ))} */}
                          {serviceType.map((servTypeObj, index) => {
                            return (
                              <option value={servTypeObj} key={index}>
                                {servTypeObj}
                              </option>
                            );
                          })}
                        </Field>
                        {errors.service && touched.service && (
                          <small className="text-danger">
                            {errors.service}
                          </small>
                        )}

                        {/* <Select
                          options={this.state.options}
                          value={selectedOption}
                          isSearchable={false}
                          name="service"
                          isClearable={false}
                          onChange={(e) => {
                            console.log(e);
                            this.setState({ selectedOption: e });
                          }}
                        /> */}
                        {/* {errors.service && touched.service && (
                          <small className="text-danger">
                            {errors.service}
                          </small>
                        )} */}
                      </FormGroup>
                    </Col>
                    <Col>
                      <FormGroup>
                        <Label htmlFor="email">Email</Label>
                        <Field
                          name="email"
                          id="email"
                          placeholder="Enter your email"
                          className="form-control"
                        />
                        {errors.email && touched.email && (
                          <small className="text-danger">{errors.email}</small>
                        )}
                      </FormGroup>
                    </Col>
                    <Col>
                      <FormGroup className="phonegrp ">
                        <div className="col-grp code-form-field">
                          <Label for="countrycode">Country Code</Label>
                          <Field
                            name="countrycode"
                            id="countrycode"
                            className="form-control code-form-field"
                            placeholder="country code"
                          />
                        </div>
                        <div className="col-grp cphone">
                          <Label for="phone">Phone</Label>
                          <Field
                            name="phone"
                            id="phone"
                            className="form-control cphone"
                            placeholder="Insert your phone"
                          />
                        </div>
                        {errors.countrycode && touched.countrycode && (
                          <small className="text-danger">
                            {errors.countrycode}
                          </small>
                        )}{" "}
                        {errors.phone && touched.phone && (
                          <small className="text-danger">{errors.phone}</small>
                        )}
                      </FormGroup>
                    </Col>
                    {/* <Col>
                  <FormGroup>
                    <Label htmlFor="phone">Phone</Label>
                    <Field
                      type="tel"
                      name="phone"
                      id="phone"
                      placeholder="Enter your phone"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="phone"
                      className="text-danger"
                      component="small"
                    />
                  </FormGroup>
                </Col> */}
                    <Col>
                      <FormGroup>
                        <Label htmlFor="requirement">Requirements</Label>
                        <Field
                          as="textarea"
                          name="requirement"
                          id="requirement"
                          placeholder="Enter your requirements"
                          className="form-control"
                        />
                        {errors.requirement && touched.requirement && (
                          <small className="text-danger">
                            {errors.requirement}
                          </small>
                        )}
                      </FormGroup>
                    </Col>
                  </div>
                  <Col>
                    <FormGroup>
                      <FormText
                        color="muted"
                        dangerouslySetInnerHTML={{
                          __html: contactusPopup.contactusTimings,
                        }}
                      ></FormText>
                    </FormGroup>
                  </Col>
                  <Col
                    style={{
                      display: this.state.showContactusSubmitted ? "" : "none",
                    }}
                  >
                    <FormGroup>
                      <FormText
                        color="text-success"
                        className="alert alert-success"
                        dangerouslySetInnerHTML={{
                          __html: contactusPopup.contactusSuccess,
                        }}
                      ></FormText>
                    </FormGroup>
                  </Col>
                  <Col
                    style={{
                      display: this.state.ContactusSubmitfailed ? "" : "none",
                    }}
                  >
                    <FormGroup>
                      <FormText
                        color="text-danger"
                        className="alert alert-danger"
                        style={{
                          display: this.state.ContactusSubmitfailed
                            ? ""
                            : "none",
                        }}
                        dangerouslySetInnerHTML={{
                          __html: contactusPopup.contactusFailure,
                        }}
                      ></FormText>
                    </FormGroup>
                  </Col>
                </Modal.Body>
                <Modal.Footer>
                  <Col
                    style={{
                      display: this.state.showContactusForm ? "" : "none",
                    }}
                  >
                    <Button
                      type="submit"
                      className="btn btn-secondary disabled"
                    >
                      Submit
                    </Button>
                  </Col>
                  <Col
                    style={{
                      display: this.state.showContactusSubmitted ? "" : "none",
                    }}
                  >
                    <Button
                      type="button"
                      onClick={this.props.handler}
                      className="btn btn-secondary "
                    >
                      OK
                    </Button>
                  </Col>
                  <Col
                    style={{
                      display: this.state.ContactusSubmitfailed ? "" : "none",
                    }}
                  >
                    <Button
                      type="button"
                      onClick={this.props.handler}
                      className="btn btn-secondary "
                    >
                      OK
                    </Button>
                  </Col>
                </Modal.Footer>
                <div
                  className="loadr"
                  style={{
                    display: this.state.showSubmitLoader ? "" : "none",
                  }}
                >
                  <Spinner style={{ width: "3rem", height: "3rem" }} />
                </div>
              </Form>
            );
          }}
        </Formik>
      </Modal>
    );
  }
}
