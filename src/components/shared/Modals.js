import styled, { keyframes } from "styled-components";

const popIn = keyframes`
  0% {
        -webkit-transform: scale(0);
        transform: scale(0);
        opacity: 0;
  }
  50% {
        -webkit-transform: scale(1.05);
        transform: scale(1.05);
        opacity: 1;
  }
  100% {
        -webkit-transform: scale(1);
        transform: scale(1);
        opacity: 1;
  }
`;

export const Modals = styled.div`

  color: palevioletred;
  font-weight: bold;
  bottom: 0;
  left: 0;
  position: absolute;
  right: 0;
  top: 0;
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
  -webkit-box-pack: center;
  -ms-flex-pack: center;
  justify-content: center;
  overflow: hidden;
  position: fixed;
  z-index: 100;
`;

export const ModalBackground = styled.div`
  bottom: 0;
  left: 0;
  position: absolute;
  right: 0;
  top: 0;
  background-color: rgba(10, 10, 10, 0.7);
`;

export const ModalContent = styled.div`
  margin: 0 20px;
  max-height: calc(100vh - 300px);
  width: 50%;
  overflow: auto;
  position: relative;
  background-color: white;
  padding: 0;
  border-radius: 5px;
  -webkit-animation: ${popIn} 1.1s both;
  animation: ${popIn} 1.1s both;
`;