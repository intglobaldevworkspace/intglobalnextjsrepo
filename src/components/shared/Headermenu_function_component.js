import React, { useState, useEffect } from "react";
import {
  Container,
  Col,
  Navbar,
  Nav,
  NavItem,
  NavLink,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Button,
} from "reactstrap";
import Link from "next/link";
import { useMediaQuery } from "react-responsive";

const Headermenu = (props) => {
  let tempmenykeys,
    menukeys = {};

  tempmenykeys = props.menudetails.topmenu.map((topmenuObj) => topmenuObj.id);
  // console.log(tempmenykeys);
  tempmenykeys.forEach((menuObj) => {
    menukeys[menuObj] = false;
  });
  console.log(menukeys);

  const [headdermenustate, setheadermenustate] = useState({
    topmenu: props.menudetails.topmenu,
    dropdownOpen: props.menudetails.dropdownOpen,
    menuitems: menukeys,
  });

  // useEffect(() => {
  //   effect
  //   return () => {
  //     cleanup
  //   }
  // }, [input])

  const toggle = (id) => {
    // setheadermenustate({[id]:!this.state[`${id}`]})
    setheadermenustate((prevState) => ({
      ...prevState,
      dropdownOpen: !prevState.dropdownOpen,
    }));

    // setheadermenustate((prevState) => ({
    //   ...prevState,
    //   [menuitems]: !prevState.menuitems[id],
    // }));
  };

  const onMouseEnter = (id) => {
    setheadermenustate((prevState) => ({
      ...prevState,
      dropdownOpen: true,
    }));
  };

  const onMouseLeave = (id) => {
    setheadermenustate((prevState) => ({
      ...prevState,
      dropdownOpen: false,
    }));
  };
  // console.log(headdermenustate.topmenu);
  console.log(headdermenustate);
  const isDesktopOrLaptop = useMediaQuery({ minDeviceWidth: 1024 });
  const isTabletOrMobile = useMediaQuery({ maxDeviceWidth: 1023 });
  //   console.log(isDesktopOrLaptop);
  //   console.log(isTabletOrMobile);
  // console.log(headdermenustate);
  return isDesktopOrLaptop ? (
    <div className="nav">
      <div className="nav__content">
        <div className="container">
          <Nav navbar className="nav__list">
            {headdermenustate.topmenu.map((menuitem, Index) => {
              {
                return menuitem.menustatus ? (
                  <NavItem className="nav__list-item" key={Index}>
                    {menuitem.submenuitem.length > 0 ? (
                      <Dropdown
                        className=""
                        onMouseOver={onMouseEnter}
                        onMouseLeave={onMouseLeave}
                        isOpen={headdermenustate.dropdownOpen}
                        toggle={toggle}
                      >
                        <DropdownToggle className="hover-target cus-cur caret">
                          {menuitem.menuname}
                        </DropdownToggle>
                        <DropdownMenu>
                          {menuitem.submenuitem.map((submenuitem, index) => {
                            if (submenuitem.topsubmenustatus) {
                              return (
                                <Link href={submenuitem.submenuurl} key={index}>
                                  <a
                                    className="hover-target cus-cur"
                                    title={submenuitem.submenuname}
                                    onClick={props.menuclosehandler}
                                  >
                                    <DropdownItem>
                                      {submenuitem.submenuname}
                                    </DropdownItem>
                                  </a>
                                </Link>
                              );
                            }
                          })}
                        </DropdownMenu>
                      </Dropdown>
                    ) : (
                      <Link href={menuitem.menuurl}>
                        <a
                          className="hover-target cus-cur"
                          title={menuitem.menuname}
                          onClick={props.menuclosehandler}
                        >
                          {menuitem.menuname}
                        </a>
                      </Link>
                    )}
                  </NavItem>
                ) : null;
              }
            })}
          </Nav>
        </div>
      </div>
    </div>
  ) : (
    <div className="nav">
      <div className="nav__content">
        <div className="container">
          <Nav navbar className="nav__list">
            {headdermenustate.topmenu.map((menuitem, Index) => {
              if (menuitem.menustatus) {
                return (
                  <NavItem className="nav__list-item" key={Index}>
                    {menuitem.submenuitem.length > 0 ? (
                      <Dropdown
                        className=""
                        isOpen={headdermenustate.dropdownOpen}
                        toggle={toggle}
                      >
                        <DropdownToggle className="hover-target cus-cur caret">
                          {menuitem.menuname}
                        </DropdownToggle>
                        <DropdownMenu>
                          {menuitem.submenuitem.map((submenuitem, index) => {
                            if (submenuitem.topsubmenustatus) {
                              return (
                                <Link href={submenuitem.submenuurl} key={index}>
                                  <a
                                    className="hover-target cus-cur"
                                    title={submenuitem.submenuname}
                                  >
                                    <DropdownItem>
                                      {submenuitem.submenuname}
                                    </DropdownItem>
                                  </a>
                                </Link>
                              );
                            }
                          })}
                        </DropdownMenu>
                      </Dropdown>
                    ) : (
                      <NavLink
                        href={menuitem.menuurl}
                        className="hover-target cus-cur"
                        title="Segments"
                      >
                        {menuitem.menuname}
                      </NavLink>
                    )}
                  </NavItem>
                );
              }
            })}
          </Nav>
        </div>
      </div>
    </div>
  );
};

export default Headermenu;
