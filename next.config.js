const withPlugins = require("next-compose-plugins");

const withSass = require("@zeit/next-sass");
const withCSS = require("@zeit/next-css");
// module.exports = withCSS(withSass());

const nextConfig = {
  // useFileSystemPublicRoutes: false,
  // distDir: 'build',
  poweredByHeader: false,
  env: {
    servUploadImg: "http://localhost:1337",
    globeLink: "http://3.7.150.91/newint_web/rotate/index.html"
  }
};

module.exports = withPlugins([
  [
    [
      withSass,
      {
        /* plugin config here ... */
      }
    ],
    [
      withCSS,
      {
        /* plugin config here ... */
      }
    ]
  ],
  nextConfig
]);
