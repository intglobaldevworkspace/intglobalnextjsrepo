import React from 'react';

import Layout from '../../src/components/Layout/Layout';
import LayoutInner from '../../src/components/LayoutInner/LayoutInner';
import Content from '../../src/components/Pages/EbookSingle/Content/Content';
import Banner from '../../src/components/Pages/EbookSingle/Banner/Banner';
import List from '../../src/components/Pages/EbookSingle/List/List';

const Inner_Page_Class = "inr";
const Ebook_Class = "ebook";
const Ebook_Single_Class = "single-ebook";

export default class EbookSingle extends React.Component {
  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(Ebook_Class);
        document.body.classList.add(Ebook_Single_Class);
      }.bind(this),
      10
    );
  }
  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }
  
  render () {
    return (
      <Layout title="Indus Net Technologies - Single Ebook Details">
        <LayoutInner className="ebooks single">
          <Content />
          <List />
        </LayoutInner>
      </Layout>
    )
  }
}
