import React, { Component } from "react";

import Layout from "../../src/components/Layout/Layout";
import LayoutInner from "../../src/components/LayoutInner/LayoutInner";
import Content from "../../src/components/Pages/EbookSingle/Content/Content";
import Banner from "../../src/components/Pages/EbookSingle/Banner/Banner";
import List from "../../src/components/Pages/EbookSingle/List/List";
import axios from "axios";
import fetch from "node-fetch";

const Inner_Page_Class = "inr";
const Ebook_Class = "ebook";
const Ebook_Single_Class = "single-ebook";

export default class EbookSingle extends Component {
  constructor(props) {
    super(props);
    console.log(this.props);
    const { ebookCatfull, qryp } = this.props;

    let spContent = ebookCatfull.ebooklistcolls.filter(
      (fullCatObj) => fullCatObj.ebooksingleurl === qryp
    );

    let ebookdet = ebookCatfull.ebooklistcolls.map((ebkObj) => {
      return {
        ebookstatus: ebkObj.ebookstatus,
        ebookcats: ebkObj.ebookcats,
        ebookshorttitle: ebkObj.ebookshorttitle,
        ebooksingleurl: ebkObj.ebooksingleurl,
        ebooklistpageimgthumb: ebkObj.ebooklistpageimg.url,
      };
    });

    this.state = {
      headerbanner: {
        htextone: ebookCatfull.ebookbannertextone,
        htexttwo: ebookCatfull.ebtexttwo,
        htextthree: ebookCatfull.ebtextthree,
        htextfour: ebookCatfull.ebtextfour,
      },
      ebooklistingpage: {
        ebookcat: ebookCatfull.ebookcats,
        ebooklistheader: ebookCatfull.listingpageheader,
        ebooklisting: ebookdet,
      },
      pagespecificdet: spContent,
    };
  }

  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(Ebook_Class);
        document.body.classList.add(Ebook_Single_Class);
      }.bind(this),
      10
    );
  }
  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const { headerbanner, ebooklistingpage, pagespecificdet } = this.state;
    return (
      <Layout title="Indus Net Technologies - Single Ebook Details">
        <LayoutInner className="ebooks single">
          <Content ebookdet={pagespecificdet} />
          <List listpage={ebooklistingpage} />
        </LayoutInner>
      </Layout>
    );
  }
}

export const getServerSideProps = async ({ query }) => {
  // Fetch the first page as default
  const urlprefix = process.env.servUploadImg;

  let ebookCatfull,
    qryp = null;
  // Fetch data from external API

  try {
    const res = await fetch(urlprefix + `/ebook`); //infographiccats
    // console.log(res);
    if (res.status !== 200) {
      throw new Error("Failed to fetch");
    }
    ebookCatfull = await res.json();

    let qry_prm = query.infographicdetail;
  } catch (err) {
    ebookCatfull = { error: { message: err.message } };
  }
  // Pass data to the page via props
  return { props: { ebookCatfull, qryp: query.ebookdetail } };
};
