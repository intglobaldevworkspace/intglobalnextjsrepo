import React, { Component } from "react";

import Layout from "../src/components/Layout/Layout";
import LayoutInner from "../src/components/LayoutInner/LayoutInner";
import Banner from "../src/components/Pages/Infographic/Banner/Banner";
import List from "../src/components/Pages/Infographic/List/List";
import fetch from "node-fetch";

const Inner_Page_Class = "inr";
const Infographic_Class = "infographic";

export default class Infographic extends Component {
  constructor(props) {
    super(props);
    // console.log(props);
    // console.log(this.props.infographiclistcolls);
    let infographicrecdet = this.props.infographiclistcolls.map(
      (ifgcolldet) => {
        return {
          id: ifgcolldet.id,
          graphicurl: ifgcolldet.graphicurl,
          shorttitle: ifgcolldet.shorttitle,
          infogdetailimg: { url: ifgcolldet.infogdetailimg.url },
        };
      }
    );
    this.state = {
      headerbanner: {
        htextone: this.props.infogtextone,
        htexttwo: this.props.infogtexttwo,
        htextthree: this.props.infogtextthree,
        htextfour: this.props.infogtextfour,
        htextfive: this.props.infogtextfive,
      },
      infoglisting: {
        infogcat: this.props.infographiccats,
        infogheader: this.props.listingheader,
        infogdetailrecord: infographicrecdet,
      },
    };
  }

  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(Infographic_Class);
      }.bind(this),
      10
    );
  }

  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const { headerbanner, infoglisting } = this.state;
    // console.log(infoglisting);
    return (
      <Layout title="Indus Net Technologies - Infographics">
        <LayoutInner className="infographics">
          <Banner bann={headerbanner} />
          <List infogdetail={infoglisting} />
        </LayoutInner>
      </Layout>
    );
  }
}

export const getServerSideProps = async () => {
  const urlprefix = process.env.servUploadImg;
  const res = await fetch(urlprefix + "/infographic");
  const infogr = await res.json();
  //   console.log("async", infogr);
  return {
    props: infogr,
  };
  //-------------------
};
