import React, { Component } from "react";

import Layout from "../src/components/Layout/Layout";
import LayoutInner from "../src/components/LayoutInner/LayoutInner";
import Banner from "../src/components/Pages/PrivacyPolicy/Banner/Banner";
import Content from "../src/components/Pages/PrivacyPolicy/Content/Content";
import PolicyContent from "../src/components/Pages/PrivacyPolicy/PolicyContent/PolicyContent";
import fetch from "node-fetch";

const Inner_Page_Class = "inr";
const Privacy_Class = "privacy-policy";

export default class PrivacyPolicy extends Component {
  constructor(props) {
    super(props);
    // console.log(this.props);
    this.state = {
      // contactuspopup: {
      //   contactusSuccess: this.props.contactussubmitmessages[0]
      //     .contactusmessage,
      //   contactusFailure: this.props.contactussubmitmessages[1]
      //     .contactusmessage,
      //   contactusTimings: this.props.contactussubmitmessages[2]
      //     .contactusmessage,
      // },
      bannerData: {
        btextone: this.props.bannertextone,
        btexttwo: this.props.bannertexttwo,
        btextthree: this.props.bannertextthree,
        btextfour: this.props.bannertextfour,
      },
      middlecontent: this.props.privacypolicymiddlecontents,
    };
  }

  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(Privacy_Class);
      }.bind(this),
      10
    );
  }

  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const { bannerData, middlecontent } = this.state;
    return (
      <Layout title="Indus Net Technologies - Privacy Policy">
        <LayoutInner className="privcy-policy">
          <Banner bdata={{ ...bannerData }} />
          <Content cntdata={middlecontent} />
          <PolicyContent />
        </LayoutInner>
      </Layout>
    );
  }
}

export const getServerSideProps = async () => {
  const urlprefix = process.env.servUploadImg;
  const res = await fetch(urlprefix + "/privacypolicy");
  const privacy = await res.json();
  // console.log("async", privacy);
  return {
    props: privacy,
  };
  //-------------------
};
