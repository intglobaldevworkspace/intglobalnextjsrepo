import React from "react";

import Layout from "../../src/components/Layout/Layout";
import LayoutInner from "../../src/components/LayoutInner/LayoutInner";
import Content from "../../src/components/Pages/SuccessStoriesSingle/Content/Content";
import Banner from "../../src/components/Pages/SuccessStoriesSingle/Banner/Banner";
import List from "../../src/components/Pages/SuccessStoriesSingle/List/List";
import fetch from "node-fetch";

const Inner_Page_Class = "inr";
const Sstory_Class = "success-stories";
const Sstory_Single_Class = "single-success-stories";

export default class SuccessStoriesSingle extends React.Component {
  constructor(props) {
    super(props);
    // console.log(this.props);
    this.state = {
      pagespecificdet: this.props.pagesuccessstorydet,
      fullpagedet: this.props.successstorydet,
    };
  }

  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(Sstory_Class);
        document.body.classList.add(Sstory_Single_Class);
      }.bind(this),
      10
    );
  }
  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const { pagespecificdet, fullpagedet } = this.state;
    // console.log(pagespecificdet, fullpagedet);
    // console.log(fullpagedet);
    return (
      <Layout title="Indus Net Technologies - Single Success Stories Details">
        <LayoutInner className="success-story single">
          <Content strydet={pagespecificdet} />
          <Banner
            banslider={{ ...fullpagedet.successstoryslidersetting }}
            compdetails={fullpagedet.customercolls}
          />
          <List
            clientfull={fullpagedet.customercolls}
            customercat={fullpagedet.successstorycats}
            ssheader={fullpagedet.successstoryheader}
          />
        </LayoutInner>
      </Layout>
    );
  }
}

export const getServerSideProps = async ({ query }) => {
  // Fetch the first page as default
  const urlprefix = process.env.servUploadImg;

  let successstorydet = null;
  // Fetch data from external API
  let pagesuccessstorydet = null;
  try {
    // const res = await fetch(
    //   urlprefix +
    //     `/customercolls?successstorydetailpageurl_eq=${query.successdetail}`
    // );
    const res = await fetch(urlprefix + `/successstory`);
    if (res.status !== 200) {
      throw new Error("Failed to fetch");
    }
    successstorydet = await res.json();
    let qry_parm = query.successdetail;
    pagesuccessstorydet = successstorydet.customercolls.find((succDetObj) => {
      return succDetObj.successstorydetailpageurl === qry_parm;
    });
  } catch (err) {
    successstorydet = { error: { message: err.message } };
  }
  // Pass data to the page via props
  return { props: { successstorydet, pagesuccessstorydet } };
};
