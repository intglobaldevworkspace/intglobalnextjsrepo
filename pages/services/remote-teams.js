import React, { Component } from "react";

import Layout from "../../src/components/Layout/Layout";
import LayoutInner from "../../src/components/LayoutInner/LayoutInner";
import Banner from "../../src/components/Pages/RemoteTeams/Banner/Banner";
import ExtendedTeam from "../../src/components/Pages/RemoteTeams/ExtendedTeam/ExtendedTeam";
import Execution from "../../src/components/Pages/RemoteTeams/Execution/Execution";
import SuccessStories from "../../src/components/SuccessStories/SuccessStories";
import Consultants from "../../src/components/Consultants/Consultants";
import Awards from "../../src/components/Awards/Awards";
import fetch from "node-fetch";
const Inner_Page_Class = "inr";
const RemoteTeam_Class = "remote-team";

export default class RemoteTeams extends Component {
  constructor(props) {
    super(props);
    console.log(this.props);
    this.state = {
      efficientExecution: {
        verticalSliderheader: this.props.rtefficientandeffectiveheader,
        rteamverticalslidercolls: this.props.rteamverticalslidercolls.map(
          (rteamVerticalSlideObj) => {
            return {
              id: rteamVerticalSlideObj.id,
              rtverticaltabpane: rteamVerticalSlideObj.rtverticaltabpane.map(
                (vtObj) => {
                  return {
                    rtverticalslidemaintab: vtObj.rtverticalslidemaintab,
                    rttabpanecontent: vtObj.rttabpanecontent.map(
                      (vtTabContent) => {
                        return {
                          id: vtTabContent.id,
                          rttabpaneheading: vtTabContent.rttabpaneheading,
                          rttabpanecontent: vtTabContent.rttabpanecontent,
                          rttabpaneimg: {
                            name: vtTabContent.rttabpaneimg.name,
                            url: vtTabContent.rttabpaneimg.url,
                            width: vtTabContent.rttabpaneimg.width,
                            height: vtTabContent.rttabpaneimg.height,
                          },
                        };
                      }
                    ),
                  };
                }
              ),
            };
          }
        ),
      },
      expertPanel: {
        panelexpheadertxt: this.props.rtpanelexpheadertxt,
        panelofexpslidersetting: {
          slidetoscroll: this.props.rtpanelofexpslidersetting.slidestoscroll,
          slidetoshow: this.props.rtpanelofexpslidersetting.slidestoshow,
        },
        panelofexpertscolls: this.props.panelofexpertscolls,
      },
      successstories: {
        customerColl: this.props.customercolls,
        successstoryHeader: this.props.reSuccessStoriesHeadertxt,
        successstoryletustalk: {
          letustalkurl: this.props.remoteteamSuccessStoryLetustalk
            .resuccessStoryurl,
          letustalktxt: this.props.remoteteamSuccessStoryLetustalk
            .reSuccessStoryLetusTalktext,
        },

        contactuspopup: {
          contactusSuccess: this.props.contactussubmitmessages[0]
            .contactusmessage,
          contactusFailure: this.props.contactussubmitmessages[1]
            .contactusmessage,
          contactusTimings: this.props.contactussubmitmessages[2]
            .contactusmessage,
        },
      },
      awardsAndAccolades: {
        awardsslidersetting: this.props.awardandrecog.awardrecog.awardslider,
        awardsheadertxt: this.props.awardandrecog.awardrecog.awardheader,
        peawardsaccolades: this.props.awardandrecog.awardrecog
          .peawardsaccolades,
      },
      headerbannertxt: {
        reheaderbannertxt: this.props.reheaderbannertxt,
      },
      extendedteams: {
        reextendedteamheader: this.props.reextendedteamheader,
        rtextendedteams: this.props.rtextendedteams.map((extTeam) => {
          return {
            id: extTeam.id,
            extendedteamtitle: extTeam.extendedteamtitle,
            rtextendedteamimg: {
              name: extTeam.rtextendedteamimg.name,
              width: extTeam.rtextendedteamimg.width,
              height: extTeam.rtextendedteamimg.height,
              url: extTeam.rtextendedteamimg.url,
            },
          };
        }),
      },
    };

    // console.log(this.state);
    // console.log(props);
  }
  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(RemoteTeam_Class);
      }.bind(this),
      10
    );
  }
  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const {
      headerbannertxt,
      efficientExecution,
      extendedteams,
      expertPanel,
      awardsAndAccolades,
      successstories,
    } = this.state;
    return (
      <Layout title="Indus Net Technologies - Remote Teams">
        <LayoutInner className="remote-teams">
          <Banner headerban={headerbannertxt} />
          <ExtendedTeam extendedteam={extendedteams} />
          <Execution effexecution={efficientExecution} />
          <Consultants experts={expertPanel} />
          <SuccessStories stories={successstories} />
          <Awards awards={awardsAndAccolades} />
        </LayoutInner>
      </Layout>
    );
  }
}

export const getServerSideProps = async () => {
  const urlprefix = process.env.servUploadImg;
  const res = await fetch(urlprefix + "/remoteteam");
  const remoteteam = await res.json();
  //   console.log("async", sitelogo);
  return {
    props: remoteteam,
  };
  //-------------------
};
