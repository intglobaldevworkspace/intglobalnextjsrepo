import React, { Component } from "react";

import Layout from "../../src/components/Layout/Layout";
import LayoutInner from "../../src/components/LayoutInner/LayoutInner";
import Banner from "../../src/components/Pages/UiUxDesign/Banner/Banner";
import Solutions from "../../src/components/Pages/UiUxDesign/Solutions/Solutions";
import Services from "../../src/components/Pages/UiUxDesign/Services/Services";
import HowWeDo from "../../src/components/Pages/UiUxDesign/HowWeDo/HowWeDo";
import UiUxTeam from "../../src/components/Pages/UiUxDesign/UiUxTeam/UiUxTeam";
import SuccessStories from "../../src/components/SuccessStories/SuccessStories";
import Consultants from "../../src/components/Consultants/Consultants";
import Awards from "../../src/components/Awards/Awards";
import fetch from "node-fetch";
const Inner_Page_Class = "inr";
const UiUxDsgin_Class = "uiux-dsign";

export default class UiUxDesign extends Component {
  constructor(props) {
    super(props);

    // console.log(this.props);

    this.state = {
      uiuxteamdata: {
        teamdata: this.props.uiuxteamdata,
        teamdataheader: this.props.uiuxteamheaderdata,
      },
      uiuxheaderbanner: { ...this.props.uiuxheaderbanner },
      solutioncont: {
        solcontheaderone: this.props.uiuxsolutiontxtone,
        solcontheadertwo: this.props.uiuxsolutiontxttwo,
      },
      howwedocont: {
        howwedoheader: this.props.uiuxhowwedoheader,
        howwedosubheader: this.props.uiuxhowwedosubheader,
        howwedocontent: this.props.uiuxhowwedodata,
      },
      servicescont: {
        serviceheader: this.props.uiuxservicesheader,
        servicesubheader: this.props.uiuxservicessubheader,
        servicedata: this.props.uiuxservicesdetails,
      },
      expertPanel: {
        panelexpheadertxt: this.props.panelexpheadertxt,
        panelofexpslidersetting: this.props.panelofexpslidersetting,
        panelofexpertscolls: this.props.panelofexpertscolls,
      },
      successstories: {
        customerColl: this.props.customercolls,
        successstoryHeader: this.props.successstoryheader,
        successstoryletustalk: {
          letustalktxt: this.props.uiuxsuccessstoryletustalk
            .uiuxsuccessstoryletustalktext,
          letustalkurl: this.props.uiuxsuccessstoryletustalk
            .uiuxsuccessstoryletustalkurl,
        },
        contactuspopup: {
          contactusSuccess: this.props.contactussubmitmessages[0]
            .contactusmessage,
          contactusFailure: this.props.contactussubmitmessages[1]
            .contactusmessage,
          contactusTimings: this.props.contactussubmitmessages[2]
            .contactusmessage,
        },
      },
      awardsAndAccolades: {
        awardsslidersetting: this.props.awardandrecogs[0].awardrecog
          .awardslider,
        awardsheadertxt: this.props.awardandrecogs[0].awardrecog.awardheader,
        peawardsaccolades: this.props.awardandrecogs[0].awardrecog
          .peawardsaccolades,
      },
    };
  }

  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(UiUxDsgin_Class);
      }.bind(this),
      10
    );
  }
  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const {
      uiuxheaderbanner,
      solutioncont,
      servicescont,
      howwedocont,
      expertPanel,
      uiuxteamdata,
      successstories,
      awardsAndAccolades,
    } = this.state;
    return (
      <Layout title="Indus Net Technologies - UI/UX Design">
        <LayoutInner className="uiux-design">
          <Banner hban={uiuxheaderbanner} />
          <Solutions solcont={{ ...solutioncont }} />
          <Services servicecont={{ ...servicescont }} />
          <HowWeDo howwedo={{ ...howwedocont }} />
          <UiUxTeam uiuxdata={uiuxteamdata} />
          <Consultants experts={expertPanel} />
          <SuccessStories stories={successstories} />
          <Awards awards={awardsAndAccolades} />
        </LayoutInner>
      </Layout>
    );
  }
}
export const getServerSideProps = async () => {
  const urlprefix = process.env.servUploadImg;
  const res = await fetch(urlprefix + "/servuiuxdesign");
  const uiuxdesign = await res.json();
  //   console.log("async", uiuxdesign);
  return {
    props: uiuxdesign,
  };
  //-------------------
};
