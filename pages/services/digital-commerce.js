import React, { Component } from "react";
import fetch from "node-fetch";

import Layout from "../../src/components/Layout/Layout";
import LayoutInner from "../../src/components/LayoutInner/LayoutInner";
import Banner from "../../src/components/Pages/DigitalCommerce/Banner/Banner";
import Solutions from "../../src/components/Pages/DigitalCommerce/Solutions/Solutions";
import Services from "../../src/components/Pages/DigitalCommerce/Services/Services";
import WhyUS from "../../src/components/Pages/DigitalCommerce/WhyUS/WhyUS";
import Process from "../../src/components/Pages/DigitalCommerce/Process/Process";
import FeaturesEcommerce from "../../src/components/Pages/DigitalCommerce/FeaturesEcommerce/FeaturesEcommerce";
import BenefitsEcommerce from "../../src/components/Pages/DigitalCommerce/BenefitsEcommerce/BenefitsEcommerce";
import EcommercePlatforms from "../../src/components/Pages/DigitalCommerce/EcommercePlatforms/EcommercePlatforms";
import SuccessStories from "../../src/components/SuccessStories/SuccessStories";
import Consultants from "../../src/components/Consultants/Consultants";
import Awards from "../../src/components/Awards/Awards";

const Inner_Page_Class = "inr";
const DigCommerce_Class = "dig-commerce";

export default class DigitalCommerce extends Component {
  constructor(props) {
    super(props);

    // console.log(this.props);
    this.state = {
      headerbanner: { ...this.props.digitalcommheaderbanner },
      solutioncontent: {
        ...this.props.digitalcommsolutioncontent,
      },
      servicescontent: {
        servheader: this.props.digitalcommerceservicesheader,
        servcontent: this.props.digitalcommservicescontent,
      },
      whyuscontent: {
        whyusheader: this.props.digitalcommwhyusheader,
        whyuscontent: this.props.digitalcommwhyuscontent,
      },
      processcontent: {
        processcontentheader: this.props.digitalcommprocessheadertext,
        processcontentletustalk: this.props.digitalcommprocessletustalk,
        contactuspopup: {
          contactusSuccess: this.props.contactussubmitmessages[0]
            .contactusmessage,
          contactusFailure: this.props.contactussubmitmessages[1]
            .contactusmessage,
          contactusTimings: this.props.contactussubmitmessages[2]
            .contactusmessage,
        },
        processcontentglobe: {
          processcontentglobecontent: this.props.digitalcommglobecontent,
          processcontentglobemiddlecontent: this.props.dccommglobalmiddlecont,
          processcontentglobebackground: this.props.dcommglobebackground,
        },
      },
      featuresecommerce: {
        featureecommheader: this.props.featureecommheader,
        featureecommcontent: this.props.featuresecommcontent,
        featureecommletustalk: this.props.featureecommletustalk,
        contactuspopup: {
          contactusSuccess: this.props.contactussubmitmessages[0]
            .contactusmessage,
          contactusFailure: this.props.contactussubmitmessages[1]
            .contactusmessage,
          contactusTimings: this.props.contactussubmitmessages[2]
            .contactusmessage,
        },
      },
      benefitsecommerce: {
        beneheader: this.props.benefitsecommheader,
        benecontent: this.props.benefitsecommcontent,
        beneletustalk: this.props.benefitsecommletustalk,
        contactuspopup: {
          contactusSuccess: this.props.contactussubmitmessages[0]
            .contactusmessage,
          contactusFailure: this.props.contactussubmitmessages[1]
            .contactusmessage,
          contactusTimings: this.props.contactussubmitmessages[2]
            .contactusmessage,
        },
      },
      ecommerceplatforms: {
        ecommplatform: this.props.ecommerceplatformheader,
        ecommplatformcontent: this.props.ecommerceplatformcontent,
      },
      expertPanel: {
        panelexpheadertxt: this.props.panelexpheadertxt,
        panelofexpslidersetting: this.props.expertpanelslider,
        panelofexpertscolls: this.props.panelofexpertscolls,
      },
      successstories: {
        customerColl: this.props.customercolls,
        successstoryHeader: this.props.dcsuccessstoryletustalkheader,
        // successstoryletustalk: this.props.digitalcommletustalk,
        successstoryletustalk: {
          letustalktxt: this.props.digitalcommletustalk.letustalktext,
          letustalkurl: this.props.digitalcommletustalk.letustalkurl,
        },
        contactuspopup: {
          contactusSuccess: this.props.contactussubmitmessages[0]
            .contactusmessage,
          contactusFailure: this.props.contactussubmitmessages[1]
            .contactusmessage,
          contactusTimings: this.props.contactussubmitmessages[2]
            .contactusmessage,
        },
      },
      awardsAndAccolades: {
        awardsslidersetting: this.props.awardandrecogs[0].awardrecog
          .awardslider,
        awardsheadertxt: this.props.awardandrecogs[0].awardrecog.awardheader,
        peawardsaccolades: this.props.awardandrecogs[0].awardrecog
          .peawardsaccolades,
      },
    };
  }

  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(DigCommerce_Class);
      }.bind(this),
      10
    );
  }
  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const {
      headerbanner,
      solutioncontent,
      servicescontent,
      whyuscontent,
      processcontent,
      featuresecommerce,
      benefitsecommerce,
      ecommerceplatforms,
      successstories,
      expertPanel,
      awardsAndAccolades,
    } = this.state;
    return (
      <Layout title="Indus Net Technologies - Digital Commerce">
        <LayoutInner className="digital-commerce">
          <Banner dchdrban={headerbanner} />
          <Solutions solcont={solutioncontent} />
          <Services servicecontent={servicescontent} />
          <WhyUS whyuscont={whyuscontent} />
          <Process processcont={processcontent} />
          <FeaturesEcommerce featuresecomm={featuresecommerce} />
          <BenefitsEcommerce benefits={benefitsecommerce} />
          <EcommercePlatforms ecommplatform={ecommerceplatforms} />
          <Consultants experts={expertPanel} />
          <SuccessStories stories={successstories} />
          <Awards awards={awardsAndAccolades} />
        </LayoutInner>
      </Layout>
    );
  }
}

export const getServerSideProps = async () => {
  const urlprefix = process.env.servUploadImg;
  const res = await fetch(urlprefix + "/servdigitalcommerce");
  const digitalcomm = await res.json();
  // console.log("async", digitalcomm);
  return {
    props: digitalcomm,
  };
  //-------------------
};
