import React, { Component } from "react";

import Layout from "../../src/components/Layout/Layout";
import LayoutInner from "../../src/components/LayoutInner/LayoutInner";
import Banner from "../../src/components/Pages/ApplicationDevelopment/Banner/Banner";
import Solutions from "../../src/components/Pages/ApplicationDevelopment/Solutions/Solutions";
import Services from "../../src/components/Pages/ApplicationDevelopment/Services/Services";
import WhyUS from "../../src/components/Pages/ApplicationDevelopment/WhyUS/WhyUS";
import Process from "../../src/components/Pages/ApplicationDevelopment/Process/Process";
import Technologies from "../../src/components/Pages/ApplicationDevelopment/Technologies/Technologies";
import SuccessStories from "../../src/components/SuccessStories/SuccessStories";
import Consultants from "../../src/components/Consultants/Consultants";
import Awards from "../../src/components/Awards/Awards";
import fetch from "node-fetch";
const Inner_Page_Class = "inr";
const ApplicationDev_Class = "applc-development";

export default class ApplicationDevelopment extends Component {
  constructor(props) {
    super(props);

    // console.log(this.props);
    this.state = {
      applidev: {
        ...this.props.applicationdev,
      },
      solutioncont: {
        solheaderone: this.props.solutionheaderone,
        solheadertwo: this.props.solutionheadertwo,
      },
      servicescont: {
        servicesheader: this.props.appdevservicesheader,
        servicessubheader: this.props.appdevservicessubheader,
        servicesdetails: this.props.appdevservicesoptions,
      },
      whyuscont: {
        whyusheader: this.props.whyusheader,
        whyussubheader: this.props.whyussubheader,
        whyusoptions: this.props.appdevwhyusoptions,
      },
      processcont: {
        processcontheader: this.props.appdevprocessheader,
        processletustalk: this.props.appdevprocessletustalk,
        processdataone: this.props.appdevprocessdetone,
        processdatatwo: this.props.appdevprocessdevtwo,
        contactuspopup: {
          contactusSuccess: this.props.contactussubmitmessages[0]
            .contactusmessage,
          contactusFailure: this.props.contactussubmitmessages[1]
            .contactusmessage,
          contactusTimings: this.props.contactussubmitmessages[2]
            .contactusmessage,
        },
        processfooter: this.props.appdevprocessfooter,
      },

      technologystack: {
        techstackheader: this.props.appdevtechnologyheader,
        techstacksubheader: this.props.appdevtechnologysubheader,
        techstackdetails: this.props.appdevtechoptions,
        viewmorebutton: this.props.appdevviewmorebutton,
      },
      expertPanel: {
        panelexpheadertxt: this.props.panelexpheadertxt,
        panelofexpslidersetting: this.props.panelofexpslidersetting,
        panelofexpertscolls: this.props.panelofexpertscolls,
      },
      successstories: {
        customerColl: this.props.customercolls,
        successstoryHeader: this.props.successstoryheader,
        successstoryletustalk: {
          letustalktxt: this.props.appdevsuccessstoryletustalk
            .appdevsuccessstoryletustalktext,
          letustalkurl: this.props.appdevsuccessstoryletustalk
            .appdevsuccesstoryletustalkurl,
        },
        contactuspopup: {
          contactusSuccess: this.props.contactussubmitmessages[0]
            .contactusmessage,
          contactusFailure: this.props.contactussubmitmessages[1]
            .contactusmessage,
          contactusTimings: this.props.contactussubmitmessages[2]
            .contactusmessage,
        },
      },
      awardsAndAccolades: {
        awardsslidersetting: this.props.awardandrecogs[0].awardrecog
          .awardslider,
        awardsheadertxt: this.props.awardandrecogs[0].awardrecog.awardheader,
        peawardsaccolades: this.props.awardandrecogs[0].awardrecog
          .peawardsaccolades,
      },
    };
  }

  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(ApplicationDev_Class);
      }.bind(this),
      10
    );
  }
  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const {
      applidev,
      expertPanel,
      successstories,
      awardsAndAccolades,
      solutioncont,
      servicescont,
      whyuscont,
      processcont,
      technologystack,
    } = this.state;
    return (
      <Layout title="Indus Net Technologies - Application Development">
        <LayoutInner className="applic-devlopmnt">
          <Banner bannerdet={applidev} />
          <Solutions sol={solutioncont} />
          <Services serv={servicescont} />
          <WhyUS whyuscont={whyuscont} />
          <Process processcont={processcont} />
          <Technologies techstack={technologystack} />
          <Consultants experts={expertPanel} />
          <SuccessStories stories={successstories} />
          <Awards awards={awardsAndAccolades} />
        </LayoutInner>
      </Layout>
    );
  }
}

export const getServerSideProps = async () => {
  const urlprefix = process.env.servUploadImg;
  const res = await fetch(urlprefix + "/servapplicationdevelop");
  const appldev = await res.json();
  //   console.log("async", appldev);
  return {
    props: appldev,
  };
  //-------------------
};
