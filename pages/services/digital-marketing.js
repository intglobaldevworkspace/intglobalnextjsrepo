import React, { Component } from "react";
import fetch from "node-fetch";

import Layout from "../../src/components/Layout/Layout";
import LayoutInner from "../../src/components/LayoutInner/LayoutInner";
import Banner from "../../src/components/Pages/DigitalMarketing/Banner/Banner";
import Company from "../../src/components/Pages/DigitalMarketing/Company/Company";
import Services from "../../src/components/Pages/DigitalMarketing/Services/Services";
import WhyUS from "../../src/components/Pages/DigitalMarketing/WhyUS/WhyUS";
import IntroducingTechShu from "../../src/components/Pages/DigitalMarketing/IntroducingTechshu/IntroducingTechShu";
import SuccessStories from "../../src/components/SuccessStories/SuccessStories";
import Consultants from "../../src/components/Consultants/Consultants";
import Awards from "../../src/components/Awards/Awards";

// import IntroducingTechShu from "../../src/components/Pages/DigitalMarketing/IntroducingTechShu/IntroducingTechShu";

const Inner_Page_Class = "inr";
const DigMarketing_Class = "dig-marketing";

export default class DigitalMarketing extends Component {
  constructor(props) {
    super(props);

    // console.log(this.props);
    this.state = {
      headerbanner: {
        dmheaderbanner: this.props.dmheaderbanner,
      },
      companyprofiledetails: {
        dmcompanyprof: this.props.dmcompanyprof,
      },
      digitalmarketingservices: {
        header: this.props.dmservices.dmservheader,
        subheader: this.props.dmservices.dmservsubheader,
        servicerecords: this.props.dmservices.dmservicesrecords,
      },
      whyuscontent: {
        whyusheader: this.props.dmwhyusheader,
        whyussubheader: this.props.dmwhyussubheader,
        whyuscontentdetails: this.props.dmwhyuscontent,
      },
      techshuDetails: {
        techshuHeader: this.props.dmtechshu.dmtechshuheader,
        techshuSubheader: this.props.dmtechshu.dmtechshusubheader,
        techshuLogo: this.props.dmtechshu.dmtechshulogo,
        techshuDescrip: this.props.dmtechshu.dmtechshudescription,
        techshumore: this.props.dmtechshu.dmknowmorelink,
        knowmorelinkurl: this.props.dmtechshu.dmknowmorelinkurl,
        knowmorelinkstatus: this.props.dmtechshu.dmknowmorelinkstatus,
      },
      expertPanel: {
        panelexpheadertxt: this.props.panelofexpertheader,
        panelofexpslidersetting: this.props.expertpanelslider,
        panelofexpertscolls: this.props.panelofexpertscolls,
      },
      successstories: {
        customerColl: this.props.customercolls,
        successstoryHeader: this.props.dmsuccessstoryheader,
        // successstoryletustalk: this.props.dmsuccessstoryletustalk,
        successstoryletustalk: {
          letustalktxt: this.props.dmsuccessstoryletustalk[0]
            .pesuccessstoryletstalktxt,
          letustalkurl: this.props.dmsuccessstoryletustalk[0]
            .pesuccessstoryletstalkurl,
        },
        contactuspopup: {
          contactusSuccess: this.props.contactussubmitmessages[0]
            .contactusmessage,
          contactusFailure: this.props.contactussubmitmessages[1]
            .contactusmessage,
          contactusTimings: this.props.contactussubmitmessages[2]
            .contactusmessage,
        },
      },
      awardsAndAccolades: {
        awardsslidersetting: this.props.awardandrecogs[0].awardrecog
          .awardslider,
        awardsheadertxt: this.props.awardandrecogs[0].awardrecog.awardheader,
        peawardsaccolades: this.props.awardandrecogs[0].awardrecog
          .peawardsaccolades,
      },
    };
  }

  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(DigMarketing_Class);
      }.bind(this),
      10
    );
  }
  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    // console.log(this.props.awardandrecogs);
    const {
      headerbanner,
      companyprofiledetails,
      digitalmarketingservices,
      whyuscontent,
      techshuDetails,
      expertPanel,
      successstories,
      awardsAndAccolades,
    } = this.state;
    return (
      <Layout title="Indus Net Technologies - Digital Marketing">
        <LayoutInner className="digital-marketing">
          <Banner hdrbanner={headerbanner} />
          <Company companydet={companyprofiledetails} />
          <Services dmsrv={digitalmarketingservices} />
          <WhyUS whyuscont={whyuscontent} />
          <IntroducingTechShu techshu={techshuDetails} />
          <Consultants experts={expertPanel} />
          <SuccessStories stories={successstories} />
          <Awards awards={awardsAndAccolades} />
        </LayoutInner>
      </Layout>
    );
  }
}

export const getServerSideProps = async () => {
  const urlprefix = process.env.servUploadImg;
  const res = await fetch(urlprefix + "/servdigitalmkting");
  const digitalmarketing = await res.json();
  // console.log("async", digitalmarketing);
  return {
    props: digitalmarketing,
  };
  //-------------------
};
