import React, { Component } from "react";
import fetch from "node-fetch";
import Layout from "../../src/components/Layout/Layout";
import LayoutInner from "../../src/components/LayoutInner/LayoutInner";
import Banner from "../../src/components/Pages/ProductEngineering/Banner/Banner";
import TechnologyPartner from "../../src/components/Pages/ProductEngineering/TechnologyPartner/TechnologyPartner";
import Solutions from "../../src/components/Pages/ProductEngineering/Solutions/Solutions";
import SoftwareSolutions from "../../src/components/Pages/ProductEngineering/SoftwareSolutions/SoftwareSolutions";
import SuccessStories from "../../src/components/SuccessStories/SuccessStories";
import Consultants from "../../src/components/Consultants/Consultants";
import Awards from "../../src/components/Awards/Awards";

const Inner_Page_Class = "inr";
const ProductEng_Class = "product-eng";

export default class ProductEngineering extends Component {
  constructor(props) {
    super(props);

    // console.log(this.props);
    this.state = {
      peheaderbanner: {
        bannerheader: this.props.peheaderbanner,
      },
      petechpartner: this.props.petechpartner,
      productSolution: {
        productSolutionitems: this.props.peproductsolutioncolls,
        productSolutionheaderOne: this.props.pesolutionheaderone,
        productSolutionheaderTwo: this.props.pesolutionheadertwo,
        letustalkbuttonendtoendsol: this.props.endtoendsolutionletustalk,

        contactuspopup: {
          contactusSuccess: this.props.contactussubmitmessages[0]
            .contactusmessage,
          contactusFailure: this.props.contactussubmitmessages[1]
            .contactusmessage,
          contactusTimings: this.props.contactussubmitmessages[2]
            .contactusmessage,
        },
      },
      softSolDetails: {
        softSolHeader: this.props.softsolutionheader,
        softSolItems: this.props.softsoloptions,
        letustalkbuttonSoftwarereEngineering: this.props.softwarereengineering,
        contactuspopup: {
          contactusSuccess: this.props.contactussubmitmessages[0]
            .contactusmessage,
          contactusFailure: this.props.contactussubmitmessages[1]
            .contactusmessage,
          contactusTimings: this.props.contactussubmitmessages[2]
            .contactusmessage,
        },
      },
      expertPanel: {
        panelexpheadertxt: this.props.panelexpheadertxt,
        panelofexpslidersetting: this.props.panelofexpslidersetting,
        panelofexpertscolls: this.props.panelofexpertscolls,
      },
      successstories: {
        customerColl: this.props.customercolls,
        successstoryHeader: this.props.pesuccessstoryheader,
        successstoryletustalk: {
          letustalktxt: this.props.pesuccessstoryletustalk
            .pesuccessstoryletstalktxt,
          letustalkurl: this.props.pesuccessstoryletustalk
            .pesuccessstoryletstalkurl,
        },
        contactuspopup: {
          contactusSuccess: this.props.contactussubmitmessages[0]
            .contactusmessage,
          contactusFailure: this.props.contactussubmitmessages[1]
            .contactusmessage,
          contactusTimings: this.props.contactussubmitmessages[2]
            .contactusmessage,
        },
      },
      awardsAndAccolades: {
        awardsslidersetting: this.props.awardandrecog.awardrecog.awardslider,
        awardsheadertxt: this.props.awardandrecog.awardrecog.awardheader,
        peawardsaccolades: this.props.awardandrecog.awardrecog
          .peawardsaccolades,
      },
    };

    // console.log(this.state);
    // console.log(props);
  }

  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(ProductEng_Class);
      }.bind(this),
      10
    );
  }
  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const {
      peheaderbanner,
      petechpartner,
      productSolution,
      softSolDetails,
      expertPanel,
      awardsAndAccolades,
      successstories,
    } = this.state;
    // console.log(header, footer);
    return (
      <Layout title="Indus Net Technologies - Product Engineering">
        <LayoutInner className="product-engineering">
          <Banner hbcontents={peheaderbanner} />
          <TechnologyPartner techpart={petechpartner} />
          <Solutions sols={productSolution} />
          <SoftwareSolutions softsol={softSolDetails} />
          <Consultants experts={expertPanel} />
          <SuccessStories stories={successstories} />
          <Awards awards={awardsAndAccolades} />
        </LayoutInner>
      </Layout>
    );
  }
}
export const getServerSideProps = async () => {
  const urlprefix = process.env.servUploadImg;
  const res = await fetch(urlprefix + "/productengineer");
  const productengineer = await res.json();
  //   console.log("async", sitelogo);
  return {
    props: productengineer,
  };
  //-------------------
};
