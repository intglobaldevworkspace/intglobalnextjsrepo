import React, { Component } from "react";
import fetch from "node-fetch";

import Layout from "../../src/components/Layout/Layout";
import LayoutInner from "../../src/components/LayoutInner/LayoutInner";
import Banner from "../../src/components/Pages/EnterpriseMobility/Banner/Banner";
import Solutions from "../../src/components/Pages/EnterpriseMobility/Solutions/Solutions";
import Services from "../../src/components/Pages/EnterpriseMobility/Services/Services";
import WhyUS from "../../src/components/Pages/EnterpriseMobility/WhyUS/WhyUS";
import Process from "../../src/components/Pages/EnterpriseMobility/Process/Process";
import Benefits from "../../src/components/Pages/EnterpriseMobility/Benefits/Benefits";
import SuccessStories from "../../src/components/SuccessStories/SuccessStories";
import Consultants from "../../src/components/Consultants/Consultants";
import Awards from "../../src/components/Awards/Awards";

const Inner_Page_Class = "inr";
const EntpMobility_Class = "entp-mobility";

export default class EnterpriseMobility extends Component {
  constructor(props) {
    super(props);

    // console.log(this.props);
    this.state = {
      headerbanner: { ...this.props.emheaderbanner },
      solutioncontent: { ...this.props.emsolutioncontent },
      servicescontent: { ...this.props.emservicescontent },
      whyuscontent: { ...this.props.emwhyuscontent },
      processcontent: { ...this.props.emprocesscontent },
      benefitcontent: { ...this.props.embenefitcontent },
      expertPanel: {
        panelexpheadertxt: this.props.panelofexpertheader,
        panelofexpslidersetting: this.props.expertpanelslider,
        panelofexpertscolls: this.props.panelofexpertscolls,
      },
      successstories: {
        customerColl: this.props.customercolls,
        successstoryHeader: this.props.emsuccessstoryheader,
        // successstoryletustalk: this.props.emsuccessstoryletustalk,
        successstoryletustalk: {
          letustalktxt: this.props.emsuccessstoryletustalk.letustalktext,
          letustalkurl: this.props.emsuccessstoryletustalk.letustalkurl,
        },
        contactuspopup: {
          contactusSuccess: this.props.contactussubmitmessages[0]
            .contactusmessage,
          contactusFailure: this.props.contactussubmitmessages[1]
            .contactusmessage,
          contactusTimings: this.props.contactussubmitmessages[2]
            .contactusmessage,
        },
      },
      awardsAndAccolades: {
        awardsslidersetting: this.props.awardandrecogs[0].awardrecog
          .awardslider,
        awardsheadertxt: this.props.awardandrecogs[0].awardrecog.awardheader,
        peawardsaccolades: this.props.awardandrecogs[0].awardrecog
          .peawardsaccolades,
      },
    };
  }
  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(EntpMobility_Class);
      }.bind(this),
      10
    );
  }
  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const {
      headerbanner,
      solutioncontent,
      servicescontent,
      whyuscontent,
      processcontent,
      benefitcontent,
      expertPanel,
      successstories,
      awardsAndAccolades,
    } = this.state;
    // console.log(header, footer);
    return (
      <Layout title="Indus Net Technologies - Enterprise Mobility">
        <LayoutInner className="enterprise-mobility">
          <Banner emhdrban={headerbanner} />
          <Solutions solcont={solutioncontent} />
          <Services servcont={servicescontent} />
          <WhyUS whyuscont={whyuscontent} />
          <Process processcont={processcontent} />
          <Benefits bene={benefitcontent} />
          <Consultants experts={expertPanel} />
          <SuccessStories stories={successstories} />
          <Awards awards={awardsAndAccolades} />
        </LayoutInner>
      </Layout>
    );
  }
}
export const getServerSideProps = async () => {
  const urlprefix = process.env.servUploadImg;
  const res = await fetch(urlprefix + "/serventerprisemobility");
  const enterprisemob = await res.json();
  // console.log("async", enterprisemob);
  return {
    props: enterprisemob,
  };
  //-------------------
};
