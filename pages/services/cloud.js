import React, { Component } from "react";

import Layout from "../../src/components/Layout/Layout";
import LayoutInner from "../../src/components/LayoutInner/LayoutInner";
import Banner from "../../src/components/Pages/Cloud/Banner/Banner";
import Solutions from "../../src/components/Pages/Cloud/Solutions/Solutions";
import Services from "../../src/components/Pages/Cloud/Services/Services";
import WhyUS from "../../src/components/Pages/Cloud/WhyUS/WhyUS";
import SuccessStories from "../../src/components/SuccessStories/SuccessStories";
import Consultants from "../../src/components/Consultants/Consultants";
import Awards from "../../src/components/Awards/Awards";
import fetch from "node-fetch";
const Inner_Page_Class = "inr";
const Cloud_Class = "cloud";

export default class Cloud extends Component {
  constructor(props) {
    super(props);

    // console.log(this.props);

    this.state = {
      headerbanner: {
        bantextone: this.props.servcloudheaderbannertextone,
        bantexttwo: this.props.servcloudheaderbannertexttwo,
        bantextthree: this.props.servcloudheaderbannertextthree,
        bantextfour: this.props.servcloudheaderbannertextfour,
      },
      solutioncont: this.props.servcloudsolutionheader,
      servicescontent: {
        servicescloudheader: this.props.servcloudservicescomp
          .servcloudservicesheader,
        servicescloudsubheader: this.props.servcloudservicescomp
          .servcloudservicessubheader,
        servicescontdata: this.props.servcloudservicescomp
          .servcloudservicesdata,
      },
      whyusdata: {
        whyusheader: this.props.servcloudwhyusheader,
        whyussubheader: this.props.servcloudwhyussubheader,
        whyusdatacontent: this.props.servcloudwhyusdata,
      },
      expertPanel: {
        panelexpheadertxt: this.props.panelexpheadertxt,
        panelofexpslidersetting: this.props.panelofexpslidersetting,
        panelofexpertscolls: this.props.panelofexpertscolls,
      },
      successstories: {
        customerColl: this.props.customercolls,
        successstoryHeader: this.props.successstoryheader,
        successstoryletustalk: {
          letustalktxt: this.props.successstorycloud
            .uiuxsuccessstoryletustalktext,
          letustalkurl: this.props.successstorycloud
            .uiuxsuccessstoryletustalkurl,
        },
        contactuspopup: {
          contactusSuccess: this.props.contactussubmitmessages[0]
            .contactusmessage,
          contactusFailure: this.props.contactussubmitmessages[1]
            .contactusmessage,
          contactusTimings: this.props.contactussubmitmessages[2]
            .contactusmessage,
        },
      },
      awardsAndAccolades: {
        awardsslidersetting: this.props.awardandrecogs[0].awardrecog
          .awardslider,
        awardsheadertxt: this.props.awardandrecogs[0].awardrecog.awardheader,
        peawardsaccolades: this.props.awardandrecogs[0].awardrecog
          .peawardsaccolades,
      },
    };
  }
  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(Cloud_Class);
      }.bind(this),
      10
    );
  }
  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const {
      headerbanner,
      solutioncont,
      servicescontent,
      whyusdata,
      expertPanel,
      successstories,
      awardsAndAccolades,
    } = this.state;
    return (
      <Layout title="Indus Net Technologies - Cloud">
        <LayoutInner className="clouds">
          <Banner hban={{ ...headerbanner }} />
          <Solutions solcont={solutioncont} />
          <Services servicescont={{ ...servicescontent }} />
          <WhyUS whyusdata={{ ...whyusdata }} />
          <Consultants experts={expertPanel} />
          <SuccessStories stories={successstories} />
          <Awards awards={awardsAndAccolades} />
        </LayoutInner>
      </Layout>
    );
  }
}

export const getServerSideProps = async () => {
  const urlprefix = process.env.servUploadImg;
  const res = await fetch(urlprefix + "/servcloud");
  const uiuxdesign = await res.json();
  //   console.log("async", uiuxdesign);
  return {
    props: uiuxdesign,
  };
  //-------------------
};
