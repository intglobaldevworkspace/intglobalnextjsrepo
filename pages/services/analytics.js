import React, { Component } from "react";

import Layout from "../../src/components/Layout/Layout";
import LayoutInner from "../../src/components/LayoutInner/LayoutInner";
import Banner from "../../src/components/Pages/Analytics/Banner/Banner";
import Analytic from "../../src/components/Pages/Analytics/Analytic/Analytic";
import Services from "../../src/components/Pages/Analytics/Services/Services";
import WhyUS from "../../src/components/Pages/Analytics/WhyUS/WhyUS";
import SuccessStories from "../../src/components/SuccessStories/SuccessStories";
import Consultants from "../../src/components/Consultants/Consultants";
import Awards from "../../src/components/Awards/Awards";
import fetch from "node-fetch";
const Inner_Page_Class = "inr";
const Analytics_Class = "analytics";

export default class Analytics extends Component {
  constructor(props) {
    super(props);

    // console.log(this.props);

    this.state = {
      hbanner: {
        bannertextone: this.props.headerbannertextone,
        bannertexttwo: this.props.headerbannertexttwo,
        bannertextthree: this.props.headerbannertextthree,
        bannertextfour: this.props.headerbannertextfour,
      },
      analytics: {
        headertextone: this.props.servsectionanalyticsheaderone,
        headertexttwo: this.props.servsectionanalyticsheadertwo,
        scheduleacallbutton: {
          scheduleacalltext: this.props.servanalyticsschedulecallbutton
            .servschedulecalltext,
          scheduleacallurl: this.props.servanalyticsschedulecallbutton
            .analyticsschedulecallurl,
          scheduleacallbuttonstatus: this.props.servanalyticsschedulecallbutton
            .analyticsschedulecallstatus,
        },
        analyticscounter: this.props.analyticscountercomp,
      },
      services: {
        servicesmainheader: this.props.servanalyticsservicescomp
          .analyticsservheader,
        servicesmainsubheader: this.props.servanalyticsservicescomp
          .servanalyticssubheader,
        servicesdata: this.props.servanalyticsservicescomp.servanalyticsdata[0]
          .servanalyticsdetails,
      },
      whyuscontdata: {
        whyusheader: this.props.servanalyticswhyuscomp.analyticswhyusheader,
        whyussubheader: this.props.servanalyticswhyuscomp
          .analyticswhyussubheader,
        whyusdatacont: this.props.servanalyticswhyuscomp.analyticswhyuscontent,
        whyusletustalk: this.props.servanalyticsletustalk,
        contactuspopup: {
          contactusSuccess: this.props.contactussubmitmessages[0]
            .contactusmessage,
          contactusFailure: this.props.contactussubmitmessages[1]
            .contactusmessage,
          contactusTimings: this.props.contactussubmitmessages[2]
            .contactusmessage,
        },
      },
      expertPanel: {
        panelexpheadertxt: this.props.panelexpheadertxt,
        panelofexpslidersetting: this.props.panelofexpslidersetting,
        panelofexpertscolls: this.props.panelofexpertscolls,
      },
      successstories: {
        customerColl: this.props.customercolls,
        successstoryHeader: this.props.servanalyticsheader,
        successstoryletustalk: {
          letustalktxt: this.props.servanalyticssuccessstory
            .uiuxsuccessstoryletustalktext,
          letustalkurl: this.props.servanalyticssuccessstory
            .uiuxsuccessstoryletustalkurl,
        },
        contactuspopup: {
          contactusSuccess: this.props.contactussubmitmessages[0]
            .contactusmessage,
          contactusFailure: this.props.contactussubmitmessages[1]
            .contactusmessage,
          contactusTimings: this.props.contactussubmitmessages[2]
            .contactusmessage,
        },
      },
      awardsAndAccolades: {
        awardsslidersetting: this.props.awardandrecogs[0].awardrecog
          .awardslider,
        awardsheadertxt: this.props.awardandrecogs[0].awardrecog.awardheader,
        peawardsaccolades: this.props.awardandrecogs[0].awardrecog
          .peawardsaccolades,
      },
    };
  }
  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(Analytics_Class);
      }.bind(this),
      10
    );
  }
  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const {
      hbanner,
      analytics,
      services,
      whyuscontdata,
      expertPanel,
      successstories,
      awardsAndAccolades,
    } = this.state;
    return (
      <Layout title="Indus Net Technologies - Analytics">
        <LayoutInner className="analtics">
          <Banner bdata={{ ...hbanner }} />
          <Analytic anly={{ ...analytics }} />
          <Services servicesdata={{ ...services }} />
          <WhyUS whyuscont={{ ...whyuscontdata }} />
          <Consultants experts={expertPanel} />
          <SuccessStories stories={successstories} />
          <Awards awards={awardsAndAccolades} />
        </LayoutInner>
      </Layout>
    );
  }
}

export const getServerSideProps = async () => {
  const urlprefix = process.env.servUploadImg;
  const res = await fetch(urlprefix + "/servanalytics");
  const servanalytics = await res.json();
  //   console.log("async", servanalytics);
  return {
    props: servanalytics,
  };
  //-------------------
};
