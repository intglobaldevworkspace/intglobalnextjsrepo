import React from "react";

import Layout from "../src/components/Layout/Layout";
import LayoutInner from "../src/components/LayoutInner/LayoutInner";
import Banner from "../src/components/Pages/TermsOfUse/Banner/Banner";
import Content from "../src/components/Pages/TermsOfUse/Content/Content";
import TermsContent from "../src/components/Pages/TermsOfUse/TermsContent/TermsContent";
import fetch from "node-fetch";

const Inner_Page_Class = "inr";
const Terms_Class = "terms-of-use";

export default class TermsOfUse extends React.Component {
  constructor(props) {
    super(props);

    // console.log(this.props);
    this.state = {
      bannerData: {
        btextone: this.props.headerbannertextone,
        btexttwo: this.props.headerbannertexttwo,
        btextthree: this.props.headerbannertextthree,
        btextfour: this.props.headerbannertextfour,
      },
      contentdata: this.props.termsofusecontentdts,
      termsdata: {
        termsheaderone: this.props.termscontentheaderone,
        termsheadertwo: this.props.termscontentheadertwo,
        termsheaderthree: this.props.termscontentheaderthree,
        termsArrayService: this.props.termsofuseservices,
        termsArrayFeepayment: this.props.termsofusefeepayments,
        termsArrayTermination: this.props.termsofuseterminations,
      },
    };

    // console.log(this.state);
  }
  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(Terms_Class);
      }.bind(this),
      10
    );
  }

  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const { bannerData, contentdata, termsdata } = this.state;
    return (
      <Layout title="Indus Net Technologies - Terms Of Use">
        <LayoutInner className="terms-use">
          <Banner bdata={{ ...bannerData }} />
          <Content contentdata={contentdata} />
          <TermsContent tdata={{ ...termsdata }} />
        </LayoutInner>
      </Layout>
    );
  }
}

export const getServerSideProps = async () => {
  const urlprefix = process.env.servUploadImg;
  const res = await fetch(urlprefix + "/termsofuse");
  const termsofuse = await res.json();
  // console.log("async", termsofuse);
  return {
    props: termsofuse,
  };
  //-------------------
};
//
