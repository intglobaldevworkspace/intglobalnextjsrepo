import React, { Component } from "react";

import Layout from "../src/components/Layout/Layout";
import LayoutInner from "../src/components/LayoutInner/LayoutInner";
import Banner from "../src/components/Pages/AboutUs/Banner/Banner";
import About from "../src/components/Pages/AboutUs/About/About";
import Values from "../src/components/Pages/AboutUs/Values/Values";
import History from "../src/components/Pages/AboutUs/History/History";
import Clients from "../src/components/Pages/AboutUs/Clients/Clients";
import MeetTeam from "../src/components/Pages/AboutUs/MeetTeam/MeetTeam";
import BeyondBusiness from "../src/components/Pages/AboutUs/BeyondBusiness/BeyondBusiness";
import MediaAwards from "../src/components/Pages/AboutUs/MediaAwards/MediaAwards";
import Partners from "../src/components/Pages/AboutUs/Partners/Partners";
import Offices from "../src/components/Pages/AboutUs/Offices/Offices";
import WorkTogether from "../src/components/Pages/AboutUs/WorkTogether/WorkTogether";

import fetch from "node-fetch";

const Inner_Page_Class = "inr";
const AboutUs_Class = "aboutus";

export default class AboutUs extends Component {
  constructor(props) {
    super(props);

    this.state = {
      headerbanner: this.props.aboutusheaderbanner,
      aboutusHistorydata: {
        // historydet: this.props.aboutushistories,
        historyHeader: this.props.aboutushistoryHeader,
        historyDet: this.props.aboutushistory.aboutushistorydet,
        historyRightScroller: {
          aboutuswhyandwhats: this.props.aboutuswhyandwhats,
          historyRightScrollerSlider: this.props.aboutusWhyandWhatSlider,
        },
      },
      goodideadetails: {
        goodideaheader: this.props.aboutusgoodidea.goodideaheader,
        goodidearepeatabletext: this.props.aboutusgoodidea
          .goodidearepeatabletext,
      },
      fastestGrowingClients: {
        fgclientsHeader: this.props.fastestgrowingclients
          .fastestgrowingclientheader,
        fgclientsDetailBottom: this.props.fastestgrowingclients
          .fastestclientdetailbottom,
        fgclientseemorestoryLink: this.props.fastestgrowingclients
          .clientseemorestorylink,
        fgclientsseemorestoryText: this.props.fastestgrowingclients
          .clientseemorestorytext,
        fgclientDetails: this.props.fastestgrowingclients
          .fastestgrowingclientdetail,
      },
      ourvalues: {
        ourvalueHeader: this.props.ourvaluesheader,
        ourvalueDetails: this.props.ourvaluesdetails,
      },
      associatePartners: {
        apheader: this.props.associatepartnerheader,
        apdetails: this.props.partnersandassociates,
        apviewall: this.props.associatepartnerviewall,
      },
      companyAddress: {
        compAddress: this.props.aucompanygmapaddresses,
        compAddressheader: this.props.companyaddrheader,
      },
      workTogether: {
        workTogetherHeaderText: this.props.aboutusLetusWorkTogetherHeader,
        workTogetherContactButtonUrl: this.props.letusWorkTogetherContactbutton
          .contactUsurl,
        workTogetherContactButtonText: this.props.letusWorkTogetherContactbutton
          .contactUsText,
        contactuspopup: {
          contactusSuccess: this.props.contactussubmitmessages[0]
            .contactusmessage,
          contactusFailure: this.props.contactussubmitmessages[1]
            .contactusmessage,
          contactusTimings: this.props.contactussubmitmessages[2]
            .contactusmessage,
        },
      },
      meetTheIntians: {
        id: this.props.meetTheIntians.id,
        meetTheIntianHeader: this.props.meetTheIntians.mtIntianHeader,
        meetTheIntianFooter: this.props.meetTheIntians.mtIntianFooter,
        mtVideoAndImage: this.props.meetTheIntians.mtVideoAndImage.map(
          (mtVidImg) => {
            return {
              id: mtVidImg.id,
              mtMediaType: mtVidImg.mtMediaType,
              mediaCaption: mtVidImg.mediaCaption,
              meMediaLinkUrl: mtVidImg.meMediaLinkUrl,
              mtVideoLink: mtVidImg.mtVideoLink,
              intfrontImage: {
                name: mtVidImg.intfrontImage.name,
                width: mtVidImg.intfrontImage.width,
                height: mtVidImg.intfrontImage.height,
                url: mtVidImg.intfrontImage.url,
              },
            };
          }
        ),
        mtIntianMoreButton: this.props.meetTheIntians.mtIntianMoreButton,
        mtIntianCareers: this.props.meetTheIntians.mtIntianCareers,
      },
      beyondbusiness: {
        beyBpartnerWithusbuttonStatus: this.props.beyondBusinessComp
          .beyBpartnerWithusbuttonStatus,
        beyBheader: this.props.beyondBusinessComp.beyBheader,
        beyBfooter: this.props.beyondBusinessComp.beyBfooter,
        beyBpartnerWithusbuttonText: this.props.beyondBusinessComp
          .beyBpartnerWithusbuttonText,
        beyBpartnerWithusbuttonUrl: this.props.beyondBusinessComp
          .beyBpartnerWithusbuttonUrl,
        beyBusinessVideoAndImage: this.props.beyondBusinessComp.beyBusinessVideoAndImage.map(
          (vidImgs) => {
            return {
              id: vidImgs.id,
              beyBMediaOptions: vidImgs.beyBMediaOptions,
              beyBVideoLink: vidImgs.beyBVideoLink,
              beyBmediaCaption: vidImgs.beyBmediaCaption,
              beyBmediaLinkURL: vidImgs.beyBmediaLinkURL,
              beyBfrontImage: {
                url: vidImgs.beyBfrontImage.url,
                name: vidImgs.beyBfrontImage.name,
                height: vidImgs.beyBfrontImage.height,
                width: vidImgs.beyBfrontImage.width,
              },
            };
          }
        ),
        beyondBusinessInstitutes: this.props.beyondBusinessComp.beyondBusinessInstitutes.map(
          (bInst) => {
            return {
              id: bInst.id,
              instituteMorebuttonStatus: bInst.instituteMorebuttonStatus,
              instituteName: bInst.instituteName,
              instituteMorebuttonText: bInst.instituteMorebuttonText,
              instituteContent: bInst.instituteContent,
              instituteMorebuttonUrl: bInst.instituteMorebuttonUrl,
              instituteImage: {
                id: bInst.instituteImage.id,
                name: bInst.instituteImage.name,
                url: bInst.instituteImage.url,
                height: bInst.instituteImage.height,
                width: bInst.instituteImage.width,
              },
            };
          }
        ),
      },
      mediaAwardsAndRecognitions: {
        awardsAndMediaHeader: this.props.mediaAwardsAndRecognitions
          .mediaAwardsHeader,
        awardsAndMediaViewAll: this.props.mediaAwardsAndRecognitions
          .mediaAwdviewAll,
        awardsAndaccolades: this.props.mediaAwardsAndRecognitions
          .peawardsaccolades,
        mediaAwardsNewsAndEvents: this.props.footernewscolls,
      },
    };
    // console.log(this.state);
  }

  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(AboutUs_Class);
      }.bind(this),
      10
    );
  }
  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const {
      header,
      footer,
      headerbanner,
      goodideadetails,
      aboutusHistorydata,
      fastestGrowingClients,
      companyAddress,
      associatePartners,
      ourvalues,
      workTogether,
      meetTheIntians,
      beyondbusiness,
      mediaAwardsAndRecognitions,
    } = this.state;
    return (
      <Layout title="Indus Net Technologies - About Us">
        <LayoutInner className="about-us">
          <Banner hbanner={headerbanner} />
          <About aboutusdetails={goodideadetails} />
          <Values ourvaluedetails={ourvalues} />
          <History historydetails={aboutusHistorydata} />
          <Clients fgclients={fastestGrowingClients} />
          <MeetTeam meetInt={meetTheIntians} />
          <BeyondBusiness beyond={beyondbusiness} />
          <MediaAwards mediaAwd={mediaAwardsAndRecognitions} />
          <Partners apartners={associatePartners} />
          <Offices companyaddr={companyAddress} />
          <WorkTogether work={workTogether} />
        </LayoutInner>
      </Layout>
    );
  }
}

export const getServerSideProps = async () => {
  const urlprefix = process.env.servUploadImg;
  const res = await fetch(urlprefix + "/aboutus");
  const aboutus = await res.json();
  console.log("aboutus API content -", aboutus);
  return {
    props: aboutus,
  };
  //-------------------
};
