import React, { Component } from "react";

import Layout from "../src/components/Layout/Layout";
import LayoutInner from "../src/components/LayoutInner/LayoutInner";
import Banner from "../src/components/Pages/Ebook/Banner/Banner";
import List from "../src/components/Pages/Ebook/List/List";
import fetch from "node-fetch";

const Inner_Page_Class = "inr";
const Ebook_Class = "ebook";

export default class Ebook extends Component {
  constructor(props) {
    super(props);
    // console.log(props);

    let ebookdet = this.props.ebooklistcolls.map((ebkObj) => {
      return {
        ebookstatus: ebkObj.ebookstatus,
        ebookcats: ebkObj.ebookcats,
        ebookshorttitle: ebkObj.ebookshorttitle,
        ebooksingleurl: ebkObj.ebooksingleurl,
        ebooklistpageimgthumb: ebkObj.ebooklistpageimg.url,
      };
    });
    this.state = {
      headerbanner: {
        htextone: this.props.ebookbannertextone,
        htexttwo: this.props.ebtexttwo,
        htextthree: this.props.ebtextthree,
        htextfour: this.props.ebtextfour,
      },
      ebooklistingpage: {
        ebookcat: this.props.ebookcats,
        ebooklistheader: this.props.listingpageheader,
        ebooklisting: ebookdet,
      },
    };
  }
  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(Ebook_Class);
      }.bind(this),
      10
    );
  }

  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const { headerbanner, ebooklistingpage } = this.state;
    return (
      <Layout title="Indus Net Technologies - Ebooks">
        <LayoutInner className="ebooks">
          <Banner bann={headerbanner} />
          <List listpage={ebooklistingpage} />
        </LayoutInner>
      </Layout>
    );
  }
}

export const getServerSideProps = async () => {
  const urlprefix = process.env.servUploadImg;
  const res = await fetch(urlprefix + "/ebook");
  const ebk = await res.json();
  //   console.log("async", ebk);
  return {
    props: ebk,
  };
  //-------------------
};
