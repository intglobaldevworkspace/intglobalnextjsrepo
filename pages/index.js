import React, { Component } from "react";
import { Container } from "reactstrap";

import Layout from "../src/components/Layout/Layout";
import Banner from "../src/components/Pages/Home/Banner/Banner";
import Customers from "../src/components/Pages/Home/Customers/Customers";
import Pertners from "../src/components/Pages/Home/Pertners/Pertners";
import Ideas from "../src/components/Pages/Home/Ideas/Ideas";
import FastFlexible from "../src/components/Pages/Home/FastFlexible/FastFlexible";
import Testimonials from "../src/components/Pages/Home/Testimonials/Testimonials";
import Consultants from "../src/components/Pages/Home/Consultants/Consultants";
import Profits from "../src/components/Pages/Home/Profits/Profits";
import fetch from "node-fetch";

const Home_Header_Intro = "introAnim";
const Home_Page_Class = "home";

export default class Index extends Component {
  constructor(props) {
    super(props);
    const customerDetailsArr = this.props.customercolls.map(
      (customerDetails) => {
        return {
          custID: customerDetails.customercolldet.id,
          cust_desc: customerDetails.customercolldet.customerdescrip,
          custlogoUrl: customerDetails.customercolldet.customerlogo.url,
          custbodylogo: customerDetails.logobody.url,
          custSubTitle: customerDetails.customercolldet.customersubtitle,
          custTitle: customerDetails.customercolldet.customertitle,
        };
      }
    );

    const clientTestimonialDetails = this.props.clienttestimonialcoll.customertestimonial.map(
      (clTestimonial) => {
        return {
          id: clTestimonial.id,
          custname: clTestimonial.custname,
          custdesignation: clTestimonial.custdesignation,
          custcompany: clTestimonial.custcompany,
          custhighlightword: clTestimonial.custhighlightword,
          custappreciation: clTestimonial.custappreciation,
          custimage: clTestimonial.hasOwnProperty("custimage")
            ? clTestimonial.custimage.url
            : "",
        };
      }
    );

    const expertPanelContent = this.props.panelofexpertscolls.map(
      (paneldetail) => {
        return {
          conImg: paneldetail.exppaneldetail.hasOwnProperty("expertphoto")
            ? paneldetail.exppaneldetail.expertphoto.url
            : "",
          conName: paneldetail.exppaneldetail.expertname,
          conSkill: paneldetail.exppaneldetail.expertskill,
          conExp: paneldetail.exppaneldetail.expertexperience,
          conSocial: paneldetail.exppaneldetail.expertpanelsocial,
        };
      }
    );
    this.state = {
      homebanner: {
        globeImg: this.props.globeimage.url,
        bannerAnimationTxts: this.props.banneranimcolls,
        digitalSuccessTxt: this.props.dsuccessdesc,
        headingstarttxt: this.props.headingstarttxt,
      },
      customerDetails: {
        arrCustomer: customerDetailsArr,
        customerDescription: this.props.businessdriventext,
      },
      partnerDetails: {
        partnerSlider: this.props.partnercolls,
        partnerContractTxt: this.props.partnertxt,
        partnerContractDescription: this.props.partnerdescrip,
        partnerContactusButton: this.props.partnershipbeyondcontactbutton,
        contactuspopup: {
          contactusSuccess: this.props.contactussubmitmessages[0]
            .contactusmessage,
          contactusFailure: this.props.contactussubmitmessages[1]
            .contactusmessage,
          contactusTimings: this.props.contactussubmitmessages[2]
            .contactusmessage,
        },
      },
      ideas: {
        transformIdeas: this.props.transformidea,
        collaborative: this.props.consultativeapproach,
        scheduleacallbutton: this.props.transformschedulecall,
      },
      fastflexible: {
        fastflexOne: this.props.fastflexibleone,
        fastflexTwo: this.props.fastflexibletwo,
      },
      testimonialDetails: {
        testimonialHeader: this.props.testimonialheader,
        clientTestimonials: clientTestimonialDetails,
      },
      beyondProfit: {
        byProfitHeader: this.props.beyondprofitheadtxt,
        byProfitImgURl: this.props.beyondprofitphoto.url,
        byProfitContent: this.props.beyondprofitdesc,
        byNumberScroller: this.props.beyondprofwords,
      },
      consultants: {
        conHeader: this.props.expertpanelheader,
        conSetting: this.props.expslidersetting,
        conExperts: expertPanelContent,
      },
    };

    // console.log(this.props);
  }

  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Home_Header_Intro);
        document.body.classList.add(Home_Page_Class);
      }.bind(this),
      1000
    );
  }
  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const {
      homebanner,
      customerDetails,
      partnerDetails,
      ideas,
      fastflexible,
      testimonialDetails,
      consultants,
      beyondProfit,
    } = this.state;
    return (
      <Layout title="Indus Net Technologies - Home">
        <Container>
          <Banner bannercontent={homebanner} />
          <Customers customer={customerDetails} />
          <Pertners partnerdetails={partnerDetails} />
          <Ideas ideas={ideas} />
          <FastFlexible ff={fastflexible} />
          <Testimonials testimonial={testimonialDetails} />
          <Consultants consultants={consultants} />
          <Profits profit={beyondProfit} />
        </Container>
      </Layout>
    );
  }
}

export const getServerSideProps = async () => {
  const urlprefix = process.env.servUploadImg;
  const res = await fetch(urlprefix + "/inthome");
  const homecontent = await res.json();
  //   console.log("async", sitelogo);
  return {
    props: homecontent,
  };
  //-------------------
};
