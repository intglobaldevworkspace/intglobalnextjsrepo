import React, { Component } from "react";

import Layout from "../../src/components/Layout/Layout";
import LayoutInner from "../../src/components/LayoutInner/LayoutInner";
import Banner from "../../src/components/Pages/Enterprise/Banner/Banner";
import TechExpertise from "../../src/components/Pages/Enterprise/TechExpertise/TechExpertise";
import TechSolutions from "../../src/components/Pages/Enterprise/TechSolutions/TechSolutions";
import WhyUS from "../../src/components/Pages/Enterprise/WhyUS/WhyUS";
import Consultants from "../../src/components/Consultants/Consultants";
import SuccessStories from "../../src/components/SuccessStories/SuccessStories";
import Awards from "../../src/components/Awards/Awards";
import fetch from "node-fetch";

const Inner_Page_Class = "inr";
const Enterprise_Class = "enterprise";
//const Home_scrl_displ = "scroll-lock";

export default class Enterprise extends Component {
  constructor(props) {
    super(props);
    // console.log(this.props);
    this.state = {
      homebanner: {
        hbantext1: this.props.homebannertextone,
        hbantext2: this.props.homebannertexttwo,
        hbantext3: this.props.homebannertextthree,
        hbantext4: this.props.homebannertextfour,
        hbantext5: this.props.homebannertextfive,
        hbantext6: this.props.homebannertextsix,
      },
      techexpertiseText: this.props.techexpertisetext,
      techsolution: {
        tsoltext1: this.props.techsolutiontextone,
        tsoltext2: this.props.techsolutiontexttwo,
        tsoldata: this.props.segmententerprisetechsol,
      },
      whyus: {
        whyusheader: this.props.whyusheader,
        whyusdata: this.props.segmententerprisewhyusdata,
      },
      expertPanel: {
        panelexpheadertxt: this.props.panelofexpertheader,
        panelofexpslidersetting: this.props.expertpanelslider,
        panelofexpertscolls: this.props.panelofexpertscolls,
      },
      successstories: {
        customerColl: this.props.customercolls,
        successstoryHeader: this.props.segEntSuccessStoryHeader,

        successstoryletustalk: {
          letustalktxt: this.props.segEntSuccessStoryLetUsTalk.letustalktext,
          letustalkurl: this.props.segEntSuccessStoryLetUsTalk.letustalkurl,
        },
        contactuspopup: {
          contactusSuccess: this.props.contactussubmitmessages[0]
            .contactusmessage,
          contactusFailure: this.props.contactussubmitmessages[1]
            .contactusmessage,
          contactusTimings: this.props.contactussubmitmessages[2]
            .contactusmessage,
        },
      },
      awardsAndAccolades: {
        awardsslidersetting: this.props.awardandrecogs[0].awardrecog
          .awardslider,
        awardsheadertxt: this.props.awardandrecogs[0].awardrecog.awardheader,
        peawardsaccolades: this.props.awardandrecogs[0].awardrecog
          .peawardsaccolades,
      },
    };
  }
  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(Enterprise_Class);
        //document.body.classList.remove(Home_scrl_displ);
      }.bind(this),
      10
    );
  }
  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const {
      homebanner,
      techexpertiseText,
      techsolution,
      whyus,
      expertPanel,
      successstories,
      awardsAndAccolades,
    } = this.state;
    return (
      <Layout title="Indus Net Technologies - Enterprise">
        <LayoutInner className="enterprises">
          <Banner hbtext={homebanner} />
          <TechExpertise texptext={techexpertiseText} />
          <TechSolutions tsol={techsolution} />
          <WhyUS wydata={whyus} />
          <Consultants experts={expertPanel} />
          <SuccessStories stories={successstories} />
          <Awards awards={awardsAndAccolades} />
        </LayoutInner>
      </Layout>
    );
  }
}

export const getServerSideProps = async () => {
  const urlprefix = process.env.servUploadImg;
  const res = await fetch(urlprefix + "/segmententerprise");
  const enterprise = await res.json();
  // console.log("async", enterprise);
  return {
    props: enterprise,
  };
  //-------------------
};
