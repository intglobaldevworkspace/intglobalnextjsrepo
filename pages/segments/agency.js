import React from "react";
import fetch from "node-fetch";

import Layout from "../../src/components/Layout/Layout";
import LayoutInner from "../../src/components/LayoutInner/LayoutInner";
import Banner from "../../src/components/Pages/Agency/Banner/Banner";
import TechPartnerships from "../../src/components/Pages/Agency/TechPartnerships/TechPartnerships";
import AchieveGoals from "../../src/components/Pages/Agency/AchieveGoals/AchieveGoals";
import WhyUS from "../../src/components/Pages/Agency/WhyUS/WhyUS";
import SuccessStories from "../../src/components/SuccessStories/SuccessStories";
import Consultants from "../../src/components/Consultants/Consultants";
import Awards from "../../src/components/Awards/Awards";

const Inner_Page_Class = "inr";
const Agency_Class = "agency";
//const Home_scrl_displ = "scroll-lock";

export default class Agency extends React.Component {
  constructor(props) {
    super(props);
    // console.log(this.props);
    this.state = {
      expertPanel: {
        panelexpheadertxt: this.props.panelofexpertheader,
        panelofexpslidersetting: this.props.expertpanelslider,
        panelofexpertscolls: this.props.panelofexpertscolls,
      },
      banner: { ...this.props.agencytopbanner },
      techpartnership: {
        tptextone: this.props.agencytechpartnership.techpartnershiptextone,
        tptexttwo: this.props.agencytechpartnership.techpartnershiptexttwo,
      },
      achievegoals: {
        achtextone: this.props.agencyachievegoaltextone,
        achtexttwo: this.props.agencyachievegoaltexttwo,
        achtextthree: this.props.agencyachievegoaltextthree,
        achtooltipdata: this.props.achievegoalrecs,
      },
      whyus: {
        header: this.props.whyusheadertext,
        whyusdata: this.props.achievegoalwhyus,
      },
      successstories: {
        customerColl: this.props.customercolls,
        successstoryHeader: this.props.agencysuccessstoryheader,

        successstoryletustalk: {
          letustalktxt: this.props.agencysuccessstoryletustalk.letustalktext,
          letustalkurl: this.props.agencysuccessstoryletustalk.letustalkurl,
        },
        contactuspopup: {
          contactusSuccess: this.props.contactussubmitmessages[0]
            .contactusmessage,
          contactusFailure: this.props.contactussubmitmessages[1]
            .contactusmessage,
          contactusTimings: this.props.contactussubmitmessages[2]
            .contactusmessage,
        },
      },
      awardsAndAccolades: {
        awardsslidersetting: this.props.awardandrecogs[0].awardrecog
          .awardslider,
        awardsheadertxt: this.props.awardandrecogs[0].awardrecog.awardheader,
        peawardsaccolades: this.props.awardandrecogs[0].awardrecog
          .peawardsaccolades,
      },
    };
  }

  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(Agency_Class);
        //document.body.classList.remove(Home_scrl_displ);
      }.bind(this),
      10
    );
  }
  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const {
      banner,
      techpartnership,
      achievegoals,
      whyus,
      expertPanel,
      successstories,
      awardsAndAccolades,
    } = this.state;
    return (
      <Layout title="Indus Net Technologies - Agency">
        <LayoutInner className="agencies">
          <Banner btext={banner} />
          <TechPartnerships tp={techpartnership} />
          <AchieveGoals ach={achievegoals} />
          <WhyUS why={whyus} />
          <Consultants experts={expertPanel} />
          <SuccessStories stories={successstories} />
          <Awards awards={awardsAndAccolades} />
        </LayoutInner>
      </Layout>
    );
  }
}

export const getServerSideProps = async () => {
  const urlprefix = process.env.servUploadImg;
  const res = await fetch(urlprefix + "/agency");
  const agency = await res.json();
  // console.log("async", agency);
  return {
    props: agency,
  };
  //-------------------
};
