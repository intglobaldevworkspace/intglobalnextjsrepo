import React, { Component } from "react";

import Layout from "../../src/components/Layout/Layout";
import LayoutInner from "../../src/components/LayoutInner/LayoutInner";
import Banner from "../../src/components/Pages/BlogSingle/Banner/Banner";
import HeaderTitle from "../../src/components/Pages/BlogSingle/HeaderTitle/HeaderTitle";
import Content from "../../src/components/Pages/BlogSingle/Content/Content";
// import Comment from "../../src/components/Pages/BlogSingle/Comment/Comment";
// import Reply from "../../src/components/Pages/BlogSingle/Forms/Reply/Reply";
import fetch from "node-fetch";
const Inner_Page_Class = "inr";
const Blogs_Class = "blogs";
const Blogs_Single_Class = "single-blog";

export default class BlogSingle extends Component {
  constructor(props) {
    super(props);
    // console.log(this.props);

    this.state = {
      blogdetailbanner: {
        blogdetailbanner: this.props.blogdet.entities[0].blogdetailbanner,
        blogarticletitle: this.props.blogdet.entities[0].blogarticletitle,
      },
      blogtitledetails: {
        blogcategories: this.props.blogdet.entities[0].blogcategories,
        blogarticletitle: this.props.blogdet.entities[0].blogarticletitle,
        blogposteddate: this.props.blogdet.entities[0].postaddedon,
        blogarticleauthor: this.props.blogdet.entities[0].blogarticleauthor,
      },
      blogcontent: {
        contentbody: this.props.blogdet.entities[0].blogarticlecontent,
        social: this.props.blogdet.entities[0].blogcontentsharesocialicons,
      },
    };
  }
  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(Blogs_Class);
        document.body.classList.add(Blogs_Single_Class);
      }.bind(this),
      10
    );
  }
  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const { blogdetailbanner, blogtitledetails, blogcontent } = this.state;
    return (
      <Layout title="Indus Net Technologies - Single Blog Details">
        <LayoutInner className="int-blogs single">
          <Banner bandet={{ ...blogdetailbanner }} />
          <HeaderTitle headerdet={{ ...blogtitledetails }} />
          <Content cont={{ ...blogcontent }} />
          {/* <Comment />
          <Reply /> */}
        </LayoutInner>
      </Layout>
    );
  }
}

export const getServerSideProps = async ({ query }) => {
  // Fetch the first page as default
  const urlprefix = process.env.servUploadImg;

  let blogdet = null;
  // Fetch data from external API
  try {
    const res = await fetch(
      urlprefix +
        `/blogarticles/bloglistheaderfooter?blogdetailurl_eq=${query.blogdet}`
    );
    if (res.status !== 200) {
      throw new Error("Failed to fetch");
    }
    blogdet = await res.json();
  } catch (err) {
    blogdet = { error: { message: err.message } };
  }
  // Pass data to the page via props
  return { props: { blogdet } };
};
