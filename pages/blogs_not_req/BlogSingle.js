import React from 'react';

import Layout from '../../src/components/Layout/Layout';
import LayoutInner from '../../src/components/LayoutInner/LayoutInner';
import Banner from '../../src/components/Pages/BlogSingle/Banner/Banner';
import HeaderTitle from '../../src/components/Pages/BlogSingle/HeaderTitle/HeaderTitle';
import Content from '../../src/components/Pages/BlogSingle/Content/Content';
import Comment from '../../src/components/Pages/BlogSingle/Comment/Comment';
import Reply from '../../src/components/Pages/BlogSingle/Forms/Reply/Reply';

const Inner_Page_Class = "inr";
const Blogs_Class = "blogs";
const Blogs_Single_Class = "single-blog";
const Blogs_prdt_displ = "product-eng";
const Blogs_em_displ = "entp-mobility";
const Blogs_dc_displ = "dig-commerce";
const Blogs_dm_displ = "dig-marketing";
const Blogs_rt_displ = "remote-team";
const Blogs_about_displ = "aboutus";

export default class BlogSingle extends React.Component {
  componentDidMount() {
    this.timeoutId = setTimeout(function () {
      document.body.classList.add(Inner_Page_Class);
      document.body.classList.add(Blogs_Class);
      document.body.classList.add(Blogs_Single_Class);
      document.body.classList.remove(Blogs_prdt_displ);
      document.body.classList.remove(Blogs_em_displ);
      document.body.classList.remove(Blogs_dc_displ);
      document.body.classList.remove(Blogs_dm_displ);
      document.body.classList.remove(Blogs_rt_displ);
      document.body.classList.remove(Blogs_about_displ);
    }.bind(this), 10);
  }
  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }
  
  render () {
    return (
      <Layout title="Indus Net Technologies - Single Blog Details">
        <LayoutInner className="int-blogs single">
          <Banner />
          <HeaderTitle />
          <Content />
          <Comment />
          <Reply />
        </LayoutInner>
      </Layout>
    )
  }
}
