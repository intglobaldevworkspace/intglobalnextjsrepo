import React, { Component } from "react";
import fetch from "node-fetch";
import Layout from "../src/components/Layout/Layout";
import LayoutInner from "../src/components/LayoutInner/LayoutInner";
import Banner from "../src/components/Pages/Blog/Banner/Banner";
import HeaderTitle from "../src/components/Pages/Blog/HeaderTitle/HeaderTitle";
import List from "../src/components/Pages/Blog/List/List";

const Inner_Page_Class = "inr";
const Blogs_Class = "blogs";

export default class Blog extends Component {
  constructor(props) {
    super(props);
    // console.log(this.props);
    this.state = {
      blogarticleContents: this.props.blogmainpage.entities,
      articletotalcount: this.props.blogmainpage.blogarticlecount,

      bloglistingbanner: {
        bannerimgtitle: this.props.blogmainpage.bloglist[0]
          .bloglistbannerimgtitle,
        bannerimageurl: this.props.blogmainpage.bloglist[0].bloglistingbannerimg
          .url,
        bannerimagestatus: this.props.blogmainpage.bloglist[0]
          .blogbannerimagestatus,
      },
      blogtitlepage: this.props.blogmainpage.bloglist[0].blogmainheader,
    };
  }

  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(Blogs_Class);
      }.bind(this),
      10
    );
  }
  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const {
      bloglistingbanner,
      blogtitlepage,
      blogarticleContents,
    } = this.state;
    return (
      <Layout title="Indus Net Technologies - Blogs">
        <LayoutInner className="int-blogs">
          <Banner blogbanner={bloglistingbanner} />
          <HeaderTitle titledetail={blogtitlepage} />
          <List bloglist={blogarticleContents} />
        </LayoutInner>
      </Layout>
    );
  }
}

export const getServerSideProps = async () => {
  const urlprefix = process.env.servUploadImg;
  // const res = await fetch(urlprefix + "/intbloglist?_start=0&_limit=9");
  const res = await fetch(urlprefix + "/intbloglist");
  const blogmainpage = await res.json();
  // console.log(urlprefix);
  // console.log("getServerSideProps", blogmainpage);
  return {
    props: { blogmainpage },
  };
  //-------------------
};
