import React from "react";
import App from "next/app";
import Head from "next/head";
import Header from "../src/components/shared/Header";
import Footer from "../src/components/shared/Footer";
import Layout from "../src/components/Layout/Layout";

// Stylings
import "bootstrap/dist/css/bootstrap.min.css";
import "../assets/css/main.scss";
import fetch from "node-fetch";
import "react-intl-tel-input/dist/main.css";
import "react-image-lightbox/style.css"; // This only needs to be imported once in your app

class MyApp extends App {
  static async getInitialProps({ Component, router, ctx }) {
    let pageProps = {};
    //-----------------
    const urlprefix = process.env.servUploadImg;

    let homecontent = [];
    const resHome = await fetch(urlprefix + "/webheaders");
    const headerContent = await resHome.json();
    homecontent.push(headerContent);
    const resFooter = await fetch(urlprefix + "/webfooters");
    const footerContent = await resFooter.json();
    homecontent.push(footerContent);
    const resSubmit = await fetch(urlprefix + "/contactussubmitmessages");
    const subContent = await resSubmit.json();
    homecontent.push(subContent);

    //----------------------
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    // return { pageProps };
    return { pageProps, homecontent };
    // return {
    //   pageProps: {
    //     ...pageProps,
    //     homecontent,
    //   },
    // };
  }

  render() {
    const { Component, pageProps, homecontent } = this.props;
    // const { className, children, cannonical } = this.props;
    // const headerType = this.props.headerType || "default";
    const title = this.props.title || "Indus Net Technologies React Next App";
    // console.log(pageProps);
    // console.log(homecontent);

    const [headerCont, footerCont, subContent] = [...homecontent];
    // console.log(headerCont, footerCont, subContent);

    const header = {
      contactus: headerCont[0].logotxt.hcontactus,
      logoinv: headerCont[0].logotxt.logoinv.url,
      logonorm: headerCont[0].logotxt.logonorm.url,
      sitemenu: headerCont[0].headermenu,
      contactuspopup: {
        contactusSuccess: subContent[0].contactusmessage,
        contactusFailure: subContent[1].contactusmessage,
        contactusTimings: subContent[2].contactusmessage,
      },
    };
    const footer = {
      socialLinks: footerCont[0].sitefooterdetail[0].fsociallinks,

      footerfirstcol: footerCont[0].sitefooterdetail[0].footerfirstcol,
      footerfourthcol: footerCont[0].sitefooterdetail[0].footercolfour,
      footersecondcol: footerCont[0].sitefooterdetail[0].footersecondcolumn,
      footerthirdcol: footerCont[0].sitefooterdetail[0].footerthirdcolumn,
      footermenu: footerCont[0].sitefooterdetail[0].footermenuitem,
      footerSocialLinks:
        footerCont[0].sitefooterdetail[0].footersocialLinkHeader,
      footercopyright: footerCont[0].sitefooterdetail[0].sitefootercopyrighttxt,

      contactus: headerCont[0].logotxt.hcontactus,

      contactuspopup: {
        contactusSuccess: subContent[0].contactusmessage,
        contactusFailure: subContent[1].contactusmessage,
        contactusTimings: subContent[2].contactusmessage,
      },
    };
    return (
      <>
        <Head>
          <title>{title}</title>
          <meta charSet="utf-8" />
          <meta name="description" content="" />
          <meta name="keywords" content="" />
          <meta property="og:title" content="" />
          <meta property="og:locale" content="en_IN" />
          {/* <meta property="og:url" content={`${process.env.BASE_URL}`}/> */}
          <meta property="og:type" content="website" />
          <meta property="og:description" content="" />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0"
          />
          <link
            href="https://fonts.googleapis.com/css2?family=Muli:wght@200;300;400;500;600;700;800&family=Poppins:wght@300;400;500;600;700&family=Work+Sans:wght@300;400;600;700;800&display=swap"
            rel="stylesheet"
          />
          <link
            rel="stylesheet"
            type="text/css"
            charSet="UTF-8"
            href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css"
          />
          <link
            rel="stylesheet"
            type="text/css"
            href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css"
          />
          <link
            rel="icon"
            href="../static/animated_favicon.gif"
            type="image/gif"
          />
          <link rel="icon" href="../static/favicon.ico" type="image/ico" />
          <link rel="icon" href="../static/favicon.ico" type="image/x-icon" />
          <link
            rel="shortcut icon"
            href="../static/favicon.ico"
            type="image/x-icon"
          />
        </Head>
        <div className="layout-container">
          <Header className="header" header={header} />
          <Layout>
            <Component {...pageProps} />
          </Layout>
          <Footer className="footer" footer={footer} />
        </div>
      </>
    );
  }
}

export default MyApp;
