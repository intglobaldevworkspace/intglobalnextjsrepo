import React, { Component } from "react";

import Layout from "../../src/components/Layout/Layout";
import LayoutInner from "../../src/components/LayoutInner/LayoutInner";
import Banner from "../../src/components/Pages/WhyUs/Banner/Banner";
import CustomerExperience from "../../src/components/Pages/WhyUs/CustomerExperience/CustomerExperience";
import WhyUS from "../../src/components/Pages/WhyUs/WhyUS/WhyUS";
import ServiceTimeline from "../../src/components/Pages/WhyUs/ServiceTimeline/ServiceTimeline";
import fetch from "node-fetch";

const Inner_Page_Class = "inr";
const WhyUS_Class = "why-us";

export default class WhyUSModels extends Component {
  constructor(props) {
    super(props);
    // console.log(props);

    this.state = {
      headerbanner: {
        bText1: this.props.bannerText1,
        bText2: this.props.bannerText2,
        bText3: this.props.bannerText3,
        bText4: this.props.bannerText4,
      },
      custExp: this.props.whyusCustExpHeader,
      whyus: {
        whyUsHeader: this.props.whyUsHeaderText,
        whyUsPoints: this.props.aboutuswhyusdata,
      },
      serviceTimeLine: {
        serviceTimeLineHeader: this.props.serviceTimeLineHeader,
        serviceTimeLineLt: {
          st1: this.props.serviceTimelineLt1,
          st2: this.props.serviceTimelineLt2,
          st3: this.props.serviceTimelineLt3,
          st4: this.props.serviceTimelineLt4,
        },
        serviceTimeLineLeftSlider: {
          slidersetting: this.props.whyusSliderSetting,
          sliderContent: this.props.aboutus_whyus_left_sliders,
        },
      },
    };
  }

  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(WhyUS_Class);
      }.bind(this),
      10
    );
  }
  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const { headerbanner, custExp, whyus, serviceTimeLine } = this.state;
    return (
      <Layout title="Indus Net Technologies - Why Us">
        <LayoutInner className="whywe">
          <Banner hban={headerbanner} />
          <CustomerExperience cexp={custExp} />
          <WhyUS whyus={whyus} />
          <ServiceTimeline servTL={serviceTimeLine} />
        </LayoutInner>
      </Layout>
    );
  }
}

export const getServerSideProps = async () => {
  const urlprefix = process.env.servUploadImg;
  const res = await fetch(urlprefix + "/aboutuswhyus");
  const aboutus_whyus = await res.json();
  //   console.log("async", aboutus_whyus);
  return {
    props: aboutus_whyus,
  };
  //-------------------
};
