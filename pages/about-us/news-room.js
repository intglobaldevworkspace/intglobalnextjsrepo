import React, { Component } from "react";

import Layout from "../../src/components/Layout/Layout";
import LayoutInner from "../../src/components/LayoutInner/LayoutInner";
import Banner from "../../src/components/Pages/NewsRoom/Banner/Banner";
import NewsList from "../../src/components/Pages/NewsRoom/NewsList/NewsList";

const Inner_Page_Class = "inr";
const NewsRoom_Class = "news-room";
//const Home_scrl_displ = "scroll-lock";

export default class NewsRoom extends Component {
  constructor(props) {
    super(props);
    // console.log(props);

    this.state = {
      headerbanner: {
        bText1: this.props.bannertext1,
        bText2: this.props.bannertext2,
        bText3: this.props.bannertext3,
        bText4: this.props.bannertext4,
      },
      newslist: {
        newslistheader: this.props.newslistheader,
        newsroomaccordsettings: this.props.newsroomaccordiansettings,
        newsroomselectedtab: this.props.newsroomselectedtab,
        newsroomtabtitle: this.props.newsroomtabs,
      },
    };
  }

  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(NewsRoom_Class);
        //document.body.classList.remove(Home_scrl_displ);
      }.bind(this),
      10
    );
  }
  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const { headerbanner, newslist } = this.state;
    return (
      <Layout title="Indus Net Technologies - News Room">
        <LayoutInner className="int-news">
          <Banner hban={headerbanner} />
          <NewsList newsl={newslist} />
        </LayoutInner>
      </Layout>
    );
  }
}

export const getServerSideProps = async () => {
  const urlprefix = process.env.servUploadImg;
  const res = await fetch(urlprefix + "/aboutusnewsroom");
  const aboutus_newsroom = await res.json();
  //   console.log("async", aboutus_newsroom);
  return {
    props: aboutus_newsroom,
  };
  //-------------------
};
