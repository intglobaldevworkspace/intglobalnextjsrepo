import React, { Component } from "react";

import Layout from "../../src/components/Layout/Layout";
import LayoutInner from "../../src/components/LayoutInner/LayoutInner";
import Banner from "../../src/components/Pages/Awards/Banner/Banner";
import AwardsList from "../../src/components/Pages/Awards/AwardsList/AwardsList";

const Inner_Page_Class = "inr";
const Awards_Class = "awards";
//const Home_scrl_displ = "scroll-lock";

export default class Awards extends Component {
  constructor(props) {
    super(props);
    // console.log(props);

    this.state = {
      headerbanner: {
        bText1: this.props.headerbanner1,
        bText2: this.props.headerbanner2,
        bText3: this.props.headerbanner3,
        bText4: this.props.headerbanner4,
        bText5: this.props.headerbanner5,
        bText6: this.props.headerbanner6,
      },
      awardDetails: {
        awardHeader: this.props.aboutusawardsheader,
        awardsList: this.props.peawardsaccolades,
        readmorelink: this.props.readmorelink,
      },
    };
  }

  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(Awards_Class);
        //document.body.classList.remove(Home_scrl_displ);
      }.bind(this),
      10
    );
  }
  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const { headerbanner, awardDetails } = this.state;
    return (
      <Layout title="Indus Net Technologies - Awards">
        <LayoutInner className="int-awards">
          <Banner ban={{ ...headerbanner }} />
          <AwardsList awards={awardDetails} />
        </LayoutInner>
      </Layout>
    );
  }
}

export const getServerSideProps = async () => {
  const urlprefix = process.env.servUploadImg;
  const res = await fetch(urlprefix + "/aboutusawards");
  const aboutus_awards = await res.json();
  //   console.log("async", aboutus_awards);
  return {
    props: aboutus_awards,
  };
  //-------------------
};
