import React, { Component } from "react";

import Layout from "../../src/components/Layout/Layout";
import LayoutInner from "../../src/components/LayoutInner/LayoutInner";
import Banner from "../../src/components/Pages/Partners/Banner/Banner";
import PartnershipsContracts from "../../src/components/Pages/Partners/PartnershipsContracts/PartnershipsContracts";
import PartnershipValues from "../../src/components/Pages/Partners/PartnershipValues/PartnershipValues";
import PartnershipPrograms from "../../src/components/Pages/Partners/PartnershipPrograms/PartnershipPrograms";
import fetch from "node-fetch";

const Inner_Page_Class = "inr";
const Partner_Class = "partner";

export default class Partner extends Component {
  constructor(props) {
    super(props);
    // console.log(props);

    this.state = {
      headerbanner: {
        htextone: this.props.partnerhomebannertxtone,
        htexttwo: this.props.partnerhomebannertxttwo,
        htextthree: this.props.partnerhomebannertxtthree,
        htextfour: this.props.partnerhomebannertxtfour,
      },
      partnershipcontent: {
        ptxtone: this.props.partnershipcontracttxtone,
        ptxttwo: this.props.partnershipcontracttxttwo,
        ptxtthree: this.props.partnershipcontracttxtthree,
      },
      partnershipvalues: {
        partval: this.props.aboutuspartnershipvalues,
        partnershipheadertxtone: this.props.partnershipvalueheadertxtone,
        partnershipheadertxttwo: this.props.partnershipvalueheadertxttwo,
      },
      partnershipprogram: {
        partnerprogram: this.props.aboutuspartnershipprograms,
        pheader: this.props.partnershipprogramheader,
      },
    };
  }

  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(Partner_Class);
      }.bind(this),
      10
    );
  }

  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const {
      headerbanner,
      partnershipcontent,
      partnershipvalues,
      partnershipprogram,
    } = this.state;
    return (
      <Layout title="Indus Net Technologies - Partner">
        <LayoutInner className="our-partners">
          <Banner bann={headerbanner} />
          <PartnershipsContracts pcontract={partnershipcontent} />
          <PartnershipValues pvals={partnershipvalues} />
          <PartnershipPrograms partnershipprog={partnershipprogram} />
        </LayoutInner>
      </Layout>
    );
  }
}

export const getServerSideProps = async () => {
  const urlprefix = process.env.servUploadImg;
  const res = await fetch(urlprefix + "/aboutuspartner");
  const aboutuspartner = await res.json();
  //   console.log("async", aboutuspartner);
  return {
    props: aboutuspartner,
  };
  //-------------------
};
