import React, { Component } from "react";

import Layout from "../../src/components/Layout/Layout";
import LayoutInner from "../../src/components/LayoutInner/LayoutInner";
import Banner from "../../src/components/Pages/Team/Banner/Banner";
import TeamExpertise from "../../src/components/Pages/Team/TeamExpertise/TeamExpertise";
import CommitmentFaces from "../../src/components/Pages/Team/CommitmentFaces/CommitmentFaces";
import AdvisoryBoard from "../../src/components/Pages/Team/AdvisoryBoard/AdvisoryBoard";
import ManagementTeam from "../../src/components/Pages/Team/ManagementTeam/ManagementTeam";
import TeamStatistics from "../../src/components/Pages/Team/TeamStatistics/TeamStatistics";

const Inner_Page_Class = "inr";
const Team_Class = "team";

export default class Team extends Component {
  constructor(props) {
    super(props);
    // console.log(props);

    this.state = {
      headerbanner: {
        bText1: this.props.bantxt1,
        bText2: this.props.bantxt2,
        bText3: this.props.bantxt3,
        bText4: this.props.bantxt4,
      },
      teamexp: this.props.teamexpertise,
      commitmentfaces: {
        commitmentheader: this.props.commitmentFacesHeader,
        commitmentfacesDetails: this.props.panelofexpertscolls,
        commitmentScrollerSettings: this.props.commitmentfacesScroller,
      },
      managementeam: {
        mgmtteam: this.props.management_team_members,
        mgmtteamheader: this.props.memberpanelHeader,
      },
      advisoryBoard: {
        advHeader: this.props.advisoryBoardHeader,
        advMembers: this.props.aboutusadvisoryboardmembers,
      },
      teamstatSettings: {
        teamstatHeader: this.props.teamstatsSettings.teamstatHeader,
        statPiechartSettings: {
          tooltipshowColorCode: this.props.teamstatsSettings
            .tooltipshowColorCode,
          titleTextStyleFontname: this.props.teamstatsSettings
            .titleTextStyleFontname,
          legendtextStylefontSize: this.props.teamstatsSettings
            .legendtextStylefontSize,
          pieHole: this.props.teamstatsSettings.pieHole,
          legendtextStyleColor: this.props.teamstatsSettings
            .legendtextStyleColor,
          pieSliceTextStyleFontSize: this.props.teamstatsSettings
            .pieSliceTextStyleFontSize,
          titleTextPosition: this.props.teamstatsSettings.titleTextPosition,
          pieSliceText: this.props.teamstatsSettings.pieSliceText,
          titleTextStyleColor: this.props.teamstatsSettings.titleTextStyleColor,
          titleTextStyleFontSize: this.props.teamstatsSettings
            .titleTextStyleFontSize,
        },
        statPiechartData: this.props.teamstatsSettings.aboutusteamdetails,
        teamStatFirst2records: this.props.whyusteamstatinitialrecs,
      },
    };
  }
  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(Team_Class);
      }.bind(this),
      10
    );
  }
  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const {
      headerbanner,
      teamexp,
      commitmentfaces,
      managementeam,
      advisoryBoard,
      teamstatSettings,
    } = this.state;
    return (
      <Layout title="Indus Net Technologies - Team">
        <LayoutInner className="our-team">
          <Banner hban={headerbanner} />
          <TeamExpertise teamxp={teamexp} />
          <CommitmentFaces commitmentface={commitmentfaces} />
          <AdvisoryBoard advbrd={advisoryBoard} />
          <ManagementTeam mgmt={managementeam} />
          <TeamStatistics teamstat={{ ...teamstatSettings }} />
        </LayoutInner>
      </Layout>
    );
  }
}

export const getServerSideProps = async () => {
  const urlprefix = process.env.servUploadImg;
  const res = await fetch(urlprefix + "/aboutusteam");
  const aboutus_team = await res.json();
  //   console.log("async", aboutus_team);
  return {
    props: aboutus_team,
  };
  //-------------------
};
