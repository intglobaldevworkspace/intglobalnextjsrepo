import React, { Component } from "react";
import fetch from "node-fetch";
import Layout from "../../src/components/Layout/Layout";
import LayoutInner from "../../src/components/LayoutInner/LayoutInner";
import Content from "../../src/components/Pages/InfographicSingle/Content/Content";
import Banner from "../../src/components/Pages/InfographicSingle/Banner/Banner";
import List from "../../src/components/Pages/InfographicSingle/List/List";

const Inner_Page_Class = "inr";
const Infographic_Class = "infographic";
const Infographic_Single_Class = "single-infographic";

export default class InfographicSingle extends Component {
  constructor(props) {
    super(props);
    console.log(this.props);
    const { infographicCatfull, qryp } = this.props;
    // console.log(infographicCatfull);

    // let spContent = infographicCatfull.infographiccats
    //   .map((fullCatObj) => {
    //     return {
    //       id: fullCatObj.id,
    //       infographiccatname: fullCatObj.infographiccatname,
    //       infographiccatstatus: fullCatObj.infographiccatstatus,
    //       infographiclistcoll: fullCatObj.infographiclistcoll,
    //       infographiclistcolls: fullCatObj.infographiclistcolls.filter(
    //         (infoObj) => infoObj.graphicurl === qryp
    //       ),
    //     };
    //   })
    //   .filter((blnkObj) => blnkObj.infographiclistcolls.length > 0);

    let spContent = infographicCatfull.infographiclistcolls.filter(
      (fullCatObj) => fullCatObj.graphicurl === qryp
    );

    // console.log(spContent);

    let infographicrecdet = infographicCatfull.infographiclistcolls.map(
      (ifgcolldet) => {
        return {
          id: ifgcolldet.id,
          graphicurl: ifgcolldet.graphicurl,
          shorttitle: ifgcolldet.shorttitle,
          infogdetailimg: { url: ifgcolldet.infogdetailimg.url },
        };
      }
    );

    // this.state = {
    //   ifgCatfull: infographiccats,
    //   pagespecificdet:
    //     spContent && spContent.length > 0 ? spContent[0] : spContent,
    // };

    this.state = {
      pagespecificdet: spContent,
      infoglisting: {
        infogcat: infographicCatfull.infographiccats,
        infogheader: infographicCatfull.listingheader,
        infogdetailrecord: infographicrecdet,
      },
    };
  }

  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(Infographic_Class);
        document.body.classList.add(Infographic_Single_Class);
      }.bind(this),
      10
    );
  }
  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const { infoglisting, pagespecificdet } = this.state;
    // console.log(this.state);
    return (
      <Layout title="Indus Net Technologies - Single Infographic Details">
        <LayoutInner className="infographics single">
          <Content imgcontentDet={pagespecificdet} />
          <List allcatdet={infoglisting} />
        </LayoutInner>
      </Layout>
    );
  }
}

export const getServerSideProps = async ({ query }) => {
  // Fetch the first page as default
  const urlprefix = process.env.servUploadImg;

  let infographicCatfull,
    qryp,
    infographicpagedet = null;
  // Fetch data from external API

  try {
    const res = await fetch(urlprefix + `/infographic`); //infographiccats
    // console.log(res);
    if (res.status !== 200) {
      throw new Error("Failed to fetch");
    }
    infographicCatfull = await res.json();
    // console.log(infographicCatfull);
    let qry_prm = query.infographicdetail;
    // infographicpagedet = infographicCatfull.map((catFullObj) => {
    //   return catFullObj.infographiclistcolls.find(
    //     (infoObj) => infoObj.graphicurl === qry_prm
    //   );
    // });
  } catch (err) {
    infographicCatfull = { error: { message: err.message } };
  }
  // Pass data to the page via props
  return { props: { infographicCatfull, qryp: query.infographicdetail } };
};
