import React, { Component } from "react";

import Layout from "../src/components/Layout/Layout";
import LayoutInner from "../src/components/LayoutInner/LayoutInner";
import Banner from "../src/components/Pages/SuccessStories/Banner/Banner";
import List from "../src/components/Pages/SuccessStories/List/List";
import fetch from "node-fetch";

const Inner_Page_Class = "inr";
const Sstory_Class = "success-stories";

export default class SuccessStories extends Component {
  constructor(props) {
    super(props);

    // console.log(this.props);
    this.state = {
      successstoryslider: { ...this.props.successstoryslidersetting },
      customerdetails: this.props.customercolls,
      categories: this.props.successstorycats,
      headertext: this.props.successstoryheader,
    };

    // console.log(this.state);
  }
  componentDidMount() {
    this.timeoutId = setTimeout(
      function () {
        document.body.className = "";
        document.body.classList.add(Inner_Page_Class);
        document.body.classList.add(Sstory_Class);
      }.bind(this),
      10
    );
  }

  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

  render() {
    const {
      successstoryslider,
      customerdetails,
      categories,
      headertext,
    } = this.state;
    return (
      <Layout title="Indus Net Technologies - Success Stories">
        <LayoutInner className="success-story">
          <Banner
            banslider={successstoryslider}
            compdetails={customerdetails}
          />
          <List
            clientfull={customerdetails}
            customercat={categories}
            ssheader={headertext}
          />
        </LayoutInner>
      </Layout>
    );
  }
}

export const getServerSideProps = async () => {
  const urlprefix = process.env.servUploadImg;
  const res = await fetch(urlprefix + "/successstory");
  const success_story = await res.json();
  //   console.log("async", success_story);
  return {
    props: success_story,
  };
  //-------------------
};
