import Document, { Html, Head, Main, NextScript } from 'next/document';
import Loader from '../src/components/Loader/Loader';

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps };
  }
  
  render() {
    return (
      <Html lang="en">
        <Head />
        <body>
          <div id="preloader">
            <Loader />
          </div>
          <div className="isLoading">
            <Main />
          </div>
          <script src="/js/preloader.js"></script>
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument